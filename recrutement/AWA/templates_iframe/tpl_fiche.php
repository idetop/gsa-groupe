<?php echo $templateDatas['header']; ?>
<div id="container980">
    <div id="subheader">
        <div id="pathway">
            <?php  echo $templateDatas['pathway'];  ?>
        </div>
        <div id="languesdisplay">
            <?php //echo $templateDatas['flags']; ?>
        </div>
        <div class="spacer"></div>
    </div>
    <?php echo $templateDatas['candidatArea']; ?>
    <div id="middle">
        <div id="menu">
            <?php echo $templateDatas['menu']; ?>
        </div>
        <div id="content">
            <div class="contenttitle">
                <div id="inituleFiche"><h1><?php echo $annonce->LIBELLE; ?></h1></div>
                <div id="refFiche"> <?php echo _OFFRE_FICHE_REF_LABEL; ?> : <?php echo $annonce->REFERENCE; ?></div>
                <div class="spacer"></div>
            </div>
            <div style="height: 20px;"></div>
            <div id="detailOffreContent">
                <div id="ficheLeft">
                    <?php if(!empty ($annonce->DESCRASSIGNMENT)){ ?>
                        <div id="mission_title"><?php echo _OFFRE_FICHE_POSTE_LABEL; ?></div>
                    <?php } ?>
                    <div id="mission_text">
                        <?php echo nl2br($annonce->DESCRASSIGNMENT); ?>
                    </div>
                        <div style="height: 20px;"></div>
                        <div id="mission_title">Profil recherché</div>
                        <div id="desctext">
                        <?php echo nl2br($annonce->TEXTE_ANNONCE); ?>
                    </div>
                </div>
                <div id="ficheRight">
                    <div id="descriptionEntreprise">
                        <?php //if($annonce->ANONYMOUS != "1"){ ?>
                            <div id="nomEntreprise">GSA RECRUTEMENT<?php //echo $annonce->RAISON_SOCIALE; ?></div>
                        <?php //} ?>
                        <?php //echo $annonce->DESCRCUSTOMER; ?>
                    </div>

                    <div style="height: 30px;"></div>

                    <div id="infocomp">
                        <div id="infocompTitre"> <?php echo _OFFRE_FICHE_INFORMATION_COMPLEMENTAIRE_LABEL; ?></div>
                        <?php if(!(empty ($annonce->PAYS) && empty ($annonce->REGION) && empty ($annonce->CITY))){ ?>
                            <div>
                            <?php echo _OFFRE_FICHE_LIEU_LABEL; ?> : <?php echo $annonce->PAYS; ?> <?php echo $annonce->REGION; ?> <?php echo $annonce->CITY; ?>
                            <?php echo $templateDatas['localisationLink']; ?>
                            </div>
                        <?php } ?>

                        <?php if(!empty ($annonce->TYPECONTRAT)){ ?>
                            <div><?php echo _OFFRE_FICHE_CONTRAT_LABEL; ?> : <?php echo $annonce->TYPECONTRAT; ?></div>
                        <?php } ?>
                        
                        <?php if(!(empty($annonce->SALARY_FROM) && empty($annonce->SALARY_TO))){ ?>
                            <div><?php echo _OFFRE_FICHE_SALAIRE_LABEL; ?> :
                            <?php if(!empty($annonce->SALARY_FROM) && !empty($annonce->SALARY_TO)){ ?>
                                <?php echo $annonce->SALARY_FROM; ?>
                                 <?php echo _OFFRE_LISTE_SALARY_TO; ?>
                                 <?php echo $annonce->SALARY_TO; ?>
                            <?php }else{ ?>
                                <?php if(!empty($annonce->SALARY_FROM)){ echo $annonce->SALARY_FROM;} ?>
                                <?php if(!empty($annonce->SALARY_TO)){ echo $annonce->SALARY_TO;} ?>
                            <?php } ?>
                            &euro;
                            </div>
                        <?php } ?>

                        <?php if(!empty ($annonce->DATE_DEBUT) && $annonce->DATE_DEBUT != "0000-00-00"){ ?>
                            <div><?php echo _OFFRE_FICHE_DATEPARUTION_LABEL; ?> : <?php echo getDateDisplay($annonce->DATE_DEBUT,"-"); ?></div>
                        <?php } ?>
                    </div>
                </div>
                <div class="spacer"></div>
            </div>

            <?php if(!_IS_GOOGLEMAP_POPUP){echo $templateDatas['localisationJsscript'];} ?>
            
            
            <div style="height: 30px;"></div>

            <div id="buttonsarea">
                <div id="retour" class="buttonclass" onclick="window.location='<?php echo $templateDatas['backButtonLink']; ?>'"><?php echo _OFFRE_FICHE_RETOUR_BTN; ?></div>
                <div id="postuler" class="buttonclass" onclick="window.location='<?php echo $templateDatas['postulButtonLink']; ?>'"><?php echo _OFFRE_FICHE_POSTULER_BTN; ?></div>
                <div class="spacer"></div>
            </div>
            <div style="height: 20px;"></div>

            <div style="height: 30px;"></div>
      <?php if(_IS_GOOGLEMAP_POPUP){echo $templateDatas['localisationJsscriptPopup'];} ?>
	  </div>
	 </div>
</div>	 
<div id="footer">
         <?php echo $templateDatas['footerText']; ?>
    </div>