<?php


/*
 * Lecture du fichier cvsx et décodage
 */
Debug::d_echo("import histo_formations ", 2,"updateDba-formations.php");

if(_IS_IMPORT_ENCODAGE){
    if(!is_file(_AWA_PATHWAY."/import_files/histo_formations.csvx")){
        exit();
    }
    Debug::d_echo("lecture du fichier", 2,"updateDba-formations.php");
    $datafile = file_get_contents(_AWA_PATHWAY."/import_files/histo_formations.csvx");
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

    $key = "admen";
    $datafile = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $datafile, MCRYPT_MODE_ECB, $iv);
    Debug::d_echo("decodage du fichier", 2,"updateDba-formations.php");
    //$datafile = utf8_encode($datafile);
}else{
    if(!is_file(_AWA_PATHWAY."/import_files/histo_formations.csv")){
        exit();
    }
    Debug::d_echo("lecture du fichier", 2,"updateDba-formations.php");
    $datafile = file_get_contents(_AWA_PATHWAY."/import_files/histo_formations.csv");
}


if(_DEBUG_IMPORT){
    print($datafile);
}


/*
 * Récuparation de chaque ligne CSV
 */
preg_match_all('#(.*?)[@]{4}\s#', $datafile, $rows);
if(_DEBUG_IMPORT){
    echo "<pre>";
    print_r($rows[1]);
    echo "</pre>";
}



/*
 * Récuparation des noms de colonnes
 */
preg_match_all('`[#]{4}(.*?)[#]{4};?`', $rows[1][0], $matches);

$columns_names = array();
$columns_names = $matches[1];

if(_DEBUG_IMPORT){
    echo "liste des columns"."\n";
    echo "<pre>";
    print_r($columns_names);
    echo "</pre>";
}


/*
 * Récupération des données
 */
$dba_rows = array();

foreach($rows[1] as $key=>$row){
    if($key != 0){
        preg_match_all('`[#]{4}?(.*?)[#]{4}?;?`', $row, $row_matches);
        $dba_rows[] = $row_matches[1];

    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "liste du contenu"."\n";
    echo "<pre>";
    print_r($dba_rows);
    echo "</pre>";
}


//On recherche la clé de ID_PERSONNE
$ID_PERSONNE_KEY = null;
foreach($columns_names as $key=>$columns_name){
//    echo $columns_name;
    if($columns_name == "ID_PERSONNE"){
        $ID_PERSONNE_KEY = $key;
        break;
    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "index personne id ".$ID_PERSONNE_KEY."\n";
}

//On recherche la clé de ID_PERSONNE_WEB
$ID_PERSONNE_WEB_KEY = null;
foreach($columns_names as $key=>$columns_name){
//    echo $columns_name;
    if($columns_name == "ID_PERSONNE_WEB"){
        $ID_PERSONNE_WEB_KEY = $key;
        break;
    }
}
if(_DEBUG_IMPORT){
    echo "<br/>"."\n";
    echo "index personne web id ".$ID_PERSONNE_WEB_KEY."\n";
}

//On recup les ID lieés à la clé ID_PERSONNE_WEB
$ID_PERSONNE_WEB_values = array();
foreach($dba_rows as $dba_row){
    $ID_PERSONNE_WEB_values[] = $dba_row[$ID_PERSONNE_WEB_KEY];
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "<pre>";
    print_r($ID_PERSONNE_WEB_values);
    echo "</pre>";
}



/*
 * Suppression des formations de la DBA
 */

$sql = "
            DELETE FROM
                histo_formations
            
        ";
if(_DEBUG_IMPORT){
    print_t($sql);
}
$select = $conn->prepare($sql);
$select->execute();
Debug::d_echo("Suppression des formations", 2,"updateDba-formations.php");



/*
 * INSERT ou UPDATE des données en DBA
 */
foreach($dba_rows as $dba_row){

    if($dba_row[$ID_PERSONNE_WEB_KEY]){
        //si id web pas null on insert
        $updateSet = "";
        foreach($columns_names as $key=>$columns_name){
            if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                $datatext = "";
                $datatext = str_replace("'", "''", $dba_row[$key]);
                $datatext = str_replace("§§", "\r\n", $datatext);
                if($updateSet == ""){
                    $updateSet .= $columns_name."='".$datatext."'";
                }else{
                    $updateSet .= ",\n".$columns_name."='".$datatext."'";
                }
            }
        }
        $sql = "
            INSERT INTO
                histo_formations
            SET
                ".$updateSet."
        ";
        if(_DEBUG_IMPORT){
            print_t("id web non null dans csv");
            print_t($sql);
        }
        $select = $conn->prepare($sql);
        $select->execute();
        Debug::d_echo("Insert id web personne ".$dba_row[$ID_PERSONNE_WEB_KEY], 2,"updateDba-formations.php");
    }else{
        //si id web null, faut regarder dans table personne si id web existe pour cette id
        $sql = "
            SELECT
                ID_PERSONNE,
                ID_PERSONNE_WEB

            FROM
                personnes
            WHERE
                ID_PERSONNE = '".$dba_row[$ID_PERSONNE_KEY]."'
        ";
        if(_DEBUG_IMPORT){
            print_t("id web null dans csv");
            print_t($sql);
        }
        $select = $conn->prepare($sql);
        $select->execute();
        Debug::d_echo("recherche id web pour id personne ".$dba_row[$ID_PERSONNE_KEY], 2,"updateDba-formations.php");
        $personne = null;
        $personne = $select->fetchObject();
        if($personne){
            
            //si id web se trouve dans table personne on l'utilise pour faire l'insert
            $updateSet = "";
            foreach($columns_names as $key=>$columns_name){
                if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                    if($columns_name == 'ID_PERSONNE_WEB'){
                        //on remplace l'id web qui est a 0 par l'id web trouver dans la table personne
                        if($updateSet == ""){
                            $updateSet .= $columns_name."='".$personne->ID_PERSONNE_WEB."'";
                        }else{
                            $updateSet .= ",\n".$columns_name."='".$personne->ID_PERSONNE_WEB."'";
                        }
                    }else{
                        $datatext = "";
                        $datatext = str_replace("'", "''", $dba_row[$key]);
                        $datatext = str_replace("§§", "\r\n", $datatext);
                        if($updateSet == ""){
                            $updateSet .= $columns_name."='".$datatext."'";
                        }else{
                            $updateSet .= ",\n".$columns_name."='".$datatext."'";
                        }
                    }
                    
                }
            }
            $sql = "
                INSERT INTO
                    histo_formations
                SET
                    ".$updateSet."
            ";
            if(_DEBUG_IMPORT){
                print_t($sql);
            }
            $select = $conn->prepare($sql);
            $select->execute();
            Debug::d_echo("insert pour id personne ".$dba_row[$ID_PERSONNE_KEY]." id web".$personne->ID_PERSONNE_WEB, 2,"updateDba-formations.php");
        }else{
            //si pas id web, on zap.
        }             
    }    
}





?>
