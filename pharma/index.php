<?php include('controllers/index.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    
<head>
    <title>Portail de GSA, une entreprise du groupe GTF</title>
    <meta name="google-site-verification" content="cfM15zJRYHBtIL-8teSrrVgsNsNz42UCDbYARVLyP8w" />
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <meta name="description" content="GSA, une entreprise du groupe GTF." />
    <meta name="keywords" content="" />
    <script type="text/javascript" src="js/swfobject.js"></script>
    <!--
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    -->

    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/coda-slider-2.0.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="../js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="../js/jquery.coda-slider-2.0.js"></script>
     <script type="text/javascript">
    $().ready(function() {
       $('#coda-slider-1').codaSlider({
           dynamicArrows: false,
           dynamicTabs: false,
           autoSlide: true,
           autoSlideInterval: 4000,
           autoSlideStopWhenClicked: true
       });
       
       //Horizontal Sliding
		$('.boxgrid.slideright').hover(function(){
			
			$(".cover", this).stop().animate({left:'325px'},{queue:false,duration:300});
			
		}, 
		function(){
			$(".cover", this).stop().animate({left:'0px'},{queue:false,duration:300});
		});
    });
    </script>
    <link type="text/css" rel="stylesheet" href="css/styles.css" />
    <link href="http://fonts.googleapis.com/css?family=Michroma" rel="stylesheet" type="text/css" />
</head>
    
<body>

<?php include('../header.php'); ?>
	
<!-- BANDEAU -->
<div id="wrapper">
	<div id="bandeau">
		
		<a href="contact.php" class="contact">contacter GSA pharma</a>
		<a href="http://gsa-groupe.fr" class="gsa-groupe">Visiter GSA Groupe</a>
		<br style="clear: left;" />
		<div id="candidats">
			
			<div>
				<p>
				Du décryptage de la solution commerciale à l'accompagnement opérationnel, GSA PHARMA révolutionne l'organisation commerciale des laboratoires.				</p>
				
			</div>
		</div>
		
		<?php 
		if(
			(strpos($_SERVER['HTTP_USER_AGENT'],'iPad') 		!= false) || 
			(strpos($_SERVER['HTTP_USER_AGENT'],'iPod') 		!= false) || 
			(strpos($_SERVER['HTTP_USER_AGENT'],'iPhone') 		!= false) ||
			(strpos($_SERVER['HTTP_USER_AGENT'],'BlackBerry') 	!= false)
		){
		?>
		
		<!-- SOLUTION ALTERNATIVE AU FLASH -->
		<div id="flash-content" style="position:relative;left:15px">
			<div class="about-our-company">
				<div class="marque-accueil" style="width:660px">
				
					<!--conseil en stratégie commerciale  -->
					<div>
						<a href="conseil-strategie-commerciale.html">
							<div class="boxgrid slideright" style="background:url('images/boxb.jpg') no-repeat center">
								<img class="cover" src="images/box1a.jpg" style="width:auto;height:auto" />
							</div>
						</a>
						<!--environnement et législation-->
						<a href="environnement-legislation.html">
							<div class="boxgrid slideright" style="background:url('images/boxb.jpg') no-repeat center">
								<img class="cover" src="images/box2a.jpg" style="width:auto;height:auto" />
							</div>
						</a>
						<!--méthodologie mode d'emploi-->
						<a href="methodologie.html">
							<div class="boxgrid slideright" style="background:url('images/boxb.jpg') no-repeat center;margin-right:0">
								<img class="cover" src="images/box3a.jpg" style="width:auto;height:auto" />
							</div>
						</a>
					</div>
					<br style="clear:both" />
					
					<div style="margin-top:63px">
						
						<!--réseaux exclusifs -->
						<a href="reseaux-exclusifs.html">
							<div class="boxgrid slideright" style="background:url('images/boxb.jpg') no-repeat center;">
								<img class="cover" src="images/box4a.jpg" style="width:auto;height:auto" />
							</div>
						</a>
						
						
						<!--réseaux multi-produits-->
						<a href="reseaux-multi-produits.html">
							<div class="boxgrid slideright" style="background:url('images/boxb.jpg') no-repeat center">
								<img class="cover" src="images/box5a.jpg" style="width:auto;height:auto" />
							</div>
						</a>
						
						<!--retour portail-->
						<a href="http://www.gsa-groupe.fr">
							<div class="boxgrid slideright" style="background:url('images/box6b.jpg') no-repeat center; margin-right: 0;">
								<img class="cover" src="images/box6a.jpg" style="width:auto;height:auto" />
							</div>
						</a>
						
						
						
						
					</div>
					<br style="clear:both" />
				</div>
			</div>
			<br style="clear:both" />
		</div>
		
		<?php } else { ?>
		
		<!-- FIN ALTERNATIVE FLASH -->

		
		
		
		<div id="flash-content">
			<script type="text/javascript">
				var so = new SWFObject("menu-pharma.swf", "menu-pharma.swf", "700", "450", "", "");
				so.addParam("allowScriptAccess", "sameDomain");
				so.addParam("wmode", "transparent");
				so.write("flash-content");
			</script>
		</div>
		<?php } ?>
	</div>
	
	<!-- CONTENU -->

	<div id="contenu">
		<div id="edito">
			<?php texte_accueil(); ?>
		</div>
		<div id="blocs">
			<div class="col25">
				<img src="images/deontologie.jpg" />
				<p>
				<a href="engagements.html">En savoir +</a>
				</p>
			</div>
			<div class="col25">
				<img src="images/competences-efficacite.jpg" />
				<p>
				<a href="engagements.html">En savoir +</a>
				</p>
			</div>
			<div class="col25">
				<img src="images/organisation.jpg" />
				<p>
				<a href="engagements.html">En savoir +</a>
				</p>
			</div>
			<div class="col25">
				<img src="images/information-communication.jpg" />
				<p>
				<a href="engagements.html">En savoir +</a>
				</p>
			</div>
		</div>
	</div>
</div>

<?php include('../footer.php'); ?>

</body>
</html>