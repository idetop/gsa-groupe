<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');

Debug::d_echo("acces ", 2,"liste.php");
Debug::d_print_r($_GET, 1,"GET","liste.php");
Debug::d_print_r($_POST, 1,"POST","liste.php");
Debug::d_print_r($_SESSION, 1,"SESSION","liste.php");



$fonctionDbaFieldName= "FONCTION";
$secteurDbaFieldName= "SECTEUR";

if(_IS_USE_FONCTION_WEB){
    $fonctionDbaFieldName= "FONCTIONWEB";
}
if(_IS_USE_SECTEUR_WEB){
    $secteurDbaFieldName= "SECTEURWEB";
}


$numberOfElementPerPage = _NOMBRE_ANNONCE_PAR_PAGE;

if(empty($_SESSION['search_pagination'] )){
    $_SESSION['search_pagination'] = 1;
}
if(!empty($_GET['p'])){
    $_SESSION['search_pagination'] = (int)$_GET['p'];
}
Debug::d_echo("page  : ".$_SESSION['search_pagination'], 2,"liste.php");

$whereSearch = "";
if(!empty($_SESSION['search_fonction'] )){
    $selectedFonctions = array();
    $selectedFonctions = explode("|",$_SESSION['search_fonction']);
    $whereSearchFonction = "";
    foreach($selectedFonctions as $key=>$selectedFonction){
        if($key == 0){
            $whereSearchFonction .= " mi.`".$fonctionDbaFieldName."` LIKE '".$selectedFonction."' ";
        }else{
            $whereSearchFonction .= " OR mi.`".$fonctionDbaFieldName."` LIKE '".$selectedFonction."' ";
        }
    }
    if(!empty($whereSearchFonction)){
        $whereSearch .= " AND ( ".$whereSearchFonction." ) \n";
    }
    
    Debug::d_echo("Search fonction : ".$_SESSION['search_fonction'], 2,"liste.php");
}

if(!empty($_SESSION['search_secteur'] )){
    $selectedSecteurs = array();
    $selectedSecteurs = explode("|",$_SESSION['search_secteur']);
    $whereSearchSecteur = "";
    foreach($selectedSecteurs as $key=>$selectedSecteur){
        if($key == 0){
            $whereSearchSecteur .= " mi.`".$secteurDbaFieldName."` LIKE '".$selectedSecteur."' ";
        }else{
            $whereSearchSecteur .= " OR mi.`".$secteurDbaFieldName."` LIKE '".$selectedSecteur."' ";
        }
    }
    if(!empty($whereSearchSecteur)){
        $whereSearch .= " AND ( ".$whereSearchSecteur." ) \n";
    }

    Debug::d_echo("Search secteur : ".$_SESSION['search_secteur'], 2,"liste.php");
}

if(!empty($_SESSION['search_keyword'] )){
    Debug::d_echo("Search keyword : ".$_SESSION['search_keyword'], 2,"liste.php");
    preg_match_all('#[^, \+%]+#', $_SESSION['search_keyword'], $matches);
//    print_t($matches);
    foreach($matches[0] as $match){
        if($match != "" && $match != " ")
        $whereSearch .= " AND ( \n";
        $whereSearch .= " mi.`".$fonctionDbaFieldName."` LIKE '%".$match."%' \n";
        $whereSearch .= " OR mi.`".$secteurDbaFieldName."` LIKE '%".$match."%' \n";
        $whereSearch .= " OR mi.`REGION` LIKE '%".$match."%' \n";
        $whereSearch .= " OR mi.`PAYS` LIKE '%".$match."%' \n";
        $whereSearch .= " OR mi.`ZIP` LIKE '%".$match."%' \n";
        $whereSearch .= " OR mi.`CITY` LIKE '%".$match."%' \n";
        $whereSearch .= " OR an.`LIBELLE` LIKE '%".$match."%' \n";
        $whereSearch .= " OR an.`REFERENCE` LIKE '%".$match."%' \n";
        $whereSearch .= " OR an.`TEXTE_ANNONCE` LIKE '%".$match."%' \n";
        $whereSearch .= " OR an.`DESCRCUSTOMER` LIKE '%".$match."%' \n";
        $whereSearch .= " OR an.`DESCRASSIGNMENT` LIKE '%".$match."%' \n";
        $whereSearch .= ") \n";
    }
}


if(!empty($_SESSION['search_location'] )){
    Debug::d_echo("Search location : ".$_SESSION['search_location'], 2,"liste.php");
    preg_match_all('#[^, \+%]+#', $_SESSION['search_location'], $matches);
//    print_t($matches);
    foreach($matches[0] as $match){
        if($match != "" && $match != " ")
        $whereSearch .= " AND ( \n";
        $whereSearch .= " mi.`PAYS` LIKE '%".$match."%' \n";
        $whereSearch .= " OR mi.`REGION` LIKE '%".$match."%' \n";
        $whereSearch .= " OR mi.`CITY` LIKE '%".$match."%' \n";
        $whereSearch .= ") \n";
    }
}


//nombre de resultat total
$sql = "
        SELECT            
            an.ID_ANNONCE           

        FROM
            annonces AS an
        INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION

        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )
            ".$whereSearch."

    ";

$sql45 = "
        SELECT
            an.ID_ANNONCE

        FROM
            tannonces AS an
        INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )
            ".$whereSearch."

    ";

if(_ADMEN_USE_ADVERT_LINES){
    $sql = $sql45;
}
$printsql = $sql;
//print_t($printsql);
$select = $conn->prepare($sql);
if(!empty($_SESSION['search_fonction'] )){
    $select->bindParam(':searchfunction', $_SESSION['search_fonction'], PDO::PARAM_STR);
}
if(!empty($_SESSION['search_secteur'] )){
    $select->bindParam(':searchsecteur', $_SESSION['search_secteur'], PDO::PARAM_STR);
}
$select->execute();
$selectResults = array();
$selectResults = $select->fetchAll();
//Debug::d_sql_error($select, 0,"sql get nb annonce","liste.php",__LINE__);
//print_t($test);
$nombreDeResultat = count($selectResults);
Debug::d_echo("nb annonces matches : ".$nombreDeResultat, 2,"liste.php");
$nombreDePage = ceil($nombreDeResultat / $numberOfElementPerPage);

$pagination_HTML = "";

if($_SESSION['search_pagination'] > 1){
    $pagination_HTML .= "<span class=\"pagi\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language'])."?p=1\" title=\""._OFFRE_LISTE_PAGINATION_DEBUT."\">&lt;&lt;</a></span>";
}
if($_SESSION['search_pagination'] > 1){
    $pagination_HTML .= "<span class=\"pagi\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language'])."?p=".($_SESSION['search_pagination']-1)."\" title=\""._OFFRE_LISTE_PAGINATION_PREC."\">&lt;</a></span>";
}
for($i = 1; $i <= $nombreDePage;$i++){
    
    if($_SESSION['search_pagination'] == $i){
        $pagination_HTML .= "<span class=\"pagi pactive\">".$i."</span>";
    }else{
        $pagination_HTML .= "<span class=\"pagi\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language'])."?p=".$i."\" title=\"\">".$i."</a></span>";
    }
    
}
if($_SESSION['search_pagination'] < $nombreDePage){
    $pagination_HTML .= "<span class=\"pagi\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language'])."?p=".($_SESSION['search_pagination']+1)."\" title=\""._OFFRE_LISTE_PAGINATION_SUIV."\">&gt;</a></span>";
}
if($_SESSION['search_pagination'] < $nombreDePage){
    $pagination_HTML .= "<span class=\"pagi\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language'])."?p=".($nombreDePage)."\" title=\""._OFFRE_LISTE_PAGINATION_FIN."\">&gt;&gt;</a></span>";
}

$start_element = $numberOfElementPerPage * ($_SESSION['search_pagination'] -1);

$annonces = array();
$sql = "
        SELECT
            an.REFERENCE,
            an.LIBELLE,
            an.TEXTE_ANNONCE,
            an.DATE_DEBUT,
            an.DESCRCUSTOMER,
            an.DESCRASSIGNMENT,
            an.ID_ANNONCE,
            mi.PAYS,
            mi.REGION,
            mi.ZIP,
            mi.CITY,
            mi.SALARY_FROM,
            mi.SALARY_TO,
            mi.TYPECONTRAT,
            mi.ID_MISSION,
            mi.`".$fonctionDbaFieldName."` AS `FONCTION`,
            mi.`".$secteurDbaFieldName."` AS `SECTEUR`,
            so.RAISON_SOCIALE,
            mi.ANONYMOUS

        FROM
            annonces AS an
        INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION
        INNER JOIN societes AS so ON so.ID_SOCIETE = mi.ID_SOCIETE
        

        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )
            ".$whereSearch."
        ORDER BY an.ID_MISSION DESC        
        LIMIT ".$start_element.",".$numberOfElementPerPage."

    ";


$sql45 = "
        SELECT
            al.REF2 AS REFERENCE,
            an.LIBELLE,
            an.TEXTE_ANNONCE,
            al.DATE_BEGIN AS DATE_DEBUT,
            an.DESCRCUSTOMER,
            an.DESCRASSIGNMENT,
            an.ID_ANNONCE,
            mi.PAYS,
            mi.REGION,
            mi.ZIP,
            mi.CITY,
            mi.SALARYMIN AS SALARY_FROM,
            mi.SALARYMAX AS SALARY_TO,
            mi.TYPECONTRAT,
            mi.ID_MISSION,
            mi.`".$fonctionDbaFieldName."` AS `FONCTION`,
            mi.`".$secteurDbaFieldName."` AS `SECTEUR`,
            so.RAISON_SOCIALE,
            mi.ANONYMOUS

        FROM
            tannonces AS an
        INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE
        INNER JOIN societes AS so ON so.ID_SOCIETE = mi.ID_SOCIETE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )
            ".$whereSearch."
        ORDER BY an.ID_MISSION DESC          
        LIMIT ".$start_element.",".$numberOfElementPerPage."

    ";

if(_ADMEN_USE_ADVERT_LINES){
    $sql = $sql45;
}
//echo $sql;
$printsql = $sql;
$select = $conn->prepare($sql);
//if(!empty($_SESSION['search_fonction'] )){
//    $selectedFonctions = array();
//    $selectedFonctions = explode("|",$_SESSION['search_fonction']);
//    $whereSearchFonction = "";
//    foreach($selectedFonctions as $key=>$selectedFonction){
//        $select->bindParam(':searchfunction'.$key, $selectedFonction, PDO::PARAM_STR);
//    }
//
//}
//if(!empty($_SESSION['search_secteur'] )){
//    $select->bindParam(':searchsecteur', $_SESSION['search_secteur'], PDO::PARAM_STR);
//}
$select->execute();
//print_t($select->errorInfo());
//print_t($printsql);
//Debug::d_sql_error($select, 0,"","liste.php",__LINE__);
while($row = $select->fetchObject()){
    if(_SHOW_SALARY==false){
        $row->SALARY_FROM=null;
        $row->SALARY_TO=null;
    }
    $annonces[] = $row;
}


$fonctions = array();
$sql = "
        SELECT DISTINCT mi.`".$fonctionDbaFieldName."` AS `FONCTION`
          
        FROM
            missions AS mi
        INNER JOIN annonces AS an ON an.ID_MISSION = mi.ID_MISSION
            
        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )
            
        ORDER BY mi.`".$fonctionDbaFieldName."` ASC

    ";



$sql45 = "
        SELECT DISTINCT mi.`".$fonctionDbaFieldName."` AS `FONCTION`

        FROM
            missions AS mi
        INNER JOIN tannonces AS an ON an.ID_MISSION = mi.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )

        ORDER BY mi.`".$fonctionDbaFieldName."` ASC

    ";

if(_ADMEN_USE_ADVERT_LINES){
    $sql = $sql45;
}

$select = $conn->prepare($sql);
$select->execute();
//Debug::d_sql_error($select, 0,"sql get fonctions","liste.php",__LINE__);
while($row = $select->fetchObject()){
    if(!empty($row->FONCTION)){
        $fonctions[] = $row;
    }
}

$secteurs = array();
$sql = "
        SELECT DISTINCT mi.`".$secteurDbaFieldName."` AS `SECTEUR`

        FROM
           missions AS mi
        INNER JOIN annonces AS an ON an.ID_MISSION = mi.ID_MISSION

        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )

        ORDER BY mi.`".$secteurDbaFieldName."` ASC

    ";


$sql45 = "
        SELECT DISTINCT mi.`".$secteurDbaFieldName."` AS `SECTEUR`

        FROM
           missions AS mi
        INNER JOIN tannonces AS an ON an.ID_MISSION = mi.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )

        ORDER BY mi.`".$secteurDbaFieldName."` ASC

    ";


if(_ADMEN_USE_ADVERT_LINES){
    $sql = $sql45;
}

$select = $conn->prepare($sql);
$select->execute();
//Debug::d_sql_error($select, 0,"sql get secteurs","liste.php",__LINE__);
while($row = $select->fetchObject()){
    if(!empty($row->SECTEUR)){
        $secteurs[] = $row;
    }
}




$locations = array();
$locations2 = array();
$locations3 = array();
$sql = "
        SELECT DISTINCT mi.`PAYS`

        FROM
           missions AS mi
        INNER JOIN annonces AS an ON an.ID_MISSION = mi.ID_MISSION

        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )

        ORDER BY mi.`PAYS` ASC

    ";


$sql45 = "
        SELECT DISTINCT mi.`PAYS`

        FROM
           missions AS mi
        INNER JOIN tannonces AS an ON an.ID_MISSION = mi.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )

        ORDER BY mi.`PAYS` ASC

    ";


if(_ADMEN_USE_ADVERT_LINES){
    $sql = $sql45;
}

$select = $conn->prepare($sql);
$select->execute();
//Debug::d_sql_error($select, 0,"sql get pays","liste.php",__LINE__);
while($row = $select->fetchObject()){
    if(!empty($row->PAYS)){
        $locations[] = $row->PAYS;
    }
}

$locations2 = array();
$locations3 = array();
foreach($locations as $key=>$location){
    $sql = "
        SELECT DISTINCT mi.`REGION`

        FROM
           missions AS mi
        INNER JOIN annonces AS an ON an.ID_MISSION = mi.ID_MISSION

        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND mi.PAYS = '".$location."'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )

        ORDER BY mi.`REGION` ASC

    ";

    $sql45 = "
        SELECT DISTINCT mi.`REGION`

        FROM
           missions AS mi
        INNER JOIN tannonces AS an ON an.ID_MISSION = mi.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND mi.PAYS = '".$location."'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )

        ORDER BY mi.`REGION` ASC

    ";
    
    if(_ADMEN_USE_ADVERT_LINES){
        $sql = $sql45;
    }
    $select = $conn->prepare($sql);
    $select->execute();
//    Debug::d_sql_error($select, 0,"sql get regions where pays=".$location,"liste.php",__LINE__);
    $locations2_temp = array();
    while($row = $select->fetchObject()){
        if(!empty($row->REGION)){
            $locations2_temp[] = $row->REGION;
        }
    }
    $locations2[] = array('pays'=>$location,'regions'=>$locations2_temp);
}
$locations = $locations2;

$locations2 = array();
$locations3 = array();
foreach($locations as $key=>$location){
    
    $locations3 = array();
    foreach($location['regions'] as $location2){
        $sql = "
            SELECT DISTINCT mi.`CITY`

            FROM
               missions AS mi
            INNER JOIN annonces AS an ON an.ID_MISSION = mi.ID_MISSION

            WHERE
                an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
                AND mi.ETAT = '1'
                AND mi.PAYS = '".$location['pays']."'
                AND mi.REGION = '".$location2."'
                AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
                AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )

            ORDER BY mi.`CITY` ASC

        ";


        $sql45 = "
            SELECT DISTINCT mi.`CITY`

            FROM
               missions AS mi
            INNER JOIN tannonces AS an ON an.ID_MISSION = mi.ID_MISSION
            INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

            WHERE
                al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
                AND mi.ETAT = '1'
                AND mi.PAYS = '".$location['pays']."'
                AND mi.REGION = '".$location2."'
                AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
                AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )

            ORDER BY mi.`CITY` ASC

        ";
        if(_ADMEN_USE_ADVERT_LINES){
            $sql = $sql45;
        }
        
        $select = $conn->prepare($sql);
        $select->execute();
//        Debug::d_sql_error($select, 0,"sql get city where pays=".$location['pays']." and region=".$location2,"liste.php",__LINE__);
        $locations3_temp = array();
        while($row = $select->fetchObject()){
            if(!empty($row->CITY)){
                $locations3_temp[] = $row->CITY;
            }
        }
        $locations3[] = array('region'=>$location2,'villes'=>$locations3_temp);

    }
    $locations2[] = array('pays'=>$location['pays'],'regions'=>$locations3);
    
}
$locations = $locations2;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title><?php echo _OFFRE_LISTE_TITLE; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content=""/>
        <meta name="keywords" content=""/>
        <?php  if(!_USE_CUSTOM_CSS){ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?>getcss_custom.php" rel="stylesheet" type="text/css" />
        <?php }else{ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?><?php echo _TEMPLATE_FOLDER_NAME; ?>/custom.css" rel="stylesheet" type="text/css" />
        <?php } ?>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/framework.js"></script>
        <script type="text/javascript" >
            $.ajaxSettings.cache = false;
            $(document).ready(function(){
                //Capture touche entré pour submit de la recherche
                $('#search_fonction').keyup(function(e) {
                    if(e.keyCode == 13){
                        saveSearch();
                    }
                 }
                );
                $('#search_secteur').keyup(function(e) {
                    if(e.keyCode == 13){
                        saveSearch();
                    }
                 }
                );
                $('#search_keyword').keyup(function(e) {
                    if(e.keyCode == 13){
                        saveSearch();
                    }
                 }
                );
                $('#search_location').keyup(function(e) {
                    if(e.keyCode == 13){
                        saveSearch();
                    }
                 }
                );

                <?php if(_IS_IFRAME_RESIZE){ ?>
                resizeiframe();
                <?php } ?>

            });

            function changeLanguage(language){
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-change_language.php",{ 'language': language});                    
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-get_url.php",
                    { 'pageref':'liste', 'langue':language },
                    function(data){
                        window.location=data;
                    }
                );

            }
            
            function saveSearch(){
                <?php
                if(_IS_SEARCH_FONCTION_MULTIPLE){
                    echo "var searchFunction = $('#search_fonction').val() || [];";
                }else{
                    echo "var searchFunction = $('#search_fonction').val();";
                }
                if(_IS_SEARCH_SECTEUR_MULTIPLE){
                    echo "var searchSecteur = $('#search_secteur').val()|| [];";
                }else{
                    echo "var searchSecteur = $('#search_secteur').val();";
                }
                ?>
                var searchLocation = $('#search_location').val();
                var searchKeyword = $('#search_keyword').val();

                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-save_search.php",
                    <?php
                    echo "{";
                        if(_IS_SEARCH_FONCTION_MULTIPLE){
                            echo "'search_fonction': searchFunction.join(\"|\"),";
                        }else{
                            echo "'search_fonction': searchFunction,";
                        }
                        if(_IS_SEARCH_SECTEUR_MULTIPLE){
                            echo "'search_secteur': searchSecteur.join(\"|\"),";
                        }else{
                            echo "'search_secteur': searchSecteur,";
                        }
                        echo "'search_location': searchLocation,";
                        echo "'search_keyword':searchKeyword";
                    echo "}";
                    ?>,
                    function(data){
//                        alert(data);
                        window.location='<?php echo _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language']); ?>';
                    }
                );
            }

            function toggle_expand_pays(id_pays){
                if($('#expanded_pays_'+id_pays).html() == '+'){
                    $('#expanded_pays_'+id_pays).html('-');
                    $('#expanded_pays_'+id_pays).css('padding-left','5px');
                    $('#pays_content_'+id_pays).show();
                }else{
                    $('#expanded_pays_'+id_pays).html('+');
                    $('#expanded_pays_'+id_pays).css('padding-left','2px');
                    $('#pays_content_'+id_pays).hide();
                }
            }

            function toggle_expand_region(id_region){
                if($('#expanded_region_'+id_region).html() == '+'){
                    $('#expanded_region_'+id_region).html('-');
                    $('#expanded_region_'+id_region).css('padding-left','5px');
                    $('#region_content_'+id_region).show();
                }else{
                    $('#expanded_region_'+id_region).html('+');
                    $('#expanded_region_'+id_region).css('padding-left','2px');
                    $('#region_content_'+id_region).hide();
                }
            }

            function select_ville($id_pays,id_region,id_ville){
                var pays = $('#pays_value_'+$id_pays).html();
                var region = $('#region_value_'+id_region).html();
                var ville = $('#ville_title_'+id_ville).html();
                $('#search_location').val(pays+" "+region+" "+ville);
                $('#location_popup_menu').hide();
            }

            function select_region($id_pays,id_region){
                var pays = $('#pays_value_'+$id_pays).html();
                var region = $('#region_value_'+id_region).html();
                $('#search_location').val(pays+" "+region);
                $('#location_popup_menu').hide();
            }

            function select_pays($id_pays){
                var pays = $('#pays_value_'+$id_pays).html();
                $('#search_location').val(pays);
                $('#location_popup_menu').hide();
            }

        </script>
    </head>
    <body>
        <?php
        $templateDatas = array();

        $templateDatas['pathway'] = "";
        $pathway = array();
        $pathway[0]['text'] = _OFFRE_LISTE_TITLE;
        $pathway[0]['url'] = null;
        $templateDatas['pathway'] =  displayPathway($pathway);
        
        $templateDatas['header'] = "";
        $templateDatas['header'] = displayHeader();

        $templateDatas['flags'] = "";
        $templateDatas['flags'] = displayFlags();

        $templateDatas['candidatArea'] = "";
        $templateDatas['candidatArea'] = displayCandidatArea($conn);

        $templateDatas['menu'] = "";
        $templateDatas['menu'] = displayMenu();


        /*
         * Formulaire de filtrage
         */

        //popup pour la liste des pays
        $templateDatas['filters'] = array();
        $templateDatas['filters']['countryPopupContent'] = "";
        $id_pays = 0;
        $id_region = 0;
        $id_ville = 0;
        foreach($locations as $pays){
            $templateDatas['filters']['countryPopupContent'] .= "<div id=\"pays_title_".$id_pays."\" class=\"pays_title hoverbale inputdesign\"><span id=\"expanded_pays_".$id_pays."\" class=\"spanexp\" onclick=\"toggle_expand_pays(".$id_pays.");\">+</span><span id=\"pays_value_".$id_pays."\" class=\"pays_value\" onclick=\"select_pays(".$id_pays.");\">".$pays['pays']."</span></div>";
            $templateDatas['filters']['countryPopupContent'] .= "<div id=\"pays_content_".$id_pays."\" class=\"pays_content\" >";
            foreach($pays['regions'] as $region){
                $templateDatas['filters']['countryPopupContent'] .= "<div id=\"region_title_".$id_region."\" class=\"region_title hoverbale inputdesign\"><span id=\"expanded_region_".$id_region."\" class=\"spanexp\" onclick=\"toggle_expand_region(".$id_region.");\">+</span><span id=\"region_value_".$id_region."\" class=\"region_value\" onclick=\"select_region(".$id_pays.",".$id_region.");\">".$region['region']."</span></div>";
                $templateDatas['filters']['countryPopupContent'] .= "<div id=\"region_content_".$id_region."\" class=\"region_content\" >";
                foreach($region['villes'] as $ville){
                    $templateDatas['filters']['countryPopupContent'] .= "<div id=\"ville_title_".$id_ville."\" class=\"ville_title hoverbale inputdesign\" onclick=\"select_ville(".$id_pays.",".$id_region.",".$id_ville.");\">".$ville."</div>";
                    $id_ville++;
                }
                $templateDatas['filters']['countryPopupContent'] .= "</div>";
                $id_region++;
            }
            $templateDatas['filters']['countryPopupContent'] .= "</div>";
            $id_pays++;
        }
        
        //liste fonction
        $templateDatas['filters']['functionListContent'] = "";
        $selectedFonctions = array();
        if(!empty($_SESSION['search_fonction'])){
            $selectedFonctions = explode("|",$_SESSION['search_fonction']);
        }
        //$selectedFonctions = explode("|",$_SESSION['search_fonction']);
        foreach($fonctions as $fonction){
            $selected = "";
            foreach($selectedFonctions as $selectedFonction){
                if($selectedFonction == $fonction->FONCTION){
                    $selected = "selected=\"selected\"";
                }
            }

            $templateDatas['filters']['functionListContent'] .= "<option value=\"".$fonction->FONCTION."\" ".$selected.">".$fonction->FONCTION."</option>";
        }





//        $templateDatas['filters']['functionListContent'] = "";
//        foreach($fonctions as $fonction){
//            $selected = "";
//            if(!empty($_SESSION['search_fonction'] )){
//                if($_SESSION['search_fonction'] == $fonction->FONCTION){
//                    $selected = "selected=\"selected\"";
//                }
//            }
//            $templateDatas['filters']['functionListContent'] .= "<option value=\"".$fonction->FONCTION."\" ".$selected.">".$fonction->FONCTION."</option>";
//        }


        //selected location value
        $templateDatas['filters']['locationSelectedValue'] = "";
        if(!empty($_SESSION['search_location'] )){
            $templateDatas['filters']['locationSelectedValue'] =  $_SESSION['search_location'];
        }


        //liste secteur
        $templateDatas['filters']['sectorListContent'] = "";
        $selectedSecteurs = array();
        if(!empty($_SESSION['search_secteur'])){
            $selectedSecteurs = explode("|",$_SESSION['search_secteur']);
        }
        //$selectedSecteurs = explode("|",$_SESSION['search_secteur']);
        foreach($secteurs as $secteur){
            $selected = "";
            foreach($selectedSecteurs as $selectedSecteur){
                if($selectedSecteur == $secteur->SECTEUR){
                    $selected = "selected=\"selected\"";
                }
            }
            $templateDatas['filters']['sectorListContent'] .=  "<option value=\"".$secteur->SECTEUR."\" ".$selected.">".$secteur->SECTEUR."</option>";
        }

//        $templateDatas['filters']['sectorListContent'] = "";
//        foreach($secteurs as $secteur){
//            $selected = "";
//            if(!empty($_SESSION['search_secteur'] )){
//                if($_SESSION['search_secteur'] == $secteur->SECTEUR){
//                    $selected = "selected=\"selected\"";
//                }
//            }
//            $templateDatas['filters']['sectorListContent'] .=  "<option value=\"".$secteur->SECTEUR."\" ".$selected.">".$secteur->SECTEUR."</option>";
//        }


        //selected keyword
        $templateDatas['filters']['keywordSelectedValue'] = "";        
        if(!empty($_SESSION['search_keyword'] )){
             $templateDatas['filters']['keywordSelectedValue'] = $_SESSION['search_keyword'];
        }


        //pas de resultat
        $templateDatas['noResultText'] = "";
        if($nombreDeResultat == 0){
            $templateDatas['noResultText'] = "<div id=\"noresultmsg\">"._OFFRE_LISTE_NORESULT_MSG."</div>";
        }


        //url search engine
        $templateDatas['urlToSearchEngine'] = "";
        $templateDatas['urlToSearchEngine'] = _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_SEARCH_".$_SESSION['awa_language'])  ;

        //pagination
        $templateDatas['pagination'] = "";
        $templateDatas['pagination'] = $pagination_HTML  ;



        //Footer
        $templateDatas['footerText'] = "";
        $templateDatas['footerText'] = displayFooter();
        ?>




        <?php include(_TEMPLATE_FOLDER_NAME."/tpl_liste.php"); ?>
    </body>
</html>
<?php
//print_t($printsql);
//print_t($_SESSION);
////echo "<pre>";
////print_r($fonctions);
////echo "</pre>";
//echo "<pre>";
//print_r($locations);
//echo "</pre>";
?>