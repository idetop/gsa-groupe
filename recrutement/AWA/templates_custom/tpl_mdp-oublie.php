<div id="container980">
    <div id="header"></div>
    <div id="menu">
        <?php echo $templateDatas['menu']; ?>
        <div class="spacer"></div>
    </div>
    <div id="subheader">
        <div id="pathway">
            <?php echo $templateDatas['pathway'];  ?>
        </div>
        <div id="languesdisplay">
            <?php echo $templateDatas['flags']; ?>
        </div>
        <div class="spacer"></div>
    </div>
    <?php echo $templateDatas['candidatArea']; ?>
    <div id="middle">
        <div id="content">



            <div class="contenttitle"><h1><?php echo _MDPOUBLIE_TITLE; ?></h1></div>

            <div style="height: 20px;"></div>
            <?php if($message != ""){ ?>
                <div class="messageok"><?php echo $message; ?></div>
            <?php } ?>
            <?php if($message2 != ""){ ?>
                <div class="messagenotok"><?php echo $message2; ?></div>
            <?php } ?>
            <div style="">
                <?php echo _MDPOUBLIE_INTRO_TEXT; ?>
            </div>
            <div style="height: 20px;"></div>
            <div style="padding-left: 250px;">
                <form id="login_form" action="" method="post">
                    <fieldset>
                        <div class="candidature_formrow" >
                            <div class="label labeldesign" style="width: 150px;"><?php echo _ESPACECANDIDAT_LOGIN_FORM_EMAIL_LABEL; ?> <span class="compulsorystar">*</span></div>
                            <div class="input inputdesign">
                                <input type="text" id="login" name="login" value=""/>
                            </div>
                            <div class="spacer"></div>
                        </div>


                        <div style="padding-top: 35px;padding-left: 150px;">
                            <div id="btn_connexion" class="buttonclass" onclick="checkForm();"><?php echo _MDPOUBLIE_SEND_BTN; ?></div>
                        </div>

                    </fieldset>
                </form>
            </div>



            <div style="height: 30px;"></div>
        </div>
        <div class="spacer"></div>
    </div>
    <div id="footer">
         <?php echo $templateDatas['footerText']; ?>
    </div>
    </div>