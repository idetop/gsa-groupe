<?php
// CONNEXION MYSQL
include('../mysql/mysql.php');

$pole = "pharma";

##################################
# textes
##################################
function textes($type,$polename,$pagename){
    $pagename = str_replace(".html","",$pagename);
    $pagename = str_replace("/","",$pagename);
	if($type == 'intro'){
		$sql = mysqli_query($connexion, "SELECT * FROM gsa_pole_head WHERE pole = '".$polename."' AND page = '".$pagename."'");
		$dat = mysqli_fetch_array($sql);
		echo stripslashes($dat['texte']);
	}
	else{
		$sql = mysqli_query($connexion, "SELECT * FROM gsa_pole_textes WHERE pole = '".$polename."' AND page = '".$pagename."' ORDER BY id ASC");
		while($row = mysqli_fetch_array($sql)){
			echo '
			<div class="para">';
                if(! empty($row['titre'])){
                    echo '<img src="images/arrow-title.gif" class="arrow" />';
                }
				echo '<p class="michroma20 left"><span>'.stripslashes($row['titre']).'</span></p>
				<br style="clear:left" />';
                if(! empty($row['image'])){
                    echo '<img src="images/'.$row['image'].'" />';
                }
				echo stripslashes($row['texte']).'
			</div>
			<br />
			<img src="images/separe-para.jpg" />
			<br />';
		}
	}
}
?>
