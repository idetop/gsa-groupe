<div id="container980">
    <div id="header"></div>
    <div id="menu">
        <?php echo $templateDatas['menu']; ?>
        <div class="spacer"></div>
    </div>
    <div id="subheader">
        <div id="pathway">
            <?php echo $templateDatas['pathway']; ?>
        </div>
        <div id="languesdisplay">
             <?php echo $templateDatas['flags']; ?>
        </div>
        <div class="spacer"></div>
    </div>
    <?php echo $templateDatas['candidatArea']; ?>
    <div id="middle">
        <div id="content">
            <div class="contenttitle"><h1><?php echo _CANDIDATURE_SPONTANE_TITLE; ?></h1></div>
            <div style="height: 20px;"></div>
            <?php if($templateDatas['displayCVReaderUpload']){ ?>
                <div id="cvreaderintrotext"><?php echo _CVREADER_UPLOAD_INTROTEXT; ?></div>
            <?php } ?>
            <div style="padding-left: 200px;">
                
                <?php if($templateDatas['displayCVReaderUpload']){ ?>
                <form id="candidature_formcvr" action="" method="post" onsubmit="" enctype="multipart/form-data">
                    <fieldset>
                        <?php echo _CVREADER_UPLOAD_LABEL; ?>
                        <input type="file" id="uploadforcvr" name="uploadforcvr"/>
                        <input type="submit" name="uploadforcvrsub" id="uploadforcvrsub" value="<?php echo _CVREADER_UPLOAD_BTN; ?>"/>
                    </fieldset>
                </form>
                <?php } ?>

                <form id="candidature_form" action="candidature-spontanee-save.php" method="post" onsubmit="" enctype="multipart/form-data">
                    <fieldset>
                        <?php if(!$templateDatas['displayCVReaderUpload']){ ?>
                            <?php  echo $formulaire_HTML;  ?>
                        <?php } ?>
                        <br/>
                        <?php if(!$templateDatas['displayCVReaderUpload']){ ?>
                            <?php if(empty($ID_PERSONNE_WEB)){ ?>
                            <div class="candidature_formrow" >
                                <div class="labelc" style="float: none; width:440px; "><?php echo _CANDIDATURE_SECU_LABEL; ?> <span class="compulsorystar">*</span></div>
                            </div>
                            <div class="candidature_formrow" >
                                <div class="labelc" style="text-align: right;"> <?php echo $_SESSION['secu_nb_1'] ?> + <?php echo $_SESSION['secu_nb_2'] ?> = </div>
                                <div class="inputc"><input type="text" id="candi_secu" name="candi_secu" value=""/></div>
                                <div class="spacer"></div>
                            </div>
                            <?php } ?>
                        <?php } ?>
                        <input type="hidden" name="emailpresence" id="emailpresence" value="0"/>
                        <div style="padding-top: 35px;padding-left: 230px;">
                            <?php if(!$templateDatas['displayCVReaderUpload']){ ?>
                            <div id="btn_envoyer" class="buttonclass" onclick="checkForm();"><?php echo _CANDIDATURE_CS_SUBMIT; ?></div>
                            <?php } ?>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div style="height: 30px;"></div>
        </div>
        <div class="spacer"></div>
    </div>
    <div id="footer">
        <?php echo $templateDatas['footerText']; ?>
    </div>
</div>