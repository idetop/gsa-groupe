<?php
/*é
 * Gestion multilingues pour le calendrier JS
 */
include_once "../display_errors.php";
@session_start();
include_once "../../../include/config.php";
@include_once '../pdo.php';
include_once "../framework.php";




echo "var A_TCALDEF = {
	'months' : ['"._CALENDAR_MONTHS_1."', '"._CALENDAR_MONTHS_2."', '"._CALENDAR_MONTHS_3."', '"._CALENDAR_MONTHS_4."', '"._CALENDAR_MONTHS_5."', '"._CALENDAR_MONTHS_6."', '"._CALENDAR_MONTHS_7."', '"._CALENDAR_MONTHS_8."', '"._CALENDAR_MONTHS_9."', '"._CALENDAR_MONTHS_10."', '"._CALENDAR_MONTHS_11."', '"._CALENDAR_MONTHS_12."'],
	'weekdays' : ['"._CALENDAR_WEEKDAYSHORT_7."', '"._CALENDAR_WEEKDAYSHORT_1."', '"._CALENDAR_WEEKDAYSHORT_2."', '"._CALENDAR_WEEKDAYSHORT_3."', '"._CALENDAR_WEEKDAYSHORT_4."', '"._CALENDAR_WEEKDAYSHORT_5."', '"._CALENDAR_WEEKDAYSHORT_6."'],
	'yearscroll': true, // show year scroller
	'weekstart': 1, // first day of week: 0-Su or 1-Mo
	'centyear'  : 70, // 2 digit years less than 'centyear' are in 20xx, othewise in 19xx.
	'imgpath' : '"._CONFIG_PROTOCOL."://"._CONFIG_DOMAINENAME._CONFIG_ROOTFOLDER."/include/calendar/img/' // directory with calendar images
}
var alert_invalidDate = '"._CALENDAR_ALERT_INVALIDDATE."';
var alert_acceptedFormat = '"._CALENDAR_ALERT_ACCEPTEDFORMAT."';
var alert_invalidMonth = '"._CALENDAR_ALERT_INVALIDMONTH."';
var alert_allowedMonthRange = '"._CALENDAR_ALERT_MONTHRANGE."';
var alert_invalidDay = '"._CALENDAR_ALERT_INVALIDDAY."';
var alert_allowedDayRange = '"._CALENDAR_ALERT_DAYRANGE."';
var popup_openCalendar = '"._CALENDAR_POPUP_OPENCALENDAR."';
var popup_previousYear = '"._CALENDAR_POPUP_PREVIOUSYEAR."';
var popup_previousMonth = '"._CALENDAR_POPUP_PREVIOUSMONTH."';
var popup_nextMonth = '"._CALENDAR_POPUP_NEXTMONTH."';
var popup_nextYear = '"._CALENDAR_POPUP_NEXTYEAR."';
";
?>
