<?php
include_once "../include/display_errors_bo.php";
@session_start();

Debug::d_echo("acces ", 2,"v-add-mailrelance.php");
Debug::d_print_r($_GET, 1,"GET","v-add-mailrelance.php");
Debug::d_print_r($_POST, 1,"POST","v-add-mailrelance.php");
Debug::d_print_r($_SESSION, 1,"SESSION","v-add-mailrelance.php");


include_once "../../include/config.php";
@include_once '../include/pdo.php';



include_once "../include/framework.php";
include_once "../include/tableau.php";

$starttime = microtime();

/*
 * Variables communes
 */
$errorMsg = "";
$returnMsg = "";
$formsubmit = false;        //true après enregistrement du formulaire
$element_id = null;         //null si add et non null si edit
$edit = false;              //true si edit, false si add


/*
 * Control d'accès
 */
if(empty($_SESSION['boe_user_id'])){
    $_SESSION['boe_user_id'] = null;
}
if(empty ($_SESSION['boe_user_email'])){
    $_SESSION['boe_user_email'] = null;
}
test_session_boe($_SESSION['boe_user_id'], $_SESSION['boe_user_email'], "../", $conn);


/*
 * Traitement des données d'entrées ($_GET)
 */
if(!empty($_GET['element_id'])){
    $element_id = $_GET['element_id'];
    Debug::d_echo("edition mail relance candidat  ".$element_id, 2,"v-add-mailrelance.php");
}else{
    if(!count($_POST)){
        Debug::d_echo("ajout mail relance candidat  ", 2,"v-add-mailrelance.php");
    }
}

$relance_ID = "";
$relance_LANGUE = "";
$relance_OBJECT = "";
$relance_BODY = "";
$relance_DEFAULT = "";

if(!empty($element_id)){
    $sql = "
        SELECT
            `fo`.*
            
        FROM
            `awa_mailsrelance` AS `fo`
        WHERE
            fo.ID =:idfo

        ";

    $select = $conn->prepare($sql);
    $select->bindParam(':idfo', $element_id, PDO::PARAM_INT);
    $select->execute();
    $relance = null;
    $relance = $select->fetchObject();
    if($relance){
        $relance_ID = $relance->ID;
        $relance_LANGUE = $relance->LANGUE;
        $relance_OBJECT = $relance->OBJECT;
        $relance_BODY = $relance->BODY;
        $relance_DEFAULT = $relance->DEFAULT;
    }
    
    
}
if(count($_POST)){
//   print_t($_POST);
   if(!empty($_POST['id'])){
       //Update


        $sql = "
            UPDATE
                awa_mailsrelance
            SET
                LANGUE =:LANGUE ,
                OBJECT =:OBJECT ,
                BODY =:BODY ,
                `DEFAULT` =:DEFAULT ,
                DATE_UPDATE = '".date('Y-m-d')."'
            WHERE
                ID =:idfo

            ";

        $select = $conn->prepare($sql);
        $select->bindParam(':idfo', $_POST['id'], PDO::PARAM_INT);
        $select->bindParam(':LANGUE', $_POST['fcs_langue'], PDO::PARAM_STR);
        $select->bindParam(':OBJECT', $_POST['fcs_objet'], PDO::PARAM_STR);
        $select->bindParam(':BODY', $_POST['fcs_body'], PDO::PARAM_STR);
        $select->bindParam(':DEFAULT', $_POST['fcs_default'], PDO::PARAM_INT);
        
        $select->execute();
        Debug::d_echo("sauvegarde mail relance candidat  ".$_POST['id'], 2,"v-add-mailrelance.php");

        echo "<script type='text/javascript' >";
        echo " closePopup_mailingcandidats('".urlencode(_CONF_FORM_MAIL_RELANCE_ADD_RETURN_MSG)."')";
        echo "</script>";
   }else{
       //Insert
       $optionselement = "";


        $sql = "
            INSERT INTO
                awa_mailsrelance
            SET
                LANGUE =:LANGUE ,
                OBJECT =:OBJECT ,
                BODY =:BODY ,
                `DEFAULT` =:DEFAULT,
                DATE_CREATION = '".date('Y-m-d')."',
                DATE_UPDATE = '".date('Y-m-d')."'

            ";

        $select = $conn->prepare($sql);
        $select->bindParam(':LANGUE', $_POST['fcs_langue'], PDO::PARAM_STR);
        $select->bindParam(':OBJECT', $_POST['fcs_objet'], PDO::PARAM_STR);
        $select->bindParam(':BODY', $_POST['fcs_body'], PDO::PARAM_STR);
        $select->bindParam(':DEFAULT', $_POST['fcs_default'], PDO::PARAM_INT);
        $select->execute();
        Debug::d_echo("sauvegarde mail relance candidat  ", 2,"v-add-mailrelance.php");

        echo "<script type='text/javascript' >";
        echo " closePopup_mailingcandidats('".urlencode(_CONF_FORM_MAIL_RELANCE_ADD_RETURN_MSG2)."')";
        echo "</script>";

   }
}
?>
<script type="text/javascript" >
    function checkForm(){
        if($('#fcs_langue').val() == ""){
            alert("<?php echo _CONF_FORM_MAIL_RELANCE_ADD_LANGUE_JS_ERROR; ?>");
            $('#fcs_langue').focus();
            return false;
        }

        if($('#fcs_objet').val() == ""){
            alert("<?php echo _CONF_FORM_MAIL_RELANCE_ADD_OBJET_JS_ERROR; ?>");
            $('#fcs_objet').focus();
            return false;
        }

        if($('#fcs_body').val() == ""){
            alert("<?php echo _CONF_FORM_MAIL_RELANCE_ADD_BODY_JS_ERROR; ?>");
            $('#fcs_body').focus();
            return false;
        }


        submitFormAjax();
    }
    
    function submitFormAjax(){
        $.ajax({
            "type": "POST",
            "url": "v-add-mailrelance.php",
            "cache": false,
            "data": $('#form_entreprise').serialize(),
            "success": function(msg){
                $('#popupdiv_content_mailingcandidats').html(msg);
            }
        });
    }

    

</script>
<form enctype="multipart/form-data" onsubmit="checkForm();return false;" action="" method="post" id="form_entreprise">
    <fieldset>
        <input type="hidden" value="<?php echo $relance_ID; ?>" id="id" name="id">
        <div class="formrow">
            <label for="fcs_langue" class="w150"><?php echo _CONF_FORM_MAIL_RELANCE_ADD_LANGUE_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $relance_LANGUE; ?>" style="width: 420px;" class="fs12 w150" id="fcs_langue" name="fcs_langue">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="fcs_objet" class="w150"><?php echo _CONF_FORM_MAIL_RELANCE_ADD_OBJET_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $relance_OBJECT; ?>" style="width: 420px;" class="fs12 w150" id="fcs_objet" name="fcs_objet">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="fcs_body" class="w150"><?php echo _CONF_FORM_MAIL_RELANCE_ADD_BODY_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <textarea cols="" rows="" style="width: 420px; height: 350px;" class="fs12 w150" id="fcs_body" name="fcs_body"><?php echo $relance_BODY; ?></textarea>
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="fcs_default" class="w150"><?php echo _CONF_FORM_MAIL_RELANCE_ADD_DEFAULT_LABEL; ?> </label>
            <select id="fcs_default" name="fcs_default" class="fs12 ">
                <option value="0" <?php if($relance_DEFAULT=="0"){echo "selected=\"selected\"";} ?>><?php echo _CONF_FORM_MAIL_RELANCE_ADD_DEFAULT_VAL0; ?></option>
                <option value="1" <?php if($relance_DEFAULT=="1"){echo "selected=\"selected\"";} ?>><?php echo _CONF_FORM_MAIL_RELANCE_ADD_DEFAULT_VAL1; ?></option>
            </select>
            <div class="spacer"></div>
        </div>
        {name}<br/>
        {firstname}<br/>
        {login}<br/>
        {password}<br/>
        {url_login}<br/>
        
        <div style="height: 20px;"></div>
        

           
        <div style="height: 20px;"></div>
        <div class="center">
            <input type="submit" name="sbm" id="sbm" value="<?php echo _CONF_FORM_MAIL_RELANCE_ADD_SUBMIT_BTN; ?>"/>
        </div>
    </fieldset>
</form>