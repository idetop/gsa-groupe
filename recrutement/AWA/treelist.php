<?php
ini_set('error_log','1');
error_reporting(E_ALL);
error_reporting(E_ALL | E_STRICT | E_PARSE);
ini_set('display_startup_errors','1');
ini_set('display_errors','1');

session_start();
date_default_timezone_set('Europe/Paris');

include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');
include_once('include/Treelist-class.php');

$treelist = new Treelist('Nationality');
$treelist->setConn($conn);
$treelist->buildChilds();



//print_t($treelist);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content=""/>
        <meta name="keywords" content=""/>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?>css/front.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?>getcss_custom.php" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/jquery-1.4.2.js"></script>
        <script type="text/javascript" >
            $.ajaxSettings.cache = false;
            $(document).ready(function(){

            });

           

        </script>
    </head>
    <body>
        <?php echo $treelist->renderHTML(); ?>


        
    </body>
</html>