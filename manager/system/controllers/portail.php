<?php
########################################################
# Supprimer > suppression compte delegue
########################################################
if(isset($_REQUEST['del']) && isset($_REQUEST['id_del'])){
    $delete = mysqli_query($connexion, "DELETE FROM imn_intranet_delegues WHERE id = '".$_REQUEST['id_del']."'");
    $retour = "success";
    $alerte = "La suppression a bien été effectué.";
}

########################################################
# Ajout > enregistrement nouvelle delegue
########################################################
if(isset($_REQUEST['submit_add']) && $_REQUEST['submit_add'] != ''){
    $nom    = addslashes($_REQUEST['nom']);
    $adress = addslashes($_REQUEST['adresse']);
    $cp     = $_REQUEST['code_postal'];
    $ville  = addslashes($_REQUEST['ville']);
    $tel    = $_REQUEST['tel'];
    $fax    = $_REQUEST['fax'];
    $email  = $_REQUEST['mail'];
    $site   = strtolower($_REQUEST['site_web']);
    $login  = addslashes($_REQUEST['login_add']);
    $pwd    = addslashes($_REQUEST['pwd_add']);
    $age    = $_REQUEST['bjour'].'/'.$_REQUEST['bmois'].'/'.$_REQUEST['bannee'];
    $fonct  = addslashes($_REQUEST['fonction']);
	// Image ?
	$filename = "";
	if(! empty($_FILES['file_photo']['name'])) {
		// renomme
		$extension  = pathinfo($_FILES['file_photo']['name'],PATHINFO_EXTENSION);
		$name = rand(10,9999999999);
		$filename = $name.".".$extension;
		$filename_th = "th_".$name.".".$extension;
		// dossier
		$dir = $_SERVER['DOCUMENT_ROOT'].'/i/downloads/';
		$ratio = 200;
		$targetFile =  str_replace('//','/',$dir).$filename;
		$tempFile = $_FILES['file_photo']['tmp_name'];
		// resize
		$tableau = getimagesize($tempFile);
		if($tableau[2] == 2 || $tableau[2] == 3 || $tableau[2] == 1) {
			move_uploaded_file($tempFile,$targetFile);
			chmod($targetFile, 0755);
			// IMAGE JPG, JPEG
			if ($tableau[2] == 2) {
				$src = imagecreatefromjpeg($dir.$filename);
				$im = imagecreatetruecolor($ratio, round(($ratio/$tableau[0])*$tableau[1]));
				imagecopyresampled($im, $src, 0, 0, 0, 0, $ratio, round($tableau[1]*($ratio/$tableau[0])), $tableau[0], $tableau[1]);
				imagejpeg($im,$dir.$filename_th,100);
				chmod($dir.$filename_th, 0755);
			}
			// IMAGE PNG
			elseif ($tableau[2] == 3) {
				$src = imagecreatefrompng($dir.$filename);
				$im = imagecreatetruecolor($ratio, round(($ratio/$tableau[0])*$tableau[1]));
				imagecopyresampled($im, $src, 0, 0, 0, 0, $ratio, round($tableau[1]*($ratio/$tableau[0])), $tableau[0], $tableau[1]);
				imagepng($im,$dir.$filename_th,9);
				chmod($dir.$filename_th, 0755);
			}
			// IMAGE GIF
			elseif ($tableau[2] == 1) {
				$src = imagecreatefromgif($dir.$filename);
				$im = imagecreatetruecolor($ratio, round(($ratio/$tableau[0])*$tableau[1]));
				imagecopyresampled($im, $src, 0, 0, 0, 0, $ratio, round($tableau[1]*($ratio/$tableau[0])), $tableau[0], $tableau[1]);
				imagegif($im,$dir.$filename_th,100);
				chmod($dir.$filename_th, 0755);
			}
			// End resize
		}
		// End image
	}
    // INSERT
    $insert = mysqli_query($connexion, "INSERT INTO imn_intranet_delegues (nom,adresse,code_postal,ville,tel,fax,mail,site_web,login,password,photo,age,fonction) VALUES
              ('".$nom."','".$adress."','".$cp."','".$ville."','".$tel."','".$fax."','".$email."','".$site."','".$login."','".$pwd."','".$filename."','".$age."','".$fonct."')");
    $retour = "success";
    $alerte = "la fiche du délégué <strong>".stripslashes($nom)."</strong> a bien été ajouté.";
}

########################################################
# Update > mise à jour infos delegue
########################################################
if(! empty($_REQUEST['update']) && isset($_REQUEST['id_update']) && $_REQUEST['id_update'] != ''){
    $nom    = addslashes($_REQUEST['nom']);
    $adress = addslashes($_REQUEST['adresse']);
    $cp     = $_REQUEST['code_postal'];
    $ville  = addslashes($_REQUEST['ville']);
    $tel    = $_REQUEST['tel'];
    $fax    = $_REQUEST['fax'];
    $email  = $_REQUEST['mail'];
    $site   = strtolower($_REQUEST['site_web']);
    $login  = addslashes($_REQUEST['login_update']);
    $pwd    = addslashes($_REQUEST['pwd_update']);
    $age    = $_REQUEST['bjour'].'/'.$_REQUEST['bmois'].'/'.$_REQUEST['bannee'];
    $fonct  = addslashes($_REQUEST['fonction']);
	// Image ?
	$filename = $_REQUEST['file_photo_old'];
	if(! empty($_FILES['file_photo']['name'])) {
		// renomme
		$extension  = pathinfo($_FILES['file_photo']['name'],PATHINFO_EXTENSION);
		$name = rand(10,9999999999);
		$filename = $name.".".$extension;
		$filename_th = "th_".$name.".".$extension;
		// dossier
		$dir = $_SERVER['DOCUMENT_ROOT'].'/i/downloads/';
		$ratio = 200;
		$targetFile =  str_replace('//','/',$dir).$filename;
		$tempFile = $_FILES['file_photo']['tmp_name'];
		// resize
		$tableau = getimagesize($tempFile);
		if($tableau[2] == 2 || $tableau[2] == 3 || $tableau[2] == 1) {
			move_uploaded_file($tempFile,$targetFile);
			chmod($targetFile, 0755);
			// IMAGE JPG, JPEG
			if ($tableau[2] == 2) {
				$src = imagecreatefromjpeg($dir.$filename);
				$im = imagecreatetruecolor($ratio, round(($ratio/$tableau[0])*$tableau[1]));
				imagecopyresampled($im, $src, 0, 0, 0, 0, $ratio, round($tableau[1]*($ratio/$tableau[0])), $tableau[0], $tableau[1]);
				imagejpeg($im,$dir.$filename_th,100);
				chmod($dir.$filename_th, 0755);
			}
			// IMAGE PNG
			elseif ($tableau[2] == 3) {
				$src = imagecreatefrompng($dir.$filename);
				$im = imagecreatetruecolor($ratio, round(($ratio/$tableau[0])*$tableau[1]));
				imagecopyresampled($im, $src, 0, 0, 0, 0, $ratio, round($tableau[1]*($ratio/$tableau[0])), $tableau[0], $tableau[1]);
				imagepng($im,$dir.$filename_th,9);
				chmod($dir.$filename_th, 0755);
			}
			// IMAGE GIF
			elseif ($tableau[2] == 1) {
				$src = imagecreatefromgif($dir.$filename);
				$im = imagecreatetruecolor($ratio, round(($ratio/$tableau[0])*$tableau[1]));
				imagecopyresampled($im, $src, 0, 0, 0, 0, $ratio, round($tableau[1]*($ratio/$tableau[0])), $tableau[0], $tableau[1]);
				imagegif($im,$dir.$filename_th,100);
				chmod($dir.$filename_th, 0755);
			}
			// End resize
		}
		// End image
	}
    // UPDATE
    $update = mysqli_query($connexion, "UPDATE imn_intranet_delegues SET
            nom         = '".$nom."',
            adresse     = '".$adress."',
            code_postal = '".$cp."',
            ville       = '".$ville."',
            tel         = '".$tel."',
            fax         = '".$fax."',
            mail        = '".$email."',
            site_web    = '".$site."',
            login       = '".$login."',
            password    = '".$pwd."',
            photo       = '".$filename."',
            age         = '".$age."',
            fonction    = '".$fonct."'
         WHERE id = '".$_REQUEST['id_update']."'");
    $retour = "success";
    $alerte = "la fiche du délégué <strong>".stripslashes($nom)."</strong> a bien été mise à jour.";
}

########################################################
# Edit > détail delegue
########################################################
if(isset($_REQUEST['action']) && isset($_REQUEST['id'])){
    $sql_edit = mysqli_query($connexion, "SELECT * FROM imn_intranet_delegues WHERE id = '".$_REQUEST['id']."'");
    $data_edit = mysqli_fetch_array($sql_edit);
    $bjour  = '01';
    $bmois  = '01';
    $bannee = date('Y')-18;
    if(! empty($data_edit['age'])){
        $bjour  = substr($data_edit['age'],0,2);
        $bmois  = substr($data_edit['age'],3,2);
        $bannee = substr($data_edit['age'],6,4);
        $age = age($data_edit['age']);
    }
}

########################################################
# liste delegues
########################################################
function liste_delegues(){
    $i = 0;
    $sql = mysqli_query($connexion, "SELECT * FROM imn_intranet_delegues WHERE nom != '' ORDER BY nom ASC");
    while($row = mysqli_fetch_array($sql)){
        $color = ++$i % 2 ? '#ffffff':'#EAEAEA';
        echo '
        <tr style="background:'.$color.'">
            <td class="td_content" style="text-align:left">
                <a href="index.php?page='.$_REQUEST['page'].'&action=edit&id='.$row['id'].'"><strong>'.stripslashes(strtoupper($row['nom'])).'</strong></a>
            </td>
            <td class="td_content" style="text-align:left">'.$row['code_postal'].' '.stripslashes($row['ville']).'</td>
            <td class="td_content" style="text-align:left">'.$row['tel'].'</td>
            <td class="td_content" style="text-align:left"><a href="mailto:'.$row['mail'].'">'.$row['mail'].'</a></td>
            <td width="65" style="padding:5px;">
                <form name="del_fiche" method="post" action="">
                    <input type="hidden" name="id_del" value="'.$row['id'].'">
                    <input id="submit_del" name="del" type="submit" value="" style="float:right" onClick="return confirm(\'\nEtes-vous sur de vouloir supprimer cette fiche ?\n\n\');">
                    <a href="index.php?page='.$_REQUEST['page'].'&action=edit&id='.$row['id'].'">
                        <input id="submit_accept" type="button" value="" style="float:right;background:url(\'src/icons/page_white_edit.png\') no-repeat;">
                    </a>
                </form>
            </td>
        </tr>';
    }
}
?>
