<?php
//ini_set('memory_limit', '1024M');
//set_time_limit('7200');
error_reporting(E_ALL);
error_reporting(E_ALL | E_STRICT | E_PARSE);
ini_set('display_startup_errors','1');
ini_set('display_errors','1');

date_default_timezone_set('Europe/Paris');

/*
 * Variable a saisir en fonction de votre configuration
 * Chemin du dossier contenant AWA par rapport à la racine du serveur
 */
define("_AWA_PATHWAY","/homez.218/gsagroup/www/recrutement/AWA/");


include_once(_AWA_PATHWAY.'include/config.php');
include_once(_AWA_PATHWAY.'include/pdo.php');


include_once(_AWA_PATHWAY.'include/display_errors_import.php');

/*
 * Permet l'affichage de debug dans la console (si le script est appellé via la console)
 * L'activation du debug peu ralentir le script
 * Ne pas activer lors de l'appel du script automatiquement via une tache CRON
 */
define("_DEBUG_IMPORT",false);




Debug::d_echo("acces ", 2,"updateDba.php");



if(_ADMEN_USE_ADVERT_LINES){
    include(_AWA_PATHWAY."adawa/import-export/updateDba-advert_lines.php");
}
include(_AWA_PATHWAY."adawa/import-export/updateDba-annonces.php");
include(_AWA_PATHWAY."adawa/import-export/updateDba-missions.php");
include(_AWA_PATHWAY."adawa/import-export/updateDba-societes.php");
//
include(_AWA_PATHWAY."adawa/import-export/updateDba-personnes.php");
include(_AWA_PATHWAY."adawa/import-export/updateDba-experiences.php");
include(_AWA_PATHWAY."adawa/import-export/updateDba-formations.php");
include(_AWA_PATHWAY."adawa/import-export/updateDba-langues.php");
//
include(_AWA_PATHWAY."adawa/import-export/updateDba-treelist.php");







/*
 * Remplacement du print_r avec des balises <pre> pour une meilleur lecture des tableaux dans le navigateur
 */
function print_t($var){
    echo "<pre>\n";
	print_r($var);
    echo "</pre>\n";
}



/*
 * Génération du chaine de caractères aléatoire
 */
function randomstr($nbelement = 8){
    $characters = array(
        "A","B","C","D","E","F","G","H","J","K","L","M",
        "N","P","Q","R","S","T","U","V","W","X","Y","Z",
        "a","b","c","d","e","f","g","h","j","k","l","m",
        "n","p","q","r","s","t","u","v","w","x","y","z",
        "1","2","3","4","5","6","7","8","9",".",":");


    $random_chars = "";
    for($i=0;$i<=$nbelement;$i++){

        $x = mt_rand(0, count($characters)-1);
        $random_chars .= $characters[$x];

    }

    return $random_chars;

}
?>
