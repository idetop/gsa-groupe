<?php
########################################################
# CONVERT TIMESTAMP > DATE JJ/MM/AAAA
########################################################
function convert_date($date,$sep){
    $retour = getdate($date);
    $j = $retour["mday"];
    $m = $retour["mon"];
    $a = $retour["year"];
    $z = "0";
    $j2 = ($j < 10)?$z.$j:$j;
    $m2 = ($m < 10)?$z.$m:$m;
    $texte = $j2.$sep.$m2.$sep.$a;
    return $texte;
}

########################################################
# CONVERT DATE JJ/MM/AAAA > TIMESTAMP
########################################################
function convert_time($date,$sep){
    list($day,$month,$year) = explode($sep,$date);
    return mktime(0,0,0,$month,$day,$year);
}

########################################################
# CONVERT AGE JJ/MM/AAAA > XX ANS
########################################################
function age($date_naissance) {
    $arr1 = explode('/',$date_naissance);
    $arr2 = explode('/',date('d/m/Y'));
    if(($arr1[1] < $arr2[1]) || (($arr1[1] == $arr2[1]) && ($arr1[0] <= $arr2[0])))
    return $arr2[2] - $arr1[2];
    return $arr2[2] - $arr1[2] - 1;
}

########################################################
# DECOUPE TITRE OU TEXTE TROP LONG
########################################################
function cut($texte,$limit){
    $result = stripslashes($texte);
        if(strlen($result) > $limit) {
            $result = substr($result,0,$limit)." ...";
        }
        return $result;
}

########################################################
# COMPTEUR ENREGISTREMENT TABLE
########################################################
function return_count($table,$row,$filtre){
    if(! empty($row)) { $where = " WHERE ".$row." = '".$filtre."'"; }
    $sql = mysqli_query($connexion, "SELECT * FROM ".$table.$where);
    $res = mysqli_num_rows($sql);
    echo $res;
}

########################################################
# UPLOAD DE PHOTOS
########################################################
function upload_photo($photo,$tmp_photo,$dir,$ratio){
    // dossier
    $targetFile =  str_replace('//','/',$dir).$photo;
    $tempFile   = $tmp_photo;
    // resize
    $tableau    = getimagesize($tempFile);
    if($tableau[2] == 2 || $tableau[2] == 3 || $tableau[2] == 1) {
        move_uploaded_file($tempFile,$targetFile);
        chmod($targetFile, 0755);
        // IMAGE JPG, JPEG
        if ($tableau[2] == 2) {
            $src = imagecreatefromjpeg($dir.$photo);
            $im = imagecreatetruecolor($ratio, round(($ratio/$tableau[0])*$tableau[1]));
            imagecopyresampled($im, $src, 0, 0, 0, 0, $ratio, round($tableau[1]*($ratio/$tableau[0])), $tableau[0], $tableau[1]);
            imagejpeg($im,$dir.$photo,100);
            chmod($dir.$photo, 0755);
        }
        // IMAGE PNG
        elseif ($tableau[2] == 3) {
            $src = imagecreatefrompng($dir.$photo);
            $im = imagecreatetruecolor($ratio, round(($ratio/$tableau[0])*$tableau[1]));
            imagecopyresampled($im, $src, 0, 0, 0, 0, $ratio, round($tableau[1]*($ratio/$tableau[0])), $tableau[0], $tableau[1]);
            imagepng($im,$dir.$photo,9);
            chmod($dir.$photo, 0755);
        }
        // IMAGE GIF
        elseif ($tableau[2] == 1) {
            $src = imagecreatefromgif($dir.$photo);
            $im = imagecreatetruecolor($ratio, round(($ratio/$tableau[0])*$tableau[1]));
            imagecopyresampled($im, $src, 0, 0, 0, 0, $ratio, round($tableau[1]*($ratio/$tableau[0])), $tableau[0], $tableau[1]);
            imagegif($im,$dir.$photo,100);
            chmod($dir.$photo, 0755);
        }
    }
}
?>
