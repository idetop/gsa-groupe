/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function checkFileExtention(filename){
     var exp=new RegExp(".(doc|DOC|docx|DOCX|pdf|PDF|rtf|RTF|ppt|PPT)$","g");
     if(exp.test(filename)){
          return false;
     }
     return true;
}

function checkInteger($input){
     var exp=new RegExp("^[0-9]+$","g");
     if(exp.test($input)){
          return false;
     }
     return true;
}

function checkEmail(str) {
    var at="@"
    var dot="."
    var lat=str.indexOf(at)
    var lstr=str.length

    if (str.indexOf(at)==-1){
        return false;
    }

    if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
        return false;
    }

    if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
        return false;
    }

    if (str.indexOf(at,(lat+1))!=-1){
        return false;
    }

    if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
        return false;
    }

    if (str.indexOf(dot,(lat+2))==-1){
        return false;
    }

    if (str.indexOf(" ")!=-1){
        return false;
    }

    return true;
}



function resizeiframe(){
//   $("#iframeawaliste",top.document).height($("#iframeawaliste",top.document).contents().find("html").height());
    var heightDiv = parent.document.getElementById("iframeawaliste").contentWindow.document.body.scrollHeight+'px';
    jQuery("#iframeawaliste",top.document).css({height:heightDiv});
//    if($.browser.mozilla){
////            heightDiv = jQuery("#iframeawaliste",top.document).contents().attr("height")+"px";
//        heightDiv = parent.document.getElementById("iframeawaliste").contentWindow.document.body.scrollHeight+'px';
//        alert('mozi'+heightDiv);
//        jQuery("#iframeawaliste",top.document).css({height:heightDiv});
//
//    } else if($.browser.opera  || $.browser.safari || $.browser.msie)  {
//        heightDiv = parent.document.getElementById("iframeawaliste").contentWindow.document.body.scrollHeight+'px';
//        alert('ie'+heightDiv);
//    	jQuery("#iframeawaliste",top.document).css({height:heightDiv});
//    }
}
