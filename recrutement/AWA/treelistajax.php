<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');
include_once('include/Treelist-class.php');

Debug::d_echo("acces ", 2,"treelistajax.php");
Debug::d_print_r($_GET, 1,"GET","treelistajax.php");
Debug::d_print_r($_POST, 1,"POST","treelistajax.php");
Debug::d_print_r($_SESSION, 1,"SESSION","treelistajax.php");

//print_t($_GET);
//$_GET['treelistname'];
//$_GET['treelistismulti'];
//$_GET['treelistidhtml'];
//$_GET['treelistidinputhidden'];
//$_GET['treelistidinputdisplay'];

$treelistname = "";
$treelistidHtml = "";
$treelistidInputDisplay = "";
$treelistidInputHidden = "";
$treelistisMulti = false;
if(!empty($_GET['treelistname'])){
    $treelistname = $_GET['treelistname'];
}else{
    exit();
}
if(!empty($_GET['treelistidhtml'])){
    $treelistidHtml = $_GET['treelistidhtml'];
}else{
    exit();
}
if(!empty($_GET['treelistidinputdisplay'])){
    $treelistidInputDisplay = $_GET['treelistidinputdisplay'];
}else{
    exit();
}
if(!empty($_GET['treelistidinputhidden'])){
    $treelistidInputHidden = $_GET['treelistidinputhidden'];
}else{
    exit();
}
if(!empty($_GET['treelistismulti'])){
    $treelistisMulti = true;
}

$treelist = new Treelist($treelistname);

$treelist->setConn($conn);
$treelist->isMultiple = $treelistisMulti;
$treelist->idhtml = $treelistidHtml;
$treelist->idInputDisplay = $treelistidInputDisplay;
$treelist->idInputHidden = $treelistidInputHidden;
$treelist->buildChilds();
//
//print_t($treelist);
echo $treelist->renderHTML();
?>