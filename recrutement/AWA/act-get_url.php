<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors.php');
include_once('include/pdo.php');
include_once('include/framework.php');

if(!empty($_POST)){
    //page ref
    //langue
    //id offre si page offre

    if(!empty ($_POST['pageref']) && !empty ($_POST['langue'])){

        //page de liste
        if($_POST['pageref'] == "search"){
            echo _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_SEARCH_".$_POST['langue']);
            exit();
        }

        //page de liste
        if($_POST['pageref'] == "liste"){
            echo _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_POST['langue']);
            exit();
        }

        //page fiche offre
        if($_POST['pageref'] == "fiche"){
            if(!empty($_POST['id'])){
                $sql = "
                    SELECT
                        an.REFERENCE,
                        an.LIBELLE,
                        an.TEXTE_ANNONCE,
                        an.DATE_DEBUT,
                        an.DESCRCUSTOMER,
                        an.DESCRASSIGNMENT,
                        an.ID_ANNONCE,
                        mi.PAYS,
                        mi.REGION,
                        mi.ZIP,
                        mi.CITY,
                        mi.SALARY_FROM,
                        mi.SALARY_TO,
                        mi.TYPECONTRAT,
                        mi.ID_MISSION,
                        so.RAISON_SOCIALE,
                        mi.ANONYMOUS,
                        an.METADESCRIPTION,
                        an.METAKEYS

                    FROM
                        annonces AS an
                    INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION
                    INNER JOIN societes AS so ON so.ID_SOCIETE = mi.ID_SOCIETE

                    WHERE
                        an.ID_ANNONCE =:idannonce
                        AND an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
                        AND mi.ETAT = '1'
                        AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
                        AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )
                ";


                $sql45 = "
                    SELECT
                        al.REF2 AS REFERENCE,
                        an.LIBELLE,
                        an.TEXTE_ANNONCE,
                        an.DATE_DEBUT,
                        an.DESCRCUSTOMER,
                        an.DESCRASSIGNMENT,
                        an.ID_ANNONCE,
                        mi.PAYS,
                        mi.REGION,
                        mi.ZIP,
                        mi.CITY,
                        mi.SALARYMIN AS SALARY_FROM,
                        mi.SALARYMAX AS SALARY_TO,
                        mi.TYPECONTRAT,
                        mi.ID_MISSION,
                        so.RAISON_SOCIALE,
                        mi.ANONYMOUS,
                        an.METADESCRIPTION,
                        an.METAKEYS

                    FROM
                        tannonces AS an
                    INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION
                    INNER JOIN societes AS so ON so.ID_SOCIETE = mi.ID_SOCIETE
                    INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

                    WHERE
                        al.ID_ANNONCE =:idannonce
                        AND an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
                        AND mi.ETAT = '1'
                        AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
                        AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )
                ";
                if(_ADMEN_USE_ADVERT_LINES){
                    $sql = $sql45;
                }
                
                $select = $conn->prepare($sql);
                $select->bindParam(':idannonce', $_POST['id'], PDO::PARAM_INT);
                $select->execute();
                $annonce = null;
                $annonce = $select->fetchObject();
                if($annonce){
                    echo rewriteAnnonceUrl($annonce->ID_ANNONCE,$annonce->LIBELLE);
                    exit();
                }else{
                    exit();
                }
            }
        }

        //page candidature spontanee
        if($_POST['pageref'] == "candidaturespontanee"){
            echo _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_CANDIDATURE_SPONTANEE_".$_POST['langue']);
            exit();
        }

        //page espace candidat
        if($_POST['pageref'] == "espacecandidat"){
            echo _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_POST['langue']);
            exit();
        }

        //page mentions légales
        if($_POST['pageref'] == "mentionslegales"){
            echo _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_MENTIONS_LEGALES_".$_POST['langue']);
            exit();
        }

        //page candidaturefin
        if($_POST['pageref'] == "candidaturefin"){
            echo _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_CANDIDATURE_END_".$_POST['langue']);
            exit();
        }

        //page mdp oublie
        if($_POST['pageref'] == "mdpoublie"){
            echo _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_MDP_OUBLIE_".$_POST['langue']);
            exit();
        }


        //page candidature
        if($_POST['pageref'] == "candidature"){
            if(!empty($_POST['id'])){
                echo _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_CANDIDATURE_OFFRE_".$_POST['langue'])."?id=".$_POST['id'];
                exit();
            }else{
                exit();
            }
            
        }
    }
}
?>
