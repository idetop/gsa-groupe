<?php
include_once "../include/display_errors_bo.php";
@session_start();

Debug::d_echo("acces ", 2,"v-add-config.php");
Debug::d_print_r($_GET, 1,"GET","v-add-config.php");
Debug::d_print_r($_POST, 1,"POST","v-add-config.php");
Debug::d_print_r($_SESSION, 1,"SESSION","v-add-config.php");


include_once('../../include/config.php');
@include_once '../include/pdo.php';
include_once "../include/framework.php";
include_once "../include/tableau.php";

$starttime = microtime();

/*
 * Variables communes
 */
$errorMsg = "";
$returnMsg = "";
$formsubmit = false;        //true après enregistrement du formulaire
$element_id = null;         //null si add et non null si edit
$edit = false;              //true si edit, false si add

/*
 * Control d'accès
 */
if(empty($_SESSION['boe_user_id'])){
    $_SESSION['boe_user_id'] = null;
}
if(empty ($_SESSION['boe_user_email'])){
    $_SESSION['boe_user_email'] = null;
}
test_session_boe($_SESSION['boe_user_id'], $_SESSION['boe_user_email'], "../", $conn);


/*
 * Traitement des données d'entrées ($_GET)
 */
if(!empty($_GET['element_id'])){
    $element_id = $_GET['element_id'];
}

$config_id = "";
$config_RECRUTEUR_EMAIL_OFFRE = "";
$config_RECRUTEUR_EMAIL_SPONTANE = "";
$config_RECRUTEUR_EMAIL_OFFRE_SRC = "";
$config_RECRUTEUR_EMAIL_SPONTANE_SRC = "";
$config_RECRUTEUR_EMAIL_NAME_SPONTANE = "";
$config_RECRUTEUR_EMAIL_NAME_OFFRE = "";

$config_RECRUTEUR_EMAIL_MAJ = "";
$config_RECRUTEUR_EMAIL_MAJ_SRC = "";
$config_RECRUTEUR_EMAIL_MAJ_NAME_SRC = "";

$config_RECRUTEUR_EMAIL_RELANCE_SRC = "";
$config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC = "";
$config_RECRUTEUR_SMTP_HOST = "localhost";
$config_RECRUTEUR_SMTP_PORT = "25";
$config_RECRUTEUR_SMTP_AUTH = "0";
$config_RECRUTEUR_SMTP_USER = "";
$config_RECRUTEUR_SMTP_PASS = "";
$config_RECRUTEUR_SMTP_SECU = "";

//if(!empty($element_id)){
    $sql = "
        SELECT
            `fo`.*

        FROM
            `awa_configs` AS `fo`
            
        ORDER BY fo.id DESC
        LIMIT 0,1

        ";

    $select = $conn->prepare($sql);
   
    $select->execute();
    $configobj = null;
    $configobj = $select->fetchObject();
    if($configobj){
        
        $config_id = $configobj->ID;
        $config_RECRUTEUR_EMAIL_OFFRE = $configobj->RECRUTEUR_EMAIL_OFFRE;
        $config_RECRUTEUR_EMAIL_SPONTANE = $configobj->RECRUTEUR_EMAIL_SPONTANE;
        $config_RECRUTEUR_EMAIL_OFFRE_SRC = $configobj->RECRUTEUR_EMAIL_OFFRE_SRC;
        $config_RECRUTEUR_EMAIL_SPONTANE_SRC = $configobj->RECRUTEUR_EMAIL_SPONTANE_SRC;
        $config_RECRUTEUR_EMAIL_NAME_SPONTANE = $configobj->RECRUTEUR_EMAIL_NAME_SPONTANE;
        $config_RECRUTEUR_EMAIL_NAME_OFFRE = $configobj->RECRUTEUR_EMAIL_NAME_OFFRE;

        $config_RECRUTEUR_EMAIL_MAJ = $configobj->RECRUTEUR_EMAIL_MAJ;
        $config_RECRUTEUR_EMAIL_MAJ_SRC = $configobj->RECRUTEUR_EMAIL_MAJ_SRC;
        $config_RECRUTEUR_EMAIL_MAJ_NAME_SRC = $configobj->RECRUTEUR_EMAIL_MAJ_NAME_SRC;

        $config_RECRUTEUR_EMAIL_RELANCE_SRC = $configobj->RECRUTEUR_EMAIL_RELANCE_SRC;
        $config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC = $configobj->RECRUTEUR_EMAIL_RELANCE_NAME_SRC;
        $config_RECRUTEUR_SMTP_HOST = $configobj->RECRUTEUR_SMTP_HOST;
        $config_RECRUTEUR_SMTP_PORT = $configobj->RECRUTEUR_SMTP_PORT;
        $config_RECRUTEUR_SMTP_AUTH = $configobj->RECRUTEUR_SMTP_AUTH;
        $config_RECRUTEUR_SMTP_USER = $configobj->RECRUTEUR_SMTP_USER;
        $config_RECRUTEUR_SMTP_PASS = $configobj->RECRUTEUR_SMTP_PASS;
        $config_RECRUTEUR_SMTP_SECU = $configobj->RECRUTEUR_SMTP_SECU;
    }
//}



if(count($_POST)){
//   print_t($_POST);
   if(!empty($_POST['id'])){
       //Update
       

        $sql = "
            UPDATE
                awa_configs
            SET
                RECRUTEUR_EMAIL_OFFRE =:RECRUTEUR_EMAIL_OFFRE,
                RECRUTEUR_EMAIL_SPONTANE =:RECRUTEUR_EMAIL_SPONTANE,
                RECRUTEUR_EMAIL_OFFRE_SRC =:RECRUTEUR_EMAIL_OFFRE_SRC,
                RECRUTEUR_EMAIL_SPONTANE_SRC =:RECRUTEUR_EMAIL_SPONTANE_SRC,
                RECRUTEUR_EMAIL_NAME_SPONTANE =:RECRUTEUR_EMAIL_NAME_SPONTANE,
                RECRUTEUR_EMAIL_NAME_OFFRE =:RECRUTEUR_EMAIL_NAME_OFFRE,

                RECRUTEUR_EMAIL_MAJ =:RECRUTEUR_EMAIL_MAJ,
                RECRUTEUR_EMAIL_MAJ_SRC =:RECRUTEUR_EMAIL_MAJ_SRC,
                RECRUTEUR_EMAIL_MAJ_NAME_SRC =:RECRUTEUR_EMAIL_MAJ_NAME_SRC,

                RECRUTEUR_EMAIL_RELANCE_SRC =:RECRUTEUR_EMAIL_RELANCE_SRC,
                RECRUTEUR_EMAIL_RELANCE_NAME_SRC =:RECRUTEUR_EMAIL_RELANCE_NAME_SRC,
                RECRUTEUR_SMTP_HOST =:RECRUTEUR_SMTP_HOST,
                RECRUTEUR_SMTP_PORT =:RECRUTEUR_SMTP_PORT,
                RECRUTEUR_SMTP_AUTH =:RECRUTEUR_SMTP_AUTH,
                RECRUTEUR_SMTP_USER =:RECRUTEUR_SMTP_USER,
                RECRUTEUR_SMTP_PASS =:RECRUTEUR_SMTP_PASS,
                RECRUTEUR_SMTP_SECU =:RECRUTEUR_SMTP_SECU

            WHERE
                ID =:id

            ";

        $select = $conn->prepare($sql);
        $select->bindParam(':id', $_POST['id'], PDO::PARAM_INT);
        $select->bindParam(':RECRUTEUR_EMAIL_OFFRE', $_POST['config_RECRUTEUR_EMAIL_OFFRE'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_EMAIL_SPONTANE', $_POST['config_RECRUTEUR_EMAIL_SPONTANE'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_EMAIL_OFFRE_SRC', $_POST['config_RECRUTEUR_EMAIL_OFFRE_SRC'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_EMAIL_SPONTANE_SRC', $_POST['config_RECRUTEUR_EMAIL_SPONTANE_SRC'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_EMAIL_NAME_SPONTANE', $_POST['config_RECRUTEUR_EMAIL_NAME_SPONTANE'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_EMAIL_NAME_OFFRE', $_POST['config_RECRUTEUR_EMAIL_NAME_OFFRE'], PDO::PARAM_STR);

        $select->bindParam(':RECRUTEUR_EMAIL_MAJ', $_POST['config_RECRUTEUR_EMAIL_MAJ'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_EMAIL_MAJ_SRC', $_POST['config_RECRUTEUR_EMAIL_MAJ_SRC'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_EMAIL_MAJ_NAME_SRC', $_POST['config_RECRUTEUR_EMAIL_MAJ_NAME_SRC'], PDO::PARAM_STR);

        $select->bindParam(':RECRUTEUR_EMAIL_RELANCE_SRC', $_POST['config_RECRUTEUR_EMAIL_RELANCE_SRC'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_EMAIL_RELANCE_NAME_SRC', $_POST['config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_SMTP_HOST', $_POST['config_RECRUTEUR_SMTP_HOST'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_SMTP_PORT', $_POST['config_RECRUTEUR_SMTP_PORT'], PDO::PARAM_INT);
        $select->bindParam(':RECRUTEUR_SMTP_AUTH', $_POST['config_RECRUTEUR_SMTP_AUTH'], PDO::PARAM_INT);
        $select->bindParam(':RECRUTEUR_SMTP_USER', $_POST['config_RECRUTEUR_SMTP_USER'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_SMTP_PASS', $_POST['config_RECRUTEUR_SMTP_PASS'], PDO::PARAM_STR);
        $select->bindParam(':RECRUTEUR_SMTP_SECU', $_POST['config_RECRUTEUR_SMTP_SECU'], PDO::PARAM_STR);

        $select->execute();
        
        $returnMsg = _CONF_SYS_SAVED_MSG;

        Debug::d_echo("Sauvegarde ", 2,"v-config.php");
        echo "<script type='text/javascript' >";
        echo " $('#popupdiv_content_config').load('v-add-config.php?msg=".urlencode($returnMsg)."');";
        echo "</script>";
        exit();
   }else{
      
   }
}
?>
<script type="text/javascript" >
    function checkForm(){
        if($('#config_RECRUTEUR_EMAIL_OFFRE').val() == ""){
            alert("<?php echo _CONF_SYS_JS_REO_ERROR; ?>");
            return false;
        }
        if($('#config_RECRUTEUR_EMAIL_OFFRE_SRC').val() == ""){
            alert("<?php echo _CONF_SYS_JS_REOS_ERROR; ?>");
            return false;
        }
        if($('#config_RECRUTEUR_EMAIL_SPONTANE').val() == ""){
            alert("<?php echo _CONF_SYS_JS_RES_ERROR; ?>");
            return false;
        }
        if($('#config_RECRUTEUR_EMAIL_SPONTANE_SRC').val() == ""){
            alert("<?php echo _CONF_SYS_JS_RESS_ERROR; ?>");
            return false;
        }


        if($('#config_RECRUTEUR_EMAIL_MAJ').val() == ""){
            alert("<?php echo _CONF_SYS_JS_REM_ERROR; ?>");
            return false;
        }
        if($('#config_RECRUTEUR_EMAIL_MAJ_SRC').val() == ""){
            alert("<?php echo _CONF_SYS_JS_REMS_ERROR; ?>");
            return false;
        }


        if($('#config_RECRUTEUR_EMAIL_RELANCE_SRC').val() == ""){
            alert("<?php echo _CONF_SYS_JS_RERS_ERROR; ?>");
            return false;
        }
        if($('#config_RECRUTEUR_SMTP_HOST').val() == ""){
            alert("<?php echo _CONF_SYS_JS_RSH_ERROR; ?>");
            return false;
        }
        if($('#config_RECRUTEUR_SMTP_PORT').val() == ""){
            alert("<?php echo _CONF_SYS_JS_RSHP_ERROR; ?>");
            return false;
        }


        submitFormAjax();
    }
    
    function submitFormAjax(){
        $.ajax({
            "type": "POST",
            "url": "v-add-config.php",
            "cache": false,
            "data": $('#form_entreprise').serialize(),
            "success": function(msg){
                $('#popupdiv_content_config').html(msg);
            }
        });
    }


    

</script>
<?php
if(isset($_GET['msg']) && $_GET['msg'] != "") {
    $message = urldecode($_GET['msg']);
    $strhead = substr($message, 0,3);
    if($strhead == "-e-"){
        $strmessage = substr($message, 3);
        $strmessageClass = "msg_error";
    }else{
        $strmessage = $message;
        $strmessageClass = "msg_ok";
    }
    echo "<div id=\"message_profil\" class=\"".$strmessageClass."\">".$strmessage."</div>";
}

?>
<div style="height: 20px;"></div>
<form enctype="multipart/form-data" onsubmit="checkForm();return false;" action="" method="post" id="form_entreprise">
    <fieldset>
        <input type="hidden" value="<?php echo $config_id; ?>" id="id" name="id">
         
        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_OFFRE" class="label_formconfig" ><?php echo _CONF_SYS_FORM_REO_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_OFFRE; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_OFFRE" name="config_RECRUTEUR_EMAIL_OFFRE">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_OFFRE_SRC" class="label_formconfig" ><?php echo _CONF_SYS_FORM_REOS_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_OFFRE_SRC; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_OFFRE_SRC" name="config_RECRUTEUR_EMAIL_OFFRE_SRC">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_NAME_OFFRE" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RENO_LABEL; ?></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_NAME_OFFRE; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_NAME_OFFRE" name="config_RECRUTEUR_EMAIL_NAME_OFFRE">
            <div class="spacer"></div>
        </div>
        <br/>
        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_SPONTANE" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RES_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_SPONTANE; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_SPONTANE" name="config_RECRUTEUR_EMAIL_SPONTANE">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_SPONTANE_SRC" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RESS_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_SPONTANE_SRC; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_SPONTANE_SRC" name="config_RECRUTEUR_EMAIL_SPONTANE_SRC">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_NAME_SPONTANE" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RENS_LABEL; ?> </label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_NAME_SPONTANE; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_NAME_SPONTANE" name="config_RECRUTEUR_EMAIL_NAME_SPONTANE">
            <div class="spacer"></div>
        </div>
        <br/>

        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_MAJ" class="label_formconfig" ><?php echo _CONF_SYS_FORM_REM_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_MAJ; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_MAJ" name="config_RECRUTEUR_EMAIL_MAJ">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_MAJ_SRC" class="label_formconfig" ><?php echo _CONF_SYS_FORM_REMS_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_MAJ_SRC; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_MAJ_SRC" name="config_RECRUTEUR_EMAIL_MAJ_SRC">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_MAJ_NAME_SRC" class="label_formconfig" ><?php echo _CONF_SYS_FORM_REMNS_LABEL; ?> </label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_MAJ_NAME_SRC; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_MAJ_NAME_SRC" name="config_RECRUTEUR_EMAIL_MAJ_NAME_SRC">
            <div class="spacer"></div>
        </div>
        <br/>







        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_RELANCE_SRC" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RERS_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_RELANCE_SRC; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_RELANCE_SRC" name="config_RECRUTEUR_EMAIL_RELANCE_SRC">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RERNS_LABEL; ?> </label>
            <input type="text" value="<?php echo $config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC" name="config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC">
            <div class="spacer"></div>
        </div>



        <div class="formrow">
            <label for="config_RECRUTEUR_SMTP_HOST" class="label_formconfig" ><?php echo _CONF_SYS_FORM_REH_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_SMTP_HOST; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_SMTP_HOST" name="config_RECRUTEUR_SMTP_HOST">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_SMTP_PORT" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RESP_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_SMTP_PORT; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_SMTP_PORT" name="config_RECRUTEUR_SMTP_PORT">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_SMTP_AUTH" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RSA_LABEL; ?></label>
            <select name="config_RECRUTEUR_SMTP_AUTH" id="config_RECRUTEUR_SMTP_AUTH" class="fs12 input_formconfig">
                <option value="0" <?php if($config_RECRUTEUR_SMTP_AUTH=="0"){echo "selected=\"selected\"";} ?>><?php echo _CONF_SYS_FORM_RSA_VAL_0; ?></option>
                <option value="1" <?php if($config_RECRUTEUR_SMTP_AUTH=="1"){echo "selected=\"selected\"";} ?>><?php echo _CONF_SYS_FORM_RSA_VAL_1; ?></option>
            </select>
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_SMTP_USER" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RSU_LABEL; ?></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_SMTP_USER; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_SMTP_USER" name="config_RECRUTEUR_SMTP_USER">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_SMTP_PASS" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RSP_LABEL; ?></label>
            <input type="text" value="<?php echo $config_RECRUTEUR_SMTP_PASS; ?>" style="" class="fs12 input_formconfig" id="config_RECRUTEUR_SMTP_PASS" name="config_RECRUTEUR_SMTP_PASS">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="config_RECRUTEUR_SMTP_SECU" class="label_formconfig" ><?php echo _CONF_SYS_FORM_RSS_LABEL; ?></label>
            <select name="config_RECRUTEUR_SMTP_SECU" id="config_RECRUTEUR_SMTP_SECU" class="fs12 input_formconfig">
                <option value="" <?php if($config_RECRUTEUR_SMTP_SECU==""){echo "selected=\"selected\"";} ?>><?php echo _CONF_SYS_FORM_RSS_VAL_DEFAULT; ?></option>
                <option value="ssl" <?php if($config_RECRUTEUR_SMTP_SECU=="ssl"){echo "selected=\"selected\"";} ?>><?php echo _CONF_SYS_FORM_RSS_VAL_SSL; ?></option>
                <option value="tls" <?php if($config_RECRUTEUR_SMTP_SECU=="tls"){echo "selected=\"selected\"";} ?>><?php echo _CONF_SYS_FORM_RSS_VAL_TLS; ?></option>
            </select>
            <div class="spacer"></div>
        </div>
        <div style="height: 20px;"></div>
        <div style="height: 20px;"></div>
        <div class="center">
            <input type="submit" name="sbm" id="sbm" value="<?php echo _CONF_SYS_FORM_SUBMIT_BTN; ?>"/>
        </div>
    </fieldset>
</form>