<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');

Debug::d_echo("acces id=".$_GET['annonce'], 2,"fiche.php");
Debug::d_print_r($_GET, 1,"GET","fiche.php");
Debug::d_print_r($_POST, 1,"POST","fiche.php");
Debug::d_print_r($_SESSION, 1,"SESSION","fiche.php");


$fonctionDbaFieldName= "FONCTION";
$secteurDbaFieldName= "SECTEUR";

if(_IS_USE_FONCTION_WEB){
    $fonctionDbaFieldName= "FONCTIONWEB";
}
if(_IS_USE_SECTEUR_WEB){
    $secteurDbaFieldName= "SECTEURWEB";
}


$id_annonce = "";
if(!empty($_GET['annonce'])){
    $id_annonce = $_GET['annonce'];
}

$source = "";
if(!empty($_GET['source'])){
    $source = $_GET['source'];
}

$annonce = null;
if(!empty ($id_annonce)){
    $sql = "
        SELECT
            an.REFERENCE,
            an.LIBELLE,
            an.TEXTE_ANNONCE,
            an.DATE_DEBUT,
            an.DESCRCUSTOMER,
            an.DESCRASSIGNMENT,
            an.ID_ANNONCE,
            mi.PAYS,
            mi.REGION,
            mi.ZIP,
            mi.CITY,
            mi.SALARY_FROM,
            mi.SALARY_TO,
            mi.TYPECONTRAT,
            mi.ID_MISSION,
            so.RAISON_SOCIALE,
            mi.ANONYMOUS,
            an.METADESCRIPTION,
            an.METAKEYS,
            mi.`".$fonctionDbaFieldName."` AS `FONCTION`,
            mi.`".$secteurDbaFieldName."` AS `SECTEUR`

        FROM
            annonces AS an
        INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION
        INNER JOIN societes AS so ON so.ID_SOCIETE = mi.ID_SOCIETE

        WHERE
            an.ID_ANNONCE =:idannonce
            AND an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )
    ";


    $sql45 = "
        SELECT
            al.REF2 AS REFERENCE,
            an.LIBELLE,
            an.TEXTE_ANNONCE,
            al.DATE_BEGIN AS DATE_DEBUT,
            an.DESCRCUSTOMER,
            an.DESCRASSIGNMENT,
            an.ID_ANNONCE,
            mi.PAYS,
            mi.REGION,
            mi.ZIP,
            mi.CITY,
            mi.SALARYMIN AS SALARY_FROM,
            mi.SALARYMAX AS SALARY_TO,
            mi.TYPECONTRAT,
            mi.ID_MISSION,
            so.RAISON_SOCIALE,
            mi.ANONYMOUS,
            an.METADESCRIPTION,
            an.METAKEYS,
            mi.`".$fonctionDbaFieldName."` AS `FONCTION`,
            mi.`".$secteurDbaFieldName."` AS `SECTEUR`

        FROM
            tannonces AS an
        INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION
        INNER JOIN societes AS so ON so.ID_SOCIETE = mi.ID_SOCIETE
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_ANNONCE =:idannonce
            AND al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )
    ";

    if(_ADMEN_USE_ADVERT_LINES){
        $sql = $sql45;
    }

    $select = $conn->prepare($sql);
    $select->bindParam(':idannonce', $id_annonce, PDO::PARAM_INT);
    $select->execute();
//    Debug::d_sql_error($select, 0,"sql get annonce","fiche.php",__LINE__);
    $annonce = null;
    $annonce = $select->fetchObject();
    if(_SHOW_SALARY==false){
        $annonce->SALARY_FROM=null;
        $annonce->SALARY_TO=null;
    }
    
    if($annonce){
//        print_t($annonce);
    }else{
        Debug::d_echo("annonce non trouvée (bad ref, unactive...) return 404", 2,"fiche.php");
        header('HTTP/1.0 404 Not Found');
        exit();
    }

    $islocalisationzip = false;
    $localisationmap = null;
    if(!empty($annonce->ZIP)){
        $sql = "
            SELECT
                vi.*

            FROM
                code_postaux AS vi

            WHERE
                vi.CP_CODE LIKE :ville
        ";
        $select = $conn->prepare($sql);
        $select->bindParam(':ville', $annonce->ZIP, PDO::PARAM_STR);
//        echo $annonce->CITY;
        $select->execute();
//        Debug::d_sql_error($select, 0,"sql get cp","fiche.php",__LINE__);
        $localisationmap = null;
        $localisationmap = $select->fetchObject();
        if($localisationmap){
            $islocalisationzip = true;
            Debug::d_echo("geolocalisation activée", 2,"fiche.php");
        }
//        print_t($localisationmap);
    }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title><?php echo $annonce->LIBELLE; ?> - <?php echo $annonce->REFERENCE; ?></title>
        <meta name="description" content="<?php if(empty ($annonce->METADESCRIPTION)){echo remove2slash($annonce->TEXTE_ANNONCE);}else{echo remove2slash($annonce->METADESCRIPTION);}?>"/>
        <meta name="keywords" content="<?php if(!empty ($annonce->METAKEYS)){ echo remove2slash($annonce->METAKEYS);} ?>"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <?php  if(!_USE_CUSTOM_CSS){ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?>getcss_custom.php" rel="stylesheet" type="text/css" />
        <?php }else{ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?><?php echo _TEMPLATE_FOLDER_NAME; ?>/custom.css" rel="stylesheet" type="text/css" />
        <?php } ?>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/framework.js"></script>
        <script type="text/javascript" >
            $.ajaxSettings.cache = false;
            $(document).ready(function(){
                <?php if(_IS_IFRAME_RESIZE){ ?>
                resizeiframe();
                <?php } ?>
            });
            
            function changeLanguage(language){
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-change_language.php",{ 'language': language});
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-get_url.php",
                    { 'pageref':'fiche', 'langue':language, 'id':<?php echo $annonce->ID_ANNONCE; ?> },
                    function(data){
                        if(data != ''){
                            window.location=data;
                        }else{
                            window.location='<?php echo _CONFIG_ROOTFOLDER; ?>';
                        }
                        
                    }
                );
            }
        </script>
    </head>
    <body>
        <?php
        $templateDatas = array();

        $templateDatas['pathway'] = "";
        $pathway = array();
        $pathway[0]['text'] = _OFFRE_LISTE_TITLE;
        $pathway[0]['url'] = _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language']);
        $pathway[1]['text'] = $annonce->LIBELLE." - ".$annonce->REFERENCE;
        $pathway[1]['url'] = null;

        $templateDatas['pathway'] = displayPathway($pathway);

        $templateDatas['header'] = "";
        $templateDatas['header'] = displayHeader2();

        $templateDatas['candidatArea'] = "";
        $templateDatas['candidatArea'] = displayCandidatArea($conn);

        $templateDatas['menu'] = "";
        $templateDatas['menu'] = displayMenu2();


        $templateDatas['localisationLink'] = "";
        if($islocalisationzip){
            $resizeScript = "";
            if(_IS_GOOGLEMAP_POPUP){
                $templateDatas['localisationLink'] = " <img src=\""._CONFIG_ROOTFOLDER."images/viewmag.png\" alt=\""._OFFRE_FICHE_LOCALISER_ALT."\" title=\""._OFFRE_FICHE_LOCALISER_ALT."\" onclick=\"openlocalisation_popup();\" />";
            }else{
                if(_IS_IFRAME_RESIZE){
                    $resizeScript = "resizeiframe();";
                }
                $templateDatas['localisationLink'] = " <img src=\""._CONFIG_ROOTFOLDER."images/viewmag.png\" alt=\""._OFFRE_FICHE_LOCALISER_ALT."\" title=\""._OFFRE_FICHE_LOCALISER_ALT."\" onclick=\"initialize();$('#map_canvas').show();".$resizeScript."\" />";
            }
            
        }

        $templateDatas['localisationJsscript'] = "";
        $templateDatas['localisationJsscriptPopup'] = "";
        if($islocalisationzip){

            if(_IS_GOOGLEMAP_POPUP){
                $templateDatas['localisationJsscriptPopup'] .= "<script type=\"text/javascript\">";
                $templateDatas['localisationJsscriptPopup'] .= "    var isInit = false;";
                $templateDatas['localisationJsscriptPopup'] .= "    function openlocalisation_popup(){";
                $templateDatas['localisationJsscriptPopup'] .= "        if(!isInit){";
                $templateDatas['localisationJsscriptPopup'] .= "            initialize();";
                $templateDatas['localisationJsscriptPopup'] .= "        }";
                $templateDatas['localisationJsscriptPopup'] .= "        isInit = true;";
                $templateDatas['localisationJsscriptPopup'] .= "        doc_height = $(document).height();";
                $templateDatas['localisationJsscriptPopup'] .= "        doc_width = $(document).width();";
                $templateDatas['localisationJsscriptPopup'] .= "        win_height = $(window).height();";
                $templateDatas['localisationJsscriptPopup'] .= "        win_width = $(window).width();";
                $templateDatas['localisationJsscriptPopup'] .= "        win_scrolltop = $(window).scrollTop();";
                $templateDatas['localisationJsscriptPopup'] .= "        popup_width = "._GOOGLEMAP_POPUP_WIDTH.";";
                $templateDatas['localisationJsscriptPopup'] .= "        popup_left = (win_width - popup_width)/2;";
                if(_IS_GOOGLEMAP_POPUP_BACKGROUND){
                    $templateDatas['localisationJsscriptPopup'] .= "        $('#bgpopup_localisation').css('top',0+'px');";
                    $templateDatas['localisationJsscriptPopup'] .= "        $('#bgpopup_localisation').css('height',doc_height+'px');";
                    $templateDatas['localisationJsscriptPopup'] .= "        $('#bgpopup_localisation').css('width',doc_width+'px');";
                    $templateDatas['localisationJsscriptPopup'] .= "        $('#bgpopup_localisation').fadeIn('fast');";
                }
                $templateDatas['localisationJsscriptPopup'] .= "        $('#popup_localisation').css('width',popup_width+'px');";
                $templateDatas['localisationJsscriptPopup'] .= "        $('#popup_localisation').css('top',win_scrolltop+50+'px');";
                $templateDatas['localisationJsscriptPopup'] .= "        $('#popup_localisation').css('left',popup_left+'px');";
                $templateDatas['localisationJsscriptPopup'] .= "        $('#popup_localisation').fadeIn('fast');";
                $templateDatas['localisationJsscriptPopup'] .= "    }";
                $templateDatas['localisationJsscriptPopup'] .= "    function closelocalisation_popup(){";
                $templateDatas['localisationJsscriptPopup'] .= "        $('#popup_localisation').fadeOut('fast');";
                if(_IS_GOOGLEMAP_POPUP_BACKGROUND){
                    $templateDatas['localisationJsscriptPopup'] .= "        $('#bgpopup_localisation').fadeOut('fast');";
                }
                $templateDatas['localisationJsscriptPopup'] .= "    }";
                $templateDatas['localisationJsscriptPopup'] .= "</script>";
                     
                $templateDatas['localisationJsscriptPopup'] .= "<script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=false\"></script>";
                $templateDatas['localisationJsscriptPopup'] .= "<script type=\"text/javascript\">";
                $templateDatas['localisationJsscriptPopup'] .= "    function initialize() {";
                $templateDatas['localisationJsscriptPopup'] .= "        var latlng = new google.maps.LatLng( ".$localisationmap->LAT.",".$localisationmap->LON.");";
                $templateDatas['localisationJsscriptPopup'] .= "        var myOptions = {";
                $templateDatas['localisationJsscriptPopup'] .= "          zoom: 4,";
                $templateDatas['localisationJsscriptPopup'] .= "          center: latlng,";
                $templateDatas['localisationJsscriptPopup'] .= "          mapTypeId: google.maps.MapTypeId.ROADMAP";

                $templateDatas['localisationJsscriptPopup'] .= "        };";
                $templateDatas['localisationJsscriptPopup'] .= "        var map = new google.maps.Map(document.getElementById(\"map_canvas\"), myOptions);";
                $templateDatas['localisationJsscriptPopup'] .= "        var marker = new google.maps.Marker({";
                $templateDatas['localisationJsscriptPopup'] .= "          position: new google.maps.LatLng( ".$localisationmap->LAT.",".$localisationmap->LON."),";
                $templateDatas['localisationJsscriptPopup'] .= "          title:\"\"";
                $templateDatas['localisationJsscriptPopup'] .= "        });";
                $templateDatas['localisationJsscriptPopup'] .= "        marker.setMap(map);";
                $templateDatas['localisationJsscriptPopup'] .= "    }";
                $templateDatas['localisationJsscriptPopup'] .= "</script>";
                $templateDatas['localisationJsscriptPopup'] .= "";

                $templateDatas['localisationJsscriptPopup'] .= "<div id=\"bgpopup_localisation\"></div>";
                $templateDatas['localisationJsscriptPopup'] .= "<div id=\"popup_localisation\">";
                $templateDatas['localisationJsscriptPopup'] .= " <div id=\"popup_localisation_header\">".$annonce->PAYS." ".$annonce->REGION." ".$annonce->CITY."<img onclick=\"closelocalisation_popup();\" style=\"cursor:pointer; position: absolute; top: 1px; right: 1px;\"  src=\""._CONFIG_ROOTFOLDER."images/img_close.png\" alt=\"\" title=\"\"/></div>";
                $templateDatas['localisationJsscriptPopup'] .= " <div id=\"map_canvas\" style=\"width:"._GOOGLEMAP_POPUP_WIDTH."px;\"></div>";
                $templateDatas['localisationJsscriptPopup'] .= "</div>";
                $templateDatas['localisationJsscriptPopup'] .= "";
                
            }else{
                $templateDatas['localisationJsscript'] .= "<script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=false\"></script>";
                $templateDatas['localisationJsscript'] .= "<script type=\"text/javascript\">";
                $templateDatas['localisationJsscript'] .= "    function initialize() {";
                $templateDatas['localisationJsscript'] .= "        var latlng = new google.maps.LatLng( ".$localisationmap->LAT.",".$localisationmap->LON.");";
                $templateDatas['localisationJsscript'] .= "        var myOptions = {";
                $templateDatas['localisationJsscript'] .= "          zoom: 4,";
                $templateDatas['localisationJsscript'] .= "          center: latlng,";
                $templateDatas['localisationJsscript'] .= "          mapTypeId: google.maps.MapTypeId.ROADMAP";

                $templateDatas['localisationJsscript'] .= "        };";
                $templateDatas['localisationJsscript'] .= "        var map = new google.maps.Map(document.getElementById(\"map_canvas\"), myOptions);";
                $templateDatas['localisationJsscript'] .= "        var marker = new google.maps.Marker({";
                $templateDatas['localisationJsscript'] .= "          position: new google.maps.LatLng( ".$localisationmap->LAT.",".$localisationmap->LON."),";
                $templateDatas['localisationJsscript'] .= "          title:\"\"";
                $templateDatas['localisationJsscript'] .= "        });";
                $templateDatas['localisationJsscript'] .= "        marker.setMap(map);";
                $templateDatas['localisationJsscript'] .= "    }";
                $templateDatas['localisationJsscript'] .= "</script>";
                $templateDatas['localisationJsscript'] .= "<div style=\"height: 30px;\"></div>";

                $templateDatas['localisationJsscript'] .= "<div id=\"map_canvas\" style=\"display:none;width:"._GOOGLEMAP_POPUP_WIDTH."px;margin:0 auto;\"></div>";
         
            }
            
        }

        $templateDatas['backButtonLink'] = "";
        $templateDatas['backButtonLink'] = _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language']);

        $templateDatas['postulButtonLink'] = "";
        $templateDatas['postulButtonLink'] = _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_CANDIDATURE_OFFRE_".$_SESSION['awa_language'])."?id=".$annonce->ID_ANNONCE."&source=".$source;


        //url search engine
        $templateDatas['urlToSearchEngine'] = "";
        $templateDatas['urlToSearchEngine'] = _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_SEARCH_".$_SESSION['awa_language'])  ;


        //Footer
        $templateDatas['footerText'] = "";
        $templateDatas['footerText'] = displayFooter();

        
        ?>
        <?php include(_TEMPLATE_FOLDER_NAME."/tpl_fiche.php"); ?>
    </body>
</html>
<?php
//print_t($annonce);
//print_t($localisationmap);
//print_t($_SESSION);
?>