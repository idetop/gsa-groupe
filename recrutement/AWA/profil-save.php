<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');
include_once("include/phpmailer/class.phpmailer.php");

Debug::d_echo("acces ", 2,"profil-save.php");
Debug::d_print_r($_GET, 1,"GET","profil-save.php");
Debug::d_print_r($_POST, 1,"POST","profil-save.php");
Debug::d_print_r($_SESSION, 1,"SESSION","profil-save.php");

$ID_PERSONNE_WEB = "";
$password = "";
if(!empty($_SESSION['awa_candidat_id']) && !empty($_SESSION['awa_candidat_login'])){
    $sql = "SELECT
                personnes.ID_PERSONNE_WEB,
                AES_DECRYPT(awa_candidats.PASSWORD,'admen') AS PASSWORD
            FROM
                awa_candidats
            INNER JOIN personnes ON personnes.ID_PERSONNE_WEB = awa_candidats.ID_PERSONNE_WEB

            WHERE
                awa_candidats.ID =:ID
                AND awa_candidats.LOGIN =:LOGIN
    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID', $_SESSION['awa_candidat_id'], PDO::PARAM_INT);
    $select->bindParam(':LOGIN', $_SESSION['awa_candidat_login'], PDO::PARAM_STR);
    $select->execute();
    $candidat = null;
    $candidat = $select->fetchObject();
    if($candidat){
        
        $ID_PERSONNE_WEB = $candidat->ID_PERSONNE_WEB;
        $password = $candidat->PASSWORD;
        Debug::d_echo("sauvegarde profil pour personne web ".$ID_PERSONNE_WEB, 2,"profil-save.php");
    }else{
        $_SESSION['awa_candidat_id'] = "";
        $_SESSION['awa_candidat_login'] = "";
        exit();
    }
}else{
    exit();
}


//print_t($_POST);
//print_t($_FILES);








//CANDIDAT = 1
//PER_ID_PERSONNE = 0
//$_POST['candi_EX_CIVILITY'];//CIVILITE
//$_POST['candi_EX_NAME'];//NOM
//$_POST['candi_EX_FIRST_NAME'];//PRENOM
//$_POST['candi_EX_ADDRESS'];//ADRESSE
//$_POST['candi_EX_ZIP'];//CODE_POSTAL
//$_POST['candi_EX_CITY'];//VILLE
//$_POST['candi_EX_PROVINCE'];//AREA
//$_POST['candi_EX_COUNTRY'];//PAYS
//$_POST['candi_EX_PHONE_PERSO'];//TEL_PERSO
//$_POST['candi_EX_PHONE_MOBILE'];//TEL_MOBILE
//$_POST['candi_EX_PHONE_PRO'];//TEL_DIRECT
//$_POST['candi_EX_FAX'];//FAX_PERSO
//$_POST['candi_EX_BIRTH_DATE'];//DATE_NAISSANCE
//$_POST['candi_EX_MARITAL_STATUS'];//SITU_FAMILLE
//$_POST['candi_EX_CHILDREN_COUNT'];//VAL_NOTE4
//$_POST['candi_EX_NATIONALITY'];//NATIONALITE
//$_POST['candi_EX_EMAIL_PRO'];//EMAIL
//$_POST['candi_EX_EMAIL_PERSO'];//EMAIL_PERSO
////
//$_POST['candi_SALARY_FIXE'];//SALAIRE_FIXE
//$_POST['candi_SALARY_VARIABLE'];//SALAIRE_VARIABLE
//$_POST['candi_SALARY_TOTAL'];//SALAIRE
//$_POST['candi_SALARY_ADVANTAGE'];//SALAIRE_AVANTAGE
//$_POST['candi_EX_AVAILABILITY'];//

//$_POST['candi_EX_MOBILITY'];//MOBILITE_TXT



/*
 * Controle données POST
 */



/*
 * Génération du contenu du mail au recruteur
 * Et récupération des données posts pour l'INSERT dans la DBA
 */

$recruteur_email_body = "";

//Civilite
$value = " ";
$personne_CIVILITE = "";
if(!empty($_POST['candi_EX_CIVILITY'])){
    $value = $_POST['candi_EX_CIVILITY'];
    $personne_CIVILITE = $value;
}

$recruteur_email_body .= "####EX_CIVILITY####=####".$value."####@@@@"."\n";

//Nom
$value = " ";
$personne_NOM = "";
if(!empty($_POST['candi_EX_NAME'])){
    $value = $_POST['candi_EX_NAME'];
    $personne_NOM = $value;
}
$recruteur_email_body .= "####EX_NAME####=####".$value."####@@@@"."\n";

//Prenom
$value = " ";
$personne_PRENOM = "";
if(!empty($_POST['candi_EX_FIRST_NAME'])){
    $value = $_POST['candi_EX_FIRST_NAME'];
    $personne_PRENOM  = $value;
}
$recruteur_email_body .= "####EX_FIRST_NAME####=####".$value."####@@@@"."\n";

//Adresse
$value = " ";
$personne_ADRESSE = "";
if(!empty($_POST['candi_EX_ADDRESS'])){
    $value = $_POST['candi_EX_ADDRESS'];
    $personne_ADRESSE = $value;
}
$recruteur_email_body .= "####EX_ADDRESS####=####".$value."####@@@@"."\n";

//Code postal
$value = " ";
$personne_CODE_POSTAL = "";
if(!empty($_POST['candi_EX_ZIP'])){
    $value = $_POST['candi_EX_ZIP'];
    $personne_CODE_POSTAL = $value;
}
$recruteur_email_body .= "####EX_ZIP####=####".$value."####@@@@"."\n";

//Ville
$value = " ";
$personne_VILLE = "";
if(!empty($_POST['candi_EX_CITY'])){
    $value = $_POST['candi_EX_CITY'];
    $personne_VILLE = $value;
}
$recruteur_email_body .= "####EX_CITY####=####".$value."####@@@@"."\n";

//Region
$value = " ";
$personne_AREA = "";
if(!empty($_POST['candi_EX_PROVINCE'])){
    $value = $_POST['candi_EX_PROVINCE'];
    $personne_AREA = $value;
}
$recruteur_email_body .= "####EX_PROVINCE####=####".$value."####@@@@"."\n";

//Pays
$value = " ";
$personne_PAYS = "";
if(!empty($_POST['candi_EX_COUNTRY'])){
    $value = $_POST['candi_EX_COUNTRY'];
    $personne_PAYS = $value;
}
$recruteur_email_body .= "####EX_COUNTRY####=####".$value."####@@@@"."\n";

//Phone perso
$value = " ";
$personne_TEL_PERSO = "";
if(!empty($_POST['candi_EX_PHONE_PERSO'])){
    $value = $_POST['candi_EX_PHONE_PERSO'];
    $personne_TEL_PERSO = $value;
}
$recruteur_email_body .= "####EX_PHONE_PERSO####=####".$value."####@@@@"."\n";

//Mobile
$value = " ";
$personne_TEL_MOBILE = "";
if(!empty($_POST['candi_EX_PHONE_MOBILE'])){
    $value = $_POST['candi_EX_PHONE_MOBILE'];
    $personne_TEL_MOBILE = $value;
}
$recruteur_email_body .= "####EX_PHONE_MOBILE####=####".$value."####@@@@"."\n";

//Phone pro
$value = " ";
$personne_TEL_DIRECT = "";
if(!empty($_POST['candi_EX_PHONE_PRO'])){
    $value = $_POST['candi_EX_PHONE_PRO'];
    $personne_TEL_DIRECT = $value;
}
$recruteur_email_body .= "####EX_PHONE_PRO####=####".$value."####@@@@"."\n";

//Fax
$value = " ";
$personne_FAX_PERSO = "";
if(!empty($_POST['candi_EX_FAX'])){
    $value = $_POST['candi_EX_FAX'];
    $personne_FAX_PERSO = $value;
}
$recruteur_email_body .= "####EX_FAX####=####".$value."####@@@@"."\n";

//Date de naissance
$value = "0000-00-00";
$personne_DATE_NAISSANCE = "0000-00-00";
if(!empty($_POST['candi_EX_BIRTH_DATE'])){
    $value = getEnglishSQLDate($_POST['candi_EX_BIRTH_DATE']);
    $personne_DATE_NAISSANCE = $value;
}
$recruteur_email_body .= "####EX_BIRTH_DATE####=####".$value."####@@@@"."\n";

//Situation familial
$value = " ";
$personne_SITU_FAMILLE = "";
if(!empty($_POST['candi_EX_MARITAL_STATUS'])){
    $value = $_POST['candi_EX_MARITAL_STATUS'];
    $personne_SITU_FAMILLE = $value;
}
$recruteur_email_body .= "####EX_MARITAL_STATUS####=####".$value."####@@@@"."\n";

//Nombre d'enfant
$value = " ";
$personne_VAL_NOTE4 = "";
if(!empty($_POST['candi_EX_CHILDREN_COUNT'])){
    $value = $_POST['candi_EX_CHILDREN_COUNT'];
    $personne_VAL_NOTE4 = $value;
}
$recruteur_email_body .= "####EX_CHILDREN_COUNT####=####".$value."####@@@@"."\n";

//Nationnalité
$value = " ";
$personne_NATIONALITE = "";
if(!empty($_POST['candi_EX_NATIONALITY'])){
    $value = $_POST['candi_EX_NATIONALITY'];
    $personne_NATIONALITE = $value;
}
$recruteur_email_body .= "####EX_NATIONALITY####=####".$value."####@@@@"."\n";

//Email pro
$value = " ";
$personne_EMAIL = "";
if(!empty($_POST['candi_EX_EMAIL_PRO'])){
    $value = $_POST['candi_EX_EMAIL_PRO'];
    $personne_EMAIL = $value;
}
$recruteur_email_body .= "####EX_EMAIL_PRO####=####".$value."####@@@@"."\n";


//Email perso
$value = " ";
$personne_EMAIL_PERSO = "";
if(!empty($_POST['candi_EX_EMAIL_PERSO'])){
    $value = $_POST['candi_EX_EMAIL_PERSO'];
    $personne_EMAIL_PERSO = $value;
}
$recruteur_email_body .= "####EX_EMAIL_PERSO####=####".$value."####@@@@"."\n";

//Salaire fixe
$value = " ";
$personne_SALAIRE_FIXE = "";
if(!empty($_POST['candi_SALARY_FIXE'])){
    $value = $_POST['candi_SALARY_FIXE'];
    $personne_SALAIRE_FIXE = $value;
}
$recruteur_email_body .= "####EX_SALARY_FIXE####=####".$value."####@@@@"."\n";


//Salaire variable
$value = " ";
$personne_SALAIRE_VARIABLE = "";
if(!empty($_POST['candi_SALARY_VARIABLE'])){
    $value = $_POST['candi_SALARY_VARIABLE'];
    $personne_SALAIRE_VARIABLE = $value;
}
$recruteur_email_body .= "####EX_SALARY_VARIABLE####=####".$value."####@@@@"."\n";

//Salaire total
$value = " ";
$personne_SALAIRE = "";
if(!empty($_POST['candi_SALARY_TOTAL'])){
    $value = $_POST['candi_SALARY_TOTAL'];
    $personne_SALAIRE = $value;
}
$recruteur_email_body .= "####EX_SALARY_TOTAL####=####".$value."####@@@@"."\n";

//Salaire avantage
$value = " ";
$personne_SALAIRE_AVANTAGE = "";
if(!empty($_POST['candi_SALARY_ADVANTAGE'])){
    $value = $_POST['candi_SALARY_ADVANTAGE'];
    $personne_SALAIRE_AVANTAGE = $value;
}
$recruteur_email_body .= "####EX_SALARY_ADVANTAGE####=####".$value."####@@@@"."\n";


//Sector
$value = " ";
$personne_SECTOR = "";
if(!empty($_POST['candi_EX_SECTOR'])){
    $value = $_POST['candi_EX_SECTOR'];
    $personne_SECTOR = $value;
}
$recruteur_email_body .= "####EX_SECTOR####=####".$value."####@@@@"."\n";

//Function
$value = " ";
$personne_FUNCTION = "";
if(!empty($_POST['candi_EX_FUNCTION'])){
    $value = $_POST['candi_EX_FUNCTION'];
    $personne_FUNCTION = $value;
}
$recruteur_email_body .= "####EX_FUNCTION####=####".$value."####@@@@"."\n";


//dispo
$value = " ";
$personne_CL1 = "";
if(!empty($_POST['candi_EX_AVAILABILITY'])){
    $value = $_POST['candi_EX_AVAILABILITY'];
    $personne_CL1 = $value;
}
$recruteur_email_body .= "####EX_AVAILABILITY####=####".$value."####@@@@"."\n";

//Mobilite
$value = " ";
$personne_MOBILITE_TXT = "";
if(!empty($_POST['candi_EX_MOBILITY'])){
    $value = $_POST['candi_EX_MOBILITY'];
    $personne_MOBILITE_TXT = $value;
}
$recruteur_email_body .= "####EX_MOBILITY####=####".$value."####@@@@"."\n";

//Comment
$value = " ";
$personne_COMMENT = "";
if(!empty($_POST['candi_EX_COMMENT'])){
    $value = $_POST['candi_EX_COMMENT'];
    $personne_COMMENT = $value;
}
$recruteur_email_body .= "####EX_COMMENT####=####".$value."####@@@@"."\n";



//custom
$sqlcustom = "SELECT * FROM awa_formulairepo_composition WHERE ID_FORMULAIRE_ELEMENT='40' ";
$select = $conn->prepare($sqlcustom);
$select->execute();
$customdatas = array();
while($row  = $select->fetchObject()){
    
    $customData = array();
    $custom_key = "";   
    $custom_dba = "";
    $exp_options = unserialize($row->OPTIONS);

    foreach($exp_options as $exp_option){
        if(preg_match('#custom_key=(.+)#', $exp_option, $match)){
            $custom_key = $match[1];
        }        
        if(preg_match('#custom_dba=(.+)#', $exp_option, $match)){
            $custom_dba = $match[1];
        }
    }

    $customData['custom_key'] = $custom_key;    
    $customData['custom_dba'] = $custom_dba;

    $customdatas[] = $customData;

}
//print_t($customdatas);
$value = " ";
$personne_CUSTOMS = array();
foreach($customdatas as $key=>$customData){
    $personne_CUSTOMS[$key]['value'] = "";
    if(!empty($_POST['candi_EX_CUSTOM_'.$customData['custom_key']])){
        $value = $_POST['candi_EX_CUSTOM_'.$customData['custom_key']];
        $personne_CUSTOMS[$key]['value'] = $value;
    }
    
    $personne_CUSTOMS[$key]['dba'] = $customData['custom_dba'];
    $recruteur_email_body .= "####EX_CUSTOM_".$customData['custom_key']."####=####".$value."####@@@@"."\n";
}


//print_t($personne_CUSTOM);
//print_t($recruteur_email_body);



//Experiences
$experiences = array();
if(isset($_POST['candi_EX_EXP_LASTINDEX'])){
    for($i = 0; $i <= $_POST['candi_EX_EXP_LASTINDEX'] ; $i++){
        $minimunData = false;

        //en cour
        $value = "0";
        if(!empty($_POST['candi_EX_ENPOSTE_'.$i])){
            $value = $_POST['candi_EX_ENPOSTE_'.$i];
            $minimunData = true;
        }
        $une_experience['EX_ENPOSTE'] = $value;
        
        //date debut
        $value = " ";
        if(!empty($_POST['candi_EX_EXP_START_'.$i])){
            $value = getEnglishSQLDate("00-".$_POST['candi_EX_EXP_START_'.$i]);
            $minimunData = true;
        }
        $une_experience['EX_EXP_START'] = $value;

        //date fin
        $value = " ";
        if(!empty($_POST['candi_EX_EXP_END_'.$i])){
            $value = getEnglishSQLDate("00-".$_POST['candi_EX_EXP_END_'.$i]);
            $minimunData = true;
        }
        $une_experience['EX_EXP_END'] = $value;

        //fonction
        $value = " ";
        if(!empty($_POST['candi_EX_EXPERIENCE_'.$i])){
            $value = $_POST['candi_EX_EXPERIENCE_'.$i];
            $minimunData = true;
        }
        $une_experience['EX_EXPERIENCE'] = $value;

        //entreprise
        $value = " ";
        if(!empty($_POST['candi_EX_EXP_COMPANY_'.$i])){
            $value = $_POST['candi_EX_EXP_COMPANY_'.$i];
            $minimunData = true;
        }
        $une_experience['EX_EXP_COMPANY'] = $value;

        if($minimunData){
            $experiences[] = $une_experience;
        }
    }
}
//print_t($experiences);

for($i = 1 ; $i <= 5 ; $i++){
        
    $value = " ";
    if(!empty($experiences[$i-1]) && !empty($experiences[$i-1]['EX_ENPOSTE'])){
        $value = $experiences[$i-1]['EX_ENPOSTE'];
    }
    $recruteur_email_body .= "####EX_EN_COURS_".$i."####=####".$value."####@@@@"."\n";

    $value = " ";
    if(!empty($experiences[$i-1]) && !empty($experiences[$i-1]['EX_EXP_START'])){
        $value = $experiences[$i-1]['EX_EXP_START'];
    }
    $recruteur_email_body .= "####EX_DATE_EXP_".$i."####=####".$value."####@@@@"."\n";

    $value = " ";
    if(!empty($experiences[$i-1]) && !empty($experiences[$i-1]['EX_EXP_END'])){
        $value = $experiences[$i-1]['EX_EXP_END'];
    }
    $recruteur_email_body .= "####EX_DATE_FIN_".$i."####=####".$value."####@@@@"."\n";

    $value = " ";
    if(!empty($experiences[$i-1]) && !empty($experiences[$i-1]['EX_EXPERIENCE'])){
        $value = $experiences[$i-1]['EX_EXPERIENCE'];
    }
    $recruteur_email_body .= "####EX_INTITULE_POSTE_".$i."####=####".$value."####@@@@"."\n";

    $value = " ";
    if(!empty($experiences[$i-1]) && !empty($experiences[$i-1]['EX_EXP_COMPANY'])){
        $value = $experiences[$i-1]['EX_EXP_COMPANY'];
    }
    $recruteur_email_body .= "####EX_RAISON_SOCIALE_".$i."####=####".$value."####@@@@"."\n";
    
}


//Formations
$formations = array();
if(isset($_POST['candi_FO_FORMA_LASTINDEX'])){
    for($i = 0; $i <= $_POST['candi_FO_FORMA_LASTINDEX'] ; $i++){
        $minimunData = false;
              
        //intitule
        $value = " ";
        if(!empty($_POST['candi_FO_INTITUL_'.$i])){
            $value = $_POST['candi_FO_INTITUL_'.$i];
            $minimunData = true;
        }
        $une_formation['FO_INTITUL'] = $value;

        //etablissement
        $value = " ";
        if(!empty($_POST['candi_FO_ETABL_'.$i])){
            $value = $_POST['candi_FO_ETABL_'.$i];
            $minimunData = true;
        }
        $une_formation['FO_ETABL'] = $value;

        //date debut
        $value = " ";
        if(!empty($_POST['candi_FO_FORMA_START_'.$i])){
            $value = getEnglishSQLDate("00-".$_POST['candi_FO_FORMA_START_'.$i]);
            $minimunData = true;
        }
        $une_formation['FO_FORMA_START'] = $value;

        //date fin
        $value = " ";
        if(!empty($_POST['candi_FO_FORMA_END_'.$i])){
            $value = getEnglishSQLDate("00-00-".$_POST['candi_FO_FORMA_END_'.$i]);
            $minimunData = true;
        }
        $une_formation['FO_FORMA_END'] = $value;

        //niveau
        $value = " ";
        if(!empty($_POST['candi_FO_NIVEAU_'.$i])){
            $value = $_POST['candi_FO_NIVEAU_'.$i];
            $minimunData = true;
        }
        $une_formation['FO_NIVEAU'] = $value;


        if($minimunData){
            $formations[] = $une_formation;
        }
    }
}

//print_t($formations);

for($i = 1 ; $i <= 5 ; $i++){

    $value = " ";
    if(!empty($formations[$i-1]) && !empty($formations[$i-1]['FO_INTITUL'])){
        $value = $formations[$i-1]['FO_INTITUL'];
    }
    $recruteur_email_body .= "####EX_DIPLOME_".$i."####=####".$value."####@@@@"."\n";

    $value = " ";
    if(!empty($formations[$i-1]) && !empty($formations[$i-1]['FO_ETABL'])){
        $value = $formations[$i-1]['FO_ETABL'];
    }
    $recruteur_email_body .= "####EX_INSTITUT_".$i."####=####".$value."####@@@@"."\n";

//    $value = " ";
//    if(!empty($formations[$i-1]) && !empty($formations[$i-1]['FO_FORMA_START'])){
//        $value = $formations[$i-1]['FO_FORMA_START'];
//    }
//    $recruteur_email_body .= "####EX_DATE_FORM_".$i."####=####".$value."####@@@@"."\n";

    $value = " ";
    if(!empty($formations[$i-1]) && !empty($formations[$i-1]['FO_FORMA_END'])){
        $value = $formations[$i-1]['FO_FORMA_END'];
    }
//    $recruteur_email_body .= "####EX_COMMENTAIRES_".$i."####=####".$value."####@@@@"."\n";
    $recruteur_email_body .= "####EX_DATE_FORM_".$i."####=####".$value."####@@@@"."\n";

    $value = " ";
    if(!empty($formations[$i-1]) && !empty($formations[$i-1]['FO_NIVEAU'])){
        $value = $formations[$i-1]['FO_NIVEAU'];
    }
    $recruteur_email_body .= "####EX_TYPOLOGIE_FORMATION_".$i."####=####".$value."####@@@@"."\n";
 
}


//Langues
$langues = array();
if(isset($_POST['candi_LG_LASTINDEX'])){
     for($i = 0; $i <= $_POST['candi_LG_LASTINDEX'] ; $i++){
        $minimunData = false;
       
        $une_langue['LG_LANGUE'] = "";
        $une_langue['LG_NIVEAU'] = "";

        //langue
        $value = " ";
        if(!empty($_POST['candi_LG_LANGUE_'.$i])){
            $value = $_POST['candi_LG_LANGUE_'.$i];
            $minimunData = true;
        }
        $une_langue['LG_LANGUE'] = $value;

        //langue
        $value = " ";
        if(!empty($_POST['candi_LG_NIVEAU_'.$i])){
            $value = $_POST['candi_LG_NIVEAU_'.$i];
            $minimunData = true;
        }
        $une_langue['LG_NIVEAU'] = $value;

        if($minimunData){
            $langues[] = $une_langue;
        }


     }
}
//print_t($langues);


for($i = 1 ; $i <= 5 ; $i++){

    $value = " ";
    if(!empty($langues[$i-1]) && !empty($langues[$i-1]['LG_LANGUE'])){
        $value = $langues[$i-1]['LG_LANGUE'];
    }
    $recruteur_email_body .= "####EX_LANGUE_LIB_".$i."####=####".$value."####@@@@"."\n";

    $value = " ";
    if(!empty($langues[$i-1]) && !empty($langues[$i-1]['LG_NIVEAU'])){
        $value = $langues[$i-1]['LG_NIVEAU'];
    }
    $recruteur_email_body .= "####EX_LEVEL_LIB_".$i."####=####".$value."####@@@@"."\n";
}



/*
 * Sauvegarde dans la table personnes
 */
if(_ADMEN_USE_ADVERT_LINES){
    $sqlsector = " SECTEUR=:SECTOR ";
}else{
    $sqlsector = " LIBRE1=:SECTOR ";
}
$sql_custom = "";
foreach($personne_CUSTOMS as $key=>$personne_custom){
    if(!empty($personne_custom['dba'])){
        $sql_custom .= " , ".$personne_custom['dba']."=:CUSTOM".$key." ";
    }
}

$sql_personnes = "
                    UPDATE 
                        personnes
                    SET
                        CIVILITE=:CIVILITE,
                        NOM=:NOM,
                        PRENOM=:PRENOM,
                        ADRESSE=:ADRESSE,
                        CODE_POSTAL=:CODE_POSTAL,
                        VILLE=:VILLE,
                        AREA=:AREA,
                        PAYS=:PAYS,
                        TEL_PERSO=:TEL_PERSO,
                        TEL_MOBILE=:TEL_MOBILE,
                        TEL_DIRECT=:TEL_DIRECT,
                        FAX_PERSO=:FAX_PERSO,
                        DATE_NAISSANCE=:DATE_NAISSANCE,
                        SITU_FAMILLE=:SITU_FAMILLE,
                        VAL_NOTE4=:VAL_NOTE4,
                        NATIONALITE=:NATIONALITE,
                        EMAIL=:EMAIL,
                        EMAIL_PERSO=:EMAIL_PERSO,
                        SALAIRE_FIXE=:SALAIRE_FIXE,
                        SALAIRE_VARIABLE=:SALAIRE_VARIABLE,
                        SALAIRE=:SALAIRE,
                        SALAIRE_AVANTAGE=:SALAIRE_AVANTAGE,
                        MOBILITE_TXT=:MOBILITE_TXT,
                        CL1=:CL1,
                        COMMENTAIRE=:COMMENTAIRE,
                        POSTE_OCCUPE=:POSTE_OCCUPE,
                        ".$sqlsector."
                        ".$sql_custom."

                    WHERE
                        ID_PERSONNE_WEB =:ID_PERSONNE_WEB

";

//print_t($sql_personnes);
$select = $conn->prepare($sql_personnes);
$select->bindParam(':CIVILITE', $personne_CIVILITE, PDO::PARAM_STR);
$select->bindParam(':NOM', $personne_NOM, PDO::PARAM_STR);
$select->bindParam(':PRENOM', $personne_PRENOM, PDO::PARAM_STR);
$select->bindParam(':ADRESSE', $personne_ADRESSE, PDO::PARAM_STR);
$select->bindParam(':CODE_POSTAL', $personne_CODE_POSTAL, PDO::PARAM_STR);
$select->bindParam(':VILLE', $personne_VILLE, PDO::PARAM_STR);
$select->bindParam(':AREA', $personne_AREA, PDO::PARAM_STR);
$select->bindParam(':PAYS', $personne_PAYS, PDO::PARAM_STR);
$select->bindParam(':TEL_PERSO', $personne_TEL_PERSO, PDO::PARAM_STR);
$select->bindParam(':TEL_MOBILE', $personne_TEL_MOBILE, PDO::PARAM_STR);
$select->bindParam(':TEL_DIRECT', $personne_TEL_DIRECT, PDO::PARAM_STR);
$select->bindParam(':FAX_PERSO', $personne_FAX_PERSO, PDO::PARAM_STR);
$select->bindParam(':DATE_NAISSANCE', $personne_DATE_NAISSANCE, PDO::PARAM_STR);
$select->bindParam(':SITU_FAMILLE', $personne_SITU_FAMILLE, PDO::PARAM_STR);
$select->bindParam(':VAL_NOTE4', $personne_VAL_NOTE4, PDO::PARAM_STR);
$select->bindParam(':NATIONALITE', $personne_NATIONALITE, PDO::PARAM_STR);
$select->bindParam(':EMAIL', $personne_EMAIL, PDO::PARAM_STR);
$select->bindParam(':EMAIL_PERSO', $personne_EMAIL_PERSO, PDO::PARAM_STR);
$select->bindParam(':SALAIRE_FIXE', $personne_SALAIRE_FIXE, PDO::PARAM_STR);
$select->bindParam(':SALAIRE_VARIABLE', $personne_SALAIRE_VARIABLE, PDO::PARAM_STR);
$select->bindParam(':SALAIRE', $personne_SALAIRE, PDO::PARAM_STR);
$select->bindParam(':SALAIRE_AVANTAGE', $personne_SALAIRE_AVANTAGE, PDO::PARAM_STR);
$select->bindParam(':MOBILITE_TXT', $personne_MOBILITE_TXT, PDO::PARAM_STR);
$select->bindParam(':CL1', $personne_CL1, PDO::PARAM_STR);
$select->bindParam(':COMMENTAIRE', $personne_COMMENT, PDO::PARAM_STR);
$select->bindParam(':POSTE_OCCUPE', $personne_FUNCTION, PDO::PARAM_STR);
$select->bindParam(':SECTOR', $personne_SECTOR, PDO::PARAM_STR);
foreach($personne_CUSTOMS as $key=>$personne_custom){
    if(!empty($personne_custom['dba'])){
        $select->bindParam(':CUSTOM'.$key, $personne_custom['value'], PDO::PARAM_STR);
    }
}

$select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_STR);

$select->execute();


$new_ID_PERSONNE_WEB = $ID_PERSONNE_WEB;

$recruteur_email_body .= "####ID_PERSONNE_WEB####=####".$new_ID_PERSONNE_WEB."####@@@@"."\n";

/*
 * Sauvegarde dans la table experiences_professionnelles
 */

//On supprime les anciennes expériences si il y en a
$sql = "
        DELETE FROM
            experiences_professionnelles
        WHERE
            ID_PERSONNE_WEB=:ID_PERSONNE_WEB
";
$select = $conn->prepare($sql);
$select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
$select->execute();

foreach($experiences as $key=>$experience){
    $sql = "
            INSERT INTO
                experiences_professionnelles
            SET
                ID_PERSONNE_WEB=:ID_PERSONNE_WEB,
                EN_COURS=:EN_COURS,
                DATE_EXP=:DATE_EXP,
                DATE_FIN=:DATE_FIN,
                INTITULE_POSTE=:INTITULE_POSTE,
                RAISON_SOCIALE=:RAISON_SOCIALE
    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->bindParam(':EN_COURS', $experience['EX_ENPOSTE'], PDO::PARAM_INT);
    $select->bindParam(':DATE_EXP', $experience['EX_EXP_START'], PDO::PARAM_STR);
    $select->bindParam(':DATE_FIN', $experience['EX_EXP_END'], PDO::PARAM_STR);
    $select->bindParam(':INTITULE_POSTE', $experience['EX_EXPERIENCE'], PDO::PARAM_STR);
    $select->bindParam(':RAISON_SOCIALE', $experience['EX_EXP_COMPANY'], PDO::PARAM_STR);
    $select->execute();
}


/*
 * Sauvegarde dans la table histo_formations
 */

//On supprime les anciennes formation si il y en a
$sql = "
        DELETE FROM
            histo_formations
        WHERE
            ID_PERSONNE_WEB=:ID_PERSONNE_WEB
";
$select = $conn->prepare($sql);
$select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
$select->execute();

foreach($formations as $key=>$formation){
    $sql = "
            INSERT INTO
                histo_formations
            SET
                ID_PERSONNE_WEB=:ID_PERSONNE_WEB,
                DATE_FORM=:DATE_FORM,
                INSTITUT=:INSTITUT,
                DIPLOME=:DIPLOME,
                TYPOLOGIE_FORMATION=:TYPOLOGIE_FORMATION

    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->bindParam(':DATE_FORM', $formation['FO_FORMA_END'], PDO::PARAM_STR);
//    $select->bindParam(':COMMENTAIRES', $formation['FO_FORMA_END'], PDO::PARAM_STR);
    $select->bindParam(':INSTITUT', $formation['FO_ETABL'], PDO::PARAM_STR);
    $select->bindParam(':DIPLOME', $formation['FO_INTITUL'], PDO::PARAM_STR);
    $select->bindParam(':TYPOLOGIE_FORMATION', $formation['FO_NIVEAU'], PDO::PARAM_STR);
    $select->execute();
}

/*
 * Sauvegarde dans la table parler_langues
 */

//On supprime les anciennes langues si il y en a
$sql = "
        DELETE FROM
            parler_langues
        WHERE
            ID_PERSONNE_WEB=:ID_PERSONNE_WEB
";
$select = $conn->prepare($sql);
$select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
$select->execute();

foreach($langues as $key=>$langue){
    $sql = "
            INSERT INTO
                parler_langues
            SET
                ID_PERSONNE_WEB=:ID_PERSONNE_WEB,
                LANGUE_LIB=:LANGUE_LIB,
                LEVEL_LIB=:LEVEL_LIB


    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->bindParam(':LANGUE_LIB', $langue['LG_LANGUE'], PDO::PARAM_STR);
    $select->bindParam(':LEVEL_LIB', $langue['LG_NIVEAU'], PDO::PARAM_STR);

    $select->execute();
}

/*
 * Sauvegarde dans la table awa_candidats
 */
$login = "";

if(!empty($personne_EMAIL) || !empty($personne_EMAIL_PERSO)){

    if(!empty($_POST['candi_EX_NEW_PASSWORD']) && !empty($_POST['candi_copy_EX_NEW_PASSWORD'])){
        if($_POST['candi_EX_NEW_PASSWORD'] == $_POST['candi_copy_EX_NEW_PASSWORD']){
            $password = $_POST['candi_EX_NEW_PASSWORD'];
        }
    }

    if(!empty($personne_EMAIL_PERSO)){
        $login = $personne_EMAIL_PERSO;
    }else{
        $login = $personne_EMAIL;
    }
    //$password = randomstr(8);
    $sql = "
        UPDATE
            awa_candidats
        SET
            LOGIN=:LOGIN,
            PASSWORD = AES_ENCRYPT(:PASSWORD,'admen'),
            ACTIVE = '1',
            LANGUE=:LANGUE

        WHERE
            ID_PERSONNE_WEB=:ID_PERSONNE_WEB
       ";
    $select = $conn->prepare($sql);
//    print_t($sql);
    $select->bindParam(':LOGIN', $login, PDO::PARAM_STR);
    $select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->bindParam(':LANGUE', $_SESSION['awa_language'], PDO::PARAM_STR);
    $select->bindParam(':PASSWORD', $password, PDO::PARAM_STR);
    
    $select->execute();
}

/*
 * Sauvegarde dans la table documents
 */

//sauvegarde CV
$cv_filename = "";
if(!empty($_FILES['candi_EX_CV']['name'])){
    Debug::d_echo("upload CV ".$_FILES['candi_EX_CV']['name'], 2,"profil-save.php");
    $sql = "SELECT ID_DOC, PATHNAME FROM documents WHERE ID_PERSONNE_WEB=:ID_PERSONNE_WEB AND TITRE_DOC = 'CV' ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    $document = null;
    $document = $select->fetchObject();
    if($document){
        
//        print_t($document);
        $sql = "DELETE FROM documents WHERE ID_DOC=:ID_DOC ";
        $select = $conn->prepare($sql);
        $select->bindParam(':ID_DOC', $document->ID_DOC, PDO::PARAM_INT);
        $select->execute();
        if(is_file($document->PATHNAME)){
            unlink($document->PATHNAME);
        }
    }




    $cv_filename = cleanFileName($_FILES['candi_EX_CV']['name']);

    $temp_filename = explode(".",$cv_filename);
    $temp_size = count($temp_filename);

    $temp_filename_txt = "";
    foreach($temp_filename as $key=>$element){
        if($key == ($temp_size - 1)){
            $temp_filename_txt .= "_cv.".$element;
        }else{
            if($key == 0){
                $temp_filename_txt .=  $element;
            }else{
                $temp_filename_txt .=  ".".$element;
            }
        }
    }
    $cv_filename = $temp_filename_txt;

    if(!is_dir("candidatfiles/".$new_ID_PERSONNE_WEB)){
        mkdir("candidatfiles/".$new_ID_PERSONNE_WEB);
    }

    move_uploaded_file($_FILES['candi_EX_CV']['tmp_name'], "candidatfiles/".$new_ID_PERSONNE_WEB."/".$cv_filename);

    $sql = "
        INSERT INTO
            documents
        SET
            ID_PERSONNE_WEB=:ID_PERSONNE_WEB,
            TITRE_DOC = 'CV',
            PATHNAME =:PATHNAME
       ";
    $select = $conn->prepare($sql);

    $select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
    $pathname = "candidatfiles/".$new_ID_PERSONNE_WEB."/".$cv_filename;
    $select->bindParam(':PATHNAME', $pathname, PDO::PARAM_STR);
    $select->execute();
}

//sauvegarde LM
$lm_filename = "";
if(!empty($_FILES['candi_EX_LM']['name'])){
    Debug::d_echo("upload LM ".$_FILES['candi_EX_LM']['name'], 2,"profil-save.php");
    $sql = "SELECT ID_DOC, PATHNAME FROM documents WHERE ID_PERSONNE_WEB=:ID_PERSONNE_WEB AND TITRE_DOC = 'LM' ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    $document = null;
    $document = $select->fetchObject();
    if($document){
        
//        print_t($document);
        $sql = "DELETE FROM documents WHERE ID_DOC=:ID_DOC ";
        $select = $conn->prepare($sql);
        $select->bindParam(':ID_DOC', $document->ID_DOC, PDO::PARAM_INT);
        $select->execute();
        if(is_file($document->PATHNAME)){
            unlink($document->PATHNAME);
        }
    }



    $lm_filename = cleanFileName($_FILES['candi_EX_LM']['name']);

    $temp_filename = explode(".",$lm_filename);
    $temp_size = count($temp_filename);

    $temp_filename_txt = "";
    foreach($temp_filename as $key=>$element){
        if($key == ($temp_size - 1)){
            $temp_filename_txt .= "_lm.".$element;
        }else{
            if($key == 0){
                $temp_filename_txt .=  $element;
            }else{
                $temp_filename_txt .=  ".".$element;
            }
        }
    }
    $lm_filename = $temp_filename_txt;

    if(!is_dir("candidatfiles/".$new_ID_PERSONNE_WEB)){
        mkdir("candidatfiles/".$new_ID_PERSONNE_WEB);
    }
    move_uploaded_file($_FILES['candi_EX_LM']['tmp_name'], "candidatfiles/".$new_ID_PERSONNE_WEB."/".$lm_filename);

    $sql = "
        INSERT INTO
            documents
        SET
            ID_PERSONNE_WEB=:ID_PERSONNE_WEB,
            TITRE_DOC = 'LM',
            PATHNAME =:PATHNAME
       ";
    $select = $conn->prepare($sql);

    $select->bindParam(':ID_PERSONNE_WEB', $new_ID_PERSONNE_WEB, PDO::PARAM_INT);
    $pathname = "candidatfiles/".$new_ID_PERSONNE_WEB."/".$lm_filename;
    $select->bindParam(':PATHNAME', $pathname, PDO::PARAM_STR);
    $select->execute();
}






/*
 * Récupération de la config d'envoi de mail en DBA
 */
$config_RECRUTEUR_EMAIL_MAJ = "";
$config_RECRUTEUR_EMAIL_MAJ_SRC = "";
$config_RECRUTEUR_EMAIL_MAJ_NAME_SRC = "";
$config_RECRUTEUR_SMTP_HOST = "localhost";
$config_RECRUTEUR_SMTP_PORT = "25";
$config_RECRUTEUR_SMTP_AUTH = "0";
$config_RECRUTEUR_SMTP_USER = "";
$config_RECRUTEUR_SMTP_PASS = "";
$config_RECRUTEUR_SMTP_SECU = "";
$sql = "
        SELECT
            `fo`.*

        FROM
            `awa_configs` AS `fo`

        ORDER BY fo.id DESC
        LIMIT 0,1

        ";

$select = $conn->prepare($sql);
$select->execute();
$configobj = null;
$configobj = $select->fetchObject();
if($configobj){
    
    $config_RECRUTEUR_EMAIL_MAJ = $configobj->RECRUTEUR_EMAIL_MAJ;
    $config_RECRUTEUR_EMAIL_MAJ_SRC = $configobj->RECRUTEUR_EMAIL_MAJ_SRC;
    $config_RECRUTEUR_EMAIL_MAJ_NAME_SRC = $configobj->RECRUTEUR_EMAIL_MAJ_NAME_SRC;
    $config_RECRUTEUR_SMTP_HOST = $configobj->RECRUTEUR_SMTP_HOST;
    $config_RECRUTEUR_SMTP_PORT = $configobj->RECRUTEUR_SMTP_PORT;
    $config_RECRUTEUR_SMTP_AUTH = $configobj->RECRUTEUR_SMTP_AUTH;
    $config_RECRUTEUR_SMTP_USER = $configobj->RECRUTEUR_SMTP_USER;
    $config_RECRUTEUR_SMTP_PASS = $configobj->RECRUTEUR_SMTP_PASS;
    $config_RECRUTEUR_SMTP_SECU = $configobj->RECRUTEUR_SMTP_SECU;
}else{
    exit();
}

/*
 * Configuration email recruteur pour maj profil
 */
$source_email = $config_RECRUTEUR_EMAIL_MAJ_SRC;
$source_name = $config_RECRUTEUR_EMAIL_MAJ_NAME_SRC;
$recruteur_email = $config_RECRUTEUR_EMAIL_MAJ;
if($config_RECRUTEUR_SMTP_SECU == "ssl"){
    $recruteur_SMTP_host = "ssl://".$config_RECRUTEUR_SMTP_HOST;
}elseif($config_RECRUTEUR_SMTP_SECU == "tls"){
    $recruteur_SMTP_host = "tls://".$config_RECRUTEUR_SMTP_HOST;
}else{
    $recruteur_SMTP_host = $config_RECRUTEUR_SMTP_HOST;
}
$recruteur_SMTP_port = $config_RECRUTEUR_SMTP_PORT ;
if($config_RECRUTEUR_SMTP_AUTH == "1"){
    $recruteur_SMTP_auth = true;
    $recruteur_SMTP_user = $config_RECRUTEUR_SMTP_USER;
    $recruteur_SMTP_pass = $config_RECRUTEUR_SMTP_PASS;
}else{
    $recruteur_SMTP_auth = false;
    $recruteur_SMTP_user = null;
    $recruteur_SMTP_pass = null;
}



/*
 * envoi mail rercuteur
 */
if(_SEND_EMAIL){
    $mail = new PHPmailer();
    $mail->IsSMTP();
    //$mail->SMTPDebug = 2;
    $mail->SMTPAuth = $recruteur_SMTP_auth;
    $mail->Host = $recruteur_SMTP_host;
    $mail->Port = (int)$recruteur_SMTP_port;
    if($recruteur_SMTP_auth){
        $mail->Username = $recruteur_SMTP_user;
        $mail->Password = $recruteur_SMTP_pass;
    }
    $mail->From = $source_email;
    $mail->FromName = $source_name;
    $mail->AddAddress($recruteur_email);
    $mail->AddReplyTo($source_email);

    if($cv_filename != ""){
        $mail->AddAttachment("candidatfiles/".$new_ID_PERSONNE_WEB."/".$cv_filename, $cv_filename);
    }
    if($lm_filename != ""){
        $mail->AddAttachment("candidatfiles/".$new_ID_PERSONNE_WEB."/".$lm_filename, $lm_filename);
    }


    $mail->Subject = utf8_decode(_EMAIL_OBJET_MAJ_CANDIDAT_PROFIL);
    $mail->Body = utf8_decode($recruteur_email_body);
    if(!$mail->Send()){ //Teste le return code de la fonction
      //echo $mail->ErrorInfo; //Affiche le message d'erreur (ATTENTION:voir section 7)
    }
    else{
    //  echo 'Mail envoyé avec succès';
        Debug::d_echo("envoi mail au recruteur ".$recruteur_email, 2,"profil-save.php");
    }
    $mail->SmtpClose();
unset($mail);
}






/*
 * envoi mail candidat si login existe
 */
if($login != "" && _SEND_CANDIDATURE_EMAIL_TO_CANDIDAT){
    /*
     * On récupère le mail  en fonction de la langue du candidat
     */
    $sql = "
        SELECT
            `awa_mailsmajpro`.*

        FROM
            `awa_mailsmajpro`
        WHERE
            awa_mailsmajpro.LANGUE = :LANGUE

        ORDER BY awa_mailsmajpro.ID DESC
        LIMIT 0,1

        ";

    $select = $conn->prepare($sql);
    $select->bindParam(':LANGUE', $_SESSION['awa_language'], PDO::PARAM_STR);
    $select->execute();

    $emailRelance = null;
    $emailRelance = $select->fetchObject();
    if($emailRelance){
        
    }else{
        /*
         * On récupère le mail de relance dans la langue par défault
         */
        $sql = "
            SELECT
                `awa_mailsmajpro`.*

            FROM
                `awa_mailsmajpro`
            WHERE
                awa_mailsmajpro.`DEFAULT` = '1'

            ORDER BY awa_mailscandidaturespontane.ID DESC
            LIMIT 0,1

            ";

        $select = $conn->prepare($sql);
        $select->execute();
        $emailRelance = null;
        $emailRelance = $select->fetchObject();
        if($emailRelance){
           
        }
    }

//    print_t($emailRelance);

     /*
     * On remplace les variables du mails
     */
    if(defined("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language'])){
        $url = "http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language']);
    }else{
        $url = "http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_"._CONFIG_DEFAULT_LANGUE_FO);
    }

    $mail_object = $emailRelance->OBJECT;
    $mail_object = str_replace("{firstname}",$personne_PRENOM, $mail_object);
    $mail_object = str_replace("{name}",$personne_NOM, $mail_object);
    $mail_object = str_replace("{login}",$login, $mail_object);
    $mail_object = str_replace("{password}",$password, $mail_object);
    $mail_object = str_replace("{url_login}",$url, $mail_object);
    $mail_object = str_replace("{day_date}",date('d-m-Y'), $mail_object);

    $mail_body = $emailRelance->BODY;
    $mail_body = str_replace("{firstname}",$personne_PRENOM, $mail_body);
    $mail_body = str_replace("{name}",$personne_NOM, $mail_body);
    $mail_body = str_replace("{login}",$login, $mail_body);
    $mail_body = str_replace("{password}",$password, $mail_body);
    $mail_body = str_replace("{url_login}",$url, $mail_body);
    $mail_body = str_replace("{day_date}",date('d-m-Y'), $mail_body);

    /*
     * envoi mail candidat
     */
    if(_SEND_EMAIL){
        $mail = new PHPmailer();
        $mail->IsSMTP();
        //$mail->SMTPDebug = 2;
        $mail->SMTPAuth = $recruteur_SMTP_auth;
        $mail->Host = $recruteur_SMTP_host;
        $mail->Port = (int)$recruteur_SMTP_port;
        if($recruteur_SMTP_auth){
            $mail->Username = $recruteur_SMTP_user;
            $mail->Password = $recruteur_SMTP_pass;
        }
        $mail->From = $source_email;
        $mail->FromName = $source_name;
        $mail->AddAddress($login);
        $mail->AddReplyTo($source_email);

        $mail->Subject = utf8_decode($mail_object);
        $mail->Body = utf8_decode($mail_body);
        if(!$mail->Send()){ //Teste le return code de la fonction
          //echo $mail->ErrorInfo; //Affiche le message d'erreur (ATTENTION:voir section 7)
        }else{
            Debug::d_echo("envoi mail au candidat ".$login, 2,"profil-save.php");
        }

        $mail->SmtpClose();
        unset($mail);
    }
}

header("Location: http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language'])."?save=1");
exit();

//phpinfo();
?>
