<?php if(! empty($_REQUEST['action'])) { include('system/libs/tinymce/tinymce.php'); }  ?>

    <script type="text/javascript">
    var val     = <?= $val; ?>;
    var incr    = 1;
    function incrementer(signe) {
        val=val+signe*incr;
        document.getElementById("qty").value=val;
    }
    function cacher_moins() {
        if (document.getElementById('qty').value == "1") {
            document.getElementById('moins').disabled = true;
            document.getElementById('moins').style.cursor = "no-drop";
        }
        else {
            document.getElementById('moins').disabled = false;
            document.getElementById('moins').style.cursor = "pointer";
        }
    }
    function cacher_plus() {
        if (document.getElementById('qty').value == "<?= $val_max; ?>") {
            document.getElementById('plus').disabled = true;
            document.getElementById('plus').style.cursor = "no-drop";
        }
        else {
            document.getElementById('plus').disabled = false;
            document.getElementById('plus').style.cursor = "pointer";
        }
    }
    </script>

    <!-- Bouton nouveau -->
    <div id="body_btn_nouveau">
        <a href="<?= $site; ?>/qui-sommes-nous.html" class="link_nouveau" target="_blank">
            <div class="btn_nouveau"><span class="span_nouveau">Accéder à la page <font color="#b9001f">&raquo;</font></span></div>
        </a>
        <a href="<?= $_SERVER['PHP_SELF']; ?>?page=<?= $page; ?>&part=<?= $part; ?>&action=add" class="link_nouveau">
            <div class="btn_nouveau"><span class="span_nouveau">Ajouter un paragraphe <font color="#b9001f">+</font></span></div>
        </a>
        <?php if(! empty($_REQUEST['action'])) {  ?>
        <a href="<?= $_SERVER['PHP_SELF']; ?>?page=<?= $page; ?>&part=<?= $part; ?>" class="link_nouveau">
            <div class="btn_nouveau"><span class="span_nouveau">Retour à la liste</span></div>
        </a>
        <?php } ?>
    </div>
    <div class="clear"></div>
</div>


<div id="content">
    <!-- Colonne gauche -->
    <?php include($b_templates.'side_left.php'); ?>

    <!-- Colonne droite -->
    <div class="colonne_right">

		<div id="title_line"><span class="span_title_line"><?= $title_page; ?></span></div>

        <?php if(empty($_REQUEST['id']) && empty($_REQUEST['action'])){ ?>
        <form name="" action="" method="post">
            <input type="hidden" name="id_intro" value="<?= $dat_head['id']; ?>" />
            <table class="table_right" width="760" cellpadding="6" cellspacing="0">
                <tbody>
                    <tr class="tr_head" style="background:#eee">
                        <td valign="top" class="td_head" style="text-align:left;font-size:14px;background:#eee;">&nbsp;TEXTE D'INTRODUCTION</td>
                    </tr>
                    <tr class="tr_head">
                        <td valign="top" class="td_head" style="text-align:left;background:#eee;">
                            <textarea name="texte" class="text" style="height:100px;width:738px;font-size:12px;"><?= stripslashes(str_replace("<br />","\r\n",$dat_head['texte'])); ?></textarea><br />
                            <input type="submit" name="submit_intro" value="Valider" style="letter-spacing:0.025em;font-size:11px;margin-top:4px;float:right;border:1px solid #000;background:#000;color:#fff;padding:3px 10px;cursor:pointer" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>

        <br />
        <table class="table_right" width="760" cellpadding="6" cellspacing="0">
            <tbody>
                <tr class="tr_head">
                    <td valign="top" width="40" class="td_head" style="text-align:center">&nbsp;Photo</td>
                    <td valign="top" class="td_head" style="text-align:left">&nbsp;Informations</td>
                    <td valign="top" width="40" class="td_head" style="text-align:right">Edit</td>
                </tr>
                <?php liste_articles($pole,$part); ?>
            </tbody>
        </table>
        <br /><br />
		<?php } ?>

		<?php if($_REQUEST['action'] == "edit") { ?>
		<form name="modif_fiche" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="photo_old" value="<?= $data_edit['image']; ?>">
            <input type="hidden" name="id_update" value="<?= $data_edit['id']; ?>">
            <input type="hidden" name="ordre_old" value="<?= $val; ?>">
            <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
                <tbody>
                    <tr style="background:#252525;">
                        <td colspan="2" style="padding:6px;color:#fff;">
                            &nbsp;&nbsp;<strong style="font-size:11px;"><?= stripslashes($data_edit['titre']); ?></strong>
                        </td>
                    </tr>

                    <tr style="background:#fbf9f9">
                        <td style="padding:10px;margin-right:20px">
                            <p style="margin-top:10px"><strong style="font-size:12px;">Position de l'article</strong></p>
                        </td>
                        <td style="padding:5px;">
                            <input type="button" value="-" class="btn_qty" id="moins" onClick="incrementer(-1);cacher_moins();cacher_plus();" style="padding:2px 7px;cursor:<?= $cursor_moins; ?>" <?= $disabled_moins; ?>>

                            <input type="text" name="ordre" value="<?= $val; ?>" class="text" id="qty" style="width:30px;text-align:center;"
                            onFocus="this.style.border='1px solid #efd006';this.style.background='#fdfae5';"
                            onBlur="this.style.border='1px solid #ccc';this.style.background='#fff';">

                            <input type="button" class="btn_qty" id="plus" value="+" onClick="incrementer(+1);cacher_moins();cacher_plus();" style="padding:2px 7px;cursor:<?= $cursor_plus; ?>" <?= $disabled_plus; ?>>
                        </td>
                    </tr>

                    <tr style="background:#ffffff;">
                        <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                            <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Détails de l'article</strong>
                        </td>
                    </tr>

                    <tr style="background:#fbf9f9">
                        <td valign="top" width="160" style="padding:10px;margin-right:20px;">
                            <img src="<?= $image; ?>" width="150" />
                        </td>
                        <td valign="top" style="padding:5px;padding-top:0">
                            <?php if(! empty($data_edit['image'])){ ?>
                            <p style="font-size:11px;margin-bottom:3px"><b>Changer l'image</b> (dimensions optimales : 245 x 245px)</p>
                            <?php } else { ?>
                            <p style="font-size:11px;margin-bottom:3px"><b>Ajouter une image</b> (dimensions optimales : 245 x 245px)</p>
                            <?php } ?>
                            <input type="file" name="photo" class="text" value="" size="76" />
                            <p style="font-size:11px;margin-bottom:3px"><b>Titre</b></p>
                            <input type="text" class="text" name="titre" value="<?= stripslashes($data_edit['titre']); ?>" style="width:550px" />
                            <?php if(! empty($data_edit['image'])){ ?>
                            <p style="margin-top:15px;">
                                <input type="checkbox" name="id_del_image" id="id_del_image" value="<?= $data_edit['id']; ?>" style="cursor:pointer" />
                                <label for="id_del_image" style="cursor:pointer">Supprimer cette image au prochain enregistrement</label>
                            </p>
                            <?php } ?>
                        </td>
                    </tr>

                    <tr style="background:#ffffff;">
                        <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                            <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Texte de l'article</strong>
                        </td>
                    </tr>
                    <tr style="background:#fbf9f9;">
                        <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                            <textarea name="texte" class="text" style="height:500px;width:560px"><?= stripslashes($data_edit['texte']); ?></textarea>
                        </td>
                    </tr>

                    <tr><td colspan="2" height="20"></td></tr>

                </tbody>
            </table>
            <table class="table_right" width="760" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr style="background:#e9e7e7;">
                        <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                            <input type="submit" name="update" class="table_submit" value="enregistrer mes modifications" />
                        </td>
                    </tr>
                </tbody>
            </table><br /><br />
		</form>
		<?php } ?>


		<?php if($_REQUEST['action'] == "add") { ?>
		<form name="modif_fiche" method="post" action="index.php?page=<?= $page; ?>&part=<?= $part; ?>" enctype="multipart/form-data">
            <input type="hidden" name="ordre_old" value="<?= $val; ?>">
            <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
                <tbody>
                    <tr style="background:#252525;">
                        <td colspan="2" style="padding:6px;color:#fff;">
                            &nbsp;&nbsp;<strong style="font-size:11px;">Nouveau paragraphe</strong>
                        </td>
                    </tr>

                    <tr style="background:#fbf9f9">
                        <td style="padding:10px;margin-right:20px">
                            <p style="margin-top:10px"><strong style="font-size:12px;">Position de l'article</strong></p>
                        </td>
                        <td style="padding:5px;">
                            <input type="button" value="-" class="btn_qty" id="moins" onClick="incrementer(-1);cacher_moins();cacher_plus();" style="padding:2px 7px;cursor:pointer" />

                            <input type="text" name="ordre" value="<?= $val; ?>" class="text" id="qty" style="width:30px;text-align:center;"
                            onFocus="this.style.border='1px solid #efd006';this.style.background='#fdfae5';"
                            onBlur="this.style.border='1px solid #ccc';this.style.background='#fff';">

                            <input type="button" class="btn_qty" id="plus" value="+" onClick="incrementer(+1);cacher_moins();cacher_plus();" style="padding:2px 7px;cursor:no-drop;" disabled />
                        </td>
                    </tr>

                    <tr style="background:#ffffff;">
                        <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                            <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Détails de l'article</strong>
                        </td>
                    </tr>

                    <tr style="background:#fbf9f9">
                        <td valign="top" width="160" style="padding:10px;margin-right:20px;">
                            <img src="../images/no-photo.jpg" width="150" />
                        </td>
                        <td valign="top" style="padding:5px;padding-top:0">
                            <p style="font-size:11px;margin-bottom:3px"><b>Choisir une image</b> (dimensions optimales : 245 x 245px)</p>
                            <input type="file" name="photo" class="text" value="" size="76" />
                            <p style="font-size:11px;margin-bottom:3px"><b>Titre</b></p>
                            <input type="text" class="text" name="titre" value="" style="width:550px" />
                        </td>
                    </tr>

                    <tr style="background:#ffffff;">
                        <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                            <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Texte de l'article</strong>
                        </td>
                    </tr>
                    <tr style="background:#fbf9f9;">
                        <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                            <textarea name="texte" class="text" style="height:500px;width:560px"></textarea>
                        </td>
                    </tr>

                    <tr><td colspan="2" height="20"></td></tr>

                </tbody>
            </table>
            <table class="table_right" width="760" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr style="background:#e9e7e7;">
                        <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                            <input type="submit" name="submit_add" class="table_submit" value="Enregistrer ce nouveau paragraphe">
                        </td>
                    </tr>
                </tbody>
            </table><br /><br />
		</form>
		<?php } ?>

    </div>
    <div class="clear"></div>
</div>
