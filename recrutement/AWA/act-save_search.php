<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors.php');
include_once('include/pdo.php');
include_once('include/framework.php');

$_SESSION['search_fonction'] = '';
$_SESSION['search_secteur'] = '';
$_SESSION['search_keyword'] = '';
$_SESSION['search_location'] = '';

if(!empty($_POST)){
//    print_t($_POST);
    
    if(!empty($_POST['search_fonction'])){
        $_SESSION['search_fonction'] = $_POST['search_fonction'];
    }

    if(!empty($_POST['search_secteur'])){
        $_SESSION['search_secteur'] = $_POST['search_secteur'];
    }

    if(!empty($_POST['search_keyword'])){
        $_SESSION['search_keyword'] = $_POST['search_keyword'];
    }

    if(!empty($_POST['search_location'])){
        $_SESSION['search_location'] = $_POST['search_location'];
    }
}
?>
