<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');

Debug::d_echo("acces ", 2,"liste.php");
Debug::d_print_r($_GET, 1,"GET","liste.php");
Debug::d_print_r($_POST, 1,"POST","liste.php");
Debug::d_print_r($_SESSION, 1,"SESSION","liste.php");



$fonctionDbaFieldName= "FONCTION";
$secteurDbaFieldName= "SECTEUR";

if(_IS_USE_FONCTION_WEB){
    $fonctionDbaFieldName= "FONCTIONWEB";
}
if(_IS_USE_SECTEUR_WEB){
    $secteurDbaFieldName= "SECTEURWEB";
}




$numberOfElementPerPage = _NOMBRE_ANNONCE_PAR_PAGE;

if(empty($_SESSION['search_pagination'] )){
    $_SESSION['search_pagination'] = 1;
}
if(!empty($_GET['p'])){
    $_SESSION['search_pagination'] = (int)$_GET['p'];
}
Debug::d_echo("page  : ".$_SESSION['search_pagination'], 2,"liste.php");


//Need to reset
$_SESSION['search_pagination'] = 1;

$fonctions = array();
$sql = "
        SELECT DISTINCT mi.`".$fonctionDbaFieldName."` AS `FONCTION`

        FROM
            missions AS mi
        INNER JOIN annonces AS an ON an.ID_MISSION = mi.ID_MISSION

        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )

        ORDER BY mi.`".$fonctionDbaFieldName."` ASC

    ";



$sql45 = "
        SELECT DISTINCT mi.`".$fonctionDbaFieldName."` AS `FONCTION`

        FROM
            missions AS mi
        INNER JOIN tannonces AS an ON an.ID_MISSION = mi.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )

        ORDER BY mi.`".$fonctionDbaFieldName."` ASC

    ";

if(_ADMEN_USE_ADVERT_LINES){
    $sql = $sql45;
}

$select = $conn->prepare($sql);
$select->execute();
//Debug::d_sql_error($select, 0,"sql get fonctions","liste.php",__LINE__);
while($row = $select->fetchObject()){
    if(!empty($row->FONCTION)){
        $fonctions[] = $row;
    }
}

$secteurs = array();
$sql = "
        SELECT DISTINCT mi.`".$secteurDbaFieldName."` AS `SECTEUR`

        FROM
           missions AS mi
        INNER JOIN annonces AS an ON an.ID_MISSION = mi.ID_MISSION

        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )

        ORDER BY mi.`".$secteurDbaFieldName."` ASC

    ";


$sql45 = "
        SELECT DISTINCT mi.`".$secteurDbaFieldName."` AS `SECTEUR`

        FROM
           missions AS mi
        INNER JOIN tannonces AS an ON an.ID_MISSION = mi.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )

        ORDER BY mi.`".$secteurDbaFieldName."` ASC

    ";


if(_ADMEN_USE_ADVERT_LINES){
    $sql = $sql45;
}

$select = $conn->prepare($sql);
$select->execute();
//Debug::d_sql_error($select, 0,"sql get secteurs","liste.php",__LINE__);
while($row = $select->fetchObject()){
    if(!empty($row->SECTEUR)){
        $secteurs[] = $row;
    }
}




$locations = array();
$locations2 = array();
$locations3 = array();
$sql = "
        SELECT DISTINCT mi.`PAYS`

        FROM
           missions AS mi
        INNER JOIN annonces AS an ON an.ID_MISSION = mi.ID_MISSION

        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )

        ORDER BY mi.`PAYS` ASC

    ";


$sql45 = "
        SELECT DISTINCT mi.`PAYS`

        FROM
           missions AS mi
        INNER JOIN tannonces AS an ON an.ID_MISSION = mi.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )

        ORDER BY mi.`PAYS` ASC

    ";


if(_ADMEN_USE_ADVERT_LINES){
    $sql = $sql45;
}

$select = $conn->prepare($sql);
$select->execute();
//Debug::d_sql_error($select, 0,"sql get pays","liste.php",__LINE__);
while($row = $select->fetchObject()){
    if(!empty($row->PAYS)){
        $locations[] = $row->PAYS;
    }
}

$locations2 = array();
$locations3 = array();
foreach($locations as $key=>$location){
    $sql = "
        SELECT DISTINCT mi.`REGION`

        FROM
           missions AS mi
        INNER JOIN annonces AS an ON an.ID_MISSION = mi.ID_MISSION

        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND mi.PAYS = '".$location."'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )

        ORDER BY mi.`REGION` ASC

    ";

    $sql45 = "
        SELECT DISTINCT mi.`REGION`

        FROM
           missions AS mi
        INNER JOIN tannonces AS an ON an.ID_MISSION = mi.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND mi.PAYS = '".$location."'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )

        ORDER BY mi.`REGION` ASC

    ";

    if(_ADMEN_USE_ADVERT_LINES){
        $sql = $sql45;
    }
    $select = $conn->prepare($sql);
    $select->execute();
//    Debug::d_sql_error($select, 0,"sql get regions where pays=".$location,"liste.php",__LINE__);
    $locations2_temp = array();
    while($row = $select->fetchObject()){
        if(!empty($row->REGION)){
            $locations2_temp[] = $row->REGION;
        }
    }
    $locations2[] = array('pays'=>$location,'regions'=>$locations2_temp);
}
$locations = $locations2;

$locations2 = array();
$locations3 = array();
foreach($locations as $key=>$location){

    $locations3 = array();
    foreach($location['regions'] as $location2){
        $sql = "
            SELECT DISTINCT mi.`CITY`

            FROM
               missions AS mi
            INNER JOIN annonces AS an ON an.ID_MISSION = mi.ID_MISSION

            WHERE
                an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
                AND mi.ETAT = '1'
                AND mi.PAYS = '".$location['pays']."'
                AND mi.REGION = '".$location2."'
                AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
                AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )

            ORDER BY mi.`CITY` ASC

        ";


        $sql45 = "
            SELECT DISTINCT mi.`CITY`

            FROM
               missions AS mi
            INNER JOIN tannonces AS an ON an.ID_MISSION = mi.ID_MISSION
            INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

            WHERE
                al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
                AND mi.ETAT = '1'
                AND mi.PAYS = '".$location['pays']."'
                AND mi.REGION = '".$location2."'
                AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
                AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )

            ORDER BY mi.`CITY` ASC

        ";
        if(_ADMEN_USE_ADVERT_LINES){
            $sql = $sql45;
        }

        $select = $conn->prepare($sql);
        $select->execute();
//        Debug::d_sql_error($select, 0,"sql get city where pays=".$location['pays']." and region=".$location2,"liste.php",__LINE__);
        $locations3_temp = array();
        while($row = $select->fetchObject()){
            if(!empty($row->CITY)){
                $locations3_temp[] = $row->CITY;
            }
        }
        $locations3[] = array('region'=>$location2,'villes'=>$locations3_temp);

    }
    $locations2[] = array('pays'=>$location['pays'],'regions'=>$locations3);

}
$locations = $locations2;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title><?php echo _OFFRE_LISTE_TITLE; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content=""/>
        <meta name="keywords" content=""/>
        <?php  if(!_USE_CUSTOM_CSS){ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?>getcss_custom.php" rel="stylesheet" type="text/css" />
        <?php }else{ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?><?php echo _TEMPLATE_FOLDER_NAME; ?>/custom.css" rel="stylesheet" type="text/css" />
        <?php } ?>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/framework.js"></script>
        <script type="text/javascript" >
            $.ajaxSettings.cache = false;
            $(document).ready(function(){
                //Capture touche entré pour submit de la recherche
                $('#search_fonction').keyup(function(e) {
                    if(e.keyCode == 13){
                        saveSearch();
                    }
                 }
                );
                $('#search_secteur').keyup(function(e) {
                    if(e.keyCode == 13){
                        saveSearch();
                    }
                 }
                );
                $('#search_keyword').keyup(function(e) {
                    if(e.keyCode == 13){
                        saveSearch();
                    }
                 }
                );
                $('#search_location').keyup(function(e) {
                    if(e.keyCode == 13){
                        saveSearch();
                    }
                 }
                );


                <?php if(_IS_IFRAME_RESIZE){ ?>
                resizeiframe();
                <?php } ?>
            });

            function changeLanguage(language){
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-change_language.php",{ 'language': language});
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-get_url.php",
                    { 'pageref':'search', 'langue':language },
                    function(data){
                        window.location=data;
                    }
                );

            }

            function saveSearch(){
                <?php
                if(_IS_SEARCH_FONCTION_MULTIPLE){
                    echo "var searchFunction = $('#search_fonction').val() || [];";
                }else{
                    echo "var searchFunction = $('#search_fonction').val();";
                }
                if(_IS_SEARCH_SECTEUR_MULTIPLE){
                    echo "var searchSecteur = $('#search_secteur').val()|| [];";
                }else{
                    echo "var searchSecteur = $('#search_secteur').val();";
                }
                ?>
                var searchLocation = $('#search_location').val();
                var searchKeyword = $('#search_keyword').val();

                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-save_search.php",
                    <?php
                    echo "{";
                        if(_IS_SEARCH_FONCTION_MULTIPLE){
                            echo "'search_fonction': searchFunction.join(\"|\"),";
                        }else{
                            echo "'search_fonction': searchFunction,";
                        }
                        if(_IS_SEARCH_SECTEUR_MULTIPLE){
                            echo "'search_secteur': searchSecteur.join(\"|\"),";
                        }else{
                            echo "'search_secteur': searchSecteur,";
                        }
                        echo "'search_location': searchLocation,";
                        echo "'search_keyword':searchKeyword";
                    echo "}";
                    ?>,
                    function(data){
//                        alert(data);
                        window.location='<?php echo _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language']); ?>';
                    }
                );
            }

            function toggle_expand_pays(id_pays){
                if($('#expanded_pays_'+id_pays).html() == '+'){
                    $('#expanded_pays_'+id_pays).html('-');
                    $('#expanded_pays_'+id_pays).css('padding-left','5px');
                    $('#pays_content_'+id_pays).show();
                }else{
                    $('#expanded_pays_'+id_pays).html('+');
                    $('#expanded_pays_'+id_pays).css('padding-left','2px');
                    $('#pays_content_'+id_pays).hide();
                }
            }

            function toggle_expand_region(id_region){
                if($('#expanded_region_'+id_region).html() == '+'){
                    $('#expanded_region_'+id_region).html('-');
                    $('#expanded_region_'+id_region).css('padding-left','5px');
                    $('#region_content_'+id_region).show();
                }else{
                    $('#expanded_region_'+id_region).html('+');
                    $('#expanded_region_'+id_region).css('padding-left','2px');
                    $('#region_content_'+id_region).hide();
                }
            }

            function select_ville($id_pays,id_region,id_ville){
                var pays = $('#pays_value_'+$id_pays).html();
                var region = $('#region_value_'+id_region).html();
                var ville = $('#ville_title_'+id_ville).html();
                $('#search_location').val(pays+" "+region+" "+ville);
                $('#location_popup_menu').hide();
            }

            function select_region($id_pays,id_region){
                var pays = $('#pays_value_'+$id_pays).html();
                var region = $('#region_value_'+id_region).html();
                $('#search_location').val(pays+" "+region);
                $('#location_popup_menu').hide();
            }

            function select_pays($id_pays){
                var pays = $('#pays_value_'+$id_pays).html();
                $('#search_location').val(pays);
                $('#location_popup_menu').hide();
            }

        </script>
    </head>
    <body>
        <?php
        $templateDatas = array();

        $templateDatas['pathway'] = "";
        $pathway = array();
        $pathway[0]['text'] = _OFFRE_LISTE_TITLE;
        $pathway[0]['url'] = null;
        $templateDatas['pathway'] =  displayPathway($pathway);

        $templateDatas['header'] = "";
        $templateDatas['header'] = displayHeader();

        $templateDatas['candidatArea'] = "";
        $templateDatas['candidatArea'] = displayCandidatArea($conn);

        $templateDatas['menu'] = "";
        $templateDatas['menu'] = displayMenu();


        /*
         * Formulaire de filtrage
         */

        //popup pour la liste des pays
        $templateDatas['filters'] = array();
        $templateDatas['filters']['countryPopupContent'] = "";
        $id_pays = 0;
        $id_region = 0;
        $id_ville = 0;
        foreach($locations as $pays){
            $templateDatas['filters']['countryPopupContent'] .= "<div id=\"pays_title_".$id_pays."\" class=\"pays_title hoverbale inputdesign\"><span id=\"expanded_pays_".$id_pays."\" class=\"spanexp\" onclick=\"toggle_expand_pays(".$id_pays.");\">+</span><span id=\"pays_value_".$id_pays."\" class=\"pays_value\" onclick=\"select_pays(".$id_pays.");\">".$pays['pays']."</span></div>";
            $templateDatas['filters']['countryPopupContent'] .= "<div id=\"pays_content_".$id_pays."\" class=\"pays_content\" >";
            foreach($pays['regions'] as $region){
                $templateDatas['filters']['countryPopupContent'] .= "<div id=\"region_title_".$id_region."\" class=\"region_title hoverbale inputdesign\"><span id=\"expanded_region_".$id_region."\" class=\"spanexp\" onclick=\"toggle_expand_region(".$id_region.");\">+</span><span id=\"region_value_".$id_region."\" class=\"region_value\" onclick=\"select_region(".$id_pays.",".$id_region.");\">".$region['region']."</span></div>";
                $templateDatas['filters']['countryPopupContent'] .= "<div id=\"region_content_".$id_region."\" class=\"region_content\" >";
                foreach($region['villes'] as $ville){
                    $templateDatas['filters']['countryPopupContent'] .= "<div id=\"ville_title_".$id_ville."\" class=\"ville_title hoverbale inputdesign\" onclick=\"select_ville(".$id_pays.",".$id_region.",".$id_ville.");\">".$ville."</div>";
                    $id_ville++;
                }
                $templateDatas['filters']['countryPopupContent'] .= "</div>";
                $id_region++;
            }
            $templateDatas['filters']['countryPopupContent'] .= "</div>";
            $id_pays++;
        }

        //liste fonction
        $templateDatas['filters']['functionListContent'] = "";
        $selectedFonctions = array();
        if(!empty($_SESSION['search_fonction'])){
            $selectedFonctions = explode("|",$_SESSION['search_fonction']);
        }
        $selectedFonctions = explode("|",$_SESSION['search_fonction']);
        foreach($fonctions as $fonction){
            $selected = "";
            foreach($selectedFonctions as $selectedFonction){
                if($selectedFonction == $fonction->FONCTION){
                    $selected = "selected=\"selected\"";
                }
            }

            $templateDatas['filters']['functionListContent'] .= "<option value=\"".$fonction->FONCTION."\" ".$selected.">".$fonction->FONCTION."</option>";
        }
//        $templateDatas['filters']['functionListContent'] = "";
//        foreach($fonctions as $fonction){
//            $selected = "";
//            if(!empty($_SESSION['search_fonction'] )){
//                if($_SESSION['search_fonction'] == $fonction->FONCTION){
//                    $selected = "selected=\"selected\"";
//                }
//            }
//            $templateDatas['filters']['functionListContent'] .= "<option value=\"".$fonction->FONCTION."\" ".$selected.">".$fonction->FONCTION."</option>";
//        }


        //selected location value
        $templateDatas['filters']['locationSelectedValue'] = "";
        if(!empty($_SESSION['search_location'] )){
            $templateDatas['filters']['locationSelectedValue'] =  $_SESSION['search_location'];
        }


        //liste secteur
        $templateDatas['filters']['sectorListContent'] = "";
        $selectedSecteurs = array();
        if(!empty($_SESSION['search_secteur'])){
            $selectedSecteurs = explode("|",$_SESSION['search_secteur']);
        }
        $selectedSecteurs = explode("|",$_SESSION['search_secteur']);
        foreach($secteurs as $secteur){
            $selected = "";
            foreach($selectedSecteurs as $selectedSecteur){
                if($selectedSecteur == $secteur->SECTEUR){
                    $selected = "selected=\"selected\"";
                }
            }
            $templateDatas['filters']['sectorListContent'] .=  "<option value=\"".$secteur->SECTEUR."\" ".$selected.">".$secteur->SECTEUR."</option>";
        }
//        $templateDatas['filters']['sectorListContent'] = "";
//        foreach($secteurs as $secteur){
//            $selected = "";
//            if(!empty($_SESSION['search_secteur'] )){
//                if($_SESSION['search_secteur'] == $secteur->SECTEUR){
//                    $selected = "selected=\"selected\"";
//                }
//            }
//            $templateDatas['filters']['sectorListContent'] .=  "<option value=\"".$secteur->SECTEUR."\" ".$selected.">".$secteur->SECTEUR."</option>";
//        }


        //selected keyword
        $templateDatas['filters']['keywordSelectedValue'] = "";
        if(!empty($_SESSION['search_keyword'] )){
             $templateDatas['filters']['keywordSelectedValue'] = $_SESSION['search_keyword'];
        }


       

       


        //Footer
        $templateDatas['footerText'] = "";
        $templateDatas['footerText'] = displayFooter();
        ?>




        <?php include(_TEMPLATE_FOLDER_NAME."/tpl_search.php"); ?>
    </body>
</html>
<?php
//print_t($printsql);
//print_t($_SESSION);
////echo "<pre>";
////print_r($fonctions);
////echo "</pre>";
//echo "<pre>";
//print_r($locations);
//echo "</pre>";
?>