<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

define("_AWA_PATHWAY","/homez.218/gsagroup/www/recrutement/AWA/");

//FIchier de configuration de l'affichage des messages d'erreurs
include_once _AWA_PATHWAY."include/display_errors.php";

include_once(_AWA_PATHWAY.'include/config.php');
@include_once _AWA_PATHWAY.'include/pdo.php';
include_once(_AWA_PATHWAY."include/phpmailer/class.phpmailer.php");


/*
 * Récupération de la config en DBA
 */
$config_RECRUTEUR_EMAIL_OFFRE = "";
$config_RECRUTEUR_EMAIL_SPONTANE = "";
$config_RECRUTEUR_EMAIL_OFFRE_SRC = "";
$config_RECRUTEUR_EMAIL_SPONTANE_SRC = "";
$config_RECRUTEUR_EMAIL_NAME_SPONTANE = "";
$config_RECRUTEUR_EMAIL_NAME_OFFRE = "";
$config_RECRUTEUR_EMAIL_RELANCE_SRC = "";
$config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC = "";
$config_RECRUTEUR_SMTP_HOST = "localhost";
$config_RECRUTEUR_SMTP_PORT = "25";
$config_RECRUTEUR_SMTP_AUTH = "0";
$config_RECRUTEUR_SMTP_USER = "";
$config_RECRUTEUR_SMTP_PASS = "";
$config_RECRUTEUR_SMTP_SECU = "";
$sql = "
        SELECT
            `fo`.*

        FROM
            `awa_configs` AS `fo`

        ORDER BY fo.id DESC
        LIMIT 0,1

        ";

$select = $conn->prepare($sql);
$select->execute();
$configobj = null;
$configobj = $select->fetchObject();
if($configobj){
    
    $config_RECRUTEUR_EMAIL_OFFRE = $configobj->RECRUTEUR_EMAIL_OFFRE;
    $config_RECRUTEUR_EMAIL_SPONTANE = $configobj->RECRUTEUR_EMAIL_SPONTANE;
    $config_RECRUTEUR_EMAIL_OFFRE_SRC = $configobj->RECRUTEUR_EMAIL_OFFRE_SRC;
    $config_RECRUTEUR_EMAIL_SPONTANE_SRC = $configobj->RECRUTEUR_EMAIL_SPONTANE_SRC;
    $config_RECRUTEUR_EMAIL_NAME_SPONTANE = $configobj->RECRUTEUR_EMAIL_NAME_SPONTANE;
    $config_RECRUTEUR_EMAIL_NAME_OFFRE = $configobj->RECRUTEUR_EMAIL_NAME_OFFRE;
    $config_RECRUTEUR_EMAIL_RELANCE_SRC = $configobj->RECRUTEUR_EMAIL_RELANCE_SRC;
    $config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC = $configobj->RECRUTEUR_EMAIL_RELANCE_NAME_SRC;
    $config_RECRUTEUR_SMTP_HOST = $configobj->RECRUTEUR_SMTP_HOST;
    $config_RECRUTEUR_SMTP_PORT = $configobj->RECRUTEUR_SMTP_PORT;
    $config_RECRUTEUR_SMTP_AUTH = $configobj->RECRUTEUR_SMTP_AUTH;
    $config_RECRUTEUR_SMTP_USER = $configobj->RECRUTEUR_SMTP_USER;
    $config_RECRUTEUR_SMTP_PASS = $configobj->RECRUTEUR_SMTP_PASS;
    $config_RECRUTEUR_SMTP_SECU = $configobj->RECRUTEUR_SMTP_SECU;
}else{
    exit();
}

/*
 * Configuration email relance pour candidat
 */
$source_email = $config_RECRUTEUR_EMAIL_RELANCE_SRC;
$source_name = $config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC;



if($config_RECRUTEUR_SMTP_SECU == "ssl"){
    $recruteur_SMTP_host = "ssl://".$config_RECRUTEUR_SMTP_HOST;
}elseif($config_RECRUTEUR_SMTP_SECU == "tls"){
    $recruteur_SMTP_host = "tls://".$config_RECRUTEUR_SMTP_HOST;
}else{
    $recruteur_SMTP_host = $config_RECRUTEUR_SMTP_HOST;
}
$recruteur_SMTP_port = $config_RECRUTEUR_SMTP_PORT ;
if($config_RECRUTEUR_SMTP_AUTH == "1"){
    $recruteur_SMTP_auth = true;
    $recruteur_SMTP_user = $config_RECRUTEUR_SMTP_USER;
    $recruteur_SMTP_pass = $config_RECRUTEUR_SMTP_PASS;
}else{
    $recruteur_SMTP_auth = false;
    $recruteur_SMTP_user = null;
    $recruteur_SMTP_pass = null;
}


/*
 * On récupère tous les candidats actifs et qui n'ont pas encore reçu le mail de relance
 * 
 */
$sql = "
        SELECT
            awa_candidats.ID,
            awa_candidats.ID_PERSONNE_WEB,
            awa_candidats.LOGIN,
            AES_DECRYPT(awa_candidats.PASSWORD,'admen') AS PASSWORD,
            awa_candidats.LANGUE,
            personnes.NOM,
            personnes.PRENOM
        FROM
            awa_candidats
        INNER JOIN personnes ON personnes.ID_PERSONNE_WEB = awa_candidats.ID_PERSONNE_WEB
        
        WHERE
            DATE_MAIL_RELANCE = '0000-00-00'
            AND ACTIVE = '1'

        GROUP BY awa_candidats.ID_PERSONNE_WEB
    ";
$select = $conn->prepare($sql);
$select->execute();
while ($row = $select->fetchObject()){
//    print_r($row);

    /*
     * On récupère le mail de relance en fonction de la langue du candidat
     */
    $sql2 = "
        SELECT
            `awa_mailsrelance`.*

        FROM
            `awa_mailsrelance`
        WHERE
            awa_mailsrelance.LANGUE = '".$row->LANGUE."'

        ORDER BY awa_mailsrelance.ID DESC
        LIMIT 0,1

        ";

    $select_mailrelance = $conn->prepare($sql2);
    $select_mailrelance->execute();

    $emailRelance = null;
    $emailRelance = $select_mailrelance->fetchObject();
    if($emailRelance){
        
    }else{
        /*
         * On récupère le mail de relance dans la langue par défault
         */
        $sql2 = "
            SELECT
                `awa_mailsrelance`.*

            FROM
                `awa_mailsrelance`
            WHERE
                awa_mailsrelance.`DEFAULT` = '1'

            ORDER BY awa_mailsrelance.ID DESC
            LIMIT 0,1

            ";

        $select_mailrelance = $conn->prepare($sql2);
        $select_mailrelance->execute();
        $emailRelance = null;
        $emailRelance = $select_mailrelance->fetchObject();
        if($emailRelance ){
           
        }
    }
    

    if(!empty($row->LOGIN) && !empty ($emailRelance)){

        /*
         * On remplace les variables du mails
         */
        if(defined("_URL_CONFIG_ESPACE_CANDIDAT_".$row->LANGUE)){
            $url = "http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$row->LANGUE);
        }else{
            $url = "http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_"._CONFIG_DEFAULT_LANGUE_FO);
        }
         
        

        
        $mail_object = $emailRelance->OBJECT;
        $mail_object = str_replace("{firstname}",$row->PRENOM, $mail_object);
        $mail_object = str_replace("{name}",$row->NOM, $mail_object);
        $mail_object = str_replace("{login}",$row->LOGIN, $mail_object);
        $mail_object = str_replace("{password}",$row->PASSWORD, $mail_object);
        $mail_object = str_replace("{url_login}",$url, $mail_object);

        $mail_body = $emailRelance->BODY;
        $mail_body = str_replace("{firstname}",$row->PRENOM, $mail_body);
        $mail_body = str_replace("{name}",$row->NOM, $mail_body);
        $mail_body = str_replace("{login}",$row->LOGIN, $mail_body);
        $mail_body = str_replace("{password}",$row->PASSWORD, $mail_body);
        $mail_body = str_replace("{url_login}",$url, $mail_body);

        /*
         * envoi mail au candidat
         */
        
        if(_SEND_EMAIL){
            
            $mail = new PHPmailer();
            $mail->IsSMTP();
            $mail->SMTPDebug = 2;
            $mail->SMTPAuth = $recruteur_SMTP_auth;
            $mail->Host = $recruteur_SMTP_host;
            $mail->Port = (int)$recruteur_SMTP_port;
            if($recruteur_SMTP_auth){
                $mail->Username = $recruteur_SMTP_user;
                $mail->Password = $recruteur_SMTP_pass;
            }
            $mail->From = $source_email;
            $mail->FromName = $source_name;
            $mail->AddAddress($row->LOGIN);
            $mail->AddReplyTo($source_email);

            $mail->Subject = utf8_decode($mail_object);
            $mail->Body = utf8_decode($mail_body);
            if(!$mail->Send()){ //Teste le return code de la fonction
              echo $mail->ErrorInfo; //Affiche le message d'erreur (ATTENTION:voir section 7)
            }else{
                echo 'Mail envoyé avec succès';
                //Update du candidat
                $sql3 = "
                        UPDATE
                            awa_candidats
                        SET
                            DATE_MAIL_RELANCE = '".date('Y-m-d')."'
                        WHERE
                            ID = '".$row->ID."'
                   ";
//                echo $sql3;
                $select3 = $conn->prepare($sql3);
                $select3->execute();

            }
            $mail->SmtpClose();
            unset($mail);
        }
    }

//print_t("############################################################################");
    
}
?>
