<?php // include('system/libs/tinymce/tinymce.php'); ?>

    <!-- Bouton nouveau -->
    <div id="body_btn_nouveau">
        <a href="<?= $site; ?>" class="link_nouveau" target="_blank">
            <div class="btn_nouveau"><span class="span_nouveau">Accéder au site <font color="#b9001f">&raquo;</font></span></div>
        </a>
        <a href="index.php?page=<?= $_REQUEST['page']; ?>&action=add" class="link_nouveau">
            <div class="btn_nouveau"><span class="span_nouveau">Ajouter une agence <font color="#b9001f">+</font></span></div>
        </a>
    </div>
    <div class="clear"></div>
</div>

<div id="content">
    <!-- Colonne gauche -->
    <?php include($b_templates.'side_left.php'); ?>

    <!-- Colonne droite -->
    <div class="colonne_right">

        <div id="title_line"><span class="span_title_line">Agences (<?php return_count('agences','',''); ?>)</span></div>
        <!-- métiers -->
        <?php if(empty($_REQUEST['id']) && empty($_REQUEST['action'])) { // Liste des métiers ?>
        <table class="table_right" width="760" cellpadding="6" cellspacing="0">
            <tbody>
                <tr class="tr_head">
                    <td class="td_head" style="text-align:left">&nbsp;Nom agence</td>
                    <td class="td_head" style="text-align:left">&nbsp;cp, ville</td>
                    <td class="td_head" style="text-align:left">&nbsp;Téléphone</td>
                    <td class="td_head" style="text-align:left">&nbsp;Email</td>
                    <td class="td_head" width="40">Edit</td>
                </tr>

                <?php liste_agences(); ?>

            </tbody>
        </table><br /><br />
        <?php } ?>

        <?php if($_REQUEST['action'] == "edit") { ?>
        <form name="modif_fiche" method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="id_update" value="<?= $data_edit['id']; ?>">
        <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
            <tbody>
                <tr style="background:#252525;">
                    <td colspan="2" style="padding:6px;color:#fff;">
                        &nbsp;&nbsp;<strong style="font-size:11px;">Fiche agence : <?= stripslashes($data_edit['nom']); ?></strong>
                    </td>
                </tr>
                <tr style="background:#ffffff;">
                    <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                        <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Informations générales</strong>
                    </td>
                </tr>

                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Nom</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="nom" value="<?= stripslashes($data_edit['nom']); ?>" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Dirigeant</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="dirigeant" value="<?= stripslashes($data_edit['dirigeant']); ?>" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Langue(s)</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="langues" value="<?= stripslashes($data_edit['langues']); ?>" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Adresse</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="adresse" value="<?= stripslashes($data_edit['adresse']); ?>" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Code postal</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="code_postal" value="<?= $data_edit['code_postal']; ?>" style="width:65px">
                        &nbsp;&nbsp;<span style="color:#515151;position:relative;top:-1px">Très important : cette information permet de retrouver l'agence sur la carte de l'annuaire.</span>
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Ville</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="ville" value="<?= stripslashes($data_edit['ville']); ?>" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Téléphone</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="tel" value="<?= $data_edit['tel']; ?>" style="width:150px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Fax</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="fax" value="<?= $data_edit['fax']; ?>" style="width:150px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Email</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="mail" value="<?= strtolower($data_edit['mail']); ?>" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Site web</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="site_web" value="<?= strtolower($data_edit['site_web']); ?>" style="width:600px"
                        onFocus="if(this.value==''){this.value='http://'}" onBlur="if(this.value=='http://'){this.value=''}">
                    </td>
                </tr>

                <tr style="background:#fff;">
                    <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                        <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/images.png"> Photo</strong>
                    </td>
                </tr>
                <tr style="background:#fff;">
                    <?php if(! empty($data_edit['photo'])) { ?>
                    <td style="padding:10px;margin-right:20px;" valign="top">
                        <p style="font-size:11px;color:#7b7c7c;margin-top:5px;margin-left:5px">Cliquez sur la photo<br />pour la remplacer</p>
                    </td>
                    <td style="padding:10px;">
                        <div id="bloc_photo" style="float:left">
                            <img src="../i/downloads/<?= $data_edit['photo']; ?>" style="background:#fff;padding:4px;border:1px solid #fff;cursor:pointer;" height="100"
                            onMouseOver="this.style.border='1px solid #efd006'" onMouseOut="this.style.border='1px solid #fff'"
                            onClick="document.getElementById('edit').style.display='none';document.getElementById('replace').style.display='block';">
                        </div>
                        <div id="edit" style="float:right">
                            <input id="submit_accept" type="button" value="" style="float:right;background:url('src/icons/page_white_edit.png') no-repeat;"
                            onClick="document.getElementById('edit').style.display='none';document.getElementById('replace').style.display='block';">
                        </div>
                        <div id="replace" style="float:right;display:none">
                            <input type="hidden" name="file_photo_old" value="<?= $data_edit['photo']; ?>">
                            <input id="submit_del" type="button" value="" style="float:right;background:url('src/icons/cancel.png') no-repeat;"
                            onClick="document.getElementById('edit').style.display='block';document.getElementById('replace').style.display='none';document.getElementById('file_photo').value='';">
                            <input type="file" name="file_photo" id ="file_photo">
                        </div>
                    </td>
                    <?php } else { ?>
                    <td style="padding:10px;margin-right:20px;">
                        <p style="font-size:11px;color:#7b7c7c;margin-top:5px;margin-left:5px">Cliquez sur <strong>parcourir</strong><br />et sélectionnez une photo</p>
                    </td>
                    <td style="padding:10px;">
                        <input type="file" name="file_photo" id ="file_photo" size="80">
                    </td>
                    <?php } ?>
                </tr>

                <tr><td colspan="2" height="10"></td></tr>

                <tr style="background:#ffffff;">
                    <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                        <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/lock.png"> Configuration du compte</strong>
                    </td>
                </tr>
                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Identifiant</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="login_update" value="<?= $data_edit['login']; ?>" style="width:150px;">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Mot de passe</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="pwd_update" value="<?= $data_edit['password']; ?>" style="width:150px;">
                    </td>
                </tr>

                <tr><td colspan="2" height="10"></td></tr>

            </tbody>
        </table>
        <table class="table_right" width="760" cellpadding="0" cellspacing="0">
            <tbody>
                <tr style="background:#e9e7e7;">
                    <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                        <input type="submit" name="update" class="table_submit" value="enregistrer mes modifications">
                    </td>
                </tr>
            </tbody>
        </table><br /><br />
        </form>
        <?php } ?>


        <?php if($_REQUEST['action'] == "add") { ?>
        <form name="modif_fiche" method="post" action="index.php?page=<?= $_REQUEST['page']; ?>" enctype="multipart/form-data">
        <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
            <tbody>
                <tr style="background:#158E02;">
                    <td colspan="2" style="padding:6px;color:#fff;">
                        &nbsp;&nbsp;<strong style="font-size:11px;">Nouvelle agence</strong>
                    </td>
                </tr>
                <tr style="background:#ffffff;">
                    <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                        <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Informations générales</strong>
                    </td>
                </tr>

                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Nom</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="nom" value="" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Dirigeant</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="dirigeant" value="" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Langue(s)</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="langues" value="" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Adresse</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="adresse" value="" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Code postal</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="code_postal" value="" style="width:65px">
                        &nbsp;&nbsp;<span style="color:#515151;position:relative;top:-1px">Très important : cette information permet de retrouver l'agence sur la carte de l'annuaire.</span>
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Ville</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="ville" value="" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Téléphone</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="tel" value="" style="width:150px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Fax</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="fax" value="" style="width:150px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Email</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="mail" value="" style="width:600px">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Site web</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="site_web" value="" style="width:600px"
                        onFocus="if(this.value==''){this.value='http://'}" onBlur="if(this.value=='http://'){this.value=''}">
                    </td>
                </tr>

                <tr style="background:#fff;">
                    <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                        <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/images.png"> Photo</strong>
                    </td>
                </tr>
                <tr style="background:#fff;">
                    <td style="padding:10px;margin-right:20px;">
                        <p style="font-size:11px;color:#7b7c7c;margin-top:5px;margin-left:5px">Cliquez sur <strong>parcourir</strong><br />et sélectionnez une photo</p>
                    </td>
                    <td style="padding:10px;">
                        <input type="file" name="file_photo" id="file_photo" size="80">
                    </td>
                </tr>

                <tr><td colspan="2" height="10"></td></tr>

                <tr style="background:#ffffff;">
                    <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                        <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/lock.png"> Configuration du compte</strong>
                    </td>
                </tr>
                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Identifiant</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="login_add" value="" style="width:150px;">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Mot de passe</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="pwd_add" value="" style="width:150px;">
                    </td>
                </tr>

                <tr><td colspan="2" height="10"></td></tr>

            </tbody>
        </table>
        <table class="table_right" width="760" cellpadding="0" cellspacing="0">
            <tbody>
                <tr style="background:#e9e7e7;">
                    <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                        <input type="submit" name="submit_add" class="table_submit" value="Enregistrer cette nouvelle agence">
                    </td>
                </tr>
            </tbody>
        </table><br /><br />
        </form>
        <?php } ?>

    </div>
    <div class="clear"></div>
</div>

