<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');
include_once("include/phpmailer/class.phpmailer.php");

Debug::d_echo("acces ", 2,"mdp-oublie.php");
Debug::d_print_r($_GET, 1,"GET","mdp-oublie.php");
Debug::d_print_r($_POST, 1,"POST","mdp-oublie.php");
Debug::d_print_r($_SESSION, 1,"SESSION","mdp-oublie.php");

$message="";
$message2="";
if(!empty($_POST['login'])){
    Debug::d_echo("oublie mdp pour ".$_POST['login'], 2,"mdp-oublie.php");
    $message2=_MDPOUBLIE_MESSAGE_NOK;
//    echo "traitement";
    $sql = "SELECT
                awa_candidats.ID,
                awa_candidats.ID_PERSONNE_WEB,
                awa_candidats.LOGIN,
                AES_DECRYPT(awa_candidats.PASSWORD,'admen') AS PASSWORD,
                awa_candidats.LANGUE,
                personnes.NOM,
                personnes.PRENOM
            FROM
                awa_candidats
            INNER JOIN personnes ON personnes.ID_PERSONNE_WEB = awa_candidats.ID_PERSONNE_WEB

            WHERE
                awa_candidats.LOGIN =:LOGIN

            GROUP BY awa_candidats.ID_PERSONNE_WEB
    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':LOGIN', $_POST['login'], PDO::PARAM_STR);
    $select->execute();
    $candidat = null;
    $candidat = $select->fetchObject();
    if($candidat){
        
        Debug::d_echo($_POST['login']." => candidat  ".$candidat->ID, 2,"mdp-oublie.php");
//        print_t($candidat);
        /*
         * Récupération de la config en DBA
         */
        $config_RECRUTEUR_EMAIL_OFFRE = "";
        $config_RECRUTEUR_EMAIL_SPONTANE = "";
        $config_RECRUTEUR_EMAIL_OFFRE_SRC = "";
        $config_RECRUTEUR_EMAIL_SPONTANE_SRC = "";
        $config_RECRUTEUR_EMAIL_NAME_SPONTANE = "";
        $config_RECRUTEUR_EMAIL_NAME_OFFRE = "";
        $config_RECRUTEUR_EMAIL_RELANCE_SRC = "";
        $config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC = "";
        $config_RECRUTEUR_SMTP_HOST = "localhost";
        $config_RECRUTEUR_SMTP_PORT = "25";
        $config_RECRUTEUR_SMTP_AUTH = "0";
        $config_RECRUTEUR_SMTP_USER = "";
        $config_RECRUTEUR_SMTP_PASS = "";
        $config_RECRUTEUR_SMTP_SECU = "";
        $sql = "
                SELECT
                    `fo`.*

                FROM
                    `awa_configs` AS `fo`

                ORDER BY fo.id DESC
                LIMIT 0,1

                ";

        $select = $conn->prepare($sql);
        $select->execute();
        $configobj = null;
        $configobj = $select->fetchObject();
        if($configobj){
            $config_RECRUTEUR_EMAIL_OFFRE = $configobj->RECRUTEUR_EMAIL_OFFRE;
            $config_RECRUTEUR_EMAIL_SPONTANE = $configobj->RECRUTEUR_EMAIL_SPONTANE;
            $config_RECRUTEUR_EMAIL_OFFRE_SRC = $configobj->RECRUTEUR_EMAIL_OFFRE_SRC;
            $config_RECRUTEUR_EMAIL_SPONTANE_SRC = $configobj->RECRUTEUR_EMAIL_SPONTANE_SRC;
            $config_RECRUTEUR_EMAIL_NAME_SPONTANE = $configobj->RECRUTEUR_EMAIL_NAME_SPONTANE;
            $config_RECRUTEUR_EMAIL_NAME_OFFRE = $configobj->RECRUTEUR_EMAIL_NAME_OFFRE;
            $config_RECRUTEUR_EMAIL_RELANCE_SRC = $configobj->RECRUTEUR_EMAIL_RELANCE_SRC;
            $config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC = $configobj->RECRUTEUR_EMAIL_RELANCE_NAME_SRC;
            $config_RECRUTEUR_SMTP_HOST = $configobj->RECRUTEUR_SMTP_HOST;
            $config_RECRUTEUR_SMTP_PORT = $configobj->RECRUTEUR_SMTP_PORT;
            $config_RECRUTEUR_SMTP_AUTH = $configobj->RECRUTEUR_SMTP_AUTH;
            $config_RECRUTEUR_SMTP_USER = $configobj->RECRUTEUR_SMTP_USER;
            $config_RECRUTEUR_SMTP_PASS = $configobj->RECRUTEUR_SMTP_PASS;
            $config_RECRUTEUR_SMTP_SECU = $configobj->RECRUTEUR_SMTP_SECU;
        }else{
            exit();
        }

        /*
         * Configuration email relance pour candidat
         */
        $source_email = $config_RECRUTEUR_EMAIL_RELANCE_SRC;
        $source_name = $config_RECRUTEUR_EMAIL_RELANCE_NAME_SRC;



        if($config_RECRUTEUR_SMTP_SECU == "ssl"){
            $recruteur_SMTP_host = "ssl://".$config_RECRUTEUR_SMTP_HOST;
        }elseif($config_RECRUTEUR_SMTP_SECU == "tls"){
            $recruteur_SMTP_host = "tls://".$config_RECRUTEUR_SMTP_HOST;
        }else{
            $recruteur_SMTP_host = $config_RECRUTEUR_SMTP_HOST;
        }
        $recruteur_SMTP_port = $config_RECRUTEUR_SMTP_PORT ;
        if($config_RECRUTEUR_SMTP_AUTH == "1"){
            $recruteur_SMTP_auth = true;
            $recruteur_SMTP_user = $config_RECRUTEUR_SMTP_USER;
            $recruteur_SMTP_pass = $config_RECRUTEUR_SMTP_PASS;
        }else{
            $recruteur_SMTP_auth = false;
            $recruteur_SMTP_user = null;
            $recruteur_SMTP_pass = null;
        }
        

        /*
         * On récupère le mail de relance en fonction de la langue du candidat
         */
        $sql2 = "
            SELECT
                `awa_mailsrelance`.*

            FROM
                `awa_mailsrelance`
            WHERE
                awa_mailsrelance.LANGUE = '".$_SESSION['awa_language']."'

            ORDER BY awa_mailsrelance.ID DESC
            LIMIT 0,1

            ";

        $select_mailrelance = $conn->prepare($sql2);
        $select_mailrelance->execute();

        $emailRelance = null;
        $emailRelance = $select_mailrelance->fetchObject();
        if($emailRelance ){
           
        }else{
            /*
             * On récupère le mail de relance dans la langue par défault
             */
            $sql2 = "
                SELECT
                    `awa_mailsrelance`.*

                FROM
                    `awa_mailsrelance`
                WHERE
                    awa_mailsrelance.`DEFAULT` = '1'

                ORDER BY awa_mailsrelance.ID DESC
                LIMIT 0,1

                ";

            $select_mailrelance = $conn->prepare($sql2);
            $select_mailrelance->execute();
            $emailRelance = null;
            $emailRelance = $select_mailrelance->fetchObject();
            
        }

        /*
         * On remplace les variables du mails
         */
        if(defined("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language'])){
            $url = "http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language']);
        }else{
            $url = "http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_"._CONFIG_DEFAULT_LANGUE_FO);
        }




        $mail_object = $emailRelance->OBJECT;
        $mail_object = str_replace("{firstname}",$candidat->PRENOM, $mail_object);
        $mail_object = str_replace("{name}",$candidat->NOM, $mail_object);
        $mail_object = str_replace("{login}",$candidat->LOGIN, $mail_object);
        $mail_object = str_replace("{password}",$candidat->PASSWORD, $mail_object);
        $mail_object = str_replace("{url_login}",$url, $mail_object);

        $mail_body = $emailRelance->BODY;
        $mail_body = str_replace("{firstname}",$candidat->PRENOM, $mail_body);
        $mail_body = str_replace("{name}",$candidat->NOM, $mail_body);
        $mail_body = str_replace("{login}",$candidat->LOGIN, $mail_body);
        $mail_body = str_replace("{password}",$candidat->PASSWORD, $mail_body);
        $mail_body = str_replace("{url_login}",$url, $mail_body);

        /*
         * envoi mail au candidat
         */
        if(_SEND_EMAIL){
            $mail = new PHPmailer();
            $mail->IsSMTP();
            //$mail->SMTPDebug = 2;
            $mail->SMTPAuth = $recruteur_SMTP_auth;
            $mail->Host = $recruteur_SMTP_host;
            $mail->Port = (int)$recruteur_SMTP_port;
            if($recruteur_SMTP_auth){
                $mail->Username = $recruteur_SMTP_user;
                $mail->Password = $recruteur_SMTP_pass;
            }
            $mail->From = $source_email;
            $mail->FromName = $source_name;
            $mail->AddAddress($candidat->LOGIN);
            $mail->AddReplyTo($source_email);

            $mail->Subject = utf8_decode($mail_object);
            $mail->Body = utf8_decode($mail_body);
            if(!$mail->Send()){ //Teste le return code de la fonction
              //echo $mail->ErrorInfo; //Affiche le message d'erreur (ATTENTION:voir section 7)
                Debug::d_echo("erreur lors de l'envoi du mail  ".$candidat->LOGIN, 2,"mdp-oublie.php");
            }
            else{
                $message = _MDPOUBLIE_MESSAGE_OK;
                $message2 = "";
                Debug::d_echo("envoi mail mdp oublié a  ".$candidat->LOGIN, 2,"mdp-oublie.php");
            }
            $mail->SmtpClose();
            unset($mail);
        }
        
    }else{
        Debug::d_echo("email non reconu ", 2,"mdp-oublie.php");
    }
}







?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title><?php echo _MDPOUBLIE_TITLE; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content=""/>
        <meta name="keywords" content=""/>
        <?php  if(!_USE_CUSTOM_CSS){ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?>getcss_custom.php" rel="stylesheet" type="text/css" />
        <?php }else{ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?><?php echo _TEMPLATE_FOLDER_NAME; ?>/custom.css" rel="stylesheet" type="text/css" />
        <?php } ?>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/framework.js"></script>
        <script type="text/javascript" >
            $.ajaxSettings.cache = false;
            $(document).ready(function(){
                <?php if(_IS_IFRAME_RESIZE){ ?>
                resizeiframe();
                <?php } ?>
            });

            function changeLanguage(language){
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-change_language.php", { 'language': language});
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-get_url.php",
                    { 'pageref':'mdpoublie', 'langue':language },
                    function(data){
                        window.location=data;
                    }
                );
            }

             function checkForm(){
               if($('#login').val() != ''){
                   if(!checkEmail($('#login').val())){
                       alert('<?php echo _CANDIDATURE_EX_EMAIL_JSERROR; ?>');
                       $('#login').focus();
                       return false;
                   }
               }else{
                   alert('<?php echo _CANDIDATURE_EX_EMAIL_JSERROR; ?>');
                   $('#login').focus();
                   return false;
               }

                $('#login_form').submit();
            }

            

        </script>
    </head>
    <body>
        <?php
        $templateDatas = array();

        $templateDatas['pathway'] = "";
        $pathway = array();
        $pathway[0]['text'] = _MDPOUBLIE_TITLE;
        $pathway[0]['url'] = null;
        $templateDatas['pathway'] =  displayPathway($pathway);

        $templateDatas['header'] = "";
        $templateDatas['header'] = displayHeader();

        $templateDatas['candidatArea'] = "";
        $templateDatas['candidatArea'] = displayCandidatArea($conn);

        $templateDatas['menu'] = "";
        $templateDatas['menu'] = displayMenu();

        //Footer
        $templateDatas['footerText'] = "";
        $templateDatas['footerText'] = displayFooter();
        ?>
        <?php include(_TEMPLATE_FOLDER_NAME."/tpl_mdp-oublie.php"); ?>
    </body>
</html>
<?php

//print_t($_SESSION);
//echo "<pre>";
//print_r($fonctions);
//echo "</pre>";
//echo "<pre>";
//print_r($secteurs);
//echo "</pre>";
?>