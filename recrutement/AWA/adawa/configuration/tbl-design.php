<?php
include_once "../include/display_errors_bo.php";
@session_start();

Debug::d_echo("acces ", 2,"tbl-design.php");
Debug::d_print_r($_GET, 1,"GET","tbl-design.php");
Debug::d_print_r($_POST, 1,"POST","tbl-design.php");
Debug::d_print_r($_SESSION, 1,"SESSION","tbl-design.php");


include_once('../../include/config.php');
@include_once '../include/pdo.php';

include_once "../include/framework.php";
include_once "../include/tableau.php";

$starttime = microtime();


/*
 * Variables communes
 */
$title = _CONF_DESIGN_PAGE_TITLE;
$tableauName = "design";
$filename = "tbl-design.php";
$numberOfRowsPerPage = 10;
//$numberOfRows = 0;
$pageNumber = 1;
$debugMode = 0;
$orderColId = null;
$order = null;
$criteriasArray = array();
$messageToDisplay = null;


/*
 * Control d'accès
 */
if(empty($_SESSION['boe_user_id'])){
    $_SESSION['boe_user_id'] = null;
}
if(empty ($_SESSION['boe_user_email'])){
    $_SESSION['boe_user_email'] = null;
}
test_session_boe($_SESSION['boe_user_id'], $_SESSION['boe_user_email'], "../", $conn);

/*
 * Traitement des données d'entrées ($_GET)
 */
//print_t($_GET);

if(isset($_GET['pageNumber'])){
    if((int)$_GET['pageNumber'] != 0){
        $pageNumber = (int)$_GET['pageNumber'];
    }
}

if(isset($_GET['numberOfRowsPerPage'])){
    if((int)$_GET['numberOfRowsPerPage'] != 0){
        $numberOfRowsPerPage = (int)$_GET['numberOfRowsPerPage'];
    }
}

if(isset($_GET['orderColId'])){
    if((int)$_GET['orderColId'] != 0){
        $orderColId = (int)$_GET['orderColId'];
    }
}

if(isset($_GET['order'])){
    if($_GET['order'] == "up"){
        $order = "up";
    }else{
        $order = "down";
    }
}

if(isset($_GET['msg'])){
   $messageToDisplay = $_GET['msg'];
}

$criteriaId = 0;
while(isset($_GET['crit'.$criteriaId])){
    $criteriasArray[$criteriaId] = $_GET['crit'.$criteriaId];
    $criteriaId++;
}


/*
 * Init tableau
 */
$tableau = new Tableau($tableauName, $filename);
$tableau->setNumberOfRowsPerPage($numberOfRowsPerPage);
$tableau->setPageNumber($pageNumber);
$tableau->tableTitle = $title;
$tableau->messageToDisplay = $messageToDisplay;


/*
 * Définition des boutons
 */
$tblbutton = new TblButton();
$tblbutton->buttonLabel = _CONF_DESIGN_TBL_ADD_BTN;
$tblbutton->altText = _CONF_DESIGN_TBL_ADD_BTN_ALT;
$tblbutton->buttonpicture = "../images/folder_new.png";
$tblbutton->popupMode = true;
$tblbutton->needMultipleSelection = false;
$tblbutton->link = "v-add-design.php?";
$tblbutton->popupWidth = 540;
$tblbutton->popupTitle = _CONF_DESIGN_TBL_ADD_BTN_POPUP_TITLE;
$tableau->addTblButton($tblbutton);




/*
 * Définition des critères de filtrage
 */

/*
 * Définition des columns du tableau
 */
$colheader = array();
//Checkbox
//$colheader[]= array("content"=>"",
//                    "align"=>"",
//                    "dbaColumnName"=>"",
//                    "orderable"=>"0",
//                    "type"=>"checkbox");
//
$colheader[]= array("content"=>_CONF_DESIGN_TBL_COL_TITRE,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

$colheader[]= array("content"=>_CONF_DESIGN_TBL_COL_DATE_CREATION,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

$colheader[]= array("content"=>_CONF_DESIGN_TBL_COL_DATE_UPDATE,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");
//
$colheader[]= array("content"=>_CONF_DESIGN_TBL_COL_ACTIF,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

$colheader[]= array("content"=>"",
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");


//Création des columns
$tableau->setHeaderRow($colheader);

//définition de l'ordre sur la column selectionnée
if($orderColId != null){
    $tableau->setOrderedColumn($orderColId, $order);
}





/*
 * Requete sql des Ã©lÃ©ments Ã  afficher
 */
//Traitement de l'ordre d'affichage
$orderSql = "";
$orderByTBLName = $tableau->getOrderSQLColumnName();
//Génération du order sql
if($orderByTBLName){
    if($orderSql == ""){
        $orderSql = "ORDER BY `".$orderByTBLName."`";
        if($order == "up"){
            $orderSql .= " ASC";
        }else{
            $orderSql .= " DESC";
        }
    }
}

//Traitement manuel des filtres


/*
 * Requête SQL de comptage
 */
//Récupération de l'id client
$sqlcount = "   SELECT
                    COUNT(fo.ID)
                    
                FROM
                    `awa_designs` AS fo
                
                ".$tableau->getFilterWhereSQL("")."
            ";
//print_t($sqlcount) ;
$select = $conn->prepare($sqlcount);
$select->execute();
$tmp  = $select->fetch();
$nbOfSQLrows = $tmp[0];
//Envoi dans le tableau le nombre de row dans la dba
$tableau->setNumberOfSQLRows($nbOfSQLrows);
//Calcul de la condition limit pour le requete sql
$limit = ($pageNumber - 1) * $numberOfRowsPerPage;

//Requete principal
$sql = "SELECT
            `fo`.*
            
            
        FROM
            `awa_designs` AS `fo`
        

        ".$tableau->getFilterWhereSQL("")."
        GROUP BY `fo`.`ID`
        
        LIMIT ".$limit.",".$numberOfRowsPerPage;

//print_t($sql);
$select = $conn->prepare($sql);
$select->execute();


/*
 * Remplissage du tableau
 */
while($row = $select->fetchObject()){
    //echo $row->id;
    $colrow = array();
    $colrow[]= array("content"=>"<a class=\"orange\" href=\"".$tableau->getAjaxPopupLink('v-add-design.php?element_id='.$row->ID,540,_CONF_DESIGN_TBL_COL_TITRE_ACT_POPUP)."\" title=\""._CONF_DESIGN_TBL_COL_TITRE_ACT_ALT."\">".$row->TITRE."</a>","align"=>"");
    $colrow[]= array("content"=>getDateDisplay($row->DATE_CREATION,'-'),"align"=>"");
    $colrow[]= array("content"=>getDateDisplay($row->DATE_UPDATE,'-'),"align"=>"");
    if($row->ACTIVE){
        $actifimg = "<img src='../images/check.png'/ height='16' title=\"\" alt=\"\"/>";
    }else{
        $actifimg = "<img src='../images/uncheck.png'/ height='16' title=\""._CONF_DESIGN_TBL_COL_ACTIF_ACT_ALT."\" alt=\"Activer\"/>";
    }
    $colrow[]= array("content"=>"<a href=\"javascript:actionAjax_".$tableau->tablename."('act-design.php?action=swthactif&id=".$row->ID."','',null);\">".$actifimg."</a>","align"=>"");
    $colrow[]= array("content"=>"<a href=\"javascript:actionAjax_".$tableau->tablename."('act-design.php?action=del&id=".$row->ID."','".urlencode($row->TITRE." "._CONF_DESIGN_TBL_COL_DEL_ACT_RETURN)."','"._CONF_DESIGN_TBL_COL_DEL_ACT_ASK." ".$row->TITRE."');\" title=\""._CONF_DESIGN_TBL_COL_DEL_ACT_ALT."\"><img src='../images/trash.png'/ height='16' title=\""._CONF_DESIGN_TBL_COL_DEL_ACT_ALT."\" alt=\""._CONF_DESIGN_TBL_COL_DEL_ACT_ALT."\"/></a>","align"=>"");

    $tableau->addRow($colrow);
}


/*
 * Affichage du tableau
 */ 
echo $tableau->displayHTML();
//print_t($_SESSION[]);
//print_t($fcriteria1);
//print_t($tableau);

//print_t($_SESSION);


$endtime = microtime();
//echo($endtime - $starttime);
?>