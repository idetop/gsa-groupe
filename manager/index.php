<?php

include('system/main_config.php');

$part = $_REQUEST['part'];
$page = $_REQUEST['page'];
if (empty($page)) { $page = "accueil"; }

session_start();

// Déconnexion
if ($_REQUEST['action'] == "logoff") {
    session_start();
    session_unregister("login");
    session_unset();
    session_destroy();
    header('location:index.php?page=login');
}

// Non-connecté
if (! isset($_SESSION["login"]) || $_SESSION["login"] == "") {
    // Si le formulaire login n'a pas encore été soumis
    if (! isset($_REQUEST['login'])) {
        include($b_templates . 'login.php');
    }
    // Login soumis, on vérifie les infos
    else {
        require_once(__DIR__ . '/../mysql/mysql.php');
        $pass_login = md5($_REQUEST['pwd']);
        $sql_login  = mysqli_query($connexion, "SELECT * FROM gsa_admin WHERE login = '" . $_REQUEST['login'] . "'") or die('Erreur !<br>' . $sql . '<br>' . mysqli_error());
        $data_login = mysqli_fetch_array($sql_login);
        // ERREUR
        if ($data_login['pwd'] != $pass_login) {
            mysqli_close($connexion);;
            header('location:index.php?page=login&error=1');
            exit();
        }
        // OK
        else {
            session_start();
            $_SESSION['login'] = $_REQUEST['login'];
            $_SESSION['id'] = $data_login['id'];
            mysqli_close($connexion);;
            header('location:index.php?page=accueil');
        }
    }
}

// Connecté
else {
    session_start();
    $sql_login  = mysqli_query($connexion, "SELECT * FROM gsa_admin WHERE login = '".$_SESSION['login']."'") or die('Erreur !<br>'.$sql.'<br>' . mysqli_error());
    $data_login = mysqli_fetch_array($sql_login);
    $login_user = $data_login['login'];
    $name_user  = $data_login['nom'];
    $id_user    = $data_login['id'];
    $statut     = $data_login['pole'];
    include('system/main_config.php');
    include($b_controllers.'globals.php');
    include($b_controllers.$page.'.php');
    include($b_controllers.'title.php');
    include($b_templates.'header.php');
    include($b_templates.$page.'.php');
    include($b_templates.'footer.php');
    mysqli_close($connexion);;
}
