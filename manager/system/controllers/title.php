<?php
$title_all = "Idetop Manager - ".$site;
$title_loading = "Chargement";

// ONGLET ACCUEIL
if($page == "accueil") {
    $title_document = "Accueil - ".$title_all;
}
if($page == "user") {
    $title_document = "Administrateurs - ".$title_all;
}

// QUI SOMMES-NOUS
if($page == "qui-sommes-nous" && $part == 'qui'){
    $title_document = "Qui sommes-nous : slider introduction - ".$title_all; 
}
if($page == "qui-sommes-nous" && $part == 'ambition'){
    $title_document = "Qui sommes-nous : notre ambition - ".$title_all; 
}
if($page == "qui-sommes-nous" && $part == 'hommes'){
    $title_document = "Les hommes et les femmes cl�s du groupe - ".$title_all; 
}
if($page == "qui-sommes-nous" && $part == 'historique'){
    $title_document = "Historique - ".$title_all; 
}

// GSA FORMATION
if($page == "gsa-formation" && $part == 'accueil'){
    $title_document = "GSA FORMATION : texte d'accueil - ".$title_all; 
    $title_page = "GSA FORMATION : texte d'accueil";
}
if($page == "gsa-formation" && $part == 'ecole-visite-medicale'){
    $title_document = "GSA FORMATION : Ecole de visite m�dicale - ".$title_all; 
    $title_page = "GSA FORMATION : Ecole de visite m�dicale";
}
if($page == "gsa-formation" && $part == 'formation-intra-entreprise'){
    $title_document = "GSA FORMATION : Formation intra-entreprise - ".$title_all; 
    $title_page = "GSA FORMATION : Formation intra-entreprise";
}
if($page == "gsa-formation" && $part == 'coaching-mode-emploi'){
    $title_document = "GSA FORMATION : Coaching mode d'emploi - ".$title_all; 
    $title_page = "GSA FORMATION : Coaching mode d'emploi";
}
if($page == "gsa-formation" && $part == 'formation-expert-apm-pharmacie'){
    $title_document = "GSA FORMATION : Formation expert pma pharmacie - ".$title_all; 
    $title_page = "GSA FORMATION : Formation expert pma pharmacie";
}
if($page == "gsa-formation" && $part == 'animation-formation-medicale'){
    $title_document = "GSA FORMATION : Animation Formation M�dicale - ".$title_all; 
    $title_page = "GSA FORMATION : Animation Formation M�dicale";
}
if($page == "gsa-formation" && $part == 'engagements'){
    $title_document = "GSA FORMATION : Engagements - ".$title_all; 
    $title_page = "GSA FORMATION : Engagements";
}

// GSA PHARMA
if($page == "gsa-pharma" && $part == 'accueil'){
    $title_document = "GSA PHARMA : texte d'accueil - ".$title_all; 
    $title_page = "GSA PHARMA : texte d'accueil";
}
if($page == "gsa-pharma" && $part == 'conseil-strategie-commerciale'){
    $title_document = "GSA PHARMA : Conseil en strat�gie commerciale - ".$title_all; 
    $title_page = "GSA PHARMA : Conseil en strat�gie commerciale";
}
if($page == "gsa-pharma" && $part == 'environnement-legislation'){
    $title_document = "GSA PHARMA : Environnement et l�gislation - ".$title_all; 
    $title_page = "GSA PHARMA : Environnement et l�gislation";
}
if($page == "gsa-pharma" && $part == 'methodologie'){
    $title_document = "GSA PHARMA : M�thodologie Mode d'emploi �thique et responsabilit� - ".$title_all; 
    $title_page = "GSA PHARMA : M�thodologie Mode d'emploi �thique et responsabilit�";
}
if($page == "gsa-pharma" && $part == 'reseaux-exclusifs'){
    $title_document = "GSA PHARMA : R�seaux exclusifs vente directe - ".$title_all; 
    $title_page = "GSA PHARMA : R�seaux exclusifs vente directe";
}
if($page == "gsa-pharma" && $part == 'reseaux-multi-produits'){
    $title_document = "GSA PHARMA : R�seaux multi-produits - ".$title_all; 
    $title_page = "GSA PHARMA : R�seaux multi-produits";
}
if($page == "gsa-pharma" && $part == 'engagements'){
    $title_document = "GSA PHARMA : Engagements - ".$title_all; 
    $title_page = "GSA PHARMA : Engagements";
}

// GSA RECRUTEMENT
if($page == "gsa-recrutement" && $part == 'accueil'){
    $title_document = "GSA RECRUTEMENT : texte d'accueil - ".$title_all; 
    $title_page = "GSA RECRUTEMENT : texte d'accueil";
}
if($page == "gsa-recrutement" && $part == 'cabinet'){
    $title_document = "GSA RECRUTEMENT : Cabinet de recrutement mode d'emploi - ".$title_all; 
    $title_page = "GSA RECRUTEMENT : Cabinet de recrutement mode d'emploi";
}
if($page == "gsa-recrutement" && $part == 'entreprises'){
    $title_document = "GSA RECRUTEMENT : Espace entreprises - ".$title_all; 
    $title_page = "GSA RECRUTEMENT : Espace entreprises";
}
if($page == "gsa-recrutement" && $part == 'candidat'){
    $title_document = "GSA RECRUTEMENT : Espace candidats - ".$title_all; 
    $title_page = "GSA RECRUTEMENT : Espace candidats";
}
if($page == "gsa-recrutement" && $part == 'recrutement'){
    $title_document = "GSA RECRUTEMENT : Recrutement de profils de haute comp�tence - ".$title_all; 
    $title_page = "GSA RECRUTEMENT : Recrutement de profils de haute comp�tence";
}
if($page == "gsa-recrutement" && $part == 'engagements'){
    $title_document = "GSA RECRUTEMENT : Engagements - ".$title_all; 
    $title_page = "GSA RECRUTEMENT : Engagements";
}

// GSA MEDICAL
if($page == "gsa-medical" && $part == 'accueil'){
    $title_document = "GSA MEDICAL : texte d'accueil - ".$title_all; 
    $title_page = "GSA MEDICAL : texte d'accueil";
}
if($page == "gsa-medical" && $part == 'conseil-strategie-operationnelle'){
    $title_document = "GSA MEDICAL : Conseil en strat�gie op�rationnelle - ".$title_all; 
    $title_page = "GSA MEDICAL : Conseil en strat�gie op�rationnelle";
}
if($page == "gsa-medical" && $part == 'qualite-certification'){
    $title_document = "GSA MEDICAL : Qualit� et certification - ".$title_all; 
    $title_page = "GSA MEDICAL : Qualit� et certification";
}
if($page == "gsa-medical" && $part == 'methodologie'){
    $title_document = "GSA MEDICAL : M�thodologie mode d'emploi - ".$title_all; 
    $title_page = "GSA MEDICAL : M�thodologie mode d'emploi";
}
if($page == "gsa-medical" && $part == 'reseaux-exclusifs'){
    $title_document = "GSA MEDICAL : R�seaux exclusifs - ".$title_all; 
    $title_page = "GSA MEDICAL : R�seaux exclusifs";
}
if($page == "gsa-medical" && $part == 'reseaux-multi-produits'){
    $title_document = "GSA MEDICAL : R�seaux multi-produits - ".$title_all; 
    $title_page = "GSA MEDICAL : R�seaux multi-produits";
}
if($page == "gsa-medical" && $part == 'engagements'){
    $title_document = "GSA MEDICAL : Engagements - ".$title_all; 
    $title_page = "GSA MEDICAL : Engagements";
}
?>