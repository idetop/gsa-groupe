<?php

$lang			= "fr";
$date 			= date("d/m/Y");
$year			= date('Y');
$heure 			= date("H:i");
$time 			= time();
$site 			= "http://gsa-groupe.fr";

$c_url			= $site  . "/manager";
$b_img 			= $c_url . "/i/img/";
$b_js 			= $c_url . "/i/js/";
$b_css 			= $c_url . "/i/css/";
$b_templates    = "system/templates/";
$b_controllers  = "system/controllers/";
$b_libs 		= "system/libs/";
$b_helpers 		= "system/helpers/";
$b_classes 		= "system/classes/";

$meta_file      = "system/meta.php";

require_once(__DIR__ . '/../../mysql/mysql.php');
