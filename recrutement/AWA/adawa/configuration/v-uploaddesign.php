<?php
include_once "../include/display_errors_bo.php";
@session_start();

Debug::d_echo("acces ", 2,"v-uploaddesign.php");
Debug::d_print_r($_GET, 1,"GET","v-uploaddesign.php");
Debug::d_print_r($_POST, 1,"POST","v-uploaddesign.php");
Debug::d_print_r($_SESSION, 1,"SESSION","v-uploaddesign.php");


include_once('../../include/config.php');
@include_once '../include/pdo.php';
include_once "../include/framework.php";
include_once "../include/tableau.php";

$starttime = microtime();

/*
 * Variables communes
 */
$errorMsg = "";
$returnMsg = "";
$formsubmit = false;        //true après enregistrement du formulaire
$element_id = null;         //null si add et non null si edit
$edit = false;              //true si edit, false si add

/*
 * Control d'accès
 */
if(empty($_SESSION['boe_user_id'])){
    $_SESSION['boe_user_id'] = null;
}
if(empty ($_SESSION['boe_user_email'])){
    $_SESSION['boe_user_email'] = null;
}
test_session_boe($_SESSION['boe_user_id'], $_SESSION['boe_user_email'], "../", $conn);


/*
 * Traitement des données d'entrées ($_GET)
 */
if(!empty($_GET['element_id'])){
    $element_id = $_GET['element_id'];
    Debug::d_echo("edition design  ".$element_id, 2,"v-uploaddesign.php");
}else{
    if(!count($_POST)){
        Debug::d_echo("ajout design  ", 2,"v-uploaddesign.php");
    }
}

$design_id = "";
$design_titre = "";
$design_actif = "0";
$design_HEADER_LOGO = "";
$design_BODY_POLICE = "'Trebuchet MS','Trebuchet','Aria',sans-serif";
$design_BODY_COLOR = "#58585A";
$design_DATA_POLICE = "'Trebuchet MS','Trebuchet','Aria',sans-serif";
$design_PATHWAY_COLOR = "#A0A0A0";
$design_PATHWAY_COLOR_HOVER = "#E67200";
$design_CANDIDAT_AREA_COLOR = "#A0A0A0";
$design_CANDIDAT_AREA_COLOR_HOVER = "#E67200";
$design_MENU_COLOR = "#00A8DD";
$design_MENU_COLOR_HOVER = "#E67200";
$design_CONTENT_VERTICAL_BAR = "#424DA6";
$design_CONTENT_HORIZONTAL_BAR = "#424DA6";
$design_CONTENT_TITRE1_COLOR = "#424DA6";
$design_CONTENT_BUTTON_COLOR = "#424DA6";
$design_CONTENT_LABEL_COLOR = "#58585A";
$design_CONTENT_INPUT_COLOR = "#58585A";
$design_CONTENT_LISTE_INTITULE_COLOR = "#00A8DD";
$design_CONTENT_LISTE_INTITULE_COLOR_HOVER = "#E67200";
$design_CONTENT_LISTE_DATE_COLOR = "#00A8DD";
$design_CONTENT_LISTE_CONTRAT_COLOR = "#A0A0A0";
$design_CONTENT_LISTE_LOCATION_COLOR = "#A0A0A0";
$design_CONTENT_LISTE_DESC_COLOR = "#58585A";
$design_CONTENT_LISTE_PAGINATION_COLOR = "#58585A";
$design_CONTENT_LISTE_PAGINATION_COLOR_HOVER = "#E67200";
$design_CONTENT_FICHE_DESC_COLOR = "#58585A";
$design_CONTENT_FICHE_MISSION_TITLE_COLOR = "#00A8DD";
$design_CONTENT_FICHE_MISSION_COLOR = "#58585A";
$design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR = "#00A8DD";
$design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR = "#424DA6";
$design_CONTENT_FICHE_CUSTOMER_COLOR = "#58585A";
$design_CONTENT_FICHE_INFO_TITLE_COLOR = "#00A8DD";
$design_CONTENT_FICHE_INFO_BORDER_COLOR = "#424DA6";
$design_CONTENT_FICHE_INFO_COLOR = "#A0A0A0";
$design_FORM_COMPULSORY_STAR_COLOR = "red";
$design_FORM_LABEL_COLOR = "#58585A";
$design_FORM_INPUT_COLOR = "#58585A";
$design_FORM_BLOC_TITLE_COLOR = "#424DA6";
$design_FORM_ADD_COLOR = "#00A8DD";
$design_FORM_ADD_HOVER_COLOR = "#E67200";
$design_FORM_DEL_COLOR = "#00A8DD";
$design_FORM_DEL_COLOR_HOVER = "#E67200";
$design_FOOTER_COLOR = "#A0A0A0";
$design_FOOTER_HOVER_COLOR = "#E67200";

if(!empty($element_id)){
    $sql = "
        SELECT
            `fo`.*

        FROM
            `awa_designs` AS `fo`
        WHERE
            fo.ID =:idfo

        ";

    $select = $conn->prepare($sql);
    $select->bindParam(':idfo', $element_id, PDO::PARAM_INT);
    $select->execute();
    $designobj = null;
    $designobj = $select->fetchObject();
    if($designobj){
        
        $design_id = $designobj->ID;
        $design_titre = $designobj->TITRE;
        $design_actif = $designobj->ACTIVE;
        $design_HEADER_LOGO = $designobj->HEADER_LOGO;
        $design_BODY_POLICE = $designobj->DATA_POLICE;
        $design_BODY_COLOR = $designobj->BODY_COLOR;
        $design_DATA_POLICE = $designobj->DATA_POLICE;
        $design_PATHWAY_COLOR = $designobj->PATHWAY_COLOR;
        $design_PATHWAY_COLOR_HOVER = $designobj->PATHWAY_COLOR_HOVER;
        $design_CANDIDAT_AREA_COLOR = $designobj->CANDIDAT_AREA_COLOR;
        $design_CANDIDAT_AREA_COLOR_HOVER = $designobj->CANDIDAT_AREA_COLOR_HOVER;
        $design_MENU_COLOR = $designobj->MENU_COLOR;
        $design_MENU_COLOR_HOVER = $designobj->MENU_COLOR_HOVER;
        $design_CONTENT_VERTICAL_BAR = $designobj->CONTENT_VERTICAL_BAR;
        $design_CONTENT_HORIZONTAL_BAR = $designobj->CONTENT_HORIZONTAL_BAR;
        $design_CONTENT_TITRE1_COLOR = $designobj->CONTENT_TITRE1_COLOR;
        $design_CONTENT_BUTTON_COLOR = $designobj->CONTENT_BUTTON_COLOR;
        $design_CONTENT_LABEL_COLOR = $designobj->CONTENT_LABEL_COLOR;
        $design_CONTENT_INPUT_COLOR = $designobj->CONTENT_INPUT_COLOR;
        $design_CONTENT_LISTE_INTITULE_COLOR = $designobj->CONTENT_LISTE_INTITULE_COLOR;
        $design_CONTENT_LISTE_INTITULE_COLOR_HOVER = $designobj->CONTENT_LISTE_INTITULE_COLOR_HOVER;
        $design_CONTENT_LISTE_DATE_COLOR = $designobj->CONTENT_LISTE_DATE_COLOR;
        $design_CONTENT_LISTE_CONTRAT_COLOR = $designobj->CONTENT_LISTE_CONTRAT_COLOR;
        $design_CONTENT_LISTE_LOCATION_COLOR = $designobj->CONTENT_LISTE_LOCATION_COLOR;
        $design_CONTENT_LISTE_DESC_COLOR = $designobj->CONTENT_LISTE_DESC_COLOR;
        $design_CONTENT_LISTE_PAGINATION_COLOR = $designobj->CONTENT_LISTE_PAGINATION_COLOR;
        $design_CONTENT_LISTE_PAGINATION_COLOR_HOVER = $designobj->CONTENT_LISTE_PAGINATION_COLOR_HOVER;
        $design_CONTENT_FICHE_DESC_COLOR = $designobj->CONTENT_FICHE_DESC_COLOR;
        $design_CONTENT_FICHE_MISSION_TITLE_COLOR = $designobj->CONTENT_FICHE_MISSION_TITLE_COLOR;
        $design_CONTENT_FICHE_MISSION_COLOR = $designobj->CONTENT_FICHE_MISSION_COLOR;
        $design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR = $designobj->CONTENT_FICHE_CUSTOMER_TITLE_COLOR;
        $design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR = $designobj->CONTENT_FICHE_CUSTOMER_BORDER_COLOR;
        $design_CONTENT_FICHE_CUSTOMER_COLOR = $designobj->CONTENT_FICHE_CUSTOMER_COLOR;
        $design_CONTENT_FICHE_INFO_TITLE_COLOR = $designobj->CONTENT_FICHE_INFO_TITLE_COLOR;
        $design_CONTENT_FICHE_INFO_BORDER_COLOR = $designobj->CONTENT_FICHE_INFO_BORDER_COLOR;
        $design_CONTENT_FICHE_INFO_COLOR = $designobj->CONTENT_FICHE_INFO_COLOR;
        $design_FORM_COMPULSORY_STAR_COLOR = $designobj->FORM_COMPULSORY_STAR_COLOR;
        $design_FORM_LABEL_COLOR = $designobj->FORM_LABEL_COLOR;
        $design_FORM_INPUT_COLOR = $designobj->FORM_INPUT_COLOR;
        $design_FORM_BLOC_TITLE_COLOR = $designobj->FORM_BLOC_TITLE_COLOR;
        $design_FORM_ADD_COLOR = $designobj->FORM_ADD_COLOR;
        $design_FORM_ADD_HOVER_COLOR = $designobj->FORM_ADD_HOVER_COLOR;
        $design_FORM_DEL_COLOR = $designobj->FORM_DEL_COLOR;
        $design_FORM_DEL_COLOR_HOVER = $designobj->FORM_DEL_COLOR_HOVER;
        $design_FOOTER_COLOR = $designobj->FOOTER_COLOR;
        $design_FOOTER_HOVER_COLOR = $designobj->FOOTER_HOVER_COLOR;
    }


}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="../css/boe_style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../include/framework.js"></script>
	<script type="text/javascript" src="../include/jquery-1.4.2.js"></script>
        <script type="text/javascript" >
          $.ajaxSettings.cache = false;
        </script>
    </head>
    <body style="background-color:#FAFAFA;">
<?php
if(count($_POST)){
//   print_t($_POST);
//   print_t($_FILES);
   
   if(!empty($_POST['id'])){
        //Update
        $logo_filename = "";
        if(!empty ($_FILES['design_HEADER_LOGO_N']['name'])){
            move_uploaded_file($_FILES['design_HEADER_LOGO_N']['tmp_name'], "../../images/logo/".$_FILES['design_HEADER_LOGO_N']['name']);
            $logo_filename = $_FILES['design_HEADER_LOGO_N']['name'];
        }else{
           $logo_filename = $_POST['design_HEADER_LOGO'];
        }


        $sql = "
            UPDATE
                awa_designs
            SET
                TITRE =:TITRE,
                ACTIVE =:ACTIVE,
                DATE_UPDATE =:DATE_UPDATE,
                HEADER_LOGO =:HEADER_LOGO,
                BODY_POLICE =:BODY_POLICE,
                BODY_COLOR =:BODY_COLOR,
                DATA_POLICE =:DATA_POLICE,
                PATHWAY_COLOR =:PATHWAY_COLOR,
                PATHWAY_COLOR_HOVER =:PATHWAY_COLOR_HOVER,
                CANDIDAT_AREA_COLOR =:CANDIDAT_AREA_COLOR,
                CANDIDAT_AREA_COLOR_HOVER =:CANDIDAT_AREA_COLOR_HOVER,
                MENU_COLOR =:MENU_COLOR,
                MENU_COLOR_HOVER =:MENU_COLOR_HOVER,
                CONTENT_VERTICAL_BAR =:CONTENT_VERTICAL_BAR,
                CONTENT_HORIZONTAL_BAR =:CONTENT_HORIZONTAL_BAR,
                CONTENT_TITRE1_COLOR =:CONTENT_TITRE1_COLOR,
                CONTENT_BUTTON_COLOR =:CONTENT_BUTTON_COLOR,
                CONTENT_LABEL_COLOR =:CONTENT_LABEL_COLOR,
                CONTENT_INPUT_COLOR =:CONTENT_INPUT_COLOR,
                CONTENT_LISTE_INTITULE_COLOR =:CONTENT_LISTE_INTITULE_COLOR,
                CONTENT_LISTE_INTITULE_COLOR_HOVER =:CONTENT_LISTE_INTITULE_COLOR_HOVER,
                CONTENT_LISTE_DATE_COLOR =:CONTENT_LISTE_DATE_COLOR,
                CONTENT_LISTE_CONTRAT_COLOR =:CONTENT_LISTE_CONTRAT_COLOR,
                CONTENT_LISTE_LOCATION_COLOR =:CONTENT_LISTE_LOCATION_COLOR,
                CONTENT_LISTE_DESC_COLOR =:CONTENT_LISTE_DESC_COLOR,
                CONTENT_LISTE_PAGINATION_COLOR =:CONTENT_LISTE_PAGINATION_COLOR,
                CONTENT_LISTE_PAGINATION_COLOR_HOVER =:CONTENT_LISTE_PAGINATION_COLOR_HOVER,
                CONTENT_FICHE_DESC_COLOR =:CONTENT_FICHE_DESC_COLOR,
                CONTENT_FICHE_MISSION_TITLE_COLOR =:CONTENT_FICHE_MISSION_TITLE_COLOR,
                CONTENT_FICHE_MISSION_COLOR =:CONTENT_FICHE_MISSION_COLOR,
                CONTENT_FICHE_CUSTOMER_TITLE_COLOR =:CONTENT_FICHE_CUSTOMER_TITLE_COLOR,
                CONTENT_FICHE_CUSTOMER_BORDER_COLOR =:CONTENT_FICHE_CUSTOMER_BORDER_COLOR,
                CONTENT_FICHE_CUSTOMER_COLOR =:CONTENT_FICHE_CUSTOMER_COLOR,
                CONTENT_FICHE_INFO_TITLE_COLOR =:CONTENT_FICHE_INFO_TITLE_COLOR,
                CONTENT_FICHE_INFO_BORDER_COLOR =:CONTENT_FICHE_INFO_BORDER_COLOR,
                CONTENT_FICHE_INFO_COLOR =:CONTENT_FICHE_INFO_COLOR,
                FORM_COMPULSORY_STAR_COLOR =:FORM_COMPULSORY_STAR_COLOR,
                FORM_LABEL_COLOR =:FORM_LABEL_COLOR,
                FORM_INPUT_COLOR =:FORM_INPUT_COLOR,
                FORM_BLOC_TITLE_COLOR =:FORM_BLOC_TITLE_COLOR,
                FORM_ADD_COLOR =:FORM_ADD_COLOR,
                FORM_ADD_HOVER_COLOR =:FORM_ADD_HOVER_COLOR,
                FORM_DEL_COLOR =:FORM_DEL_COLOR,
                FORM_DEL_COLOR_HOVER =:FORM_DEL_COLOR_HOVER,
                FOOTER_COLOR =:FOOTER_COLOR,
                FOOTER_HOVER_COLOR =:FOOTER_HOVER_COLOR
            WHERE
                ID =:idfo

            ";

        $select = $conn->prepare($sql);
        $select->bindParam(':idfo', $_POST['id'], PDO::PARAM_INT);

        $select->bindParam(':TITRE', $_POST['design_titre'], PDO::PARAM_STR);
        $select->bindParam(':ACTIVE', $_POST['design_actif'], PDO::PARAM_INT);
        $updateDate = date("Y-m-d");
        $select->bindParam(':DATE_UPDATE', $updateDate, PDO::PARAM_STR);

        $select->bindParam(':HEADER_LOGO', $logo_filename, PDO::PARAM_STR);
        $select->bindParam(':BODY_POLICE', $_POST['design_BODY_POLICE'], PDO::PARAM_STR);
        $select->bindParam(':BODY_COLOR', $_POST['design_BODY_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':DATA_POLICE', $_POST['design_DATA_POLICE'], PDO::PARAM_STR);
        $select->bindParam(':PATHWAY_COLOR', $_POST['design_PATHWAY_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':PATHWAY_COLOR_HOVER', $_POST['design_PATHWAY_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':CANDIDAT_AREA_COLOR', $_POST['design_CANDIDAT_AREA_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CANDIDAT_AREA_COLOR_HOVER', $_POST['design_CANDIDAT_AREA_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':MENU_COLOR', $_POST['design_MENU_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':MENU_COLOR_HOVER', $_POST['design_MENU_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_VERTICAL_BAR', $_POST['design_CONTENT_VERTICAL_BAR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_HORIZONTAL_BAR', $_POST['design_CONTENT_HORIZONTAL_BAR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_TITRE1_COLOR', $_POST['design_CONTENT_TITRE1_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_BUTTON_COLOR', $_POST['design_CONTENT_BUTTON_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LABEL_COLOR', $_POST['design_CONTENT_LABEL_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_INPUT_COLOR', $_POST['design_CONTENT_INPUT_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_INTITULE_COLOR', $_POST['design_CONTENT_LISTE_INTITULE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_INTITULE_COLOR_HOVER', $_POST['design_CONTENT_LISTE_INTITULE_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_DATE_COLOR', $_POST['design_CONTENT_LISTE_DATE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_CONTRAT_COLOR', $_POST['design_CONTENT_LISTE_CONTRAT_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_LOCATION_COLOR', $_POST['design_CONTENT_LISTE_LOCATION_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_PAGINATION_COLOR', $_POST['design_CONTENT_LISTE_PAGINATION_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_PAGINATION_COLOR_HOVER', $_POST['design_CONTENT_LISTE_PAGINATION_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_DESC_COLOR', $_POST['design_CONTENT_LISTE_DESC_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_DESC_COLOR', $_POST['design_CONTENT_FICHE_DESC_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_MISSION_TITLE_COLOR', $_POST['design_CONTENT_FICHE_MISSION_TITLE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_MISSION_COLOR', $_POST['design_CONTENT_FICHE_MISSION_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_CUSTOMER_TITLE_COLOR', $_POST['design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_CUSTOMER_COLOR', $_POST['design_CONTENT_FICHE_CUSTOMER_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_CUSTOMER_BORDER_COLOR', $_POST['design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_INFO_TITLE_COLOR', $_POST['design_CONTENT_FICHE_INFO_TITLE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_INFO_COLOR', $_POST['design_CONTENT_FICHE_INFO_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_INFO_BORDER_COLOR', $_POST['design_CONTENT_FICHE_INFO_BORDER_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_COMPULSORY_STAR_COLOR', $_POST['design_FORM_COMPULSORY_STAR_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_LABEL_COLOR', $_POST['design_FORM_LABEL_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_INPUT_COLOR', $_POST['design_FORM_INPUT_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_BLOC_TITLE_COLOR', $_POST['design_FORM_BLOC_TITLE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_ADD_COLOR', $_POST['design_FORM_ADD_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_ADD_HOVER_COLOR', $_POST['design_FORM_ADD_HOVER_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_DEL_COLOR', $_POST['design_FORM_DEL_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_DEL_COLOR_HOVER', $_POST['design_FORM_DEL_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':FOOTER_COLOR', $_POST['design_FOOTER_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FOOTER_HOVER_COLOR', $_POST['design_FOOTER_HOVER_COLOR'], PDO::PARAM_STR);

        $select->execute();

        Debug::d_echo("sauvegarde design  ".$_POST['id'], 2,"v-uploaddesign.php");

        echo "<script type='text/javascript' >";
        echo " parent.closePopup_design('".urlencode(_CONF_DESIGN_SAVE_MSG)."')";
        echo "</script>";
   }else{
        //Insert


       $logo_filename = "";
        if(!empty ($_FILES['design_HEADER_LOGO_N']['name'])){
            move_uploaded_file($_FILES['design_HEADER_LOGO_N']['tmp_name'], "../../images/logo/".$_FILES['design_HEADER_LOGO_N']['name']);
            $logo_filename = $_FILES['design_HEADER_LOGO_N']['name'];
        }

        
        $sql = "
            INSERT INTO
                awa_designs
            SET
                TITRE =:TITRE,
                ACTIVE =:ACTIVE,
                DATE_UPDATE =:DATE_UPDATE,
                DATE_CREATION =:DATE_CREATION,
                HEADER_LOGO =:HEADER_LOGO,
                BODY_POLICE =:BODY_POLICE,
                BODY_COLOR =:BODY_COLOR,
                DATA_POLICE =:DATA_POLICE,
                PATHWAY_COLOR =:PATHWAY_COLOR,
                PATHWAY_COLOR_HOVER =:PATHWAY_COLOR_HOVER,
                CANDIDAT_AREA_COLOR =:CANDIDAT_AREA_COLOR,
                CANDIDAT_AREA_COLOR_HOVER =:CANDIDAT_AREA_COLOR_HOVER,
                MENU_COLOR =:MENU_COLOR,
                MENU_COLOR_HOVER =:MENU_COLOR_HOVER,
                CONTENT_VERTICAL_BAR =:CONTENT_VERTICAL_BAR,
                CONTENT_HORIZONTAL_BAR =:CONTENT_HORIZONTAL_BAR,
                CONTENT_TITRE1_COLOR =:CONTENT_TITRE1_COLOR,
                CONTENT_BUTTON_COLOR =:CONTENT_BUTTON_COLOR,
                CONTENT_LABEL_COLOR =:CONTENT_LABEL_COLOR,
                CONTENT_INPUT_COLOR =:CONTENT_INPUT_COLOR,
                CONTENT_LISTE_INTITULE_COLOR =:CONTENT_LISTE_INTITULE_COLOR,
                CONTENT_LISTE_INTITULE_COLOR_HOVER =:CONTENT_LISTE_INTITULE_COLOR_HOVER,
                CONTENT_LISTE_DATE_COLOR =:CONTENT_LISTE_DATE_COLOR,
                CONTENT_LISTE_CONTRAT_COLOR =:CONTENT_LISTE_CONTRAT_COLOR,
                CONTENT_LISTE_LOCATION_COLOR =:CONTENT_LISTE_LOCATION_COLOR,
                CONTENT_LISTE_DESC_COLOR =:CONTENT_LISTE_DESC_COLOR,
                CONTENT_LISTE_PAGINATION_COLOR =:CONTENT_LISTE_PAGINATION_COLOR,
                CONTENT_LISTE_PAGINATION_COLOR_HOVER =:CONTENT_LISTE_PAGINATION_COLOR_HOVER,
                CONTENT_FICHE_DESC_COLOR =:CONTENT_FICHE_DESC_COLOR,
                CONTENT_FICHE_MISSION_TITLE_COLOR =:CONTENT_FICHE_MISSION_TITLE_COLOR,
                CONTENT_FICHE_MISSION_COLOR =:CONTENT_FICHE_MISSION_COLOR,
                CONTENT_FICHE_CUSTOMER_TITLE_COLOR =:CONTENT_FICHE_CUSTOMER_TITLE_COLOR,
                CONTENT_FICHE_CUSTOMER_BORDER_COLOR =:CONTENT_FICHE_CUSTOMER_BORDER_COLOR,
                CONTENT_FICHE_CUSTOMER_COLOR =:CONTENT_FICHE_CUSTOMER_COLOR,
                CONTENT_FICHE_INFO_TITLE_COLOR =:CONTENT_FICHE_INFO_TITLE_COLOR,
                CONTENT_FICHE_INFO_BORDER_COLOR =:CONTENT_FICHE_INFO_BORDER_COLOR,
                CONTENT_FICHE_INFO_COLOR =:CONTENT_FICHE_INFO_COLOR,
                FORM_COMPULSORY_STAR_COLOR =:FORM_COMPULSORY_STAR_COLOR,
                FORM_LABEL_COLOR =:FORM_LABEL_COLOR,
                FORM_INPUT_COLOR =:FORM_INPUT_COLOR,
                FORM_BLOC_TITLE_COLOR =:FORM_BLOC_TITLE_COLOR,
                FORM_ADD_COLOR =:FORM_ADD_COLOR,
                FORM_ADD_HOVER_COLOR =:FORM_ADD_HOVER_COLOR,
                FORM_DEL_COLOR =:FORM_DEL_COLOR,
                FORM_DEL_COLOR_HOVER =:FORM_DEL_COLOR_HOVER,
                FOOTER_COLOR =:FOOTER_COLOR,
                FOOTER_HOVER_COLOR =:FOOTER_HOVER_COLOR
            ";

        $select = $conn->prepare($sql);


        $select->bindParam(':TITRE', $_POST['design_titre'], PDO::PARAM_STR);
        $select->bindParam(':ACTIVE', $_POST['design_actif'], PDO::PARAM_INT);
        $updateDate = date("Y-m-d");
        $select->bindParam(':DATE_UPDATE', $updateDate, PDO::PARAM_STR);
        $select->bindParam(':DATE_CREATION', $updateDate, PDO::PARAM_STR);
        $select->bindParam(':HEADER_LOGO', $logo_filename, PDO::PARAM_STR);
        $select->bindParam(':BODY_POLICE', $_POST['design_BODY_POLICE'], PDO::PARAM_STR);
        $select->bindParam(':BODY_COLOR', $_POST['design_BODY_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':DATA_POLICE', $_POST['design_DATA_POLICE'], PDO::PARAM_STR);
        $select->bindParam(':PATHWAY_COLOR', $_POST['design_PATHWAY_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':PATHWAY_COLOR_HOVER', $_POST['design_PATHWAY_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':CANDIDAT_AREA_COLOR', $_POST['design_CANDIDAT_AREA_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CANDIDAT_AREA_COLOR_HOVER', $_POST['design_CANDIDAT_AREA_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':MENU_COLOR', $_POST['design_MENU_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':MENU_COLOR_HOVER', $_POST['design_MENU_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_VERTICAL_BAR', $_POST['design_CONTENT_VERTICAL_BAR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_HORIZONTAL_BAR', $_POST['design_CONTENT_HORIZONTAL_BAR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_TITRE1_COLOR', $_POST['design_CONTENT_TITRE1_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_BUTTON_COLOR', $_POST['design_CONTENT_BUTTON_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LABEL_COLOR', $_POST['design_CONTENT_LABEL_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_INPUT_COLOR', $_POST['design_CONTENT_INPUT_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_INTITULE_COLOR', $_POST['design_CONTENT_LISTE_INTITULE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_INTITULE_COLOR_HOVER', $_POST['design_CONTENT_LISTE_INTITULE_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_DATE_COLOR', $_POST['design_CONTENT_LISTE_DATE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_CONTRAT_COLOR', $_POST['design_CONTENT_LISTE_CONTRAT_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_LOCATION_COLOR', $_POST['design_CONTENT_LISTE_LOCATION_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_DESC_COLOR', $_POST['design_CONTENT_LISTE_DESC_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_PAGINATION_COLOR', $_POST['design_CONTENT_LISTE_PAGINATION_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_LISTE_PAGINATION_COLOR_HOVER', $_POST['design_CONTENT_LISTE_PAGINATION_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_DESC_COLOR', $_POST['design_CONTENT_FICHE_DESC_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_MISSION_TITLE_COLOR', $_POST['design_CONTENT_FICHE_MISSION_TITLE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_MISSION_COLOR', $_POST['design_CONTENT_FICHE_MISSION_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_CUSTOMER_TITLE_COLOR', $_POST['design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_CUSTOMER_COLOR', $_POST['design_CONTENT_FICHE_CUSTOMER_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_CUSTOMER_BORDER_COLOR', $_POST['design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_INFO_TITLE_COLOR', $_POST['design_CONTENT_FICHE_INFO_TITLE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_INFO_COLOR', $_POST['design_CONTENT_FICHE_INFO_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':CONTENT_FICHE_INFO_BORDER_COLOR', $_POST['design_CONTENT_FICHE_INFO_BORDER_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_COMPULSORY_STAR_COLOR', $_POST['design_FORM_COMPULSORY_STAR_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_LABEL_COLOR', $_POST['design_FORM_LABEL_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_INPUT_COLOR', $_POST['design_FORM_INPUT_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_BLOC_TITLE_COLOR', $_POST['design_FORM_BLOC_TITLE_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_ADD_COLOR', $_POST['design_FORM_ADD_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_ADD_HOVER_COLOR', $_POST['design_FORM_ADD_HOVER_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_DEL_COLOR', $_POST['design_FORM_DEL_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FORM_DEL_COLOR_HOVER', $_POST['design_FORM_DEL_COLOR_HOVER'], PDO::PARAM_STR);
        $select->bindParam(':FOOTER_COLOR', $_POST['design_FOOTER_COLOR'], PDO::PARAM_STR);
        $select->bindParam(':FOOTER_HOVER_COLOR', $_POST['design_FOOTER_HOVER_COLOR'], PDO::PARAM_STR);
        $select->execute();

        Debug::d_echo("sauvegarde design  ", 2,"v-uploaddesign.php");

        echo "<script type='text/javascript' >";
        echo " parent.closePopup_design('".urlencode(_CONF_DESIGN_SAVE_MSG)."')";
        echo "</script>";

   }
}
?>



<script type="text/javascript" >
    function checkForm(){
        if($('#design_titre').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_TITRE_ERROR; ?>");
            $('#design_titre').focus();
            return false;
        }

        if($('#design_HEADER_LOGO').val() == "" && $('#design_HEADER_LOGO_N').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_HEADER_LOGO_ERROR; ?>");
            $('#design_HEADER_LOGO_N').focus();
            return false;
        }

        if($('#design_BODY_POLICE').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_BODY_POLICE_ERROR; ?>");
            $('#design_BODY_POLICE').focus();
            return false;
        }
        if($('#design_BODY_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_BODY_COLOR_ERROR; ?>");
            $('#design_BODY_COLOR').focus();
            return false;
        }
        if($('#design_DATA_POLICE').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_DATA_POLICE_ERROR; ?>");
            $('#design_DATA_POLICE').focus();
            return false;
        }
        if($('#design_PATHWAY_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_PATHWAY_COLOR_ERROR; ?>");
            $('#design_PATHWAY_COLOR').focus();
            return false;
        }
        if($('#design_PATHWAY_COLOR_HOVER').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_PATHWAY_COLOR_HOVER_ERROR; ?>");
            $('#design_PATHWAY_COLOR_HOVER').focus();
            return false;
        }
        if($('#design_CANDIDAT_AREA_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CANDIDAT_AREA_COLOR_ERROR; ?>");
            $('#design_CANDIDAT_AREA_COLOR').focus();
            return false;
        }
        if($('#design_CANDIDAT_AREA_COLOR_HOVER').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CANDIDAT_AREA_COLOR_HOVER_ERROR; ?>");
            $('#design_CANDIDAT_AREA_COLOR_HOVER').focus();
            return false;
        }
        if($('#design_MENU_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_MENU_COLOR_ERROR; ?>");
            $('#design_MENU_COLOR').focus();
            return false;
        }
        if($('#design_MENU_COLOR_HOVER').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_MENU_COLOR_HOVER_ERROR; ?>");
            $('#design_MENU_COLOR_HOVER').focus();
            return false;
        }
        if($('#design_CONTENT_VERTICAL_BAR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_VERTICAL_BAR_ERROR; ?>");
            $('#design_CONTENT_VERTICAL_BAR').focus();
            return false;
        }
        if($('#design_CONTENT_HORIZONTAL_BAR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_HORIZONTAL_BAR_ERROR; ?>");
            $('#design_CONTENT_HORIZONTAL_BAR').focus();
            return false;
        }
        if($('#design_CONTENT_TITRE1_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_TITRE1_COLOR_ERROR; ?>");
            $('#design_CONTENT_TITRE1_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_BUTTON_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_BUTTON_COLOR_ERROR; ?>");
            $('#design_CONTENT_BUTTON_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_LABEL_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_LABEL_COLOR_ERROR; ?>");
            $('#design_CONTENT_LABEL_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_INPUT_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_INPUT_COLOR_ERROR; ?>");
            $('#design_CONTENT_INPUT_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_LISTE_INTITULE_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_LISTE_INTITULE_COLOR_ERROR; ?>");
            $('#design_CONTENT_LISTE_INTITULE_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_LISTE_INTITULE_COLOR_HOVER').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_LISTE_INTITULE_COLOR_HOVER_ERROR; ?>");
            $('#design_CONTENT_LISTE_INTITULE_COLOR_HOVER').focus();
            return false;
        }
        if($('#design_CONTENT_LISTE_DATE_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_LISTE_DATE_COLOR_ERROR; ?>");
            $('#design_CONTENT_LISTE_DATE_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_LISTE_CONTRAT_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_LISTE_CONTRAT_COLOR_ERROR; ?>");
            $('#design_CONTENT_LISTE_CONTRAT_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_LISTE_LOCATION_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_LISTE_LOCATION_COLOR_ERROR; ?>");
            $('#design_CONTENT_LISTE_LOCATION_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_LISTE_DESC_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_LISTE_DESC_COLOR_ERROR; ?>");
            $('#design_CONTENT_LISTE_DESC_COLOR').focus();
            return false;
        }        
        if($('#design_CONTENT_LISTE_PAGINATION_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_LISTE_PAGINATION_COLOR; ?>");
            $('#design_CONTENT_LISTE_PAGINATION_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_LISTE_PAGINATION_COLOR_HOVER').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_LISTE_PAGINATION_COLOR_HOVER; ?>");
            $('#design_CONTENT_LISTE_PAGINATION_COLOR_HOVER').focus();
            return false;
        }
        if($('#design_CONTENT_FICHE_DESC_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_FICHE_DESC_COLOR_ERROR; ?>");
            $('#design_CONTENT_FICHE_DESC_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_FICHE_MISSION_TITLE_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_FICHE_MISSION_TITLE_COLOR_ERROR; ?>");
            $('#design_CONTENT_FICHE_MISSION_TITLE_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_FICHE_MISSION_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_FICHE_MISSION_COLOR_ERROR; ?>");
            $('#design_CONTENT_FICHE_MISSION_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_FICHE_CUSTOMER_TITLE_COLOR_ERROR; ?>");
            $('#design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_FICHE_CUSTOMER_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_FICHE_CUSTOMER_COLOR_ERROR; ?>");
            $('#design_CONTENT_FICHE_CUSTOMER_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_FICHE_CUSTOMER_BORDER_COLOR_ERROR; ?>");
            $('#design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_FICHE_INFO_TITLE_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_FICHE_INFO_TITLE_COLOR_ERROR; ?>");
            $('#design_CONTENT_FICHE_INFO_TITLE_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_FICHE_INFO_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_FICHE_INFO_COLOR_ERROR; ?>");
            $('#design_CONTENT_FICHE_INFO_COLOR').focus();
            return false;
        }
        if($('#design_CONTENT_FICHE_INFO_BORDER_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_CONTENT_FICHE_INFO_BORDER_COLOR_ERROR; ?>");
            $('#design_CONTENT_FICHE_INFO_BORDER_COLOR').focus();
            return false;
        }
        if($('#design_FORM_COMPULSORY_STAR_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_FORM_COMPULSORY_STAR_COLOR_ERROR; ?>");
            $('#design_FORM_COMPULSORY_STAR_COLOR').focus();
            return false;
        }
        if($('#design_FORM_LABEL_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_FORM_LABEL_COLOR_ERROR; ?>");
            $('#design_FORM_LABEL_COLOR').focus();
            return false;
        }
        if($('#design_FORM_INPUT_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_FORM_INPUT_COLOR_ERROR; ?>");
            $('#design_FORM_INPUT_COLOR').focus();
            return false;
        }
        if($('#design_FORM_BLOC_TITLE_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_FORM_BLOC_TITLE_COLOR_ERROR; ?>");
            $('#design_FORM_BLOC_TITLE_COLOR').focus();
            return false;
        }
        if($('#design_FORM_ADD_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_FORM_ADD_COLOR_ERROR; ?>");
            $('#design_FORM_ADD_COLOR').focus();
            return false;
        }
        if($('#design_FORM_ADD_HOVER_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_FORM_ADD_HOVER_COLOR_ERROR; ?>");
            $('#design_FORM_ADD_HOVER_COLOR').focus();
            return false;
        }
        if($('#design_FORM_DEL_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_FORM_DEL_COLOR_ERROR; ?>");
            $('#design_FORM_DEL_COLOR').focus();
            return false;
        }
        if($('#design_FORM_DEL_COLOR_HOVER').val() == ""){
            alert("Veuillez saisir la couleur des suppression du formulaire de candidature (hover)");
            $('#design_FORM_DEL_COLOR_HOVER').focus();
            return false;
        }
        if($('#design_FOOTER_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_FOOTER_COLOR_ERROR; ?>");
            $('#design_FOOTER_COLOR').focus();
            return false;
        }
        if($('#design_FOOTER_HOVER_COLOR').val() == ""){
            alert("<?php echo _CONF_DESIGN_ADD_JS_FOOTER_HOVER_COLOR_ERROR; ?>");
            $('#design_FOOTER_HOVER_COLOR').focus();
            return false;
        }


        return true;
//        submitFormAjax();
    }

//    function submitFormAjax(){
//        $.ajax({
//            "type": "POST",
//            "url": "v-add-design.php",
//            "cache": false,
//            "data": $('#form_entreprise').serialize(),
//            "success": function(msg){
//                $('#popupdiv_content_design').html(msg);
//            }
//        });
//    }

    function updateColor(id){
        var color = $('#'+id).val();
        $('#'+id).css('color', color);
    }



</script>
        <form enctype="multipart/form-data" onsubmit="return checkForm();" action="v-uploaddesign.php" method="post" id="form_entreprise">
            <fieldset>
                <input type="hidden" value="<?php echo $design_id; ?>" id="id" name="id"/>

                <div class="formrow">
                    <label for="design_titre" class="label_formdesign" ><?php echo _CONF_DESIGN_ADD_TITRE_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" value="<?php echo $design_titre; ?>" style="" class="fs12 input_formdesign" id="design_titre" name="design_titre"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_actif" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_ACTIF_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <select id="design_actif" name="design_actif" class="fs12 input_formdesign">
                        <option value="0" <?php if($design_actif=="0"){echo "selected=\"selected\"";} ?>><?php echo _CONF_DESIGN_ADD_ACTIF_VAL_0; ?></option>
                        <option value="1" <?php if($design_actif=="1"){echo "selected=\"selected\"";} ?>><?php echo _CONF_DESIGN_ADD_ACTIF_VAL_1; ?></option>
                    </select>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_HEADER_LOGO" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_HEADER_LOGO_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" value="<?php echo $design_HEADER_LOGO; ?>" style="" class="fs12 input_formdesign" id="design_HEADER_LOGO" name="design_HEADER_LOGO" readonly="readonly"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_HEADER_LOGO_N" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_HEADER_LOGO_N_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="file" style="" class="fs12 input_formdesign" id="design_HEADER_LOGO_N" name="design_HEADER_LOGO_N"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_BODY_POLICE" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_BODY_POLICE_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" value="<?php echo $design_BODY_POLICE; ?>" style="" class="fs12 input_formdesign" id="design_BODY_POLICE" name="design_BODY_POLICE"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_BODY_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_BODY_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_BODY_COLOR');" value="<?php echo $design_BODY_COLOR; ?>" style="<?php if(!empty($design_BODY_COLOR)){echo "color:".$design_BODY_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_BODY_COLOR" name="design_BODY_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_DATA_POLICE" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_DATA_POLICE_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text"  value="<?php echo $design_DATA_POLICE; ?>" style="" class="fs12 input_formdesign" id="design_DATA_POLICE" name="design_DATA_POLICE"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_PATHWAY_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_PATHWAY_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_PATHWAY_COLOR');" value="<?php echo $design_PATHWAY_COLOR; ?>" style="<?php if(!empty($design_PATHWAY_COLOR)){echo "color:".$design_PATHWAY_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_PATHWAY_COLOR" name="design_PATHWAY_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_PATHWAY_COLOR_HOVER" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_PATHWAY_COLOR_HOVER_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_PATHWAY_COLOR_HOVER');" value="<?php echo $design_PATHWAY_COLOR_HOVER; ?>" style="<?php if(!empty($design_PATHWAY_COLOR_HOVER)){echo "color:".$design_PATHWAY_COLOR_HOVER.";";} ?>" class="fs12 input_formdesign" id="design_PATHWAY_COLOR_HOVER" name="design_PATHWAY_COLOR_HOVER"/>
                    <div class="spacer"></div>
                </div>

                <div class="formrow">
                    <label for="design_CANDIDAT_AREA_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CANDIDAT_AREA_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CANDIDAT_AREA_COLOR');" value="<?php echo $design_CANDIDAT_AREA_COLOR; ?>" style="<?php if(!empty($design_CANDIDAT_AREA_COLOR)){echo "color:".$design_CANDIDAT_AREA_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CANDIDAT_AREA_COLOR" name="design_CANDIDAT_AREA_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CANDIDAT_AREA_COLOR_HOVER" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CANDIDAT_AREA_COLOR_HOVER_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CANDIDAT_AREA_COLOR_HOVER');" value="<?php echo $design_CANDIDAT_AREA_COLOR_HOVER; ?>" style="<?php if(!empty($design_CANDIDAT_AREA_COLOR_HOVER)){echo "color:".$design_CANDIDAT_AREA_COLOR_HOVER.";";} ?>" class="fs12 input_formdesign" id="design_CANDIDAT_AREA_COLOR_HOVER" name="design_CANDIDAT_AREA_COLOR_HOVER"/>
                    <div class="spacer"></div>
                </div>

                <div class="formrow">
                    <label for="design_MENU_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_MENU_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_MENU_COLOR');" value="<?php echo $design_MENU_COLOR; ?>" style="<?php if(!empty($design_MENU_COLOR)){echo "color:".$design_MENU_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_MENU_COLOR" name="design_MENU_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_MENU_COLOR_HOVER" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_MENU_COLOR_HOVER_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_MENU_COLOR_HOVER');" value="<?php echo $design_MENU_COLOR_HOVER; ?>" style="<?php if(!empty($design_MENU_COLOR_HOVER)){echo "color:".$design_MENU_COLOR_HOVER.";";} ?>" class="fs12 input_formdesign" id="design_MENU_COLOR_HOVER" name="design_MENU_COLOR_HOVER"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_VERTICAL_BAR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_VERTICAL_BAR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_VERTICAL_BAR');" value="<?php echo $design_CONTENT_VERTICAL_BAR; ?>" style="<?php if(!empty($design_CONTENT_VERTICAL_BAR)){echo "color:".$design_CONTENT_VERTICAL_BAR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_VERTICAL_BAR" name="design_CONTENT_VERTICAL_BAR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_HORIZONTAL_BAR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_HORIZONTAL_BAR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_HORIZONTAL_BAR');" value="<?php echo $design_CONTENT_HORIZONTAL_BAR; ?>" style="<?php if(!empty($design_CONTENT_HORIZONTAL_BAR)){echo "color:".$design_CONTENT_HORIZONTAL_BAR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_HORIZONTAL_BAR" name="design_CONTENT_HORIZONTAL_BAR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_TITRE1_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_TITRE1_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_TITRE1_COLOR');" value="<?php echo $design_CONTENT_TITRE1_COLOR; ?>" style="<?php if(!empty($design_CONTENT_TITRE1_COLOR)){echo "color:".$design_CONTENT_TITRE1_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_TITRE1_COLOR" name="design_CONTENT_TITRE1_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_BUTTON_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_BUTTON_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_BUTTON_COLOR');" value="<?php echo $design_CONTENT_BUTTON_COLOR; ?>" style="<?php if(!empty($design_CONTENT_BUTTON_COLOR)){echo "color:".$design_CONTENT_BUTTON_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_BUTTON_COLOR" name="design_CONTENT_BUTTON_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_LABEL_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_LABEL_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_LABEL_COLOR');" value="<?php echo $design_CONTENT_LABEL_COLOR; ?>" style="<?php if(!empty($design_CONTENT_LABEL_COLOR)){echo "color:".$design_CONTENT_LABEL_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_LABEL_COLOR" name="design_CONTENT_LABEL_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_INPUT_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_INPUT_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_INPUT_COLOR');" value="<?php echo $design_CONTENT_INPUT_COLOR; ?>" style="<?php if(!empty($design_CONTENT_INPUT_COLOR)){echo "color:".$design_CONTENT_INPUT_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_INPUT_COLOR" name="design_CONTENT_INPUT_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_LISTE_INTITULE_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_LISTE_INTITULE_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_LISTE_INTITULE_COLOR');" value="<?php echo $design_CONTENT_LISTE_INTITULE_COLOR; ?>" style="<?php if(!empty($design_CONTENT_LISTE_INTITULE_COLOR)){echo "color:".$design_CONTENT_LISTE_INTITULE_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_LISTE_INTITULE_COLOR" name="design_CONTENT_LISTE_INTITULE_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_LISTE_INTITULE_COLOR_HOVER" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_LISTE_INTITULE_COLOR_HOVER_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_LISTE_INTITULE_COLOR_HOVER');" value="<?php echo $design_CONTENT_LISTE_INTITULE_COLOR_HOVER; ?>" style="<?php if(!empty($design_CONTENT_LISTE_INTITULE_COLOR_HOVER)){echo "color:".$design_CONTENT_LISTE_INTITULE_COLOR_HOVER.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_LISTE_INTITULE_COLOR_HOVER" name="design_CONTENT_LISTE_INTITULE_COLOR_HOVER"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_LISTE_DATE_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_LISTE_DATE_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_LISTE_DATE_COLOR');" value="<?php echo $design_CONTENT_LISTE_DATE_COLOR; ?>" style="<?php if(!empty($design_CONTENT_LISTE_DATE_COLOR)){echo "color:".$design_CONTENT_LISTE_DATE_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_LISTE_DATE_COLOR" name="design_CONTENT_LISTE_DATE_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_LISTE_CONTRAT_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_LISTE_CONTRAT_COLOR_LABEL;?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_LISTE_CONTRAT_COLOR');" value="<?php echo $design_CONTENT_LISTE_CONTRAT_COLOR; ?>" style="<?php if(!empty($design_CONTENT_LISTE_CONTRAT_COLOR)){echo "color:".$design_CONTENT_LISTE_CONTRAT_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_LISTE_CONTRAT_COLOR" name="design_CONTENT_LISTE_CONTRAT_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_LISTE_LOCATION_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_LISTE_LOCATION_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_LISTE_LOCATION_COLOR');" value="<?php echo $design_CONTENT_LISTE_LOCATION_COLOR; ?>" style="<?php if(!empty($design_CONTENT_LISTE_LOCATION_COLOR)){echo "color:".$design_CONTENT_LISTE_LOCATION_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_LISTE_LOCATION_COLOR" name="design_CONTENT_LISTE_LOCATION_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_LISTE_DESC_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_LISTE_DESC_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_LISTE_DESC_COLOR');" value="<?php echo $design_CONTENT_LISTE_DESC_COLOR; ?>" style="<?php if(!empty($design_CONTENT_LISTE_DESC_COLOR)){echo "color:".$design_CONTENT_LISTE_DESC_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_LISTE_DESC_COLOR" name="design_CONTENT_LISTE_DESC_COLOR"/>
                    <div class="spacer"></div>
                </div>


                <div class="formrow">
                    <label for="design_CONTENT_LISTE_PAGINATION_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_LISTE_PAGINATION_COLOR; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_LISTE_PAGINATION_COLOR');" value="<?php echo $design_CONTENT_LISTE_PAGINATION_COLOR; ?>" style="<?php if(!empty($design_CONTENT_LISTE_PAGINATION_COLOR)){echo "color:".$design_CONTENT_LISTE_PAGINATION_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_LISTE_PAGINATION_COLOR" name="design_CONTENT_LISTE_PAGINATION_COLOR"/>
                    <div class="spacer"></div>
                </div>

                <div class="formrow">
                    <label for="design_CONTENT_LISTE_PAGINATION_COLOR_HOVER" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_LISTE_PAGINATION_COLOR_HOVER; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_LISTE_PAGINATION_COLOR_HOVER');" value="<?php echo $design_CONTENT_LISTE_PAGINATION_COLOR_HOVER; ?>" style="<?php if(!empty($design_CONTENT_LISTE_PAGINATION_COLOR_HOVER)){echo "color:".$design_CONTENT_LISTE_PAGINATION_COLOR_HOVER.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_LISTE_PAGINATION_COLOR_HOVER" name="design_CONTENT_LISTE_PAGINATION_COLOR_HOVER"/>
                    <div class="spacer"></div>
                </div>






                <div class="formrow">
                    <label for="design_CONTENT_FICHE_DESC_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_FICHE_DESC_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_FICHE_DESC_COLOR');" value="<?php echo $design_CONTENT_FICHE_DESC_COLOR; ?>" style="<?php if(!empty($design_CONTENT_FICHE_DESC_COLOR)){echo "color:".$design_CONTENT_FICHE_DESC_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_FICHE_DESC_COLOR" name="design_CONTENT_FICHE_DESC_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_FICHE_MISSION_TITLE_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_FICHE_MISSION_TITLE_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_FICHE_MISSION_TITLE_COLOR');" value="<?php echo $design_CONTENT_FICHE_MISSION_TITLE_COLOR; ?>" style="<?php if(!empty($design_CONTENT_FICHE_MISSION_TITLE_COLOR)){echo "color:".$design_CONTENT_FICHE_MISSION_TITLE_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_FICHE_MISSION_TITLE_COLOR" name="design_CONTENT_FICHE_MISSION_TITLE_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_FICHE_MISSION_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_FICHE_MISSION_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_FICHE_MISSION_COLOR');" value="<?php echo $design_CONTENT_FICHE_MISSION_COLOR; ?>" style="<?php if(!empty($design_CONTENT_FICHE_MISSION_COLOR)){echo "color:".$design_CONTENT_FICHE_MISSION_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_FICHE_MISSION_COLOR" name="design_CONTENT_FICHE_MISSION_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR');" value="<?php echo $design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR; ?>" style="<?php if(!empty($design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR)){echo "color:".$design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR" name="design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_FICHE_CUSTOMER_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_FICHE_CUSTOMER_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_FICHE_CUSTOMER_COLOR');" value="<?php echo $design_CONTENT_FICHE_CUSTOMER_COLOR; ?>" style="<?php if(!empty($design_CONTENT_FICHE_CUSTOMER_COLOR)){echo "color:".$design_CONTENT_FICHE_CUSTOMER_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_FICHE_CUSTOMER_COLOR" name="design_CONTENT_FICHE_CUSTOMER_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR');" value="<?php echo $design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR; ?>" style="<?php if(!empty($design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR)){echo "color:".$design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR" name="design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_FICHE_INFO_TITLE_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_FICHE_INFO_TITLE_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_FICHE_INFO_TITLE_COLOR');" value="<?php echo $design_CONTENT_FICHE_INFO_TITLE_COLOR; ?>" style="<?php if(!empty($design_CONTENT_FICHE_INFO_TITLE_COLOR)){echo "color:".$design_CONTENT_FICHE_INFO_TITLE_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_FICHE_INFO_TITLE_COLOR" name="design_CONTENT_FICHE_INFO_TITLE_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_FICHE_INFO_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_FICHE_INFO_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_FICHE_INFO_COLOR');" value="<?php echo $design_CONTENT_FICHE_INFO_COLOR; ?>" style="<?php if(!empty($design_CONTENT_FICHE_INFO_COLOR)){echo "color:".$design_CONTENT_FICHE_INFO_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_FICHE_INFO_COLOR" name="design_CONTENT_FICHE_INFO_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_CONTENT_FICHE_INFO_BORDER_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_CONTENT_FICHE_INFO_BORDER_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_CONTENT_FICHE_INFO_BORDER_COLOR');" value="<?php echo $design_CONTENT_FICHE_INFO_BORDER_COLOR; ?>" style="<?php if(!empty($design_CONTENT_FICHE_INFO_BORDER_COLOR)){echo "color:".$design_CONTENT_FICHE_INFO_BORDER_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_CONTENT_FICHE_INFO_BORDER_COLOR" name="design_CONTENT_FICHE_INFO_BORDER_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_FORM_COMPULSORY_STAR_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_FORM_COMPULSORY_STAR_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_FORM_COMPULSORY_STAR_COLOR');" value="<?php echo $design_FORM_COMPULSORY_STAR_COLOR; ?>" style="<?php if(!empty($design_FORM_COMPULSORY_STAR_COLOR)){echo "color:".$design_FORM_COMPULSORY_STAR_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_FORM_COMPULSORY_STAR_COLOR" name="design_FORM_COMPULSORY_STAR_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_FORM_LABEL_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_FORM_LABEL_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_FORM_LABEL_COLOR');" value="<?php echo $design_FORM_LABEL_COLOR; ?>" style="<?php if(!empty($design_FORM_LABEL_COLOR)){echo "color:".$design_FORM_LABEL_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_FORM_LABEL_COLOR" name="design_FORM_LABEL_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_FORM_INPUT_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_FORM_INPUT_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_FORM_INPUT_COLOR');" value="<?php echo $design_FORM_INPUT_COLOR; ?>" style="<?php if(!empty($design_FORM_INPUT_COLOR)){echo "color:".$design_FORM_INPUT_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_FORM_INPUT_COLOR" name="design_FORM_INPUT_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_FORM_BLOC_TITLE_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_FORM_BLOC_TITLE_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_FORM_BLOC_TITLE_COLOR');" value="<?php echo $design_FORM_BLOC_TITLE_COLOR; ?>" style="<?php if(!empty($design_FORM_BLOC_TITLE_COLOR)){echo "color:".$design_FORM_BLOC_TITLE_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_FORM_BLOC_TITLE_COLOR" name="design_FORM_BLOC_TITLE_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_FORM_ADD_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_FORM_ADD_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_FORM_ADD_COLOR');" value="<?php echo $design_FORM_ADD_COLOR; ?>" style="<?php if(!empty($design_FORM_ADD_COLOR)){echo "color:".$design_FORM_ADD_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_FORM_ADD_COLOR" name="design_FORM_ADD_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_FORM_ADD_HOVER_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_FORM_ADD_HOVER_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_FORM_ADD_HOVER_COLOR');" value="<?php echo $design_FORM_ADD_HOVER_COLOR; ?>" style="<?php if(!empty($design_FORM_ADD_HOVER_COLOR)){echo "color:".$design_FORM_ADD_HOVER_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_FORM_ADD_HOVER_COLOR" name="design_FORM_ADD_HOVER_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_FORM_DEL_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_FORM_DEL_COLOR_LABEL; ?>  <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_FORM_DEL_COLOR');" value="<?php echo $design_FORM_DEL_COLOR; ?>" style="<?php if(!empty($design_FORM_DEL_COLOR)){echo "color:".$design_FORM_DEL_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_FORM_DEL_COLOR" name="design_FORM_DEL_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_FORM_DEL_COLOR_HOVER" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_FORM_DEL_COLOR_HOVER_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_FORM_DEL_COLOR_HOVER');" value="<?php echo $design_FORM_DEL_COLOR_HOVER; ?>" style="<?php if(!empty($design_FORM_DEL_COLOR_HOVER)){echo "color:".$design_FORM_DEL_COLOR_HOVER.";";} ?>" class="fs12 input_formdesign" id="design_FORM_DEL_COLOR_HOVER" name="design_FORM_DEL_COLOR_HOVER"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_FOOTER_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_FOOTER_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_FOOTER_COLOR');" value="<?php echo $design_FOOTER_COLOR; ?>" style="<?php if(!empty($design_FOOTER_COLOR)){echo "color:".$design_FOOTER_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_FOOTER_COLOR" name="design_FOOTER_COLOR"/>
                    <div class="spacer"></div>
                </div>
                <div class="formrow">
                    <label for="design_FOOTER_HOVER_COLOR" class="label_formdesign"><?php echo _CONF_DESIGN_ADD_design_FOOTER_HOVER_COLOR_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
                    <input type="text" onblur="updateColor('design_FOOTER_HOVER_COLOR');" value="<?php echo $design_FOOTER_HOVER_COLOR; ?>" style="<?php if(!empty($design_FOOTER_HOVER_COLOR)){echo "color:".$design_FOOTER_HOVER_COLOR.";";} ?>" class="fs12 input_formdesign" id="design_FOOTER_HOVER_COLOR" name="design_FOOTER_HOVER_COLOR"/>
                    <div class="spacer"></div>
                </div>

                <div style="height: 20px;"></div>




                <div style="height: 20px;"></div>
                <div class="center">
                    <input type="submit" name="sbm" id="sbm" value="<?php echo _CONF_DESIGN_ADD_SUBMIT_BTN; ?>"/>
                </div>
            </fieldset>
        </form>
    </body>
</html>