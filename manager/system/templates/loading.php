<?php if(!isset($_SESSION["login"])) { $title_document = "Connexion - Idetop Manager"; } ?>
<div id="loading">
    <table width="100%" height:100%>
        <tr><td align="center" valign="middle" width="100%" height="100%" style="padding-top:8%;">
            <center>
                <img src="<?= $b_img; ?>logo.png" />
                <p style="margin-top:50px;"><img src="<?= $b_img; ?>loading.gif" /> <?= $title_loading; ?> ...</p>
            </center>
        </td></tr>
    </table>
</div>
<script type="text/javascript">
//<![CDATA[
var ld = (document.all);
var ns4 = document.layers;
var ns6 = document.getElementById&&!document.all;
var ie4 = document.all;
if(ns4) ld = document.loading;
else if(ns6) ld = document.getElementById("loading").style;
else if(ie4) ld = document.all.loading.style;
function init() {
    if(ns4){document.title = "<?= $title_document; ?>";ld.visibility = "hidden";}
    else if(ns6||ie4) document.title = "<?= $title_document; ?>";ld.display = "none";
	<?php if($retour == "success") { ?>$(document).ready(function(){$("#alertbox").show().delay(3500).fadeOut(700);});<?php } ?>
    <?php if($_REQUEST['action'] == 'export_xls') { echo 'window.location = "export/'.$file_xls.'";'; } ?>
}
//]]>
</script>
