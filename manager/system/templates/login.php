<?php
include('system/main_config.php');
include($b_libs.'gzip.php');

// !isset erreur
$message = "Merci de saisir vos identifiants de connexion";
$style = "color:#737373;";

// Else
if($_REQUEST['error'] == "1") {
    $message = 'Erreur de connexion - Vérifier vos identifiants';
    $style = "color:#b9001f;font-weight:bold;";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= $lang; ?>" lang="<?= $lang; ?>">

<head>
    <title>Chargement ...</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <meta name="language" content="<?= $lang; ?>" />
    <link rel="shortcut icon" href="<?= $b_img; ?>favicon.ico" type="image/ico" />
    <link rel="stylesheet" type="text/css" href="<?= $b_css; ?>idemanager.css" />
    <!--[if lt IE 7.]><script defer type="text/javascript" src="<?= $b_js; ?>pngfix.js"></script><![endif]-->
    <!-- init module -->
    <script type="text/javascript" src="<?= $b_js; ?>jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="<?= $b_js; ?>jquery-ui-1.8.6.custom.min.js"></script>
    <script type="text/javascript" src="<?= $b_js; ?>jpreload.js"></script>
    <!-- end init -->
    <script type="text/javascript">
    //<![CDATA[
    function check_form() {
        var form_err = "";
        var consign_login = document.getElementById('consigne_login');
        if(document.form_login.login.value == "Identifiant") { form_err += "1"; }
        if(document.form_login.pwd.value == "password") { form_err += "1"; }
        if (form_err != "") {
            consign_login.innerHTML = '<span style="font-weight:bold;color:#b9001f">Attention, tous les champs n\'ont pas été rempli !</span>';
            return false;
        }
        consign_login.innerHTML = '<span style="font-weight:bold;color:#19a30e">Identification en cours ...</span>';
        return true;
    }
    //]]>
    </script>
</head>

<body onload="init();">
<center>

<?php include($b_templates.'loading.php'); ?>

<div id="content" style="margin-top:9%;margin-left:0;background:none;">
    <center>
        <form name="form_login" action="index.php" method="post" onSubmit="return check_form();">
            <p id="consigne_login" class="consigne_login" style="<?= $style; ?>"><?= $message; ?></p>
            <div id="bloc_login">
                <div id="div_login">
                    <input type="text" class="input_login" name="login" value="Identifiant"
                    onFocus="if(this.value=='Identifiant') {this.value=''};" onBlur="if(this.value=='') {this.value='Identifiant'};" /><br/>
                    <input type="password" class="input_login" name="pwd" value="password"
                    onFocus="if(this.value=='password') {this.value=''};" onBlur="if(this.value=='') {this.value='password'};" />
                </div>
            </div>
            <div id="bloc_connexion">
                <input type="submit" id="connexion" name="connexion" value="" />
                <div class="clear"></div>
            </div>
        </form>
        <p class="copyright_login" style="margin-top:100px;padding:0;">
            &copy; Développé par <a href="http://www.idetop.com" target="_blank">Idetop | Marketing & Publicité</a> &middot; 6 Quai André Lassagne 69001 LYON &middot; Tél : 04 7200 3333 (coût d'un appel local) &middot; Email : <a href="mailto:info@idetop.com">info@idetop.com</a>
        </p>
    </center>
</div>
</center>
</body>
</html>

