<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8" />
        <meta name="google-site-verification" content="cfM15zJRYHBtIL-8teSrrVgsNsNz42UCDbYARVLyP8w" />
        <meta name="description" content="GSA, une entreprise du groupe GTF." />
        <meta name="keywords" content="" />

        <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/coda-slider-2.0.css" type="text/css" media="screen" />
        <link type="text/css" rel="stylesheet" href="css/styles.css" />

        <title>Portail de GSA, une entreprise du groupe GTF</title>
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <a href="index.html" class="logo"><img src="images/logo.png" alt="" /></a>
                <div class="menutop">
                    <a href="http://gsa-medical.fr" class="medical">
                        <img src="images/spacer.gif" />
                    </a>
                    <a href="http://gsa-pharma.fr" class="pharma">
                        <img src="images/spacer.gif" />
                    </a>
                    <a href="http://gsa-formation.fr" class="formation">
                        <img src="images/spacer.gif" />
                    </a>
                    <a href="http://gsa-recrutement.fr" class="recrutement">
                        <img src="images/spacer.gif" />
                    </a>
                </div>
                <p class="coordonnees">
                    01 47 61 96 31<br />
                    3 rue des Quatre Chemin&eacute;es<br />
                    92514 BOULOGNE CEDEX
                </p>
            </div>
            <br style="clear: both" />

            <!-- BANDEAU -->
            <div id="bandeau">

                <?php
                if (
                    (strpos($_SERVER['HTTP_USER_AGENT'],'iPad')         != false) ||
                    (strpos($_SERVER['HTTP_USER_AGENT'],'iPod')         != false) ||
                    (strpos($_SERVER['HTTP_USER_AGENT'],'iPhone')       != false) ||
                    (strpos($_SERVER['HTTP_USER_AGENT'],'BlackBerry')   != false)
                ) :
                ?>
                <!-- FLASH = 0 -->
                <div id="flash-content" style="margin-top:50px;position:relative;left:15px">
                    <div class="about-our-company">
                        <div class="marque-accueil" style="width:660px">

                            <!--qui sommes-nous-->
                            <div>
                                <a href="qui-sommes-nous.html">
                                    <div class="boxgrid slideright" style="background:url('images/box1b.jpg') no-repeat center">
                                        <img class="cover" src="images/box1a.jpg" style="width:auto;height:auto" />
                                    </div>
                                </a>
                                <!--GSA Formation-->
                                <a href="http://gsa-formation.fr" alt="Site de GSA Formation">
                                    <div class="boxgrid slideright" style="background:url('images/box2b.jpg') no-repeat center">
                                        <img class="cover" src="images/box2a.jpg" style="width:auto;height:auto" />
                                    </div>
                                </a>
                                <!--GSA Pharma-->
                                <a href="http://gsa-pharma.fr" alt="Site de GSA Pharma">
                                    <div class="boxgrid slideright" style="background:url('images/box3b.jpg') no-repeat center;margin-right:0">
                                        <img class="cover" src="images/box3a.jpg" style="width:auto;height:auto" />
                                    </div>
                                </a>
                            </div>
                            <br style="clear:both" />

                            <div style="margin-top:63px">
                                <!--100% services-->
                                <a href="qui-sommes-nous.html#contenu">
                                    <div class="boxgrid slideright" style="background:url('images/box1b.jpg') no-repeat center">
                                        <img class="cover" src="images/box6a.jpg" style="width:auto;height:auto" />
                                    </div>
                                </a>
                                <!--GSA Recrutement-->
                                <a href="http://gsa-recrutement.fr" alt="Site de GSA Recrutement">
                                    <div class="boxgrid slideright" style="background:url('images/box5b.jpg') no-repeat center">
                                        <img class="cover" src="images/box5a.jpg" style="width:auto;height:auto" />
                                    </div>
                                </a>
                                <!--GSA M&eacute;dical-->
                                <a href="http://gsa-medical.fr" alt="Site de GSA M&eacute;dical">
                                    <div class="boxgrid slideright" style="background:url('images/box4b.jpg') no-repeat center;margin-right:0">
                                        <img class="cover" src="images/box4a.jpg" style="width:auto;height:auto" />
                                    </div>
                                </a>
                            </div>
                            <br style="clear:both" />
                        </div>
                    </div>
                    <br style="clear:both" />
                </div>

                <?php else : ?>
                <!-- FLASH = 1 -->
                <div id="flash-content">
                    <script type="text/javascript">
                        var so = new SWFObject("menu-portail.swf", "menu-portail.swf", "700", "450", "", "");
                        so.addParam("allowScriptAccess", "sameDomain");
                        so.addParam("wmode", "transparent");
                        so.write("flash-content");
                    </script>
                </div>
                <?php endif; ?>


                <div id="candidats">
                    <a href="contact.php" class="contact">contactez-nous</a>
                    <a href="" class="out">
                        <img src="images/outoffrance.jpg" />
                    </a>

                    <div>
                        <p>
                            <br /><br /><br /><br /><br />
                            <a href="http://gsa-recrutement.fr/AWA/" target="_blank">
                                Nous recherchons des <b>profils uniques</b> pour des <b>exp&eacute;riences professionnelles</b> d’exception.
                            </a>
                        </p>
                        <div id="liens">
                            <a href="http://gsa-recrutement.fr/AWA/" target="_blank"> > Consulter les offres d'emploi</a><br />
                            <a href="http://gsa-recrutement.fr/AWA/candidature-spontanee.htm" target="_blank"> > D&eacute;posez votre CV</a>
                            <a href="http://gsa-recrutement.fr/AWA/" target="_blank"> > Alertes E-mails</a>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

        <!-- CONTENU -->
        <div id="contenu" style="margin-top: 0px;">
            <div id="colgauche">
                <div class="left webtv">
                    <div id="flash-content2">
                        <script type="text/javascript">
                            var so = new SWFObject("anim-logo.swf", "anim-logo.swf", "480", "320", "", "");
                            so.addParam("allowScriptAccess", "sameDomain");
                            so.addParam("wmode", "transparent");
                            so.write("flash-content2");
                        </script>
                    </div>
                    <br style="clear:both" />
                    <h4>GSA se dote d'une nouvelle identit&eacute;</h4>
                    <p>
                        La nouvelle identit&eacute; visuelle de GSA renforce l'identit&eacute; de ses quatre pôles : GSA Recrutement, GSA Formation, GSA Pharma et GSA M&eacute;dical.
                    </p>
                </div>
                <div id="carou" class="left">
                    <h3>Galerie Photos</h3>

                    <div id="coda-nav-left-1" class="btn gauche prev">
                        <img src="images/fleche-gauche.png" />
                    </div>

                    <div id="slide-container">

                        <div class="coda-slider-wrapper">
                            <div class="coda-slider preload" id="coda-slider-1">
                                <div class="panel">
                                    <!-- slide -->
                                    <div class="panel-wrapper">
                                        <a href=""><img src="images/carou3.jpg" /></a>
                                        <div class="legende">
                                            <h4>S&eacute;minaire 2011</h4>
                                            <p style="text-align:left">Rassemblement d'un r&eacute;seau multi-produits orient&eacute; vente directe pharmacie</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <!-- slide -->
                                    <div class="panel-wrapper">
                                        <a href=""><img src="images/carou2.jpg" /></a>
                                        <div class="legende">
                                            <h4>S&eacute;minaire 2011</h4>
                                            <p style="text-align:left">Rassemblement d'un r&eacute;seau multi-produits orient&eacute; vente directe pharmacie</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel">
                                    <!-- slide -->
                                    <div class="panel-wrapper">
                                        <a href=""><img src="images/carou1.jpg" /></a>
                                        <div class="legende">
                                            <h4>S&eacute;minaire 2011</h4>
                                            <p style="text-align:left">Rassemblement d'un r&eacute;seau multi-produits orient&eacute; vente directe pharmacie</p>
                                        </div>
                                    </div>
                                </div>
                                </div><!-- .coda-slider-wrapper -->
                            </div>

                        </div>

                        <div id="coda-nav-right-1" class="btn droite next">
                            <img src="images/fleche-droite.png" class="next" />
                        </div>

                    </div>

                </div>
                <div id="coldroite">
                    <div class="left">
                        <p class="extrait">
                            Bienvenue dans le groupe GTF, une entreprise où les bonnes relations humaines sont plac&eacute;es au cœur des activit&eacute;s.
                        </p>
                        <p>
                            Depuis 1986, le groupe GTF &eacute;crit son histoire en se positionnant comme le point de ralliement des industries de sant&eacute;.
                        </p>
                        <a href="qui-sommes-nous.html" class="lecture-video">
                            En savoir plus sur GTF
                        </a>
                    </div>
                    <br style="clear: left;"/><br />
                    <div class="left">
                        <ul>
                            Les dates cl&eacute;s du groupe GTF, de sa naissance à aujourd'hui.
                        </ul>
                        <a href="qui-sommes-nous.html#bandeau_timeline" class="lecture-video">
                            En savoir +
                        </a>
                    </div>
                    <div class="opssis">
                        <p><b>GSA</b> adhère à la Convention Collective de l’Industrie Pharmaceutique et est membre de l’OPPSIS</p>
                        <a href="oppsis.html" class="lecture-video">En savoir +</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div id="ss-footer">
                <div class="bloc_pole">
                    <a href="http://gsa-groupe.fr" class="retour-gsa">
                        <img src="images/spacer-footer.gif" />
                    </a>
                    <div class="blocs-footer">
                        <a href="http://gsa-medical.fr" class="lien_pole"><img src="images/carre-medical2.jpg" alt="" /></a>
                        <a href="http://gsa-pharma.fr" class="lien_pole"><img src="images/carre-pharma2.jpg" alt="" /></a>
                        <a href="http://gsa-formation.fr" class="lien_pole"><img src="images/carre-formation2.jpg" alt="" /></a>
                        <a href="http://gsa-recrutement.fr" class="lien_pole"><img src="images/carre-recrutement2.jpg" alt="" /></a>
                    </div>

                </div>
                <div>
                    <span><a href="http://maps.google.fr/maps?q=Gerard+Sinabian+Associes+-+GSA,+Boulogne-Billancourt&hl=fr&ie=UTF8&sll=50.972242,1.493266&sspn=1.400924,3.56781&oq=gsa+boulogn&hq=Gerard+Sinabian+Associes+-+GSA,&hnear=Boulogne-Billancourt,+Hauts-De-Seine,+%C3%8Ele-de-France&t=m&z=14&iwloc=A" target="_blank">plan d'accès</a></span>
                    <h2>Contact</h2>
                    <p style="color:#6D6D6D">
                        0 147 619 631<br />
                        3, rue des 4 chemin&eacute;es<br />
                        92514 BOULOGNE CEDEX
                    </p>
                </div>
                <br style="clear: left" /><br /><br />
                <p class="lien_footer">
                    <a href="index.html">Accueil</a>&nbsp;&nbsp;/&nbsp;&nbsp;
                    <a href="http://gsa-groupe.fr">GSA Groupe GTF</a>&nbsp;&nbsp;/&nbsp;&nbsp;
                    <a href="http://gsa-medical.fr">Vente et Promotion m&eacute;dicale</a>&nbsp;&nbsp;/&nbsp;&nbsp;
                    <a href="http://gsa-formation.fr">Espace Management et Formation</a>&nbsp;&nbsp;/&nbsp;&nbsp;
                    <a href="http://gsa-recrutement.fr">Espace Recrutement</a>&nbsp;&nbsp;/&nbsp;&nbsp;
                    <a href="http://gsa-recrutement.fr/AWA/" target="_blank">Espace Candidat</a>&nbsp;&nbsp;/&nbsp;&nbsp;
                    <a href="http://gsa-groupe.fr/mentions.php" target="_blank">Mentions L&eacute;gales</a>
                </p>
                <br /><br />
            </div>
        </div>

        <!-- Scripts -->

        <script type="text/javascript" src="js/swfobject.js"></script>
        <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/jquery.coda-slider-2.0.js"></script>

        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-29344630-1']);
            _gaq.push(['_trackPageview']);
            (function(){
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>

    </body>
</html>
