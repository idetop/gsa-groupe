<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');

if(!empty($_POST)){
    if(!empty($_POST['language'])){
        if($_POST['language'] == "fr"){
            $_SESSION['awa_language'] = "fr";
        }elseif($_POST['language'] == "en"){
            $_SESSION['awa_language'] = "en";
        }
        Debug::d_echo("changement de langue ".$_SESSION['awa_language'], 2,"act-change_language.php");
    }
}

?>
