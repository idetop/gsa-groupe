<?php
//Créé
//
//FIchier de configuration de l'affichage des messages d'erreurs
include_once "include/display_errors_bo.php";

//Ouverture de la session
@session_start();

Debug::d_echo("acces ", 2,"login.php");
Debug::d_print_r($_GET, 1,"GET","login.php");
Debug::d_print_r($_POST, 1,"POST","login.php");
Debug::d_print_r($_SESSION, 1,"SESSION","login.php");

include_once('../include/config.php');
@include_once 'include/pdo.php';
include_once "include/framework.php";

//print_t($_POST);

if(!empty($_POST['txt_login']) && !empty($_POST['txt_passwd'])){

    $login=$_POST['txt_login'];
    $mdp=$_POST['txt_passwd'];

    $req = "SELECT
                ID,
                LOGIN
          FROM
                awa_recruteurs
          WHERE
                LOGIN LIKE :email
                AND PASS LIKE AES_ENCRYPT(:mdp,'admen')
                AND ACTIVE = 1
            ";

    $select = $conn->prepare($req);
    $select->bindParam(':email', $login, PDO::PARAM_STR);
    $select->bindParam(':mdp', $mdp, PDO::PARAM_STR);
    $select->execute();
    
    $data = null ;
    $data = $select->fetchObject();
    if($data){

	$_SESSION['boe_user_id'] = $data->ID;
	$_SESSION['boe_user_email'] = $data->LOGIN;
        Debug::d_echo("login ok ".$data->ID, 2,"login.php");
	header('Location: configuration/v-config.php');
        exit();
    } else {
        Debug::d_echo("login failed ".$_POST['txt_login'], 2,"check-login.php");
	 unset($_POST);
	 $_SESSION['boe_error_log'] = _LOGIN_FORM_ERROR_BAD_LOGINMDP;
	 header('Location:./');
         exit();
    }
}else{
    Debug::d_echo("login failed ", 2,"check-login.php");
    $_SESSION['boe_error_log'] = _LOGIN_FORM_ERROR_EMPTY_LOGINMDP;
    header('Location:./');
    exit();
}

?>
