<?php

$pole = str_replace("gsa-","",$_REQUEST['page']);

// controle des accès
if($statut != 'gsa'){
    if($statut != $pole){
        echo "
        <script>
        alert('Cette espace ne vous est pas autorisé. Vous allez redirigé vers la page d\'accueil.');
        window.location.href='index.php?page=accueil';
        </script>";
    }
}


####################################################################################################################
# TEXTE ACCUEIL
####################################################################################################################

if($part == 'accueil'){

    #########################
    # UPDATE
    #########################
    if(! empty($_REQUEST['update'])){
        $intro1         = addslashes($_REQUEST['intro1']);
        $intro2         = addslashes($_REQUEST['intro2']);
        $update_texte   = mysqli_query($connexion, "UPDATE gsa_pole_accueil SET intro1 = '".$intro1."',intro2 = '".$intro2."' WHERE pole = '".$pole."'");
        // update photo
        if(! empty($_FILES['image']['name'])){
            // renomme
            $extension  = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);
            $name       = 'GSA-'.rand(10,9999999999);
            $photo      = $name.".".$extension;
            upload_photo($photo,$_FILES['image']['tmp_name'],'../'.$pole.'/images/','328');
            $update_photo = mysqli_query($connexion, "UPDATE gsa_pole_accueil SET img = '".$photo."' WHERE pole = '".$pole."'");
        }
        $retour         = "success";
        $alerte         = "Les textes et image ont bien été mis à jour.";
    }

    #########################
    # EDITER
    #########################
    $sql_edit       = mysqli_query($connexion, "SELECT * FROM gsa_pole_accueil WHERE pole = '".$pole."'");
    $data_edit      = mysqli_fetch_array($sql_edit);
}



####################################################################################################################
# ARTICLES
####################################################################################################################

else{

    #########################
    # AJOUTER
    #########################
    if($_REQUEST['action'] == 'add'){
        $sql_last       = mysqli_query($connexion, "SELECT * FROM gsa_pole_textes WHERE pole = '".$pole."' AND page = '".$_GET['part']."' ORDER BY ordre DESC LIMIT 0,1");
        $data_last      = mysqli_fetch_array($sql_last);
        $val            = $data_last['ordre']+1;
        $val_max        = $val;
    }
    if(! empty($_REQUEST['submit_add'])){
        $titre          = addslashes($_REQUEST['titre']);
        $texte          = addslashes($_REQUEST['texte']);
        // insert
        $insert         = mysqli_query($connexion, "INSERT INTO gsa_pole_textes (pole,page,titre,texte) VALUES ('".$pole."','".$part."','".$titre."','".$texte."')");
        // insert photo
        if(! empty($_FILES['photo']['name'])){
            $sql_last   = mysqli_query($connexion, "SELECT * FROM gsa_pole_textes ORDER BY id DESC LIMIT 0,1");
            $dat_last   = mysqli_fetch_array($sql_last);
            // renomme
            $extension  = pathinfo($_FILES['photo']['name'],PATHINFO_EXTENSION);
            $name       = 'GSA-'.rand(10,9999999999);
            $photo      = $name.".".$extension;
            upload_photo($photo,$_FILES['photo']['tmp_name'],'../'.$pole.'/images/','245');
            $update_photo = mysqli_query($connexion, "UPDATE gsa_pole_textes SET image = '".$photo."' WHERE id = '".$dat_last['id']."'");
        }
        $retour         = "success";
        $alerte         = "Le nouveau paragraphe a bien été enregistré.";
    }

    #########################
    # UPDATE ARTICLES
    #########################
    if(! empty($_REQUEST['update'])){
        $id_update      = $_REQUEST['id_update'];
        $titre          = addslashes($_REQUEST['titre']);
        $texte          = addslashes($_REQUEST['texte']);
		$ordre_old      = $_REQUEST['ordre_old'];
        $ordre_new      = $_REQUEST['ordre'];
        // update texte
        $update_texte   = mysqli_query($connexion, "UPDATE gsa_pole_textes SET titre = '".$titre."',texte = '".$texte."',ordre = '".$ordre_old."' WHERE id = '".$id_update."'");
        // update photo
        if(! empty($_FILES['photo']['name'])){
            // renomme
            $extension  = pathinfo($_FILES['photo']['name'],PATHINFO_EXTENSION);
            $name       = 'GSA-'.rand(10,9999999999);
            $photo      = $name.".".$extension;
            upload_photo($photo,$_FILES['photo']['tmp_name'],'../'.$pole.'/images/','245');
            $update_photo = mysqli_query($connexion, "UPDATE gsa_pole_textes SET image = '".$photo."' WHERE id = '".$id_update."'");
        }
        // supprimer image
        if(! empty($_REQUEST['id_del_image'])){
            // suppression de l'image
            $del_image      = mysqli_query($connexion, "UPDATE gsa_pole_textes SET image = '' WHERE id = '".$id_update."'");
        }
        // update ordre
        if($ordre_old != $ordre_new){
            $update_ordre1 = mysqli_query($connexion, "UPDATE gsa_pole_textes SET ordre = '".$ordre_old."' WHERE ordre = '".$ordre_new."'");
            $update_ordre2 = mysqli_query($connexion, "UPDATE gsa_pole_textes SET ordre = '".$ordre_new."' WHERE id = '".$id_update."'");
        }
        $retour         = "success";
        $alerte         = "Le paragraphe à bien été mis à jour.";
    }

    #########################
    # UPDATE TEXTES INTRO
    #########################
    if(! empty($_REQUEST['submit_intro'])){
        $id_update      = $_REQUEST['id_intro'];
        $texte          = addslashes($_REQUEST['texte']);
        $texte          = str_replace("\r\n","<br />",($texte));
        // update texte
        $update_texte   = mysqli_query($connexion, "UPDATE gsa_pole_head SET texte = '".$texte."' WHERE id = '".$id_update."'");
        $retour         = "success";
        $alerte         = "Le texte à bien été mis à jour.";
    }

    #########################
    # SUPPRIMER ARTICLE
    #########################
    if(! empty($_REQUEST['id_del'])){
        $id_del         = $_REQUEST['id_del'];
        // suppression du slide
        $delete         = mysqli_query($connexion, "DELETE FROM gsa_pole_textes WHERE id = '".$id_del."'");
        // ok
        $retour         = "success";
        $alerte         = "Le paragraphe à bien été supprimé.";
    }

    #########################
    # EDITER
    #########################
    if(! empty($_REQUEST['id'])){
        $sql_edit       = mysqli_query($connexion, "SELECT * FROM gsa_pole_textes WHERE id = '".$_REQUEST['id']."'");
        $data_edit      = mysqli_fetch_array($sql_edit);
        $image          = '../'.$pole.'/images/'.$data_edit['image'];
        if(empty($data_edit['image'])){
            $image = '../images/no-photo.jpg';
        }
		$val            = $data_edit['ordre'];
        // find dernier article
        $sql_last       = mysqli_query($connexion, "SELECT * FROM gsa_pole_textes WHERE pole = '".$pole."' AND page = '".$_GET['part']."' ORDER BY ordre DESC LIMIT 0,1");
        $data_last      = mysqli_fetch_array($sql_last);
        $val_max        = $data_last['ordre'];
        $cursor_moins   = "pointer";
        $cursor_plus    = "pointer";
        $disabled_moins = ''; if($data_edit['ordre'] == '1') { $disabled_moins = 'disabled'; $cursor_moins = "no-drop"; }
        $disabled_plus  = ''; if($data_edit['ordre'] == $data_last['ordre']) { $disabled_plus = 'disabled'; $cursor_plus = "no-drop"; }
    }

    #########################
    # liste des articles
    #########################
    function liste_articles($polename,$pagename){
        $sql = mysqli_query($connexion, "SELECT * FROM gsa_pole_textes WHERE pole = '".$polename."' AND page = '".$pagename."' ORDER BY ordre ASC");
        while($row = mysqli_fetch_array($sql)){
            $color = ++$i % 2 ? '#ffffff':'#EAEAEA';
            $image = '../'.$polename.'/images/'.$row['image'];
            if(empty($row['image'])){
                $image = '../images/no-photo.jpg';
            }
            echo '
            <tr class="tr_content" style="background:'.$color.'">
                <td class="td_content" valign="top">
                    <a href="index.php?page='.$_REQUEST['page'].'&part='.$_REQUEST['part'].'&action=edit&id='.$row['id'].'"><img src="'.$image.'" width="100" /></a>
                </td>
                <td class="td_content" valign="top" style="text-align:left">
                    <b><a href="index.php?page='.$_REQUEST['page'].'&part='.$_REQUEST['part'].'&action=edit&id='.$row['id'].'">'.stripslashes($row['titre']).'</a></b>
                    <p>'.substr(stripslashes($row['texte']),0,300).' ...</p>
                </td>
                <td width="60" valign="top" style="padding:5px;">
                    <form name="del_fiche" method="post" action="" style="margin-top:17px">
                        <input type="hidden" name="id_del" value="'.$row['id'].'">
                        <input type="hidden" name="statut_del" value="'.$row['statut'].'">
                        <input id="submit_del" class="vtip" title="Supprimer" name="del" type="submit" value="" style="float:right" onClick="return confirm(\'\nEtes-vous sur de vouloir supprimer ce paragraphe ?\n\n\');"><br /><br />
                        <a class="vtip" title="Editer" href="index.php?page='.$_REQUEST['page'].'&part='.$_REQUEST['part'].'&action=edit&id='.$row['id'].'">
                            <input id="submit_accept" type="button" value="" style="float:right;background:url(\'src/icons/page_white_edit.png\') no-repeat;">
                        </a>
                    </form>
                </td>
            </tr>';
        }
    }
    // texte d'intro
    $sql_head = mysqli_query($connexion, "SELECT * FROM gsa_pole_head WHERE pole = '".$pole."' AND page = '".$part."'");
    $dat_head = mysqli_fetch_array($sql_head);

}
?>
