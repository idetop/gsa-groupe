<?php // include('system/libs/tinymce/tinymce.php');  ?>

    <!-- Bouton nouveau -->
    <div id="body_btn_nouveau">
        <a href="<?= $site; ?>/qui-sommes-nous.html" class="link_nouveau" target="_blank">
            <div class="btn_nouveau"><span class="span_nouveau">Accéder à la page <font color="#b9001f">&raquo;</font></span></div>
        </a>
        <a href="<?= $_SERVER['PHP_SELF']; ?>?page=<?= $page; ?>&part=<?= $part; ?>&action=add" class="link_nouveau">
            <div class="btn_nouveau"><span class="span_nouveau">Ajouter une date <font color="#b9001f">+</font></span></div>
        </a>
    </div>
    <div style="background:#D6D6D6;width:100%;padding:5px">aaa</div>
    <div class="clear"></div>
</div>


<div id="content">
    <!-- Colonne gauche -->
    <?php include($b_templates.'side_left.php'); ?>

    <!-- Colonne droite -->
    <div class="colonne_right">

		<div id="title_line"><span class="span_title_line">Historique du Groupe GTF (<?php return_count('gsa_about_historique','',''); ?>)</span></div>

        <?php if(empty($_REQUEST['id']) && empty($_REQUEST['action'])) {  ?>
        <table class="table_right" width="760" cellpadding="6" cellspacing="0">
            <tbody>
                <tr class="tr_head">
                    <td valign="top" width="80" class="td_head" style="text-align:center">&nbsp;Année</td>
                    <td valign="top" class="td_head" style="text-align:left">&nbsp;Texte</td>
                    <td valign="top" width="40" class="td_head">Edit</td>
                </tr>
                <?php liste_dates(); ?>
            </tbody>
        </table>
        <br /><br />
		<?php } ?>

		<?php if($_REQUEST['action'] == "edit") { ?>
		<form name="modif_fiche" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="ordre_old" value="<?= $val; ?>">
            <input type="hidden" name="photo_old" value="<?= $data_edit['photo']; ?>">
            <input type="hidden" name="id_update" value="<?= $data_edit['id']; ?>">
            <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
                <tbody>
                    <tr style="background:#252525;">
                        <td colspan="2" style="padding:6px;color:#fff;">
                            &nbsp;&nbsp;<strong style="font-size:11px;"><?= stripslashes($data_edit['nom']); ?></strong>
                        </td>
                    </tr>

                    <tr style="background:#ffffff;">
                        <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                            <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Détails du poste</strong>
                        </td>
                    </tr>

                    <tr style="background:#fbf9f9">
                        <td valign="top" width="160" style="padding:10px;margin-right:20px;">
                            <img src="../images/<?= $data_edit['photo']; ?>" />
                        </td>
                        <td valign="top" style="padding:5px;padding-top:0">
                            <p style="font-size:11px;margin-bottom:3px"><b>Changer la photo</b> (dimensions optimales : 150 x 150px)</p>
                            <input type="file" name="photo" class="text" value="" size="76" />
                            <p style="font-size:11px;margin-bottom:3px"><b>Nom</b></p>
                            <input type="text" class="text" name="nom" value="<?= stripslashes($data_edit['nom']); ?>" style="width:550px" />
                            <p style="font-size:11px;margin-bottom:3px"><b>Poste</b></p>
                            <input type="text" class="text" name="poste" value="<?= stripslashes($data_edit['poste']); ?>" style="width:550px" />
                            <p style="font-size:11px;margin-bottom:3px"><b>Pôle</b></p>
                            <input type="text" class="text" name="pole" value="<?= stripslashes($data_edit['pole']); ?>" style="width:550px" />
                            <p style="font-size:11px;margin-bottom:3px"><b>Niveau d'affichage</b></p>
                            <select name="statut" class="text" style="width:250px"><?php options_statut($data_edit['statut']); ?> </select>
                            <p style="font-size:11px;margin-bottom:3px"><b>Ordre</b></p>
                            <input type="button" value="-" class="btn_qty" id="moins" onClick="incrementer(-1);cacher_moins();cacher_plus();" style="padding:2px 7px;cursor:<?= $cursor_moins; ?>" <?= $disabled_moins; ?>>
                            <input type="text" name="ordre" value="<?= $val; ?>" class="text" id="qty" style="width:30px;text-align:center;"
                            onFocus="this.style.border='1px solid #efd006';this.style.background='#fdfae5';"
                            onBlur="this.style.border='1px solid #ccc';this.style.background='#fff';">
                            <input type="button" class="btn_qty" id="plus" value="+" onClick="incrementer(+1);cacher_moins();cacher_plus();" style="padding:2px 7px;cursor:<?= $cursor_plus; ?>" <?= $disabled_plus; ?>>&nbsp;&nbsp;
                            <span style="font-size:11px;color:#777777">(ordre d'affichage, de gauche à droite)</span>
                        </td>
                    </tr>

                    <tr><td colspan="2" height="20"></td></tr>

                </tbody>
            </table>
            <table class="table_right" width="760" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr style="background:#e9e7e7;">
                        <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                            <input type="submit" name="update" class="table_submit" value="enregistrer mes modifications" />
                        </td>
                    </tr>
                </tbody>
            </table><br /><br />
		</form>
		<?php } ?>


		<?php if($_REQUEST['action'] == "add") { ?>
		<form name="modif_fiche" method="post" action="index.php?page=<?= $page; ?>&part=<?= $part; ?>" enctype="multipart/form-data">
            <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
                <tbody>
                    <tr style="background:#252525;">
                        <td colspan="2" style="padding:6px;color:#fff;">
                            &nbsp;&nbsp;<strong style="font-size:11px;">Nouveau poste</strong>
                        </td>
                    </tr>

                    <tr style="background:#ffffff;">
                        <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                            <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Détails du poste</strong>
                        </td>
                    </tr>

                    <tr style="background:#fbf9f9">
                        <td valign="top" width="160" style="padding:10px;margin-right:20px;">
                            <img src="../images/no-photo.jpg" />
                        </td>
                        <td valign="top" style="padding:5px;padding-top:0">
                            <p style="font-size:11px;margin-bottom:3px"><b>Choisir une photo</b> (dimensions optimales : 150 x 150px)</p>
                            <input type="file" name="photo" class="text" value="" size="76" />
                            <p style="font-size:11px;margin-bottom:3px"><b>Nom</b></p>
                            <input type="text" class="text" name="nom" value="" style="width:550px" />
                            <p style="font-size:11px;margin-bottom:3px"><b>Poste</b></p>
                            <input type="text" class="text" name="poste" value="" style="width:550px" />
                            <p style="font-size:11px;margin-bottom:3px"><b>Pôle</b></p>
                            <input type="text" class="text" name="pole" value="" style="width:550px" />
                            <p style="font-size:11px;margin-bottom:3px"><b>Niveau d'affichage</b></p>
                            <select name="statut" class="text" style="width:250px">
                                <option value="1">Mettre en avant sur fond blanc</option>
                                <option value="2">Liste normale sur fond neutre</option>
                            </select>
                            <p style="font-size:11px;margin-bottom:3px"><b>Ordre</b></p>
                            <span style="color:#595959">Vous aurez la possibilité de modifier l'ordre de ce poste une fois sa fiche créée.</span>
                        </td>
                    </tr>

                    <tr><td colspan="2" height="20"></td></tr>

                </tbody>
            </table>
            <table class="table_right" width="760" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr style="background:#e9e7e7;">
                        <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                            <input type="submit" name="submit_add" class="table_submit" value="Enregistrer ce nouveau poste">
                        </td>
                    </tr>
                </tbody>
            </table><br /><br />
		</form>
		<?php } ?>

    </div>
    <div class="clear"></div>
</div>
