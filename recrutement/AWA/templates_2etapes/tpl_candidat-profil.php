<div id="container980">
    <div id="header"></div>
    <div id="subheader">
        <div id="pathway">
            <?php echo $templateDatas['pathway'];  ?>
        </div>
        <div id="languesdisplay">
            <?php echo $templateDatas['flags']; ?>
        </div>
        <div class="spacer"></div>
    </div>
    <?php echo $templateDatas['candidatArea']; ?>
    <div id="middle">
        <div id="menu">
             <?php echo $templateDatas['menu']; ?>
        </div>
        <div id="content">
            <div class="contenttitle"><h1><?php echo _ESPACECANDIDAT_PROFIL_TITLE; ?></h1></div>
            <div style="height: 20px;"></div>
            <?php if(!empty($_GET['save'])){ ?>
                <div class="messageok"><?php echo _ESPACECANDIDAT_PROFIL_SAVED_MESSAGE; ?></div>
            <?php } ?>

            <div style="padding-left: 100px;">
                <form id="candidature_form" action="profil-save.php" method="post" onsubmit="" enctype="multipart/form-data">
                    <fieldset>
                        <?php echo $formulaire_HTML; ?>
                        <div style="padding-top: 35px;padding-left: 230px;">
                            <div id="btn_envoyer" class="buttonclass" onclick="checkForm();"><?php echo _CANDIDATURE_CS_SUBMIT; ?></div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div style="height: 30px;"></div>
        </div>
        <div class="spacer"></div>
    </div>
    <div id="footer">
         <?php echo $templateDatas['footerText']; ?>
    </div>
</div>