<?php

/*
 * Lecture du fichier cvsx et décodage
 */

if(_IS_IMPORT_ENCODAGE){
    if(_ADMEN_USE_ADVERT_LINES){
        Debug::d_echo("import tannonces ", 2,"updateDba-annonces.php");
        if(!is_file(_AWA_PATHWAY."/import_files/tannonces.csvx")){
            Debug::d_echo("fichier "._AWA_PATHWAY."/import_files/tannonces.csvx manquant", 2,"updateDba-annonces.php");
            exit();
        }
        $datafile = file_get_contents(_AWA_PATHWAY."/import_files/tannonces.csvx");
    }else{
        Debug::d_echo("import annonces ", 2,"updateDba-annonces.php");
        if(!is_file(_AWA_PATHWAY."/import_files/annonces.csvx")){
            Debug::d_echo("fichier "._AWA_PATHWAY."/import_files/annonces.csvx manquant", 2,"updateDba-annonces.php");
            exit();
        }
        $datafile = file_get_contents(_AWA_PATHWAY."/import_files/annonces.csvx");
    }
    Debug::d_echo("lecture du fichier", 2,"updateDba-annonces.php");
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

    $key = "admen";
    $datafile = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $datafile, MCRYPT_MODE_ECB, $iv);
    Debug::d_echo("decodage du fichier", 2,"updateDba-annonces.php");
}else{
    if(_ADMEN_USE_ADVERT_LINES){
        Debug::d_echo("import tannonces ", 2,"updateDba-annonces.php");
        if(!is_file(_AWA_PATHWAY."/import_files/tannonces.csv")){
            Debug::d_echo("fichier "._AWA_PATHWAY."/import_files/tannonces.csv manquant", 2,"updateDba-annonces.php");
            exit();
        }
        $datafile = file_get_contents(_AWA_PATHWAY."/import_files/tannonces.csv");
    }else{
        Debug::d_echo("import annonces ", 2,"updateDba-annonces.php");
        if(!is_file(_AWA_PATHWAY."/import_files/annonces.csv")){
            Debug::d_echo("fichier "._AWA_PATHWAY."/import_files/annonces.csv manquant", 2,"updateDba-annonces.php");
            exit();
        }
        $datafile = file_get_contents(_AWA_PATHWAY."/import_files/annonces.csv");
    }
}






//$datafile = utf8_encode($datafile);
if(_DEBUG_IMPORT){
    print($datafile);
}



/*
 * Récuparation de chaque ligne CSV
 */
//preg_match_all('#([\# a-zA-Z1-9;_]*?)[@]{4}\s#', $datafile, $rows);
preg_match_all('#(.*?)[@]{4}\s#', $datafile, $rows);
if(_DEBUG_IMPORT){
    echo "<pre>";
    print_r($rows);
    echo "</pre>";
}



/*
 * Récuparation des noms de colonnes
 */
preg_match_all('`[#]{4}(.*?)[#]{4};?`', $rows[1][0], $matches);

$columns_names = array();
$columns_names = $matches[1];

if(_DEBUG_IMPORT){
    echo "liste des columns"."\n";
    echo "<pre>";
    print_r($columns_names);
    echo "</pre>";
}

/*
 * Récupération des données
 */
$dba_rows = array();

foreach($rows[1] as $key=>$row){
    if($key != 0){
        preg_match_all('`[#]{4}?(.*?)[#]{4}?;?`', $row, $row_matches);
        $dba_rows[] = $row_matches[1];
        
    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "liste du contenu"."\n";
    echo "<pre>";
    print_r($dba_rows);
    echo "</pre>";
}



/*
 * Suppression des annonces de la DBA qui ne se trouve pas dans le fichier csv
 */

//On recherche la clé de ID_ANNONCE
$ID_ANNONCE_KEY = null;
foreach($columns_names as $key=>$columns_name){
    if(_DEBUG_IMPORT){
        echo $columns_name;
    }
    if($columns_name == "ID_ANNONCE"){
        $ID_ANNONCE_KEY = $key;
        break;
    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "index annonce id ".$ID_ANNONCE_KEY."\n";
}
//On recup les ID lieés à la clé ID_ANNONCE
$ID_ANNONCE_values = array();
foreach($dba_rows as $dba_row){
    $ID_ANNONCE_values[] = $dba_row[$ID_ANNONCE_KEY];
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "<pre>";
    print_r($ID_ANNONCE_values);
    echo "</pre>";
}
//On genere la requete SQL de suppression
$whereDeleteAnnonce = "";
foreach($ID_ANNONCE_values as $ID_ANNONCE_value){
    if($whereDeleteAnnonce == ""){
        $whereDeleteAnnonce .= "WHERE !( ID_ANNONCE = '".$ID_ANNONCE_value."' "."\n";
    }else{
        $whereDeleteAnnonce .= " OR ID_ANNONCE = '".$ID_ANNONCE_value."' "."\n";
    }
}
$whereDeleteAnnonce .= " ) "."\n";

//on supprime les annonces en trop
$sql = "
        DELETE FROM
            annonces
        ".$whereDeleteAnnonce."
";

$sql45 = "
        DELETE FROM
            tannonces
        ".$whereDeleteAnnonce."
";
if(_ADMEN_USE_ADVERT_LINES){
    $sql = $sql45;
}
if(_DEBUG_IMPORT){
    print_t($sql);
}
$select = $conn->prepare($sql);
$select->execute();

if(_DEBUG_IMPORT){
    echo $select->rowCount();
    Debug::d_echo("Suppression de ".$select->rowCount()." elements", 2,"updateDba-annonces.php");
}

/*
 * INSERT ou UPDATE des données en DBA
 */
foreach($dba_rows as $dba_row){
    $sql = "
            SELECT
                ID_ANNONCE
            FROM
                annonces
            WHERE
                ID_ANNONCE = '".$dba_row[$ID_ANNONCE_KEY]."'
    ";

    $sql45 = "
            SELECT
                ID_ANNONCE
            FROM
                tannonces
            WHERE
                ID_ANNONCE = '".$dba_row[$ID_ANNONCE_KEY]."'
    ";
    if(_ADMEN_USE_ADVERT_LINES){
        $sql = $sql45;
    }
    if(_DEBUG_IMPORT){
        print_t($sql);
    }
    $select = $conn->prepare($sql);
    $select->execute();
    $selectResult = null;
    $selectResult = $select->fetchObject();
    if($selectResult){
        //UPDATE
        $updateSet = "";
        foreach($columns_names as $key=>$columns_name){
            if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                $datatext = "";
                $datatext = str_replace("'", "''", $dba_row[$key]);
                $datatext = str_replace("§§", "\r\n", $datatext);
                if($updateSet == ""){
                    $updateSet .= $columns_name."='".$datatext."'";
                }else{
                    $updateSet .= ",\n".$columns_name."='".$datatext."'";
                }
            }
        }
        $sql = "
            UPDATE
                annonces
            SET
                ".$updateSet."
            WHERE
                ID_ANNONCE = '".$dba_row[$ID_ANNONCE_KEY]."'
        ";
        
        $sql45 = "
            UPDATE
                tannonces
            SET
                ".$updateSet."
            WHERE
                ID_ANNONCE = '".$dba_row[$ID_ANNONCE_KEY]."'
        ";
        if(_ADMEN_USE_ADVERT_LINES){
            $sql = $sql45;
        }
//        $sql = utf8_encode($sql);
        if(_DEBUG_IMPORT){
            print_t($sql);
        }
        $select = $conn->prepare($sql);
        $select->execute();
        Debug::d_echo("Update ".$dba_row[$ID_ANNONCE_KEY], 2,"updateDba-annonces.php");
        if(_DEBUG_IMPORT){
            print_t("###################################################################################################");
        }

    }else{
        //INSERT
        $insertSet = "";
        foreach($columns_names as $key=>$columns_name){
            if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                $datatext = "";
                $datatext = str_replace("'", "''", $dba_row[$key]);
                $datatext = str_replace("§§", "\r\n", $datatext);
                if($insertSet == ""){
                    $insertSet .= $columns_name."='".$datatext."'";
                }else{
                    $insertSet .= ",\n".$columns_name."='".$datatext."'";
                }
            }
        }
        $sql = "
            INSERT INTO
                annonces
            SET
                ".$insertSet."
        ";

        $sql45 = "
            INSERT INTO
                tannonces
            SET
                ".$insertSet."
        ";
        if(_ADMEN_USE_ADVERT_LINES){
            $sql = $sql45;
        }
//        $sql = utf8_encode($sql);
        if(_DEBUG_IMPORT){
            print_t($sql);
        }
        $select = $conn->prepare($sql);
        $select->execute();
        Debug::d_echo("insert ".$dba_row[$ID_ANNONCE_KEY], 2,"updateDba-annonces.php");
        if(_DEBUG_IMPORT){
            print_t("###################################################################################################");
        }

    }
}
Debug::d_echo("#######################################################################", 2,"updateDba-annonces.php");

?>
