
    <div class="colonne_left">

        <?php if ($page == 'accueil') : ?><li class="menu_left_active" onClick="window.location.href='index.php?page=accueil'">
        <?php else : ?><li class="menu_left" onClick="window.location.href='index.php?page=accueil'">
        <?php endif; ?>Accueil</li>

        <?php if ($statut == 'gsa') : ?>
            <?php if ($page == 'user') : ?><li class="menu_left_active" onClick="window.location.href='index.php?page=user'">
            <?php else : ?><li class="menu_left" onClick="window.location.href='index.php?page=user'">
            <?php endif; ?>Administrateurs</li>

            <?php if ($page == 'qui-sommes-nous') : ?><li class="menu_left_active" onClick="window.location.href='index.php?page=qui-sommes-nous&part=qui'">
            <?php else : ?><li class="menu_left" onClick="window.location.href='index.php?page=qui-sommes-nous&part=qui'">
            <?php endif; ?>
                Qui sommes-nous ?
                <div class="submenu_left">
                    <hr class="clear" />
                    <a href="index.php?page=qui-sommes-nous&part=qui">Qui sommes-nous</a><hr class="clear light" />
                    <a href="index.php?page=qui-sommes-nous&part=ambition">Notre ambition</a>
                </div>
            </li>
        <?php endif; ?>

        <?php if ($statut == 'formation' || $statut == 'gsa') { ?>
        <?php if ($page == 'gsa-formation') { ?><li class="menu_left_active" onClick="window.location.href='index.php?page=gsa-formation&part=accueil'">
        <?php } else { ?><li class="menu_left" onClick="window.location.href='index.php?page=gsa-formation&part=accueil'"><?php } ?>
            GSA Formation
            <div class="submenu_left">
                <hr class="clear" />
                <a href="index.php?page=gsa-formation&part=accueil">Accueil</a><hr class="clear light" />
                <a href="index.php?page=gsa-formation&part=ecole-visite-medicale">Ecole de visite médicale</a><hr class="clear light" />
                <a href="index.php?page=gsa-formation&part=formation-intra-entreprise">Formation intra-entreprise</a><hr class="clear light" />
                <a href="index.php?page=gsa-formation&part=coaching-mode-emploi">Coaching mode d'emploi</a><hr class="clear light" />
                <a href="index.php?page=gsa-formation&part=animation-formation-medicale">Animation Formation Médicale</a><hr class="clear light" />
                <a href="index.php?page=gsa-formation&part=formation-specifique-pharmacie-pour-les-apm">Formation spécifique Pharmacie pour les APM</a><hr class="clear light" />
                <a href="index.php?page=gsa-formation&part=engagements">Engagements</a>
            </div>
        </li>
        <?php } ?>

        <?php if ($statut == 'pharma' || $statut == 'gsa') { ?>
        <?php if ($page == 'gsa-pharma') { ?><li class="menu_left_active" onClick="window.location.href='index.php?page=gsa-pharma&part=accueil'">
        <?php } else { ?><li class="menu_left" onClick="window.location.href='index.php?page=gsa-pharma&part=accueil'"><?php } ?>
            GSA Pharma
            <div class="submenu_left">
                <hr class="clear" />
                <a href="index.php?page=gsa-pharma&part=accueil">Accueil</a><hr class="clear light" />
                <a href="index.php?page=gsa-pharma&part=conseil-strategie-commerciale">Conseil en stratégie commerciale</a><hr class="clear light" />
                <a href="index.php?page=gsa-pharma&part=environnement-legislation">Environnement et législation</a><hr class="clear light" />
                <a href="index.php?page=gsa-pharma&part=methodologie">Méthodologie mode d'emploi</a><hr class="clear light" />
                <a href="index.php?page=gsa-pharma&part=reseaux-exclusifs">Réseaux exclusifs</a><hr class="clear light" />
                <a href="index.php?page=gsa-pharma&part=reseaux-multi-produits">Réseaux multi-produits</a><hr class="clear light" />
                <a href="index.php?page=gsa-pharma&part=engagements">Engagements</a>
            </div>
        </li>
        <?php } ?>

        <?php if ($statut == 'recrutement' || $statut == 'gsa') { ?>
        <?php if ($page == 'gsa-recrutement') { ?><li class="menu_left_active" onClick="window.location.href='index.php?page=gsa-recrutement&part=accueil'">
        <?php } else { ?><li class="menu_left" onClick="window.location.href='index.php?page=gsa-recrutement&part=accueil'"><?php } ?>
            GSA Recrutement
            <div class="submenu_left">
                <hr class="clear" />
                <a href="index.php?page=gsa-recrutement&part=accueil">Accueil</a><hr class="clear light" />
                <a href="index.php?page=gsa-recrutement&part=cabinet">Cabinet de recrutement mode d'emploi</a><hr class="clear light" />
                <a href="index.php?page=gsa-recrutement&part=entreprises">Espace entreprises</a><hr class="clear light" />
                <a href="index.php?page=gsa-recrutement&part=candidat">Espace candidats</a><hr class="clear light" />
                <a href="index.php?page=gsa-recrutement&part=recrutement">Recrutement de profils de haute compétence</a><hr class="clear light" />
                <a href="index.php?page=gsa-recrutement&part=engagements">Engagements</a>
            </div>
        </li>
        <?php } ?>

        <?php if ($statut == 'medical' || $statut == 'gsa') { ?>
        <?php if ($page == 'gsa-medical') { ?><li class="menu_left_active" onClick="window.location.href='index.php?page=gsa-medical&part=accueil'">
        <?php } else { ?><li class="menu_left" onClick="window.location.href='index.php?page=gsa-medical&part=accueil'"><?php } ?>
            GSA Médical
            <div class="submenu_left">
                <hr class="clear" />
                <a href="index.php?page=gsa-medical&part=accueil">Accueil</a><hr class="clear light" />
                <a href="index.php?page=gsa-medical&part=conseil-strategie-operationnelle">Conseil en stratégie opérationnelle</a><hr class="clear light" />
                <a href="index.php?page=gsa-medical&part=qualite-certification">Qualité et certification</a><hr class="clear light" />
                <a href="index.php?page=gsa-medical&part=methodologie">Méthodologie mode d'emploi</a><hr class="clear light" />
                <a href="index.php?page=gsa-medical&part=reseaux-exclusifs">Réseaux exclusifs</a><hr class="clear light" />
                <a href="index.php?page=gsa-medical&part=reseaux-multi-produits">Réseaux multi-produits</a><hr class="clear light" />
                <a href="index.php?page=gsa-medical&part=engagements">Engagements</a>
            </div>
        </li>
        <?php } ?>
    </div>
