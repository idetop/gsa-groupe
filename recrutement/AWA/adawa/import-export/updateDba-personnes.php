<?php


/*
 * Lecture du fichier cvsx et décodage
 */
Debug::d_echo("import personnes ", 2,"updateDba-personnes.php");
if(_IS_IMPORT_ENCODAGE){
    if(!is_file(_AWA_PATHWAY."/import_files/personnes.csvx")){
        Debug::d_echo("fichier "._AWA_PATHWAY."/import_files/personnes.csvx manquant", 2,"updateDba-personnes.php");
        exit();
    }
    Debug::d_echo("lecture du fichier", 2,"updateDba-personnes.php");
    $datafile = file_get_contents(_AWA_PATHWAY."/import_files/personnes.csvx");
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

    $key = "admen";
    $datafile = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $datafile, MCRYPT_MODE_ECB, $iv);
    Debug::d_echo("decodage du fichier", 2,"updateDba-personnes.php");
    //$datafile = utf8_encode($datafile);
}else{
    if(!is_file(_AWA_PATHWAY."/import_files/personnes.csv")){
        Debug::d_echo("fichier "._AWA_PATHWAY."/import_files/personnes.csv manquant", 2,"updateDba-personnes.php");
        exit();
    }
    Debug::d_echo("lecture du fichier", 2,"updateDba-personnes.php");
    $datafile = file_get_contents(_AWA_PATHWAY."/import_files/personnes.csv");
}
if(_DEBUG_IMPORT){
    print($datafile);
}


/*
 * Récuparation de chaque ligne CSV
 */
preg_match_all('#(.*?)[@]{4}\s#', $datafile, $rows);
if(_DEBUG_IMPORT){
    echo "<pre>";
    print_r($rows[1]);
    echo "</pre>";
}


/*
 * Récuparation des noms de colonnes
 */
preg_match_all('`[#]{4}(.*?)[#]{4};?`', $rows[1][0], $matches);

$columns_names = array();
$columns_names = $matches[1];

if(_DEBUG_IMPORT){
    echo "liste des columns"."\n";
    echo "<pre>";
    print_r($columns_names);
    echo "</pre>";
}

/*
 * Récupération des données
 */
$dba_rows = array();

foreach($rows[1] as $key=>$row){
    if($key != 0){
        preg_match_all('`[#]{4}?(.*?)[#]{4}?;?`', $row, $row_matches);
        $dba_rows[] = $row_matches[1];
        
    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "liste du contenu"."\n";
    echo "<pre>";
    print_r($dba_rows);
    echo "</pre>";
}


//On recherche la clé de ID_PERSONNE
$ID_PERSONNE_KEY = null;
foreach($columns_names as $key=>$columns_name){
    if(_DEBUG_IMPORT){
        echo $columns_name;
    }
    if($columns_name == "ID_PERSONNE"){
        $ID_PERSONNE_KEY = $key;
        break;
    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "index personne id ".$ID_PERSONNE_KEY."\n";
}







//On recherche la clé de ID_PERSONNE_WEB
$ID_PERSONNE_WEB_KEY = null;
foreach($columns_names as $key=>$columns_name){
    if(_DEBUG_IMPORT){
        echo $columns_name;
    }
    if($columns_name == "ID_PERSONNE_WEB"){
        $ID_PERSONNE_WEB_KEY = $key;
        break;
    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "index personne web id ".$ID_PERSONNE_WEB_KEY."\n";
}

////On recup les ID lieés à la clé ID_PERSONNE_WEB
//$ID_PERSONNE_WEB_values = array();
//foreach($dba_rows as $dba_row){
//    $ID_PERSONNE_WEB_values[] = $dba_row[$ID_PERSONNE_WEB_KEY];
//}
//echo ""."\n";
//echo "<pre>";
//print_r($ID_PERSONNE_WEB_values);
//echo "</pre>";


//On recherche la clé de EMAIL
$ID_MAIL_KEY = null;
foreach($columns_names as $key=>$columns_name){
    if($columns_name == "EMAIL"){
        $ID_MAIL_KEY = $key;
        break;
    }
}

//On recherche la clé de EMAIL_PERSO
$ID_EMAIL_PERSO_KEY = null;
foreach($columns_names as $key=>$columns_name){
    if($columns_name == "EMAIL_PERSO"){
        $ID_EMAIL_PERSO_KEY = $key;
        break;
    }
}




/*
 * INSERT ou UPDATE des données en DBA
 */



foreach($dba_rows as $dba_row){
//    $time_1 = microtime(true);
    $candidat_web_id = null;
    
    if($dba_row[$ID_PERSONNE_WEB_KEY]){
        Debug::d_echo("id web present ".$dba_row[$ID_PERSONNE_WEB_KEY], 2,"updateDba-personnes.php");
        //Vérification si dans DBA
        $sql = "
            SELECT
                ID_PERSONNE_WEB,
                EMAIL,
                EMAIL_PERSO
            FROM
                personnes
            WHERE
                ID_PERSONNE_WEB = '".$dba_row[$ID_PERSONNE_WEB_KEY]."'
        ";
        $select = $conn->prepare($sql);
        $select->execute();
        $selectResult = null;
        $selectResult = $select->fetchObject();
        if($selectResult){

            //UPDATE personne awa
            $updateSet = "";
            foreach($columns_names as $key=>$columns_name){
                if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                    $datatext = "";
                    $datatext = str_replace("'", "''", $dba_row[$key]);
                    $datatext = str_replace("§§", "\r\n", $datatext);
                    if($updateSet == ""){
                        $updateSet .= $columns_name."='".$datatext."'";
                    }else{
                        $updateSet .= ",\n".$columns_name."='".$datatext."'";
                    }
                }
            }
            $sql = "
                UPDATE
                    personnes
                SET
                    ".$updateSet."
                WHERE
                    ID_PERSONNE_WEB = '".$dba_row[$ID_PERSONNE_WEB_KEY]."'
            ";
            if(_DEBUG_IMPORT){
                print_t($sql);
            }
            $select = $conn->prepare($sql);
            $select->execute();
            Debug::d_echo("update ".$dba_row[$ID_PERSONNE_WEB_KEY], 2,"updateDba-personnes.php");

            $candidat_web_id = $dba_row[$ID_PERSONNE_WEB_KEY];





        }else{
            //ici on doit faire un insert dans le cas ou n'est plus présent en DBA
            $updateSet = "";
            foreach($columns_names as $key=>$columns_name){
                if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                    $datatext = "";
                    $datatext = str_replace("'", "''", $dba_row[$key]);
                    $datatext = str_replace("§§", "\r\n", $datatext);
                    if($updateSet == ""){
                        $updateSet .= $columns_name."='".$datatext."'";
                    }else{
                        $updateSet .= ",\n".$columns_name."='".$datatext."'";
                    }
                }
            }
            $sql = "
                INSERT INTO
                    personnes
                SET
                    ".$updateSet."
            ";
            if(_DEBUG_IMPORT){
                print_t($sql);
            }
            $select = $conn->prepare($sql);
            $select->execute();
            Debug::d_echo("insert ".$dba_row[$ID_PERSONNE_WEB_KEY], 2,"updateDba-personnes.php");

            $candidat_web_id = $dba_row[$ID_PERSONNE_WEB_KEY];
        }

    }else{
        Debug::d_echo("pas id web ", 2,"updateDba-personnes.php");
        //Control si présence ID_PERSONNE en DBA
        $sql = "
            SELECT
                ID_PERSONNE,
                ID_PERSONNE_WEB,
                EMAIL,
                EMAIL_PERSO
            FROM
                personnes
            WHERE
                ID_PERSONNE = '".$dba_row[$ID_PERSONNE_KEY]."'
        ";
        $select = $conn->prepare($sql);
        $select->execute();
        $personne = null;
        $personne = $select->fetchObject();
        if($personne){
            
            //UPDATE personne admen
            $insertSet = "";
            foreach($columns_names as $key=>$columns_name){
                if($columns_name != "ID_PERSONNE_WEB"){
                    if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                        $datatext = "";
                        $datatext = str_replace("'", "''", $dba_row[$key]);
                        $datatext = str_replace("§§", "\r\n", $datatext);
                        if($insertSet == ""){
                            $insertSet .= $columns_name."='".$datatext."'";
                        }else{
                            $insertSet .= ",\n".$columns_name."='".$datatext."'";
                        }
                    }
                }
            }
            $sql = "
                UPDATE
                    personnes
                SET
                    ".$insertSet."
                WHERE
                    ID_PERSONNE_WEB = '".$personne->ID_PERSONNE_WEB."'
            ";
            if(_DEBUG_IMPORT){
                print_t($sql);
            }
            $select = $conn->prepare($sql);
            $select->execute();
            Debug::d_echo("update id personne ".$dba_row[$ID_PERSONNE_KEY]. " id web ".$personne->ID_PERSONNE_WEB, 2,"updateDba-personnes.php");

            $candidat_web_id = $personne->ID_PERSONNE_WEB;


        }else{
            //INSERT personne admen
            $insertSet = "";
            foreach($columns_names as $key=>$columns_name){
                if($columns_name != "ID_PERSONNE_WEB"){
                    if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                        $datatext = "";
                        $datatext = str_replace("'", "''", $dba_row[$key]);
                        $datatext = str_replace("§§", "\r\n", $datatext);
                        if($insertSet == ""){
                            $insertSet .= $columns_name."='".$datatext."'";
                        }else{
                            $insertSet .= ",\n".$columns_name."='".$datatext."'";
                        }
                    }
                }
            }
            $sql = "
                INSERT INTO
                    personnes
                SET
                    ".$insertSet."
            ";
            if(_DEBUG_IMPORT){
                print_t($sql);
            }
            $select = $conn->prepare($sql);
            $select->execute();
              
            Debug::d_echo("update id personne ".$dba_row[$ID_PERSONNE_KEY], 2,"updateDba-personnes.php");

            $new_ID_PERSONNE_WEB = 0;
            $sql = "SELECT ID_PERSONNE_WEB FROM personnes WHERE ID_PERSONNE_WEB=LAST_INSERT_ID()";
            $select = $conn->prepare($sql);
            $select->execute();
            $lastInsert = $select->fetchObject();
            $new_ID_PERSONNE_WEB = $lastInsert->ID_PERSONNE_WEB;

            $candidat_web_id = $new_ID_PERSONNE_WEB;
        }
   }

    /*
     * INSERT UPDATE table awa_candidats
     */
     if($ID_EMAIL_PERSO_KEY != null && $dba_row[$ID_EMAIL_PERSO_KEY]){
         $login = $dba_row[$ID_EMAIL_PERSO_KEY];
     }elseif($ID_MAIL_KEY != null && $dba_row[$ID_MAIL_KEY]){
         $login = $dba_row[$ID_EMAIL_PERSO_KEY];
     }else{
         $login = null;
     }

    //verification présence grace id web
    $sqlcandi = "
                SELECT
                    ID_PERSONNE_WEB,
                    LOGIN
                FROM
                    awa_candidats
                WHERE
                    ID_PERSONNE_WEB = '".$candidat_web_id."'
            ";
    if(_DEBUG_IMPORT){
        print_t($sqlcandi);
    }
    $selectcandi = $conn->prepare($sqlcandi);
    $selectcandi->execute();
    $selectResult = null;
    $selectResult = $selectcandi->fetchObject();
    if($selectResult){
         if($login){
             //si present et emails ok on update
             $sqlcandi = "
                    UPDATE
                        awa_candidats
                    SET
                        LOGIN = '".$login."'

                    WHERE
                        ID_PERSONNE_WEB = '".$candidat_web_id."'
                ";
            if(_DEBUG_IMPORT){
                print_t($sqlcandi);
            }
            $selectcandi = $conn->prepare($sqlcandi);
            $selectcandi->execute();
            Debug::d_echo("update compte candidat ".$candidat_web_id, 2,"updateDba-personnes.php");
         }else{
             //si present et mails non ok on vire
             $sqlcandi = "
                    DELETE FROM
                        awa_candidats
                    WHERE
                        ID_PERSONNE_WEB = '".$candidat_web_id."'
                ";
            if(_DEBUG_IMPORT){
                print_t($sqlcandi);
            }
            $selectcandi = $conn->prepare($sqlcandi);
            $selectcandi->execute();
            Debug::d_echo("supression compte candidat ".$candidat_web_id, 2,"updateDba-personnes.php");
        }

    }else{
        if($login){
             //si non present et emails ok on ajoute
            $sqlcandi = "
                INSERT INTO
                    awa_candidats
                SET
                    LOGIN = '".$login."',
                    ID_PERSONNE_WEB = '".$candidat_web_id."',
                    PASSWORD = AES_ENCRYPT('".randomstr(8)."','admen'),
                    ACTIVE = '1'
                ";
            if(_DEBUG_IMPORT){
                print_t($sqlcandi);
            }
            $selectcandi = $conn->prepare($sqlcandi);
            $selectcandi->execute();
            Debug::d_echo("insert compte candidat ".$candidat_web_id, 2,"updateDba-personnes.php");
        }else{
             //si non present et email non ok on fait rien.
        }
    }
    if(_DEBUG_IMPORT){
        print_t("###################################################################################################");
    }



//$time_2 = microtime(true);
//$time = round($time_2 - $time_1,3);
//Debug::d_echo("chrono ".$time, 2,"updateDba-personnes.php");


/*****************************************************************************************************************/


    
}




//function randomstr($nbelement = 8){
//    $characters = array(
//        "A","B","C","D","E","F","G","H","J","K","L","M",
//        "N","P","Q","R","S","T","U","V","W","X","Y","Z",
//        "a","b","c","d","e","f","g","h","j","k","l","m",
//        "n","p","q","r","s","t","u","v","w","x","y","z",
//        "1","2","3","4","5","6","7","8","9"," ",".",":");
//
//
//    $random_chars = "";
//    for($i=0;$i<=$nbelement;$i++){
//
//        $x = mt_rand(0, count($characters)-1);
//        $random_chars .= $characters[$x];
//
//    }
//
//    return $random_chars;
//
//}
?>
