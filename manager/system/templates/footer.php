<div id="footer_line">
    <div id="footer">
        <div class="top"><a href="#top_page"><b>remonter la page</b></a></div>
        <p class="copyright_login">
            &copy; Développé par <a href="http://www.idetop.com" target="_blank">Idetop | Marketing & Publicité</a> &middot; 6 Quai André Lassagne 69001 LYON &middot; Tél : 04 7200 3333 (coût d'un appel local) &middot; Email : <a href="mailto:info@idetop.com">info@idetop.com</a>
        </p>
    </div>
</div>

<script type="text/javascript">
//<![CDATA[
var scrolling = function(){
    var speed = 800;
    jQuery('a[href^="#"]').bind('click',function(){
        var id = jQuery(this).attr('href');
        if(id == '#') goTo('body');
        else goTo(id);
        return(false);
        void(0);
    });
    function goTo(ancre){
        jQuery('html,body').animate({
            scrollTop:jQuery(ancre).offset().top
        },
        speed,'swing',function(){
            if(ancre != 'body') window.location.hash = ancre;
            else window.location.hash = '#';
             jQuery(ancre).attr('tabindex','-1');
            jQuery(ancre).focus();
            jQuery(ancre).removeAttr('tabindex');
        });
    }
};
jQuery(function(){scrolling();});
//]]>
</script>

</center>
</body>
</html>
