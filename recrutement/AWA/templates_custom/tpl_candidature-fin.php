<div id="container980">
    <div id="header"></div>
    <div id="menu">
        <?php echo $templateDatas['menu']; ?>
        <div class="spacer"></div>
    </div>
    <div id="subheader">
        <div id="pathway">
            <?php  echo $templateDatas['pathway']; ?>
        </div>
        <div id="languesdisplay">
           <?php echo $templateDatas['flags']; ?>
        </div>
        <div class="spacer"></div>
    </div>
    <?php echo $templateDatas['candidatArea']; ?>
    <div id="middle">
        <div id="content">
            <?php include("langues/candidature-fin_".$_SESSION['awa_language'].".php"); ?>
            <div style="height: 30px;"></div>
        </div>
        <div class="spacer"></div>
    </div>
    <div id="footer">
         <?php echo $templateDatas['footerText']; ?>
    </div>
</div>