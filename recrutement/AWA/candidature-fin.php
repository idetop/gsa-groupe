<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');

Debug::d_echo("acces ", 2,"candidature-fin.php");
Debug::d_print_r($_GET, 1,"GET","candidature-fin.php");
Debug::d_print_r($_POST, 1,"POST","candidature-fin.php");
Debug::d_print_r($_SESSION, 1,"SESSION","candidature-fin.php");



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title><?php echo _CANDIDATURE_FIN_TITLE; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content=""/>
        <meta name="keywords" content=""/>
        <?php  if(!_USE_CUSTOM_CSS){ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?>getcss_custom.php" rel="stylesheet" type="text/css" />
        <?php }else{ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?><?php echo _TEMPLATE_FOLDER_NAME; ?>/custom.css" rel="stylesheet" type="text/css" />
        <?php } ?>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/framework.js"></script>
        <script type="text/javascript" >
            $.ajaxSettings.cache = false;
            $(document).ready(function(){
                <?php if(_IS_IFRAME_RESIZE){ ?>
                resizeiframe();
                <?php } ?>
            });

           function changeLanguage(language){
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-change_language.php", { 'language': language});
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-get_url.php",
                    { 'pageref':'candidaturefin', 'langue':language },
                    function(data){
                        window.location=data;
                    }
                );
            }

        </script>
    </head>
    <body>
        <?php
        $templateDatas = array();

        $templateDatas['pathway'] = "";
        $pathway = array();
        $pathway[0]['text'] = _CANDIDATURE_FIN_TITLE;
        $pathway[0]['url'] = null;
        $templateDatas['pathway'] =  displayPathway($pathway);

        $templateDatas['header'] = "";
        $templateDatas['header'] = displayHeader();

        $templateDatas['candidatArea'] = "";
        $templateDatas['candidatArea'] = displayCandidatArea($conn);

        $templateDatas['menu'] = "";
        $templateDatas['menu'] = displayMenu();

        //url search engine
        $templateDatas['urlToSearchEngine'] = "";
        if(_SEPARATE_SEARCH){
            $templateDatas['urlToSearchEngine'] = _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_SEARCH_".$_SESSION['awa_language'])  ;
        }else{
            $templateDatas['urlToSearchEngine'] = _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language']);
        }



        //Footer
        $templateDatas['footerText'] = "";
        $templateDatas['footerText'] = displayFooter();
        ?>
        <?php include(_TEMPLATE_FOLDER_NAME."/tpl_candidature-fin.php"); ?>
    </body>
</html>
<?php

//print_t($_SESSION);
//echo "<pre>";
//print_r($fonctions);
//echo "</pre>";
//echo "<pre>";
//print_r($secteurs);
//echo "</pre>";
?>