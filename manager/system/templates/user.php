
    <!-- Bouton nouveau -->
    <div id="body_btn_nouveau">
        <!--<a href="javascript:history.go(-1);" class="retour_on" title="Page précédente"><img src="src/icons/arrow_left.png"></a>-->
        <a href="<?= $site; ?>" class="link_nouveau" target="_blank">
            <div class="btn_nouveau"><span class="span_nouveau">Accéder au site <font color="#b9001f">&raquo;</font></span></div>
        </a>
        <?php if($statut == 'gsa'){ ?>
        <!--
        <a href="index.php?page=user&action=add" class="link_nouveau">
            <div class="btn_nouveau"><span class="span_nouveau">Ajouter un utilisateur <font color="#b9001f">+</font></span></div>
        </a>
        -->
        <?php } ?>
    </div>
    <div class="clear"></div>
</div>

<div id="content">
    <!-- Colonne gauche -->
    <?php include($b_templates.'side_left.php'); ?>

    <!-- Colonne droite -->
    <div class="colonne_right">

        <div id="title_line"><span class="span_title_line">Utilisateurs (<?php return_count('gsa_admin','',''); ?>)</span></div>

        <?php if(empty($_REQUEST['action'])) {  ?>
        <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;">
            <tbody>
                <?php $i=0; while($row_users = mysqli_fetch_array($sql_users)) { $color = ++$i % 2 ? '#ffffff':'#EAEAEA'; ?>
                <tr style="background:<?= $color; ?>;">
                    <td style="padding:15px;">
                        <form name="del_fiche" method="post" action="">
                            <input type="hidden" name="id" value="<?= $row_users['id']; ?>">
                            <div style="float:left;width:380px;margin-left:10px;position:relative;top:2px;">
                                <strong>
                                    <a href="index.php?page=user&action=edit&id=<?= $row_users['id']; ?>" style="font-size:12px;color:#5c3d3d">
                                        <?= stripslashes($row_users['nom']); ?>
                                    </a>
                                </strong><br />
                                <div style="font-size:11px;margin-top:7px;">
                                    <span style="float:left;"><b>Identifiant : </b><?= $row_users['login']; ?></span>
                                    <a href="mailto:<?= $row_users['email']; ?>" style="float:right;position:relative;left:336px;"><?= $row_users['email']; ?></a>
                                </div>
                            </div>
                            <!--<input id="submit_del" name="del" type="submit" value="" style="float:right;" onClick="return confirm('\nEtes-vous sur de vouloir supprimer ce compte ?\n\n');">-->
                            <a href="index.php?page=user&action=edit&id=<?= $row_users['id']; ?>">
                                <input id="submit_accept" type="button" value="" style="float:right;background:url('src/icons/page_white_edit.png') no-repeat;">
                            </a>
                        </form>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table><br /><br />
        <?php } ?>

        <?php if($_REQUEST['action'] == "edit") { // Détails fiche utilisateur ?>
        <form name="modif_fiche" method="post" action="">
        <input type="hidden" name="id" value="<?= $data_user['id']; ?>">
        <input type="hidden" name="pole" value="<?= $data_user['pole']; ?>">
        <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
            <tbody>
                <tr style="background:#1b1b1b;">
                    <td colspan="2" style="padding:6px;color:#fff;">
                        <?php if($_SESSION['login'] != $data_user['login']) { ?>
                        &nbsp;&nbsp;<strong style="font-size:11px;">Compte de <?= stripslashes($data_user['nom']); ?></strong>
                        <?php } else { ?>
                        &nbsp;&nbsp;<strong style="font-size:11px;">Mes informations</strong>
                        <?php } ?>
                    </td>
                </tr>
                <tr style="background:#ffffff;">
                    <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                        <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Informations générales</strong>
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Nom</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="nom" value="<?= stripslashes($data_user['nom']); ?>" style="width:600px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Poste</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="poste" value="<?= stripslashes($data_user['poste']); ?>" style="width:600px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Adresse</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="adresse" value="<?= stripslashes($data_user['adresse']); ?>" style="width:600px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Code postal</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="cp" value="<?= $data_user['cp']; ?>" style="width:100px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Ville</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="ville" value="<?= stripslashes($data_user['ville']); ?>" style="width:600px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Tél</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="tel" value="<?= $data_user['tel']; ?>" style="width:150px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Mobile</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="mobile" value="<?= $data_user['mobile']; ?>" style="width:150px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Email</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="email" value="<?= $data_user['email']; ?>" style="width:600px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                        <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/lock.png"> Configuration du compte</strong>
                    </td>
                </tr>
                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Identifiant</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="login_update" value="<?= $data_user['login']; ?>" style="width:100px;">
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Mot de passe</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="password" name="pwd_update" value="<?= $data_user['pwd']; ?>" style="width:100px;">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" height="10"></td>
                </tr>
            </tbody>
        </table>
        <table class="table_right" width="760" cellpadding="0" cellspacing="0">
            <tbody>
                <tr style="background:#e9e7e7;">
                    <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                        <input type="submit" name="update" class="table_submit" value="enregistrer mes modifications">
                    </td>
                </tr>
            </tbody>
        </table><br /><br />
        </form>
        <?php } ?>

        <?php if($_REQUEST['action'] == "add") { // Création nouveau compte utilisateur ?>
        <form name="add_fiche" method="post" action="index.php?page=user">
        <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
            <tbody>
                <tr style="background:#159904;">
                    <td colspan="2" style="padding:6px;color:#fff;">
                        &nbsp;&nbsp;<strong id="title_nouveau" style="font-size:11px;">Nouveau compte utilisateur</strong>
                    </td>
                </tr>
                <tr style="background:#ffffff;">
                    <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                        <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Informations générales</strong>
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Nom</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="nom" value="" style="width:600px;" onFocus="this.style.border='1px solid #efd006';this.style.background='#fdfae5';" onBlur="this.style.border='1px solid #ccc';this.style.background='#fff';if(this.value==''){document.getElementById('title_nouveau').innerHTML='Nouveau compte utilisateur';};" onKeyUp="document.getElementById('title_nouveau').innerHTML='Création du compte de '+this.value;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Adresse</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="adresse" value="" style="width:600px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Code postal</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="cp" value="" style="width:100px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Ville</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="ville" value="" style="width:600px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Tél</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="tel" value="" style="width:150px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Fax</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="fax" value="" style="width:150px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Email</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="email" value="" style="width:600px;">
                    </td>
                </tr>

                <tr style="background:#ffffff;">
                    <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                        <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/lock.png"> Configuration du compte</strong>
                    </td>
                </tr>
                <tr style="background:#ffffff;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Identifiant</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="login_nv" value="" style="width:150px;" onFocus=";document.getElementById('tooltip1').style.display='block'" onBlur="document.getElementById('tooltip1').style.display='none'">
                        <div id="tooltip1" class="tooltip" style="margin-top:-25px;margin-left:170px;">Evitez les espaces et les caractères spéciaux</div>
                    </td>
                </tr>
                <tr style="background:#fbf9f9;">
                    <td style="padding:10px;margin-right:20px;">
                        &nbsp;&nbsp;<strong style="font-size:12px;">Mot de passe</strong>
                    </td>
                    <td style="padding:5px;">
                        <input class="text" type="text" name="pwd_nv" value="" style="width:150px;" onFocus="document.getElementById('tooltip2').style.display='block'" onBlur="document.getElementById('tooltip2').style.display='none'">
                        <div id="tooltip2" class="tooltip" style="margin-top:-25px;margin-left:170px;">Evitez les espaces et les caractères spéciaux</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" height="10"></td>
                </tr>
            </tbody>
        </table>
        <table class="table_right" width="760" cellpadding="0" cellspacing="0">
            <tbody>
                <tr style="background:#e9e7e7;">
                    <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                        <input type="submit" name="submit_add" class="table_submit" value="enregistrer ce nouveau compte">
                    </td>
                </tr>
            </tbody>
        </table><br /><br />
        </form>
        <?php } ?>

    </div>
    <div class="clear"></div>
</div>

