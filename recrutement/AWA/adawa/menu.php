<?php
/* é
 */
include_once "include/display_errors.php";
@session_start();

include_once('../include/config.php');
include_once "include/framework.php";


?>
var MENU_ITEMS = [
        ['<?php echo _MENU_ADMINISTRATION; ?>', '', null,
            ['<?php echo _MENU_ADMINISTRATION_CONF_SYS; ?>', '../configuration/v-config.php', null,],
            ['<?php echo _MENU_ADMINISTRATION_DESIGN_FO; ?>', '../configuration/v-design.php', null,],
            ['<?php echo _MENU_ADMINISTRATION_CANDIDATURE_OFFRE; ?>', '../formulaires/v-formulaireco.php', null,],
            ['<?php echo _MENU_ADMINISTRATION_CANDIDATURE_SPN; ?>', '../formulaires/v-formulairecs.php', null,],
            ['<?php echo _MENU_ADMINISTRATION_PROFIL; ?>', '../formulaires/v-formulairepo.php', null,]
        ],
        ['<?php echo _MENU_MAILING; ?>', '', null,
            ['<?php echo _MENU_MAILING_RELANCE; ?>', '../mailing/v-mailingcandidats.php', null,],
            ['<?php echo _MENU_MAILING_CANDIDATURE; ?>', '../mailing/v-configmailcandidature.php', null,],
            ['<?php echo _MENU_MAILING_CANDIDATURE_SPN; ?>', '../mailing/v-configmailcandidaturesp.php', null,],
            ['<?php echo _MENU_MAILING_PROFIL; ?>', '../mailing/v-configmailmajpro.php', null,]
        ],
        ['<?php echo _MENU_LOGOFF; ?>', '../deconnexion.php', null, null]
	
];
