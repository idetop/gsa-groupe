<?php
//langue d'affichage par défaut du front
if(!empty($_GET['awaloadlanguage'])){
    if($_GET['awaloadlanguage'] == "fr"){
        $_SESSION['awa_language'] = "fr";
    }elseif($_GET['awaloadlanguage'] == "en"){
        $_SESSION['awa_language'] = "en";
    }
}



/*
 * Controle la langue de l'url d'entré sur le site.
 * Si une personne arrive sur le site via une url anglaise,
 * la langue du site basculera sur la langue liée à cette url et non pas
 * sur la langue par défaut
 */
function checkEntryUrl(){
    $customUrls = array();
    //liste des urls possibles, à éditer lors du rajout de langue
    //espace candidat
    $customUrls[] = array("url"=>_URL_CONFIG_ESPACE_CANDIDAT_fr,"langue"=>"fr");
    $customUrls[] = array("url"=>_URL_CONFIG_ESPACE_CANDIDAT_en,"langue"=>"en");
    //Candidature spontanée
    $customUrls[] = array("url"=>_URL_CONFIG_CANDIDATURE_SPONTANEE_fr,"langue"=>"fr");
    $customUrls[] = array("url"=>_URL_CONFIG_CANDIDATURE_SPONTANEE_en,"langue"=>"en");
    //candidature sur une offre
    $customUrls[] = array("url"=>_URL_CONFIG_CANDIDATURE_OFFRE_fr,"langue"=>"fr");
    $customUrls[] = array("url"=>_URL_CONFIG_CANDIDATURE_OFFRE_en,"langue"=>"en");
    //fin de la candidature
    $customUrls[] = array("url"=>_URL_CONFIG_CANDIDATURE_END_fr,"langue"=>"fr");
    $customUrls[] = array("url"=>_URL_CONFIG_CANDIDATURE_END_en,"langue"=>"en");
    //mention legales
    $customUrls[] = array("url"=>_URL_CONFIG_MENTIONS_LEGALES_fr,"langue"=>"fr");
    $customUrls[] = array("url"=>_URL_CONFIG_MENTIONS_LEGALES_en,"langue"=>"en");
    //mot de passe oublié
    $customUrls[] = array("url"=>_URL_CONFIG_MDP_OUBLIE_fr,"langue"=>"fr");
    $customUrls[] = array("url"=>_URL_CONFIG_MDP_OUBLIE_en,"langue"=>"en");
    //page de recherche d'offres
    $customUrls[] = array("url"=>_URL_CONFIG_OFFRE_SEARCH_fr,"langue"=>"fr");
    $customUrls[] = array("url"=>_URL_CONFIG_OFFRE_SEARCH_en,"langue"=>"en");
    //page de liste d'offres
    $customUrls[] = array("url"=>_URL_CONFIG_OFFRE_LISTE_fr,"langue"=>"fr");
    $customUrls[] = array("url"=>_URL_CONFIG_OFFRE_LISTE_en,"langue"=>"en");
    //page de fiche d'offre
    $customUrls[] = array("url"=>_URL_CONFIG_OFFRE_FICHE_fr."/","langue"=>"fr");
    $customUrls[] = array("url"=>_URL_CONFIG_OFFRE_FICHE_en."/","langue"=>"en");


    foreach($customUrls as $url){
        if(preg_match("#".$url['url']."#", $_SERVER['REQUEST_URI'])){
            return $url['langue'];
        }
    }
    return null;
}



/*
 * Gestion du contenu de la zone des drapeaux pour le changement de langue
 */
function displayFlags(){
    $returnhtml = "";
    $returnhtml .= "<img onclick=\"changeLanguage('fr');\" src=\""._CONFIG_ROOTFOLDER."images/fr.png\" alt=\"\"/>";
    $returnhtml .= "<img onclick=\"changeLanguage('en');\" src=\""._CONFIG_ROOTFOLDER."images/uk.png\" alt=\"\"/>";
//    $returnhtml .= "<img onclick=\"changeLanguage('de');\" src=\""._CONFIG_ROOTFOLDER."images/de.png\" alt=\"\"/>";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";

    return $returnhtml;
}

/*
 * Gestion de l'affichage du header
 */
function displayHeader(){
    $returnhtml = "";
    $returnhtml .= "<div id=\"conteneur_head\"><div id=\"logo_head\"><a href=\"http://gsa-recrutement.fr/AWA/\" target=\"_self\"><img border=\"0\" src=\"images/logo_front.png\"></a></div></div>";
    $returnhtml .= "";
    return $returnhtml;
}
function displayHeader2(){
    $returnhtml = "";
    $returnhtml .= "<div id=\"conteneur_head\"><div id=\"logo_head\"><a href=\"http://gsa-recrutement.fr/AWA/\" target=\"_self\"><img border=\"0\" src=\"../../images/logo_front.png\"></a></div></div>";
    $returnhtml .= "";
    return $returnhtml;
}


/*
 * Gestion de l'affichage du menu
 */
function displayMenu(){
    $returnhtml = "";
    //$returnhtml .= "<div class=\"menu_element\"><a href=\""._CONFIG_ROOTFOLDER."\" title=\"\">"._MENU_HOME."</a></div>";
    //$returnhtml .= "<div class=\"retour_element\"><a href=\"http://gsa-recrutement.fr/\"><img border=\"0\" src=\"images/retour.jpg\"></a></div>";
    if(_SEPARATE_SEARCH){
        $returnhtml .= "<div class=\"menu_element\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_SEARCH_".$_SESSION['awa_language'])."\" title=\"\">"._MENU_OFFRE."</a></div>";
    }else{
        $returnhtml .= "<div class=\"menu_element\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language'])."\" title=\"\">"._MENU_OFFRE."</a></div>";
    }
    $returnhtml .= "<div class=\"menu_element\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language'])."\" title=\"\">"._MENU_ESPACECANDIDAT."</a></div>";
    $returnhtml .= "<div class=\"menu_element\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_CANDIDATURE_SPONTANEE_".$_SESSION['awa_language'])."\" title=\"\">"._MENU_CANDIDATURESPONTANEE."</a></div>";
    $returnhtml .= "";
    return $returnhtml;
}

function displayMenu2(){
    $returnhtml = "";
    //$returnhtml .= "<div class=\"menu_element\"><a href=\""._CONFIG_ROOTFOLDER."\" title=\"\">"._MENU_HOME."</a></div>";
    $returnhtml .= "<div class=\"retour_element\"><a href=\"http://gsa-recrutement.fr/\"><img border=\"0\" src=\"../../images/retour.jpg\"></a></div>";
    if(_SEPARATE_SEARCH){
        $returnhtml .= "<div class=\"menu_element\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_SEARCH_".$_SESSION['awa_language'])."\" title=\"\">"._MENU_OFFRE."</a></div>";
    }else{
        $returnhtml .= "<div class=\"menu_element\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_LISTE_".$_SESSION['awa_language'])."\" title=\"\">"._MENU_OFFRE."</a></div>";
    }
    $returnhtml .= "<div class=\"menu_element\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language'])."\" title=\"\">"._MENU_ESPACECANDIDAT."</a></div>";
    $returnhtml .= "<div class=\"menu_element\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_CANDIDATURE_SPONTANEE_".$_SESSION['awa_language'])."\" title=\"\">"._MENU_CANDIDATURESPONTANEE."</a></div>";
    $returnhtml .= "";
    return $returnhtml;
}

/*
 * Gestion de l'affichage du footer
 */
function displayFooter(){
    $returnhtml = "";
    $returnhtml .= "<div style=\"text-align: center;\"><a href=\""._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_MENTIONS_LEGALES_".$_SESSION['awa_language'])."\" title=\""._FOOTER_MENTIONLEGAL_LINK_ALT."\">"._FOOTER_MENTIONLEGAL_LINK_TEXT."</a> - "._FOOTER_COPYRIGHT."</div>";
    return $returnhtml;
}

?>
