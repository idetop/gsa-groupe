<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= $lang; ?>" lang="<?= $lang; ?>">

<head>
    <?php include($meta_file); ?>
    <link rel="stylesheet" type="text/css" href="<?= $b_css; ?>idemanager.css" />
    <!--[if lt IE 7.]><script defer type="text/javascript" src="<?= $b_js; ?>pngfix.js"></script><![endif]-->
    <!-- Librairie JQuery -->
    <script type="text/javascript" src="<?= $b_js; ?>jquery-1.4.4.min.js"></script>
	<!-- librairie jquery ui -->
	<?php if($page == "calendrier") { ?>
	<link rel="stylesheet" href="<?= $b_js; ?>jquery-ui-1.8.8.custom/development-bundle/themes/base/jquery.ui.all.css">
	<script type="text/javascript" src="<?= $b_js; ?>jquery-ui-1.8.8.custom/js/jquery-ui-1.8.8.custom.min.js"></script>
	<script type="text/javascript" src="<?= $b_js; ?>jquery-ui-1.8.8.custom/development-bundle/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="<?= $b_js; ?>jquery-ui-1.8.8.custom/development-bundle/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="<?= $b_js; ?>jquery-ui-1.8.8.custom/development-bundle/ui/ui/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="<?= $b_js; ?>jquery-ui-1.8.8.custom/development-bundle/ui/jquery.ui.slider.js"></script>
	<script type="text/javascript" src="<?= $b_js; ?>jquery-ui-1.8.8.custom/development-bundle/ui/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="<?= $b_js; ?>jquery-ui-1.8.8.custom/development-bundle/ui/jquery.ui.autocomplete.js"></script>
	<?php } ?>
	<?php if($page == "contacts") { ?>
	<link rel="stylesheet" type="text/css" href="<?= $b_js; ?>slidernav/slidernav.css" media="screen, projection" />
	<script type="text/javascript" src="<?= $b_js; ?>slidernav/slidernav.js"></script>
	<?php } ?>
    <!-- Preload images -->
    <script type="text/javascript" src="<?= $b_js; ?>jpreload.js"></script>
    <!-- Tooltip -->
    <script type="text/javascript" src="<?= $b_js; ?>jqFancyTransitions.1.8.js"></script>
    <script type="text/javascript" src="<?= $b_js; ?>vtip.js"></script>
    <!-- Modal - ColorBox -->
    <link rel="stylesheet" type="text/css" href="<?= $b_js; ?>colorbox/theme_light/colorbox.css" />
    <script type="text/javascript" src="<?= $b_js; ?>colorbox/jquery.colorbox.js"></script>
    <script type="text/javascript">
    //<![CDATA[
    function confirm_del() { return confirm('\nVous êtes sur le point de supprimer cette image. Continuer ?\n\n'); }
    function confirm_logoff() { return confirm('\nVous êtes sur le point de vous déconnecter. Continuer ?\n\n'); }
    // Menu,Sub-Menu functions
    var timeout = 0; var closetimer = 0; var ddmenuitem = 0;
    function menu_open(){menu_canceltimer();menu_close();ddmenuitem = $(this).find('ul').eq(0).css('visibility','visible');}
    function menu_close(){if(ddmenuitem) ddmenuitem.css('visibility','hidden');}
    function menu_timer(){closetimer = window.setTimeout(menu_close, timeout);}
    function menu_canceltimer(){if(closetimer){window.clearTimeout(closetimer);closetimer = null;}}
    document.onclick = menu_close;
    // Init Jquery plugins
    $(document).ready(function(){
		<?php if($page == "contacts") { ?>
		$('#slider').sliderNav();
		$('#transformers').sliderNav({items:['autobots','decepticons'], debug: true, height: '300', arrows: false});
		<?php } ?>
        // Menu,Sub-Menu Jquery init
        $('#body_menu > li').bind('mouseover', menu_open)
        $('#body_menu > li').bind('mouseout',  menu_timer)
        // colorbox
        $("a[rel='images']").colorbox();
		$(".video").colorbox({iframe:true, innerWidth:600, innerHeight:400});
		$(".frame").colorbox({width:"90%", height:"75%", iframe:true});
		<?php if($page == "calendrier") { ?>
		// datepicker
		$( ".datepicker" ).datepicker();
		<?php } ?>
    });
    //]]>
    </script>
</head>

<body onload="init();">
<center>

<div id="alertbox"><?= $alerte; ?></div>

<a id="top_page"></a>

<?php include($b_templates.'loading.php'); ?>

<div id="header_line"></div>

<div id="header">
    <a href="?page=accueil" class="logo_header"><img src="<?= $b_img; ?>header_logo.gif" /></a>
    <a id="refresh" class="vtip" href="javascript:;" onClick="document.location.reload();return(false);" title="Actualiser la page"></a>
    <!-- Menu -->
    <ul id="body_menu">
		<?php
        if(
            $page == "accueil" ||
            $page == "user"
        ) {
        ?>
        <li class="active"><?php } else { ?><li><?php } ?>
            <a href="index.php?page=accueil"><span class="menu_span"><img class="icon_menu" src="<?= $b_img; ?>accueil.png"> Accueil</span></a>
            <?php if($statut == 'gsa'){ ?>
            <ul>
                <li><a href="index.php?page=user">Administrateurs (<?php return_count('gsa_admin','',''); ?>)</a></li>
            </ul>
            <?php } ?>
        </li>
		<?php
        if(
            $page == "qui-sommes-nous" ||
            $page == "galerie-photos" ||
            $page == "gsa-formation" ||
            $page == "gsa-pharma" ||
            $page == "gsa-recrutement" ||
            $page == "gsa-medical"
        ) {
        ?>
        <li class="active"><?php } else { ?><li><?php } ?>
            <a href="javascript:;" style="cursor:default;"><span class="menu_span"><img class="icon_menu" src="<?= $b_img; ?>Desktop.png"> Le site</span></a>
            <ul>
                <?php if($statut == 'gsa'){ ?>
                <li><a href="index.php?page=qui-sommes-nous&part=qui">Qui sommes-nous ?</a></li>
                <?php } ?>
                <?php if($statut == 'formation' || $statut == 'gsa'){ ?>
                <li><a href="index.php?page=gsa-formation&part=accueil">GSA Formation</a></li>
                <?php } ?>
                <?php if($statut == 'pharma' || $statut == 'gsa'){ ?>
                <li><a href="index.php?page=gsa-pharma&part=accueil">GSA Pharma</a></li>
                <?php } ?>
                <?php if($statut == 'recrutement' || $statut == 'gsa'){ ?>
                <li><a href="index.php?page=gsa-recrutement&part=accueil">GSA Recrutement</a></li>
                <?php } ?>
                <?php if($statut == 'medical' || $statut == 'gsa'){ ?>
                <li><a href="index.php?page=gsa-medical&part=accueil">GSA Médical</a></li>
                <?php } ?>
            </ul>
        </li>
    </ul>
    <div class="clear"></div>
    <!-- Bloc user -->
    <div id="user_log">
        <p class="user_log_content">
            <strong><?= $name_user; ?></strong><br />
            <a href="index.php?page=user&action=edit&id=<?= $_SESSION['id']; ?>" class="link_user">Mes informations</a><br />
            <a href="index.php?action=logoff" class="link_user" onClick="return confirm_logoff();">Déconnexion</a>
        </p>
    </div>


