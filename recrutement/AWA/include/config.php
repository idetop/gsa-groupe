<?php
/*
 * Configuration des paramettres de la base de données
 */
define("_CONFIG_DBA_HOST","mysql51-55.perso");
define("_CONFIG_DBA_NAME","gsagroup001");
define("_CONFIG_DBA_USER","gsagroup001");
define("_CONFIG_DBA_PASS","sctRqqog");

define("_ADMEN_USE_ADVERT_LINES",true); //false => ADMEN <4.5 ; true => ADMEN = 4.5



/*
 * Configuration de base de l'url du site
 * Nom domaine + eventuelement un ensemble de sous dossier permetant d'arriver au site.
 */
$httphost = "http://gsa-recrutement.fr/";
if(!empty($_SERVER['HTTP_HOST'])){
    $httphost = $_SERVER['HTTP_HOST'];
}

define("_CONFIG_DOMAIN_NAME",$httphost);  //html domaine name
define("_CONFIG_ROOTFOLDER","/AWA/");  //html root folder



/*
 * Configuration de l'id du type de support des annonces sur AWA
 * DOIT ETRE EN ACCORD AVEC LA BASE DE DONNEES D'ADMEN
 */
define("_CONFIG_SUPPORT_ID","4");  //Id support front office


/*
 * ACTIVATION DE CVREADER
 */
define("_USE_CVREADER",false);
define("_CVREADER_LOGIN","");
define("_CVREADER_PASS","");




/*
 * GESTION EMAIL CANDIDAT
 * Si false, on envoi pas les mails de candidature aux candidats
 */
define("_SEND_CANDIDATURE_EMAIL_TO_CANDIDAT",true);


/*
 * GESTION SAUVEGARDE CANDIDAT
 * Si false, on ne conserve pas les données des candidats dans la base AWA
 */
define("_SAVE_CANDIDAT",true);

/*
 * Activation des envois d'emails
 * Doit être à true pour la prod
 */
define("_SEND_EMAIL",true); //Permet de desactiver l'envoi d'email



/*
 * SEPARATION RECHERCHE - LISTE
 * Mettre à true si recherche en étape a partir du menu
 * Attention, il faut aussi modifier les templates correspondants
 */
define("_SEPARATE_SEARCH",false);

/*
 * CSS CUSTOM
 * Mettre à true pour pouvoir utiliser votre propre CSS
 * False, utilisera le CSS par defaut lier à la configuration du CSS via ADAWA
 */
define("_USE_CUSTOM_CSS",false);

/*
 * NOM DOSSIER TEMPLATE
 */
define("_TEMPLATE_FOLDER_NAME","templates_iframe");


/*
 * AUTO RESIZING IFRAME
 * Si TRUE, redimensionnement automatique de l'iframe qui contient la liste (+moteur de recherche si definit dans le template),
 * fiche des offres, et pages de candidature
 * Fonctionne uniquement si même domaine
 * L'iframe doit avoir comme id:iframeawaliste
 */
define("_IS_IFRAME_RESIZE",true);






/*
 * Choix du nombre d'annonces à afficher par page dans la page de liste des annonces.
 */
define("_NOMBRE_ANNONCE_PAR_PAGE",20);


/* afficher le salaire dans la liste des annonces et leur contenu*/
define("_SHOW_SALARY",false);

/*
 * Configuration des langues par défaut pour le fo et bo
 */
define("_CONFIG_DEFAULT_LANGUE_FO","fr");  //Langue par defaut du front office
define("_CONFIG_DEFAULT_LANGUE_BO","fr");  //Langue par defaut du back office



/*
 * Configuration des urls
 * ATTENTION DOIT ETRE EN ACCORD AVEC LE FICHIER .htaccess QUI SE TROUVE A LA RACINE DU DOSSIER DU SITE
 * ATTENTION A NE PAS METTRE D'URL IDENTIQUE
 * Pour le cas des fiches d'offres, on prend le texte défini ci-dessous + le titre de l'url
 * Don il ne faut pas rajouter .html pour la définition de l'url des fiches.
 */
define("_URL_CONFIG_OFFRE_LISTE_fr","offres.htm");  //url pour la liste des offres en francais
define("_URL_CONFIG_OFFRE_LISTE_en","offers.htm");  //url pour la liste des offres en anglais

define("_URL_CONFIG_OFFRE_SEARCH_fr","recherche-offres.htm");  //url pour la recherche des offres en francais
define("_URL_CONFIG_OFFRE_SEARCH_en","search-offers.htm");  //url pour la recherche des offres en anglais

define("_URL_CONFIG_OFFRE_FICHE_fr","offres");  //prefix url pour la fiche des offres en francais
define("_URL_CONFIG_OFFRE_FICHE_en","offers");  //prefix url pour la fiche des offres en anglais

define("_URL_CONFIG_ESPACE_CANDIDAT_fr","espace-candidat.htm");  //url pour l'espace candidat en français
define("_URL_CONFIG_ESPACE_CANDIDAT_en","candidat-space.htm");   //url pour l'espace candidat en anglais

define("_URL_CONFIG_CANDIDATURE_SPONTANEE_fr","candidature-spontanee.htm");     //url pour la candidature spontanee en francais
define("_URL_CONFIG_CANDIDATURE_SPONTANEE_en","unsolicited-application.htm");   //url pour la candidature spontanee en anglais

define("_URL_CONFIG_CANDIDATURE_OFFRE_fr","candidature.htm");       //url pour la candidature  en francais
define("_URL_CONFIG_CANDIDATURE_OFFRE_en","application.htm");       //url pour la candidature  en anglais

define("_URL_CONFIG_CANDIDATURE_END_fr","candidature-fin.htm");       //url pour la candidature  en francais
define("_URL_CONFIG_CANDIDATURE_END_en","application-end.htm");       //url pour la candidature  en anglais

define("_URL_CONFIG_MENTIONS_LEGALES_fr","mentions-legales.htm");  //url pour les mentions legales en francaise
define("_URL_CONFIG_MENTIONS_LEGALES_en","legal-notices.htm");     //url pour les mentions legales en francaise

define("_URL_CONFIG_MDP_OUBLIE_fr","oublie-mot-de-passe.htm");      //url pour les mentions legales en francaise
define("_URL_CONFIG_MDP_OUBLIE_en","forgotten-password.htm");       //url pour les mentions legales en francaise





/*
 * Utilisation de l'encodage sur les CSV import
 * si true, les csv devront avoir l'extension .csvx et être encodé
 * si false, les csv devront avoir l'extension .csv et ne pas être encodé
 */
define("_IS_IMPORT_ENCODAGE",false);


/*
 * GOOGLE MAP popup
 * si true on affiche la google map dans un popup
 */
define("_IS_GOOGLEMAP_POPUP",false);
define("_IS_GOOGLEMAP_POPUP_BACKGROUND",false);
define("_GOOGLEMAP_POPUP_WIDTH","600");


/*
 * SEARCH ENGINE CONFIG
 * Mettre a true pour pouvoir faire des selections multiples dans les champs du moteur de recherche
 * Ne pas oublier d'ajuste le template (si multiple ajuste le tag size sur l'element select du formulaire)
 */
define("_IS_SEARCH_FONCTION_MULTIPLE",false);
define("_IS_SEARCH_SECTEUR_MULTIPLE",false);


/*
 * Si utilisation des champs missions.FONCTIONWEB et / ou missions.SECTEUR
 * Mettre les variables si dessous à true
 */
define("_IS_USE_FONCTION_WEB",false);
define("_IS_USE_SECTEUR_WEB",false);
?>
