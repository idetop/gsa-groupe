<?php
/*é
 * Fichier de langue pour le framework
 */

//Traduction pour les tableaux du framework need to set valid criteriaName

define("_FW_TABLEAU_ERROR_SELECTATLEAST1","Sélectionnez au moins un élément");
define("_FW_TABLEAU_ERROR_NEEDVALIDCRITERIANAME","Il faut un nom valide pour le critère de filtrage");
define("_FW_TABLEAU_ERROR_NEEDVALIDCRITERIATYPE","Il faut un type valide pour le critère de filtrage");
define("_FW_TABLEAU_ERROR_NEEDVALIDCRITERIALABEL","Il faut un label pour le critère de filtrage");
define("_FW_TABLEAU_FILTRAGE_SUBMITTEXT","Validez");
define("_FW_TABLEAU_TRIEUP_ALT","Trie par ordre croissant");
define("_FW_TABLEAU_TRIEDOWN_ALT","Trie par ordre décroissant");
define("_FW_TABLEAU_PAGINATION_DEBUT_TEXT","Début");
define("_FW_TABLEAU_PAGINATION_DEBUT_ALT","Retournez à la première page");
define("_FW_TABLEAU_PAGINATION_PREC_TEXT","Précédant");
define("_FW_TABLEAU_PAGINATION_PREC_ALT","Retournez à la page précédante");
define("_FW_TABLEAU_PAGINATION_SUIVANT_TEXT","Suivant");
define("_FW_TABLEAU_PAGINATION_SUIVANT_ALT","Allez à la page suivante");
define("_FW_TABLEAU_PAGINATION_FIN_TEXT","Fin");
define("_FW_TABLEAU_PAGINATION_FIN_ALT","Allez à la dernière page");
define("_FW_TABLEAU_REFRESH_ALT","Rafraichir le tableau");

//Traduction pour le calendrier JS
define("_CALENDAR_MONTHS_1","Janvier");
define("_CALENDAR_MONTHS_2","Février");
define("_CALENDAR_MONTHS_3","Mars");
define("_CALENDAR_MONTHS_4","Avril");
define("_CALENDAR_MONTHS_5","Mai");
define("_CALENDAR_MONTHS_6","Juin");
define("_CALENDAR_MONTHS_7","Juillet");
define("_CALENDAR_MONTHS_8","Août");
define("_CALENDAR_MONTHS_9","Septembre");
define("_CALENDAR_MONTHS_10","Octobre");
define("_CALENDAR_MONTHS_11","Novembre");
define("_CALENDAR_MONTHS_12","Décembre");

define("_CALENDAR_WEEKDAYSHORT_1","Lu");
define("_CALENDAR_WEEKDAYSHORT_2","Ma");
define("_CALENDAR_WEEKDAYSHORT_3","Me");
define("_CALENDAR_WEEKDAYSHORT_4","Je");
define("_CALENDAR_WEEKDAYSHORT_5","Ve");
define("_CALENDAR_WEEKDAYSHORT_6","Sa");
define("_CALENDAR_WEEKDAYSHORT_7","Di");

define("_CALENDAR_ALERT_INVALIDDATE","Invalid date:");
define("_CALENDAR_ALERT_ACCEPTEDFORMAT","Accepted format is dd-mm-yyyy.");
define("_CALENDAR_ALERT_INVALIDMONTH","Invalid month value:");
define("_CALENDAR_ALERT_MONTHRANGE","Allowed range is 01-12.");
define("_CALENDAR_ALERT_INVALIDDAY","Invalid day of month value:");
define("_CALENDAR_ALERT_DAYRANGE","Allowed range for selected month is 01 -");

define("_CALENDAR_POPUP_OPENCALENDAR","Open Calendar");
define("_CALENDAR_POPUP_PREVIOUSYEAR","Previous Year");
define("_CALENDAR_POPUP_PREVIOUSMONTH","Previous Month");
define("_CALENDAR_POPUP_NEXTMONTH","Next Month");
define("_CALENDAR_POPUP_NEXTYEAR","Next Year");

?>
