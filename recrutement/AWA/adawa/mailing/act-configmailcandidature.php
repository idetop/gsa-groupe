<?php
/* é
 */
include_once "../include/display_errors_bo.php";
@session_start();

include_once "../../include/config.php";
@include_once '../include/pdo.php';
include_once "../include/framework.php";
include_once "../include/tableau.php";

/*
 * Variables communes
 */
$action = null;

$element_id = null;
$element_mail = null;

/*
 * Control d'accès
 */
if(empty($_SESSION['boe_user_id'])){
    $_SESSION['boe_user_id'] = null;
}
if(empty ($_SESSION['boe_user_email'])){
    $_SESSION['boe_user_email'] = null;
}
test_session_boe($_SESSION['boe_user_id'], $_SESSION['boe_user_email'], "../", $conn);


/*
 * Traitement des données d'entrées ($_GET)
 */
//print_t($_GET);
if(isset($_GET['action'])){
   if($_GET['action'] == "swthactif"){
       $action = "swthactif";
   }elseif($_GET['action'] == "del"){
       $action = "del";
   }else{
       exit();
   }
}
if(isset($_GET['id']) && $_GET['id'] != ""){
    $element_id = $_GET['id'];
}


/*
 * ACTIONS
 */
switch ($action){

    

    case "del":
        delElement($element_id,$conn);
        break;


}


function delElement($element_id,$conn){
    $sql ="DELETE FROM
                awa_mailscandidatures
           WHERE
                ID =:idfo
        ";
    $select = $conn->prepare($sql);
    $select->bindParam(':idfo', $element_id, PDO::PARAM_INT);
    $select->execute(); 
    Debug::d_echo("effacement mail candidature ".$element_id, 2,"act-configmailcandidature.php");
}

?>
