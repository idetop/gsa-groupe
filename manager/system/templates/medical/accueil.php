<?php include('system/libs/tinymce/tinymce.php');  ?>


    <!-- Bouton nouveau -->
    <div id="body_btn_nouveau">
        <a href="<?= $site; ?>/<?= $pole; ?>" class="link_nouveau" target="_blank">
            <div class="btn_nouveau"><span class="span_nouveau">Accéder à la page <font color="#b9001f">&raquo;</font></span></div>
        </a>
    </div>
    <div class="clear"></div>
</div>


<div id="content">
    <!-- Colonne gauche -->
    <?php include($b_templates.'side_left.php'); ?>

    <!-- Colonne droite -->
    <div class="colonne_right">

		<div id="title_line"><span class="span_title_line"><?= $title_page; ?></span></div>

		<form name="modif_fiche" method="post" action="" enctype="multipart/form-data">
            <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
                <tbody>
                    <tr style="background:#252525;">
                        <td colspan="2" style="padding:6px;color:#fff;">
                            &nbsp;&nbsp;<strong style="font-size:11px;">Text gras : intro 1</strong>
                        </td>
                    </tr>

                    <tr style="background:#ffffff">
                        <td valign="top" colspan="2" style="padding:10px;margin-right:20px;">
                            <textarea name="intro1" style="position:relative;top:8px;width:500px;height:250px"><?= stripslashes($data_edit['intro1']); ?></textarea>
                        </td>
                    </tr>

                    <tr><td colspan="2" height="10"></td></tr>

                    <tr style="background:#252525;">
                        <td colspan="2" style="padding:6px;color:#fff;">
                            &nbsp;&nbsp;<strong style="font-size:11px;">Text normal : intro 2</strong>
                        </td>
                    </tr>

                    <tr style="background:#ffffff">
                        <td valign="top" colspan="2" style="padding:10px;margin-right:20px;">
                            <textarea name="intro2" style="position:relative;top:8px;width:500px;height:250px"><?= stripslashes($data_edit['intro2']); ?></textarea>
                        </td>
                    </tr>

                    <tr><td colspan="2" height="10"></td></tr>

					<!-- photo -->
					<tr style="background:#fff;">
						<td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
							<br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/images.png"> Photo</strong>
						</td>
					</tr>
					<tr style="background:#fff;">
						<td valign="top" style="padding:10px;margin-right:20px;">
							<p style="font-size:10px;color:#7b7c7c;margin-top:5px;margin-left:5px;">Cliquez sur la photo pour la remplacer.</p>
							<div id="bloc_photo" style="float:left;">
								<img class="vtip" title="Cliquez ici pour remplacer cette image" src="../<?= $pole; ?>/images/<?= $data_edit['img']; ?>" style="background:#fff;padding:4px;border:1px solid #fff;cursor:pointer;"
								onMouseOver="this.style.border='1px solid #efd006'" onMouseOut="this.style.border='1px solid #fff'" height="125"
								onClick="document.getElementById('edit').style.display='none';document.getElementById('replace').style.display='block';">
							</div>
						</td>
						<td valign="top" style="padding:10px;">
							<div id="edit" style="float:right;">
								<input id="submit_accept" class="vtip" title="Choisir une autre image" type="button" value="" style="float:right;background:url('src/icons/page_white_edit.png') no-repeat;"
								onClick="document.getElementById('edit').style.display='none';document.getElementById('replace').style.display='block';">
							</div>
							<div id="replace" style="float:right;display:none;">
								<input type="hidden" name="image_old" value="<?= $data_edit['img']; ?>">
								<input id="submit_del" type="button" value="" style="float:right;background:url('src/icons/cancel.png') no-repeat;" class="vtip" title="Annuler"
								onClick="document.getElementById('edit').style.display='block';document.getElementById('replace').style.display='none';document.getElementById('image').value='';">
								<input type="file" name="image" id="image">
							</div>
						</td>
					</tr>

                </tbody>
            </table>
            <table class="table_right" width="760" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr style="background:#e9e7e7;">
                        <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                            <input type="submit" name="update" class="table_submit" value="enregistrer mes modifications" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
		</form>

    </div>
    <div class="clear"></div>
</div>
