<?php
//Créé 
$connectionString  = "mysql:dbname="._CONFIG_DBA_NAME.";";
$connectionString .= "host="._CONFIG_DBA_HOST.";";


$connectionUser = _CONFIG_DBA_USER;
$connectionPassword = _CONFIG_DBA_PASS;

try {   
	$conn = new PDO($connectionString, $connectionUser, $connectionPassword);
}catch(PDOException $e){ echo $e->getMessage();}

$conn->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
$conn->exec('SET CHARACTER SET utf8');

?>
