<?php
//Créé 
/*
 * REDIRECTION SUR HTTPS
 */
 
//if(!isset($_SERVER['HTTPS'])){
//    header("location:https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
//    exit();
//}
//FIchier de configuration de l'affichage des messages d'erreurs
include_once "include/display_errors_bo.php";

//Ouverture de la session
@session_start();

Debug::d_echo("acces ", 2,"index.php");
Debug::d_print_r($_GET, 1,"GET","index.php");
Debug::d_print_r($_POST, 1,"POST","index.php");
Debug::d_print_r($_SESSION, 1,"SESSION","index.php");


//Inclusion des fichiers
include_once('../include/config.php');
include_once "include/framework.php";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title><?php echo _LOGIN_PAGE_TITLE; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/boe_style.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="include/jquery-1.4.2.js"></script>
    </head>
    <body>
	<div id="main">
	    <div id="content">
		<div id="header">
		    <?php
			include_once 'header_index.php';
		    ?>
		</div>
		<div id="corps">
		    <div id="global_login">
			<h1 class="mgB25"><?php echo _LOGIN_BLOC_TITLE; ?></h1>
			<form id="frm_login_boe" method="post" action="login.php">
			    <div id="login">
				<label id="lbl_login" for="txt_login" class="w150 fs12"><?php echo _LOGIN_FORM_LOGIN_LABEL; ?> : </label>
				<input type="text" id="txt_login" name="txt_login" class="fs12 w150"/>
				<div class="spacer mgT10"></div>
				<label id="lbl_passwd" for="txt_passwd" class="w150 fs12"><?php echo _LOGIN_FORM_MDP_LABEL; ?> : </label>
				<input type="password" id="txt_passwd" name="txt_passwd" class="fs12 w150"/>
				<div class="spacer"></div>
				<div class="right mgT10">
				    <input type="submit" id="sub_login" value="<?php echo _LOGIN_FORM_SUBMIT_BTN; ?>" />
				</div>
			    </div>
			    <div class="spacer"></div>

			    <div class="red bold center mgT10 fs12">
				<?php
                                if(!empty($_SESSION['boe_error_log'])){
                                    echo $_SESSION['boe_error_log'];
                                    $_SESSION['boe_error_log']="";
                                    unset($_SESSION['boe_error_log']);
                                }

                                if(!empty($_SESSION['wrong_session'])){
                                    echo _LOGIN_FORM_ERROR_EXPIRE;
                                    $_SESSION['wrong_session']="";
                                    unset($_SESSION['wrong_session']);
                                }
				?>
			    </div>
			</form>
		    </div>
		</div>
		<div id="footer">
		   <?php include_once 'footer.php'; ?>
		</div>
	    </div>
	</div>
    </body>
</html>




