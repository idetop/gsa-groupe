<?php


/*
 * Lecture du fichier cvsx et décodage
 */
Debug::d_echo("import missions ", 2,"updateDba-missions.php");
if(_IS_IMPORT_ENCODAGE){
    if(!is_file(_AWA_PATHWAY."/import_files/missions.csvx")){
        Debug::d_echo("fichier "._AWA_PATHWAY."/import_files/missions.csvx manquant", 2,"updateDba-missions.php");
        exit();
    }
    $datafile = file_get_contents(_AWA_PATHWAY."/import_files/missions.csvx");
    Debug::d_echo("lecture du fichier", 2,"updateDba-missions.php");

    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

    $key = "admen";
    $datafile = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $datafile, MCRYPT_MODE_ECB, $iv);
    Debug::d_echo("decodage du fichier", 2,"updateDba-missions.php");
    //$datafile = utf8_encode($datafile);
}else{
    if(!is_file(_AWA_PATHWAY."/import_files/missions.csv")){
        Debug::d_echo("fichier "._AWA_PATHWAY."/import_files/missions.csv manquant", 2,"updateDba-missions.php");
        exit();
    }
    $datafile = file_get_contents(_AWA_PATHWAY."/import_files/missions.csv");
}
if(_DEBUG_IMPORT){
    print($datafile);
}

/*
 * Récuparation de chaque ligne CSV
 */
preg_match_all('#(.*?)[@]{4}\s#', $datafile, $rows);
if(_DEBUG_IMPORT){
    echo "<pre>";
    print_r($rows[1]);
    echo "</pre>";
}


/*
 * Récuparation des noms de colonnes
 */
preg_match_all('`[#]{4}(.*?)[#]{4};?`', $rows[1][0], $matches);

$columns_names = array();
$columns_names = $matches[1];

if(_DEBUG_IMPORT){
    echo "liste des columns"."\n";
    echo "<pre>";
    print_r($columns_names);
    echo "</pre>";
}

/*
 * Récupération des données
 */
$dba_rows = array();

foreach($rows[1] as $key=>$row){
    if($key != 0){
        preg_match_all('`[#]{4}?(.*?)[#]{4}?;?`', $row, $row_matches);
        $dba_rows[] = $row_matches[1];
        
    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "liste du contenu"."\n";
    echo "<pre>";
    print_r($dba_rows);
    echo "</pre>";
}


/*
 * Suppression des annonces de la DBA qui ne se trouve pas dans le fichier csv
 */

//On recherche la clé de ID_ANNONCE
$ID_MISSION_KEY = null;
foreach($columns_names as $key=>$columns_name){
    if(_DEBUG_IMPORT){
        echo $columns_name;
    }
    if($columns_name == "ID_MISSION"){
        $ID_MISSION_KEY = $key;
        break;
    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "index mission id ".$ID_MISSION_KEY."\n";
}

//On recup les ID lieés à la clé ID_MISSION
$ID_MISSION_values = array();
foreach($dba_rows as $dba_row){
    $ID_MISSION_values[] = $dba_row[$ID_MISSION_KEY];
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "<pre>";
    print_r($ID_MISSION_values);
    echo "</pre>";
}

//On genere la requete SQL de suppression
$whereDeleteMission = "";
foreach($ID_MISSION_values as $ID_ANNONCE_value){
    if($whereDeleteMission == ""){
        $whereDeleteMission .= "WHERE !( ID_MISSION = '".$ID_ANNONCE_value."' "."\n";
    }else{
        $whereDeleteMission .= " OR ID_MISSION = '".$ID_ANNONCE_value."' "."\n";
    }
}
$whereDeleteMission .= " ) "."\n";

//on supprime les annonces en trop
$sql = "
        DELETE FROM
            missions
        ".$whereDeleteMission."
";
if(_DEBUG_IMPORT){
    print_t($sql);
}
$select = $conn->prepare($sql);
$select->execute();

if(_DEBUG_IMPORT){
    echo $select->rowCount();
    Debug::d_echo("Suppression de ".$select->rowCount()." elements", 2,"updateDba-missions.php");
}

/*
 * INSERT ou UPDATE des données en DBA
 */
foreach($dba_rows as $dba_row){
    $sql = "
            SELECT
                ID_MISSION
            FROM
                missions
            WHERE
                ID_MISSION = '".$dba_row[$ID_MISSION_KEY]."'
    ";
    if(_DEBUG_IMPORT){
        print_t($sql);
    }
    $select = $conn->prepare($sql);
    $select->execute();
    $selectResult = null;
    $selectResult = $select->fetchObject();
    if($selectResult){
        //UPDATE
        $updateSet = "";
        foreach($columns_names as $key=>$columns_name){
            if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                $datatext = "";
                $datatext = str_replace("'", "''", $dba_row[$key]);
                $datatext = str_replace("§§", "\r\n", $datatext);
                if($updateSet == ""){
                    $updateSet .= $columns_name."='".$datatext."'";
                }else{
                    $updateSet .= ",\n".$columns_name."='".$datatext."'";
                }
            }
        }
        $sql = "
            UPDATE
                missions
            SET
                ".$updateSet."
            WHERE
                ID_MISSION = '".$dba_row[$ID_MISSION_KEY]."'
        ";
//        $sql = utf8_encode($sql);
        if(_DEBUG_IMPORT){
            print_t($sql);
        }
        $select = $conn->prepare($sql);
        $select->execute();
        Debug::d_echo("Update ".$dba_row[$ID_MISSION_KEY], 2,"updateDba-missions.php");
        if(_DEBUG_IMPORT){
            print_t("###################################################################################################");
        }

    }else{
        //INSERT
        $insertSet = "";
        foreach($columns_names as $key=>$columns_name){
            if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                $datatext = "";
                $datatext = str_replace("'", "''", $dba_row[$key]);
                $datatext = str_replace("§§", "\r\n", $datatext);
                if($insertSet == ""){
                    $insertSet .= $columns_name."='".$datatext."'";
                }else{
                    $insertSet .= ",\n".$columns_name."='".$datatext."'";
                }
            }
        }
        $sql = "
            INSERT INTO
                missions
            SET
                ".$insertSet."
        ";
//        $sql = utf8_encode($sql);
        if(_DEBUG_IMPORT){
            print_t($sql);
        }
        $select = $conn->prepare($sql);
        $select->execute();
        Debug::d_echo("Insert ".$dba_row[$ID_MISSION_KEY], 2,"updateDba-missions.php");
        if(_DEBUG_IMPORT){
            print_t("###################################################################################################");
        }

    }
}

Debug::d_echo("#######################################################################", 2,"updateDba-missions.php");

?>
