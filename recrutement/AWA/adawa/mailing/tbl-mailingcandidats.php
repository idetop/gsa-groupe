<?php
include_once "../include/display_errors_bo.php";
@session_start();
Debug::d_echo("acces ", 2,"tbl-mailingcandidats.php");
Debug::d_print_r($_GET, 1,"GET","tbl-mailingcandidats.php");
Debug::d_print_r($_POST, 1,"POST","tbl-mailingcandidats.php");
Debug::d_print_r($_SESSION, 1,"SESSION","tbl-mailingcandidats.php");

include_once "../../include/config.php";
@include_once '../include/pdo.php';
include_once "../include/framework.php";
include_once "../include/tableau.php";

$starttime = microtime();


/*
 * Variables communes
 */
$title = _CONF_FORM_MAIL_RELANCE_PAGE_TITLE;
$tableauName = "mailingcandidats";
$filename = "tbl-mailingcandidats.php";
$numberOfRowsPerPage = 10;
//$numberOfRows = 0;
$pageNumber = 1;
$debugMode = 0;
$orderColId = null;
$order = null;
$criteriasArray = array();
$messageToDisplay = null;


/*
 * Control d'accès
 */
if(empty($_SESSION['boe_user_id'])){
    $_SESSION['boe_user_id'] = null;
}
if(empty ($_SESSION['boe_user_email'])){
    $_SESSION['boe_user_email'] = null;
}
test_session_boe($_SESSION['boe_user_id'], $_SESSION['boe_user_email'], "../", $conn);

/*
 * Traitement des données d'entrées ($_GET)
 */
//print_t($_GET);

if(isset($_GET['pageNumber'])){
    if((int)$_GET['pageNumber'] != 0){
        $pageNumber = (int)$_GET['pageNumber'];
    }
}

if(isset($_GET['numberOfRowsPerPage'])){
    if((int)$_GET['numberOfRowsPerPage'] != 0){
        $numberOfRowsPerPage = (int)$_GET['numberOfRowsPerPage'];
    }
}

if(isset($_GET['orderColId'])){
    if((int)$_GET['orderColId'] != 0){
        $orderColId = (int)$_GET['orderColId'];
    }
}

if(isset($_GET['order'])){
    if($_GET['order'] == "up"){
        $order = "up";
    }else{
        $order = "down";
    }
}

if(isset($_GET['msg'])){
   $messageToDisplay = $_GET['msg'];
}

$criteriaId = 0;
while(isset($_GET['crit'.$criteriaId])){
    $criteriasArray[$criteriaId] = $_GET['crit'.$criteriaId];
    $criteriaId++;
}


/*
 * Init tableau
 */
$tableau = new Tableau($tableauName, $filename);
$tableau->setNumberOfRowsPerPage($numberOfRowsPerPage);
$tableau->setPageNumber($pageNumber);
$tableau->tableTitle = $title;
$tableau->messageToDisplay = $messageToDisplay;


/*
 * Définition des boutons
 */
$tblbutton = new TblButton();
$tblbutton->buttonLabel = _CONF_FORM_MAIL_RELANCE_ADD_LABEL;
$tblbutton->altText = _CONF_FORM_MAIL_RELANCE_ADD_ALT;
$tblbutton->buttonpicture = "../images/folder_new.png";
$tblbutton->popupMode = true;
$tblbutton->needMultipleSelection = false;
$tblbutton->link = "v-add-mailrelance.php?";
$tblbutton->popupWidth = 620;
$tblbutton->popupTitle = _CONF_FORM_MAIL_RELANCE_ADD_POPUP;
$tableau->addTblButton($tblbutton);




/*
 * Définition des critères de filtrage
 */

/*
 * Définition des columns du tableau
 */
$colheader = array();
//Checkbox
//$colheader[]= array("content"=>"",
//                    "align"=>"",
//                    "dbaColumnName"=>"",
//                    "orderable"=>"0",
//                    "type"=>"checkbox");

$colheader[]= array("content"=>_CONF_FORM_MAIL_RELANCE_COL_LANGUE,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

$colheader[]= array("content"=>_CONF_FORM_MAIL_RELANCE_COL_OBJET,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

$colheader[]= array("content"=>_CONF_FORM_MAIL_RELANCE_COL_BODY,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");


//
$colheader[]= array("content"=>_CONF_FORM_MAIL_RELANCE_COL_DATECREATION,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

$colheader[]= array("content"=>_CONF_FORM_MAIL_RELANCE_COL_DATEUPDATE,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

$colheader[]= array("content"=>_CONF_FORM_MAIL_RELANCE_COL_DEFAULT,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

$colheader[]= array("content"=>"",
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");


//Création des columns
$tableau->setHeaderRow($colheader);

//définition de l'ordre sur la column selectionnée
if($orderColId != null){
    $tableau->setOrderedColumn($orderColId, $order);
}





/*
 * Requete sql des Ã©lÃ©ments Ã  afficher
 */
//Traitement de l'ordre d'affichage
$orderSql = "";
$orderByTBLName = $tableau->getOrderSQLColumnName();
//Génération du order sql
if($orderByTBLName){
    if($orderSql == ""){
        $orderSql = "ORDER BY `".$orderByTBLName."`";
        if($order == "up"){
            $orderSql .= " ASC";
        }else{
            $orderSql .= " DESC";
        }
    }
}

//Traitement manuel des filtres


/*
 * Requête SQL de comptage
 */
//Récupération de l'id client
$sqlcount = "   SELECT
                    pe.ID
                    
                FROM
                    `awa_mailsrelance` AS pe
                
                ".$tableau->getFilterWhereSQL("")."
            ";
//print_t($sqlcount) ;
$select = $conn->prepare($sqlcount);
$select->execute();
//$tmp  = $select->fetch();
//$nbOfSQLrows = $tmp[0];
$selectResults = $select->fetchAll();
$nbOfSQLrows = count($selectResults);

//Envoi dans le tableau le nombre de row dans la dba
$tableau->setNumberOfSQLRows($nbOfSQLrows);
//Calcul de la condition limit pour le requete sql
$limit = ($pageNumber - 1) * $numberOfRowsPerPage;

//Requete principal
$sql = "SELECT
            `pe`.*
            
            
        FROM
            `awa_mailsrelance` AS `pe`
        

        ".$tableau->getFilterWhereSQL("")."
        
        
        LIMIT ".$limit.",".$numberOfRowsPerPage;

//print_t($sql);
$select = $conn->prepare($sql);
$select->execute();


/*
 * Remplissage du tableau
 */
while($row = $select->fetchObject()){
    //echo $row->id;
    $colrow = array();
    $colrow[]= array("content"=>$row->LANGUE,"align"=>"");
    $colrow[]= array("content"=>"<a class=\"orange\" href=\"".$tableau->getAjaxPopupLink('v-add-mailrelance.php?element_id='.$row->ID,620,_CONF_FORM_MAIL_RELANCE_COL_OBJET_POPUP)."\" title=\""._CONF_FORM_MAIL_RELANCE_COL_OBJET_ALT."\">".$row->OBJECT."</a>","align"=>"");
    $colrow[]= array("content"=>$row->BODY,"align"=>"");
    $colrow[]= array("content"=>" ".getDateDisplay($row->DATE_CREATION,"-"),"align"=>"");
    $colrow[]= array("content"=>" ".getDateDisplay($row->DATE_UPDATE,"-"),"align"=>"");
    if($row->DEFAULT){
        $actifimg = "<img src='../images/check.png'/ height='16' title=\"\" alt=\"\"/>";
    }else{
        $actifimg = "<img src='../images/uncheck.png'/ height='16' title=\"\" alt=\"\"/>";
    }
    $colrow[]= array("content"=>$actifimg,"align"=>"");
    $colrow[]= array("content"=>"<a href=\"javascript:actionAjax_".$tableau->tablename."('act-mailrelance.php?action=del&id=".$row->ID."','".urlencode($row->LANGUE." "._CONF_FORM_MAIL_RELANCE_COL_DEL_RETURN)."','"._CONF_FORM_MAIL_RELANCE_COL_DEL_ASK." : ".$row->LANGUE."');\" title=\""._CONF_FORM_MAIL_RELANCE_COL_DEL_ALT."\"><img src='../images/trash.png'/ height='16' title=\""._CONF_FORM_MAIL_RELANCE_COL_DEL_ALT."\" alt=\""._CONF_FORM_MAIL_RELANCE_COL_DEL_ALT."\"/></a>","align"=>"");
    
//    $colrow[]= array("content"=>" ".$row->VILLE,"align"=>"");
   
    
    $tableau->addRow($colrow);
}


/*
 * Affichage du tableau
 */ 
echo $tableau->displayHTML();
//print_t($_SESSION[]);
//print_t($fcriteria1);
//print_t($tableau);

//print_t($_SESSION);


$endtime = microtime();
//echo($endtime - $starttime);
?>