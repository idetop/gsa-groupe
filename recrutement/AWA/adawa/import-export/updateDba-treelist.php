<?php


/*
 * Lecture du fichier cvsx et décodage
 */
Debug::d_echo("import treelist ", 2,"updateDba-treelist.php");
if(_IS_IMPORT_ENCODAGE){
    if(!is_file(_AWA_PATHWAY."/import_files/treelist.csvx")){
        Debug::d_echo("fichier "._AWA_PATHWAY."/import_files/treelist.csvx manquant", 2,"updateDba-treelist.php");
        exit();
    }
    Debug::d_echo("lecture du fichier", 2,"updateDba-treelist.php");
    $datafile = file_get_contents(_AWA_PATHWAY."/import_files/treelist.csvx");
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

    $key = "admen";
    $datafile = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $datafile, MCRYPT_MODE_ECB, $iv);
    Debug::d_echo("decodage du fichier", 2,"updateDba-treelist.php");
}else{
    if(!is_file(_AWA_PATHWAY."/import_files/treelist.csv")){
        Debug::d_echo("fichier "._AWA_PATHWAY."/import_files/treelist.csv manquant", 2,"updateDba-treelist.php");
        exit();
    }
    Debug::d_echo("lecture du fichier", 2,"updateDba-treelist.php");
    $datafile = file_get_contents(_AWA_PATHWAY."/import_files/treelist.csv");
}
//$datafile = utf8_encode($datafile);
if(_DEBUG_IMPORT){
    print($datafile);
}


/*
 * Récuparation de chaque ligne CSV
 */
preg_match_all('#(.*?)[@]{4}\s#', $datafile, $rows);
if(_DEBUG_IMPORT){
    echo "<pre>";
    print_r($rows[1]);
    echo "</pre>";
}


/*
 * Récuparation des noms de colonnes
 */
preg_match_all('`[#]{4}(.*?)[#]{4};?`', $rows[1][0], $matches);

$columns_names = array();
$columns_names = $matches[1];

if(_DEBUG_IMPORT){
    echo "liste des columns"."\n";
    echo "<pre>";
    print_r($columns_names);
    echo "</pre>";
}


/*
 * Récupération des données
 */
$dba_rows = array();

foreach($rows[1] as $key=>$row){
    if($key != 0){
        preg_match_all('`[#]{4}?(.*?)[#]{4}?;?`', $row, $row_matches);
        $dba_rows[] = $row_matches[1];

    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "liste du contenu"."\n";
    echo "<pre>";
    print_r($dba_rows);
    echo "</pre>";
}


/*
 * Suppression des treelist de la DBA qui ne se trouve pas dans le fichier csv
 */

//On recherche la clé de T_ID_MOTCLE
$ID_TREELIST_KEY = null;
foreach($columns_names as $key=>$columns_name){
    if(_DEBUG_IMPORT){
        echo $columns_name;
    }
    if($columns_name == "T_ID_MOTCLE"){
        $ID_TREELIST_KEY = $key;
        break;
    }
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "index treelist id ".$ID_TREELIST_KEY."\n";
}

//On recup les ID lieés à la clé T_ID_MOTCLE
$ID_TREELIST_values = array();
foreach($dba_rows as $dba_row){
    $ID_TREELIST_values[] = $dba_row[$ID_TREELIST_KEY];
}
if(_DEBUG_IMPORT){
    echo ""."\n";
    echo "<pre>";
    print_r($ID_TREELIST_values);
    echo "</pre>";
}
//On genere la requete SQL de suppression
$whereDeleteTreelist = "";
foreach($ID_TREELIST_values as $ID_TREELIST_value){
    if($whereDeleteTreelist == ""){
        $whereDeleteTreelist .= "WHERE !( T_ID_MOTCLE = '".$ID_TREELIST_value."' "."\n";
    }else{
        $whereDeleteTreelist .= " OR T_ID_MOTCLE = '".$ID_TREELIST_value."' "."\n";
    }
}
$whereDeleteTreelist .= " ) "."\n";

//on supprime les treelist en trop
$sql = "
        DELETE FROM
            treelist
        ".$whereDeleteTreelist."
";
if(_DEBUG_IMPORT){
    print_t($sql);
}
$select = $conn->prepare($sql);
$select->execute();

if(_DEBUG_IMPORT){
    echo $select->rowCount();
    Debug::d_echo("Suppression de ".$select->rowCount()." elements", 2,"updateDba-treelist.php");
}


/*
 * INSERT ou UPDATE des données en DBA
 */
foreach($dba_rows as $dba_row){
    $sql = "
            SELECT
                T_ID_MOTCLE
            FROM
                treelist
            WHERE
                T_ID_MOTCLE = '".$dba_row[$ID_TREELIST_KEY]."'
    ";
    if(_DEBUG_IMPORT){
        print_t($sql);
    }
    $select = $conn->prepare($sql);
    $select->execute();
    $selectResult = null;
    $selectResult = $select->fetchObject();
    if($selectResult){
        //UPDATE
        $updateSet = "";
        foreach($columns_names as $key=>$columns_name){
            if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                $datatext = "";
                $datatext = str_replace("'", "''", $dba_row[$key]);
                $datatext = str_replace("§§", "\r\n", $datatext);
                if($updateSet == ""){
                    $updateSet .= $columns_name."='".$datatext."'";
                }else{
                    $updateSet .= ",\n".$columns_name."='".$datatext."'";
                }
            }
        }
        $sql = "
            UPDATE
                treelist
            SET
                ".$updateSet."
            WHERE
                T_ID_MOTCLE = '".$dba_row[$ID_TREELIST_KEY]."'
        ";
//        $sql = utf8_encode($sql);
        if(_DEBUG_IMPORT){
            print_t($sql);
        }
        $select = $conn->prepare($sql);
        $select->execute();
        Debug::d_echo("Update ".$dba_row[$ID_TREELIST_KEY], 2,"updateDba-treelist.php");
        if(_DEBUG_IMPORT){
            print_t("###################################################################################################");
        }

    }else{
        //INSERT
        $insertSet = "";
        foreach($columns_names as $key=>$columns_name){
            if($dba_row[$key] != "NULL" && $dba_row[$key] != ""){
                $datatext = "";
                $datatext = str_replace("'", "''", $dba_row[$key]);
                $datatext = str_replace("§§", "\r\n", $datatext);
                if($insertSet == ""){
                    $insertSet .= $columns_name."='".$datatext."'";
                }else{
                    $insertSet .= ",\n".$columns_name."='".$datatext."'";
                }
            }
        }
        $sql = "
            INSERT INTO
                treelist
            SET
                ".$insertSet."
        ";
//        $sql = utf8_encode($sql);
        if(_DEBUG_IMPORT){
            print_t($sql);
        }
        $select = $conn->prepare($sql);
        $select->execute();
        Debug::d_echo("Insert ".$dba_row[$ID_TREELIST_KEY], 2,"updateDba-treelist.php");
        if(_DEBUG_IMPORT){
            print_t("###################################################################################################");
        }
    }
}

Debug::d_echo("#######################################################################", 2,"updateDba-treelist.php");

?>
