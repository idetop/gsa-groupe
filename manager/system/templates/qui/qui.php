<?php include('system/libs/tinymce/tinymce.php');  ?>

    <script type="text/javascript">
    var val     = <?= $val; ?>;
    var incr    = 1;
    function incrementer(signe) {
        val=val+signe*incr;
        document.getElementById("qty").value=val;
    }
    function cacher_moins() {
        if (document.getElementById('qty').value == "1") {
            document.getElementById('moins').disabled = true;
            document.getElementById('moins').style.cursor = "no-drop";
        }
        else {
            document.getElementById('moins').disabled = false;
            document.getElementById('moins').style.cursor = "pointer";
        }
    }
    function cacher_plus() {
        if (document.getElementById('qty').value == "<?= $val_max; ?>") {
            document.getElementById('plus').disabled = true;
            document.getElementById('plus').style.cursor = "no-drop";
        }
        else {
            document.getElementById('plus').disabled = false;
            document.getElementById('plus').style.cursor = "pointer";
        }
    }
    </script>

    <!-- Bouton nouveau -->
    <div id="body_btn_nouveau">
        <a href="<?= $site; ?>/qui-sommes-nous.html" class="link_nouveau" target="_blank">
            <div class="btn_nouveau"><span class="span_nouveau">Accéder à la page <font color="#b9001f">&raquo;</font></span></div>
        </a>
        <a href="<?= $_SERVER['PHP_SELF']; ?>?page=<?= $page; ?>&part=<?= $part; ?>&action=add" class="link_nouveau">
            <div class="btn_nouveau"><span class="span_nouveau">Ajouter un slide <font color="#b9001f">+</font></span></div>
        </a>
        <?php if(! empty($_REQUEST['action'])) {  ?>
        <a href="<?= $_SERVER['PHP_SELF']; ?>?page=<?= $page; ?>&part=<?= $part; ?>" class="link_nouveau">
            <div class="btn_nouveau"><span class="span_nouveau">Retour à la liste</span></div>
        </a>
        <?php } ?>
    </div>
    <div class="clear"></div>
</div>


<div id="content">
    <!-- Colonne gauche -->
    <?php include($b_templates.'side_left.php'); ?>

    <!-- Colonne droite -->
    <div class="colonne_right">

		<div id="title_line"><span class="span_title_line">Slides QUI SOMMES-NOUS (<?php return_count('gsa_about_slider','',''); ?>)</span></div>

        <?php if(empty($_REQUEST['id']) && empty($_REQUEST['action'])) {  ?>
        <table class="table_right" width="760" cellpadding="6" cellspacing="0">
            <tbody>
                <tr class="tr_head">
                    <td valign="top" width="40" class="td_head" style="text-align:center">&nbsp;Ordre</td>
                    <td valign="top" class="td_head" style="text-align:left">&nbsp;Texte</td>
                    <td valign="top" width="40" class="td_head" style="text-align:right">Edit</td>
                </tr>
                <?php liste_slides_qui(); ?>
            </tbody>
        </table>
        <br /><br />
		<?php } ?>

		<?php if($_REQUEST['action'] == "edit") { ?>
		<form name="modif_fiche" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="ordre_old" value="<?= $val; ?>">
            <input type="hidden" name="id_update" value="<?= $data_edit['id']; ?>">
            <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
                <tbody>
                    <tr style="background:#252525;">
                        <td colspan="2" style="padding:6px;color:#fff;">
                            &nbsp;&nbsp;<strong style="font-size:11px;">Slide <?= stripslashes($val); ?></strong>
                        </td>
                    </tr>

                    <tr style="background:#ffffff;">
                        <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                            <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Informations générales</strong>
                        </td>
                    </tr>

                    <tr style="background:#fbf9f9">
                        <td style="padding:10px;margin-right:20px;">
                            <strong style="font-size:12px;">Ordre</strong>
                        </td>
                        <td style="padding:5px;">
                            <input type="button" value="-" class="btn_qty" id="moins" onClick="incrementer(-1);cacher_moins();cacher_plus();" style="padding:2px 7px;cursor:<?= $cursor_moins; ?>" <?= $disabled_moins; ?>>

                            <input type="text" name="ordre" value="<?= $val; ?>" class="text" id="qty" style="width:30px;text-align:center;"
                            onFocus="this.style.border='1px solid #efd006';this.style.background='#fdfae5';"
                            onBlur="this.style.border='1px solid #ccc';this.style.background='#fff';">

                            <input type="button" class="btn_qty" id="plus" value="+" onClick="incrementer(+1);cacher_moins();cacher_plus();" style="padding:2px 7px;cursor:<?= $cursor_plus; ?>" <?= $disabled_plus; ?>>&nbsp;&nbsp;

                            <span style="font-size:11px;color:#777777">(ordre d'affichage du slide)</span>
                        </td>
                    </tr>
                    <tr style="background:#ffffff">
                        <td valign="top" colspan="2" style="padding:10px;margin-right:20px;">
                            <p><strong style="font-size:12px;">Texte</strong></p>
                            <textarea name="texte" style="position:relative;top:8px;width:500px;height:250px"><?= stripslashes($data_edit['texte']); ?></textarea>
                        </td>
                    </tr>

                    <tr><td colspan="2" height="10"></td></tr>

                </tbody>
            </table>
            <table class="table_right" width="760" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr style="background:#e9e7e7;">
                        <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                            <input type="submit" name="update" class="table_submit" value="enregistrer mes modifications" />
                        </td>
                    </tr>
                </tbody>
            </table><br /><br />
		</form>
		<?php } ?>


		<?php if($_REQUEST['action'] == "add") { ?>
		<form name="modif_fiche" method="post" action="index.php?page=<?= $page; ?>&part=<?= $part; ?>" enctype="multipart/form-data">
            <input type="hidden" name="ordre_old" value="<?= $val; ?>">
            <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
                <tbody>
                    <tr style="background:#158E02;">
                        <td colspan="2" style="padding:6px;color:#fff;">
                            &nbsp;&nbsp;<strong style="font-size:11px;">Nouvelle slide</strong>
                        </td>
                    </tr>

                    <tr style="background:#ffffff;">
                        <td colspan="2" style="padding:6px;border-bottom:1px solid #ccc;">
                            <br />&nbsp;&nbsp;<strong style="font-size:13px;color:#000;"><img src="src/icons/exclamation.png"> Informations générales</strong>
                        </td>
                    </tr>

                    <tr style="background:#fbf9f9">
                        <td style="padding:10px;margin-right:20px;">
                            <strong style="font-size:12px;">Ordre</strong>
                        </td>
                        <td style="padding:5px;">
                            <input type="button" value="-" class="btn_qty" id="moins" onClick="incrementer(-1);cacher_moins();cacher_plus();" style="padding:2px 7px;cursor:pointer">

                            <input type="text" name="ordre" value="<?= $val; ?>" class="text" id="qty" style="width:30px;text-align:center;"
                            onFocus="this.style.border='1px solid #efd006';this.style.background='#fdfae5';"
                            onBlur="this.style.border='1px solid #ccc';this.style.background='#fff';">

                            <input type="button" class="btn_qty" id="plus" value="+" onClick="incrementer(+1);cacher_moins();cacher_plus();" style="padding:2px 7px;cursor:no-drop;" disabled>&nbsp;&nbsp;

                            <span style="font-size:11px;color:#777777">(ordre d'affichage du slide)</span>
                        </td>
                    </tr>
                    <tr style="background:#ffffff">
                        <td valign="top" colspan="2" style="padding:10px;margin-right:20px;">
                            <p><strong style="font-size:12px;">Texte</strong></p>
                            <textarea name="texte" style="position:relative;top:8px;width:500px;height:250px"></textarea>
                        </td>
                    </tr>

                    <tr><td colspan="2" height="10"></td></tr>

                </tbody>
            </table>
            <table class="table_right" width="760" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr style="background:#e9e7e7;">
                        <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                            <input type="submit" name="submit_add" class="table_submit" value="Enregistrer ce nouveau slide">
                        </td>
                    </tr>
                </tbody>
            </table><br /><br />
		</form>
		<?php } ?>

    </div>
    <div class="clear"></div>
</div>
