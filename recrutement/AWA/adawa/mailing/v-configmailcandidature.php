<?php
//Créé 

//FIchier de configuration de l'affichage des messages d'erreurs
include_once "../include/display_errors_bo.php";

//Ouverture de la session
@session_start();
Debug::d_echo("acces ", 2,"v-configmailcandidature.php");
Debug::d_print_r($_GET, 1,"GET","v-configmailcandidature.php");
Debug::d_print_r($_POST, 1,"POST","v-configmailcandidature.php");
Debug::d_print_r($_SESSION, 1,"SESSION","v-configmailcandidature.php");


//Inclusion des fichiers
include_once('../../include/config.php');
include_once "../include/framework.php";

/*
 * Control d'accès
 */
if(empty($_SESSION['boe_user_id'])){
    $_SESSION['boe_user_id'] = null;
}
if(empty ($_SESSION['boe_user_email'])){
    $_SESSION['boe_user_email'] = null;
}
test_session_boe($_SESSION['boe_user_id'], $_SESSION['boe_user_email'], "../", $conn);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title><?php echo _CONF_FORM_MAIL_CANDIDATURE_PAGE_TITLE; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="../css/boe_style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../include/framework.js"></script>
	<script type="text/javascript" src="../include/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="../include/menu/menu.js"></script>
        <script type="text/javascript" src="../include/menu/menu_tpl.js"></script>
        <link rel="stylesheet" href="../include/menu/menu.css" type="text/css"/>
        <script type="text/javascript" src="../menu.php"></script>
        <script type="text/javascript" src="../include/calendar/calendarTranslation.js.php"></script>
        <script type="text/javascript" src="../include/calendar/calendar_eu.js"></script>
	<link rel="stylesheet" href="../include/calendar/calendar.css" type="text/css"/>
        <script type="text/javascript" >
          $.ajaxSettings.cache = false;
          $(document).ready(function(){          
            $('#tableau_configmailcandidature').load('tbl-configmailcandidature.php');
          });

      </script>
    </head>
    <body>
        <div id="main">
	    <div id="content">
		<div id="header">
                     <?php
			include_once '../header.php';
		    ?>
		</div>
                <div id="menu" style="z-index: 10;">
                    <script type="text/javascript">
                        new menu (MENU_ITEMS, MENU_TPL);
                    </script>
                    <div id="flagsarea"><?php echo displayFlags(); ?></div>
                </div>
                <div id="corps" >
                    <div id="tableau_configmailcandidature" style="width: 98%; margin: 0 auto;"></div>
                    <br/>
                    <br/>
                    
		</div>
		<div id="footer">
		   <?php 
                   include_once '../footer.php';
                   ?>
		</div>
	    </div>
	</div>
    </body>

</html>
