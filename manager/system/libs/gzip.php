<?php
// Compression gzip
$phpver = phpversion();
if($phpver >= "4.0.4pl1") {
	if(extension_loaded("zlib")) {
		if (headers_sent() != TRUE) {
			$gz_possible = isset($HTTP_SERVER_VARS["HTTP_ACCEPT_ENCODING"]) && eregi("gzip, deflate",$HTTP_SERVER_VARS["HTTP_ACCEPT_ENCODING"]);
			if ($gz_possible) ob_start("ob_gzhandler");
		}
	}
}
else if($phpver > "4.0") {
	if(strstr($HTTP_SERVER_VARS['HTTP_ACCEPT_ENCODING'], 'gzip')) {
		if(extension_loaded("zlib")) {
			ob_start();
			ob_implicit_flush(0);
			header("Content-Encoding: gzip");
		}
	}
}
?>