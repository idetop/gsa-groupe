<?php
@include_once 'pdo.php';
include_once 'framework_custom.php';


if(empty($_SESSION['awa_language'] )){
    $_SESSION['awa_language'] = _CONFIG_DEFAULT_LANGUE_FO;
    //Vérification de la langue de l'url d'entré sur le site
    $lg = null;
    $lg = checkEntryUrl();
    if($lg){
        $_SESSION['awa_language'] = $lg;
    }
}



//chargement du fichier de langue associé
include_once("langues/front_".$_SESSION['awa_language'].".php");
if(is_file("langues/front_custom_".$_SESSION['awa_language'].".php")){
    include_once("langues/front_custom_".$_SESSION['awa_language'].".php");
}





/*
 * Récupération du nom et prénom du candidat identifié pour l'afficher sous la zone des drapeaux
 */
function displayCandidatArea($conn){
    $returnHTML = "";
    
    if(!empty($_SESSION['awa_candidat_id']) && !empty($_SESSION['awa_candidat_login'])){
        $sql = "SELECT
                    personnes.NOM,
                    personnes.PRENOM
                FROM
                    awa_candidats
                INNER JOIN personnes ON personnes.ID_PERSONNE_WEB = awa_candidats.ID_PERSONNE_WEB

                WHERE
                    awa_candidats.ID =:ID
                    AND awa_candidats.LOGIN =:LOGIN
        ";
        $select = $conn->prepare($sql);
        $select->bindParam(':ID', $_SESSION['awa_candidat_id'], PDO::PARAM_INT);
        $select->bindParam(':LOGIN', $_SESSION['awa_candidat_login'], PDO::PARAM_STR);
        $select->execute();
        $candidat = null;
        $candidat = $select->fetchObject();
        if($candidat){
            $returnHTML .= "<div id=\"subheader2\">";
            $returnHTML .= _ESPACECANDIDAT_BONJOUR." ".$candidat->PRENOM." ".$candidat->NOM.", ";
            $returnHTML .= " <a href=\""._CONFIG_ROOTFOLDER."logoff.php\" title=\"\">"._ESPACECANDIDAT_DECONNEXION."</a>";
            $returnHTML .= "</div>";
            
        }else{
            $_SESSION['awa_candidat_id'] = "";
            $_SESSION['awa_candidat_login'] = "";
        }
    }
    
    return $returnHTML;
}







/*
 * Gestion de l'affichage du pathway
 */
function displayPathway($pathway = array()){
    $returnhtml = "";
    $returnhtml .= "<a href=\""._CONFIG_ROOTFOLDER."\" title=\""._PATWHAY_HOME."\">"._PATWHAY_HOME."</a>";
    foreach($pathway as $pathElement){
        $returnhtml .= " >> ";
        if(!empty($pathElement["url"])){
            $returnhtml .= "<a href=\"".$pathElement['url']."\" title=\"".$pathElement['text']."\">";
        }
        $returnhtml .= $pathElement['text'];
        if(!empty($pathElement["url"])){
            $returnhtml .= "</a>";
        }
    }
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    
    return $returnhtml;
}




/*
 * Suppression des doubles quotes
 */
function remove2slash($input){
    $output = str_replace('"', '', $input);
    return $output;
}

/*
 * Remplacement du print_r avec des balises <pre> pour une meilleur lecture des tableaux
 */
function print_t($var){
    echo "<pre>\n";
	print_r($var);
    echo "</pre>\n";
}




/*
 * Retourne une date au format SQL (YYYY-MM-DD)
 * Accepte en entrée DD-MM-YYYY ou DD/MM/YYYY
 */
function getEnglishSQLDate($datefr){
    $temp = array();
    str_replace("/", "-", $datefr);
    $temp = explode("-",$datefr);
    if(count($temp) != 3){
//        exit("BAD DATE FORMAT");
    }
    if($temp[2] > 1900){
        return $temp[2]."-".$temp[1]."-".$temp[0];
    }elseif($temp[0] > 1900){
        return $temp[0]."-".$temp[1]."-".$temp[2];
    }elseif($temp[2] == 0 && $temp[1] == 0 && $temp[0] == 0){
        return "0000-00-00";
    }else{
//        exit("BAD DATE FORMAT");
    }
}

/*
 * Conversion des dates sql en date fr
 */
function getDateDisplay($date_sql,$separateur){
//    print_t($date_sql);
    $temp=array();
    $temp=explode("-",$date_sql);
    if(count($temp) != 3){
//        exit("BAD DATE FORMAT");
    }
    if($temp[2] > 1900){
        return $temp[0].$separateur.$temp[1].$separateur.$temp[2];
    }elseif($temp[0] > 1900){
        return $temp[2].$separateur.$temp[1].$separateur.$temp[0];
    }elseif($temp[2] == 0 && $temp[1] == 0 && $temp[0] == 0){
        return "00-00-0000";
    }else{
//        exit("BAD DATE FORMAT2");
    }
    
}

/*
 * Génération des urls des offres
 */
function rewriteAnnonceUrl($id,$title){
    $title = strtolower($title);
    $title = str_replace(" ", "-", $title);
    $title = str_replace("/", "-", $title);
    $title = str_replace("é", "e", $title);
    $title = str_replace("è", "e", $title);
    $title = str_replace("ê", "e", $title);
    $title = str_replace("à", "a", $title);
    $title = str_replace("â", "a", $title);
    $title = str_replace("ç", "c", $title);
    $title = str_replace("ù", "u", $title);
    $title = str_replace(".", "-", $title);
    $title = str_replace(",", "-", $title);
    $title = str_replace("?", "-", $title);
    $title = str_replace("!", "-", $title);
    $title = str_replace(";", "-", $title);
    $title = str_replace(":", "-", $title);
    $title = str_replace("(", "-", $title);
    $title = str_replace(")", "-", $title);
    $title = str_replace("%", "-", $title);
    $title = str_replace("'", "-", $title);
    $title = str_replace('"', "-", $title);
    $title = str_replace('ô', "o", $title);

    return _CONFIG_ROOTFOLDER.constant("_URL_CONFIG_OFFRE_FICHE_".$_SESSION['awa_language'])."/".$title."/".$id.".htm";
}



/*
 * Génération de la vue du formulaire en fonction des paramettres en DBA
 */
function getFormulaireHTMLJSelement($ref,$isCompulsory,$options,$formulaireInputs,$treelist,$treelist_multi,$conn,$cvreaderdata,$formulaire){
    $html = "";
    $js = "";
    $checkjs = "";


    $resizeIframeCode = "";
    if(_IS_IFRAME_RESIZE){
        $resizeIframeCode = "resizeiframe();";
    }

    /*
     * CUSTOM
     */
    if($ref == "EX_CUSTOM"){
        $custom_key = "";
        $custom_translation = "";
        $isCompulsory = "";
        $custom_translationc = "";
        $custom_dba = "";
        $exp_options = unserialize($options);
        foreach($exp_options as $exp_option){
            if(preg_match('#custom_key=(.+)#', $exp_option, $match)){
                $custom_key = $match[1];
            }
            if(preg_match('#custom_traduction=(.+)#', $exp_option, $match)){
                $custom_translation = $match[1];
            }
            if($exp_option == "compulsory"){
                $isCompulsory = "1";
            }
            if(preg_match('#custom_traductionc=(.+)#', $exp_option, $match)){
                $custom_translationc = $match[1];
            }
            if(preg_match('#custom_dba=(.+)#', $exp_option, $match)){
                $custom_dba = $match[1];
            }
        }


        $data = "";
        if(!empty($formulaireInputs['personne'])){
            if(!empty($custom_dba)){
                $data = $formulaireInputs['personne']->$custom_dba;
            }
        }
        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref."_".$custom_key;
            $treelist->idInputDisplay = "tr_candi_".$ref."_".$custom_key;
            $treelist->idInputHidden = "candi_".$ref."_".$custom_key;
            $treelist->buildChilds();


            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">".constant($custom_translation)."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_".$custom_key."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."_".$custom_key."\" name=\"tr_candi_".$ref."_".$custom_key."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input  type=\"hidden\" id=\"candi_".$ref."_".$custom_key."\" name=\"candi_".$ref."_".$custom_key."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "<div id=\"candi_".$ref."_".$custom_key."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_".$custom_key."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";
            $html .= "";
            $html .= "";
            $html .= "";

            if($isCompulsory == "1"){
                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."_".$custom_key."').val())){";
                $checkjs .= "  alert('".constant($custom_translationc)."');";
                $checkjs .= "  $('#tr_candi_".$ref."_".$custom_key."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";
            }


        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">".constant($custom_translation)."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."_".$custom_key."\" name=\"candi_".$ref."_".$custom_key."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."_".$custom_key."').val() == ''){";
               $checkjs .=  "alert('".constant($custom_translationc)."');";
               $checkjs .=  "$('#candi_".$ref."_".$custom_key."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        
        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * EMPTY ROW
     */
    if($ref == "ROW_EMPTY"){
        $rt_text = "";
        $exp_options = unserialize($options);
        foreach($exp_options as $exp_option){
            if(preg_match('#rt_text=(.+)#', $exp_option, $match)){
                    $rt_text = $match[1];
                    $rt_text = constant($rt_text);
                }
        }
        $html .= "<div class=\"candidature_formrow\" style=\"height:20px;\">".$rt_text."</div>";
        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }

    
    /*
     * Champ NOM
     */
    if($ref == "EX_NAME"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->NOM;
        }elseif(!empty($cvreaderdata['etatCivil']['nom'])){
            $data = utf8_decode($cvreaderdata['etatCivil']['nom']); 
        }
        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();
            

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_NAME."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input  type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";
            $html .= "";
            $html .= "";
            $html .= "";

            if($isCompulsory == "1"){
                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_NAME_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";
            }


        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_NAME."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
           
            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_NAME_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ PRENOM
     */
    if($ref == "EX_FIRST_NAME"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->PRENOM;
        }elseif(!empty($cvreaderdata['etatCivil']['prenom'])){
            $data = utf8_decode($cvreaderdata['etatCivil']['prenom']);
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();
            
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_FIRST_NAME."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_FIRST_NAME_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";
             
            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_FIRST_NAME."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_FIRST_NAME_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ CIVILIE
     */
    if($ref == "EX_CIVILITY"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->CIVILITE;
        }elseif(isset($cvreaderdata['etatCivil']['@attributes']['civilite'])){
            if($cvreaderdata['etatCivil']['@attributes']['civilite'] == 0){
                $data = "Mme";
            }elseif($cvreaderdata['etatCivil']['@attributes']['civilite'] == 1){
                $data = "Mlle";
            }else{
                $data = "M.";
            }
             
        }
        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_CIVILITY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_CIVILITY_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_CIVILITY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<select name=\"candi_".$ref."\" id=\"candi_".$ref."\">";
            $html .=    "<option value=\"\">"._CANDIDATURE_EX_CIVILITY_DEFAULT."</option>";
            $civi_options = unserialize($options);
            foreach($civi_options as $civi_option){
                if($civi_option == "_CANDIDATURE_EX_CIVILITY_M"){
                    $selected = "";
                    if($data == "M."){
                        $selected="selected=\"selected\"";
                    }
                    $html .=    "<option value=\"M.\" ".$selected.">"._CANDIDATURE_EX_CIVILITY_M."</option>";
                }elseif($civi_option == "_CANDIDATURE_EX_CIVILITY_MM"){
                    $selected = "";
                    if($data == "Mme"){
                        $selected="selected=\"selected\"";
                    }
                    $html .=    "<option value=\"Mme\" ".$selected.">"._CANDIDATURE_EX_CIVILITY_MM."</option>";
                }elseif($civi_option == "_CANDIDATURE_EX_CIVILITY_ML"){
                    $selected = "";
                    if($data == "Mlle"){
                        $selected="selected=\"selected\"";
                    }
                   $html .=    "<option value=\"Mlle\" ".$selected.">"._CANDIDATURE_EX_CIVILITY_ML."</option>";
                }
            }
            $html .=   "</select>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
        }
        

        if($isCompulsory == "1"){
           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
           $checkjs .=  "alert('"._CANDIDATURE_EX_CIVILITY_JSERROR."');";
           $checkjs .=  "$('#candi_".$ref."').focus();";
           $checkjs .=  "return false;";
           $checkjs .= "}";
        }

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ Adresse
     */
    if($ref == "EX_ADDRESS"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->ADRESSE;
        }elseif(!empty($cvreaderdata['etatCivil']['adresse'])){
            $data = utf8_decode($cvreaderdata['etatCivil']['adresse']);
        }
        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_ADDRESS."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_ADDRESS_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_ADDRESS."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<textarea id=\"candi_".$ref."\" name=\"candi_".$ref."\" cols=\"\" rows=\"\" >".cleanInput($data)."</textarea>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_ADDRESS_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ Code postal
     */
    if($ref == "EX_ZIP"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->CODE_POSTAL;
        }elseif(!empty($cvreaderdata['etatCivil']['codePostal'])){
            $data = utf8_decode($cvreaderdata['etatCivil']['codePostal']);
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_ZIP."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_ZIP_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_ZIP."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_ZIP_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ ville
     */
    if($ref == "EX_CITY"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->VILLE;
        }elseif(!empty($cvreaderdata['etatCivil']['ville'])){
            $data = utf8_decode($cvreaderdata['etatCivil']['ville']);
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_CITY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_CITY_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_CITY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_CITY_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ région
     */
    if($ref == "EX_PROVINCE"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->AREA;
        }
        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_PROVINCE."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_PROVINCE_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_PROVINCE."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_PROVINCE_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }

    /*
     * Champ pays
     */
    if($ref == "EX_COUNTRY"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->PAYS;
        }elseif(!empty($cvreaderdata['etatCivil']['pays'])){
            $data = utf8_decode($cvreaderdata['etatCivil']['pays']);
            $sql = "SELECT NAME FROM countries WHERE CISO = '".$data."'";
            $select = $conn->prepare($sql);            
            $select->execute();
            $selectresult = null;
            $selectresult = $select->fetchObject();
            if($selectresult){
                $data = $selectresult->NAME;
            }
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_COUNTRY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_COUNTRY_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_COUNTRY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_COUNTRY_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ telephone
     */
    if($ref == "EX_PHONE_PERSO"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->TEL_PERSO;
        }elseif(!empty($cvreaderdata['etatCivil']['telFixe'])){
            $data = utf8_decode($cvreaderdata['etatCivil']['telFixe']);
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_PHONE_PERSO."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_PHONE_PERSO_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_PHONE_PERSO."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_PHONE_PERSO_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ mobile
     */
    if($ref == "EX_PHONE_MOBILE"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->TEL_MOBILE;
        }elseif(!empty($cvreaderdata['etatCivil']['telPortable'])){
            $data = utf8_decode($cvreaderdata['etatCivil']['telPortable']);
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_PHONE_MOBILE."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_PHONE_MOBILE_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_PHONE_MOBILE."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_PHONE_MOBILE_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ telephone pro
     */
    if($ref == "EX_PHONE_PRO"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->TEL_DIRECT;
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_PHONE_PRO."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_PHONE_PRO_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_PHONE_PRO."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_PHONE_PRO_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ fax
     */
    if($ref == "EX_FAX"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->FAX_PERSO;
        }
        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_FAX."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_FAX_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_FAX."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_FAX_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ date de naissance
     */
    if($ref == "EX_BIRTH_DATE"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->DATE_NAISSANCE;
            if($data == "0000-00-00"){
                $data = "";
            }        
            if($data != ""){
                $data = getDateDisplay($data, "-");
            }
        }elseif(!empty($cvreaderdata['etatCivil']['anneeNaissance'])){
                        
            if($cvreaderdata['etatCivil']['jourNaissance'] != ""){
                $data .= $cvreaderdata['etatCivil']['jourNaissance'];
            }
            if($cvreaderdata['etatCivil']['moisNaissance'] != ""){
                $data .= "-".$cvreaderdata['etatCivil']['moisNaissance'];
            }
            if($cvreaderdata['etatCivil']['anneeNaissance'] != ""){
                $data .= "-".$cvreaderdata['etatCivil']['anneeNaissance'];
            }
                 
        }
        $html .= "<div class=\"candidature_formrow\" >";
        $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_BIRTH_DATE."";
        if($isCompulsory == "1"){
            $html .= " <span class=\"compulsorystar\">*</span>";
        }
        $html .=  "</div>";
        $html .=  "<div class=\"inputc\">";
        $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
        $html .=  "</div>";
        $html .=  "<div class=\"spacer\"></div>";
        $html .= "</div>";
        $html .= "";

        if($isCompulsory == "1"){
           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
           $checkjs .=  "alert('"._CANDIDATURE_EX_BIRTH_DATE_JSERROR."');";
           $checkjs .=  "$('#candi_".$ref."').focus();";
           $checkjs .=  "return false;";
           $checkjs .= "}else{";
           $checkjs .=  "var exp=new RegExp(\"^[0-9]{2}-[0-9]{2}-[0-9]{4}$\",\"g\");";
           $checkjs .=  "if(!exp.test($('#candi_".$ref."').val())){";
           $checkjs .=   "alert('"._CANDIDATURE_EX_BIRTH_DATE_JSERROR2."');";
           $checkjs .=   "$('#candi_".$ref."').focus();";
           $checkjs .=   "return false;";
           $checkjs .=  "}";
           $checkjs .= "}";
        }else{
            //Control de la date uniquement si saisie
           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
//           $checkjs .=  "alert('"._CANDIDATURE_EX_BIRTH_DATE_JSERROR."');";
//           $checkjs .=  "$('#candi_".$ref."').focus();";
//           $checkjs .=  "return false;";
           $checkjs .= "}else{";
           $checkjs .=  "var exp=new RegExp(\"^[0-9]{2}-[0-9]{2}-[0-9]{4}$\",\"g\");";
           $checkjs .=  "if(!exp.test($('#candi_".$ref."').val())){";
           $checkjs .=   "alert('"._CANDIDATURE_EX_BIRTH_DATE_JSERROR2."');";
           $checkjs .=   "$('#candi_".$ref."').focus();";
           $checkjs .=   "return false;";
           $checkjs .=  "}";
           $checkjs .= "}";
        }

        
        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }

    /*
     * Champ situation personnel
     */
    if($ref == "EX_MARITAL_STATUS"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->SITU_FAMILLE;
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_MARITAL_STATUS."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_MARITAL_STATUS_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_MARITAL_STATUS."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_MARITAL_STATUS_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ nombre d enfants
     */
    if($ref == "EX_CHILDREN_COUNT"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->VAL_NOTE4;
        }
        $html .= "<div class=\"candidature_formrow\" >";
        $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_CHILDREN_COUNT."";
        if($isCompulsory == "1"){
            $html .= " <span class=\"compulsorystar\">*</span>";
        }
        $html .=  "</div>";
        $html .=  "<div class=\"inputc\">";
        $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
        $html .=  "</div>";
        $html .=  "<div class=\"spacer\"></div>";
        $html .= "</div>";
        $html .= "";

        if($isCompulsory == "1"){
            $checkjs .= "if($('#candi_".$ref."').val() == ''){";
            $checkjs .=  "alert('"._CANDIDATURE_EX_CHILDREN_COUNT_JSERROR."');";
            $checkjs .=  "$('#candi_".$ref."').focus();";
            $checkjs .=  "return false;";
            $checkjs .= "}";
        }
        //controle integer
        $checkjs .= "if($('#candi_".$ref."').val() != ''){";
        $checkjs .=  "if(checkInteger($('#candi_".$ref."').val())){";
        $checkjs .=   "alert('"._CANDIDATURE_EX_CHILDREN_COUNT_JSERROR2."');";
        $checkjs .=   "$('#candi_".$ref."').focus();";
        $checkjs .=   "return false;";
        $checkjs .=  "}";
        $checkjs .= "}";

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ nationnalité
     */
    if($ref == "EX_NATIONALITY"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->NATIONALITE;
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_NATIONALITY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_NATIONALITY_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_NATIONALITY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_NATIONALITY_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ email pro
     */
    if($ref == "EX_EMAIL_PRO"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->EMAIL;
        }elseif(!empty($cvreaderdata['etatCivil']['emailPro'])){
            $data = utf8_decode($cvreaderdata['etatCivil']['emailPro']);
        }
        $html .= "<div class=\"candidature_formrow\" >";
        $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EMAIL_PRO."";
        if($isCompulsory == "1"){
            $html .= " <span class=\"compulsorystar\">*</span>";
        }
        $html .=  "</div>";
        $html .=  "<div class=\"inputc\">";
        $html .=   "<input type=\"text\" id=\"candi_".$ref."\" onblur=\"checkEmailDba('candi_".$ref."');\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
        $html .=  "</div>";
        $html .=  "<div class=\"spacer\"></div>";
        $html .= "</div>";
        $html .= "";

        if($isCompulsory == "1"){
           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
           $checkjs .=  "alert('"._CANDIDATURE_EX_EMAIL_PRO_JSERROR."');";
           $checkjs .=  "$('#candi_".$ref."').focus();";
           $checkjs .=  "return false;";
           $checkjs .= "}";
        }
        //controle email
        $checkjs .= "if($('#candi_".$ref."').val() != ''){";
        $checkjs .=  "if(!checkEmail($('#candi_".$ref."').val())){";
        $checkjs .=   "alert('"._CANDIDATURE_EX_EMAIL_PRO_JSERROR2."');";
        $checkjs .=   "$('#candi_".$ref."').focus();";
        $checkjs .=   "return false;";
        $checkjs .=  "}";
        $checkjs .= "}";

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ email perso
     */
    if($ref == "EX_EMAIL_PERSO"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->EMAIL_PERSO;
        }elseif(!empty($cvreaderdata['etatCivil']['emailPerso'])){
            $data = utf8_decode($cvreaderdata['etatCivil']['emailPerso']);
        }
        $html .= "<div class=\"candidature_formrow\" >";
        $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EMAIL_PERSO."";
        if($isCompulsory == "1"){
            $html .= " <span class=\"compulsorystar\">*</span>";
        }
        $html .=  "</div>";
        $html .=  "<div class=\"inputc\">";
        $html .=   "<input type=\"text\" id=\"candi_".$ref."\" onblur=\"checkEmailDba('candi_".$ref."');\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
        $html .=  "</div>";
        $html .=  "<div class=\"spacer\"></div>";
        $html .= "</div>";
        $html .= "";

        if($isCompulsory == "1"){
           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
           $checkjs .=  "alert('"._CANDIDATURE_EX_EMAIL_PERSO_JSERROR."');";
           $checkjs .=  "$('#candi_".$ref."').focus();";
           $checkjs .=  "return false;";
           $checkjs .= "}";
        }
        //controle email
        $checkjs .= "if($('#candi_".$ref."').val() != ''){";
        $checkjs .=  "if(!checkEmail($('#candi_".$ref."').val())){";
        $checkjs .=   "alert('"._CANDIDATURE_EX_EMAIL_PERSO_JSERROR2."');";
        $checkjs .=   "$('#candi_".$ref."').focus();";
        $checkjs .=   "return false;";
        $checkjs .=  "}";
        $checkjs .= "}";

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    if($formulaire == "profil"){
        /*
         * Champ new mot de passe
         */
        if($ref == "EX_NEW_PASSWORD"){

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_NEW_PASSWORD."";
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"password\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_NEW_PASSWORD_COPY."";
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"password\" id=\"candi_copy_".$ref."\" name=\"candi_copy_".$ref."\" value=\"\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            //controle mot de passe identique
            $checkjs .= "if($('#candi_".$ref."').val() != '' || $('#candi_copy_".$ref."').val() != ''){";
            $checkjs .=  "if($('#candi_".$ref."').val() != $('#candi_copy_".$ref."').val()){";
            $checkjs .=   "alert('"._CANDIDATURE_NEW_PASSWORD_JSERROR."');";
            $checkjs .=   "$('#candi_".$ref."').focus();";
            $checkjs .=   "return false;";
            $checkjs .=  "}";
            $checkjs .= "}";

            return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
        }
    }


    /*
     * Champ Experience
     */
    if($ref == "EX_EXPERIENCES_BLOC"){

        $is_title = false;
        $is_add = false;
        $is_enposte = false;
        $is_dates = false;
        $is_dates_comp = false;
        $is_function = false;
        $is_function_comp = false;
        $function_treelist = "";
        $is_function_treelist_multi = false;
        $is_societe = false;
        $is_societe_comp = false;
        $societe_treelist = "";
        $is_societe_treelist_multi = false;
        $nb_default = 1;


        $exp_options = unserialize($options);
        foreach($exp_options as $exp_option){
            if($exp_option == "title"){
                $is_title = true;
            }
            if($exp_option == "adddel"){
                $is_add = true;
            }
            if($exp_option == "enposte"){
                $is_enposte = true;
            }
            if($exp_option == "dates"){
                $is_dates = true;
            }
            if($exp_option == "dates_comp"){
                $is_dates_comp = true;
            }
            if($exp_option == "fonction"){
                $is_function = true;
            }
            if($exp_option == "fonction_comp"){
                $is_function_comp = true;
            }
            if(preg_match('#function_treelist=(.+)#', $exp_option, $match)){
                $function_treelist = $match[1];
            }
            if($exp_option == "function_treelist_multi"){
                $is_function_treelist_multi = true;
            }
            if($exp_option == "societe"){
                $is_societe = true;
            }
            if($exp_option == "societe_comp"){
                $is_societe_comp = true;
            }
            if(preg_match('#societe_treelist=(.+)#', $exp_option, $match)){
                $societe_treelist = $match[1];
            }
            if($exp_option == "societe_treelist_multi"){
                $is_societe_treelist_multi = true;
            }
            if(preg_match('#nb=([0-9])#', $exp_option, $match)){
                $nb_default = $match[1];
            }
        }
        $cvr_exeperience  = array();
        //si il y a des expériences on ne prend pas en compte le nombre d'element pas default.
        if(count($formulaireInputs['experiences'])){
            $nb_default = count($formulaireInputs['experiences']);
        }elseif(isset($cvreaderdata['experiences']['experience'])){
            if(isset($cvreaderdata['experiences']['experience'][0])){
                $tempExp = $cvreaderdata['experiences']['experience'];
            }else{
                $tempExp[0] = $cvreaderdata['experiences']['experience'];
            }
            $cvrexpid = 0;
            foreach($tempExp as $key=>$exp){
                
                $dateDebut = "";
                if($exp['moisDebut'] != "" && $exp['anneeDebut'] != ""){
                    if((int)$exp['anneeDebut'] < 100){
                        if((int)$exp['anneeDebut'] < 30){
                            if((int)$exp['anneeDebut'] < 10 ){
                                $dtemp = "200".(int)$exp['anneeDebut'];
                            }else{
                                $dtemp = "20".(int)$exp['anneeDebut'];
                            }
                        }else{
                            $dtemp = "19".(int)$exp['anneeDebut'];
                        }
                    }else{
                        $dtemp = $exp['anneeDebut'];
                    }
                    $dateDebut = "01-".$exp['moisDebut']."-".$dtemp;

                }
                $dateFin = "";
                if($exp['moisFin'] != "" && $exp['anneeFin'] != ""){
                    if((int)$exp['anneeFin'] < 100){
                        if((int)$exp['anneeFin'] < 30){
                            if((int)$exp['anneeFin'] < 10){
                                $dtemp = "200".(int)$exp['anneeFin'];
                            }else{
                                $dtemp = "20".(int)$exp['anneeFin'];
                            }
                        }else{
                            $dtemp = "19".(int)$exp['anneeFin'];
                        }
                    }else{
                        $dtemp = $exp['anneeFin'];
                    }
                    $dateFin = "01-".$exp['moisFin']."-".$dtemp;
                }
                
                $cvr_exeperience[$cvrexpid]['enPoste'] = $exp['enPoste'];
                $cvr_exeperience[$cvrexpid]['description'] = utf8_decode($exp['description']);
                $cvr_exeperience[$cvrexpid]['entreprise'] = utf8_decode($exp['entreprise']);
                $cvr_exeperience[$cvrexpid]['fonction'] = utf8_decode($exp['fonction']);
                $cvr_exeperience[$cvrexpid]['datedebut'] = $dateDebut;
                $cvr_exeperience[$cvrexpid]['datefin'] = $dateFin;

                $cvrexpid++;

               

            }

            $nb_default = count($cvr_exeperience);

        }
        
        $id = 0;
        if($is_title){
            $html .= "<div id=\"experience_bloc_top\" >";
            $html .=  _CANDIDATURE_EX_EXPERIENCES_BLOC;
            $html .= "</div>";
        }
        if($is_add){
            $html .= "<div id=\"experience_bloc_add\" style=\"\">";
            $html .=  "<span onclick=\"addExpJS();\">"._CANDIDATURE_EX_EXPERIENCES_ADD."</span>";
            $html .= "</div>";
        }
        
        $html .= "<div id=\"experience_bloc_dyna\" >";
        $lastExpIndex = 0;
        for($id = 0; $id < $nb_default; $id++){
            $html .= "<div id=\"experience_bloc_".$id."\" >";

            if($is_enposte){
                $data = "";
                if(!empty($formulaireInputs['experiences'][$id])){
                    $data = $formulaireInputs['experiences'][$id]->EN_COURS;
                }elseif(!empty($cvr_exeperience[$id])){
                    $data = $cvr_exeperience[$id]['enPoste'];
                }
                $checked = "";
                if($data == "1"){
                    $checked="checked=\"checked\"";
                }
                $html .= "<div class=\"candidature_formrow\" >";
                $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_ENPOSTE."";
                $html .=  "</div>";
                $html .=  "<div class=\"inputc\">";
                $html .=   "<input type=\"checkbox\" id=\"candi_EX_ENPOSTE_".$id."\" name=\"candi_EX_ENPOSTE_".$id."\" value=\"1\" style=\"width:15px;\" ".$checked."/>";
                $html .=  "</div>";
                $html .=  "<div class=\"spacer\"></div>";
                $html .= "</div>";
            }

            if($is_dates){
                $data1 = "";
                if(!empty($formulaireInputs['experiences'][$id])){
                    if($formulaireInputs['experiences'][$id]->DATE_EXP != "0000-00-00"){
                        $data1 = $formulaireInputs['experiences'][$id]->DATE_EXP;
                    }
                }elseif(!empty($cvr_exeperience[$id])){
                    $data1 = $cvr_exeperience[$id]['datedebut'];
                }
//                print_t("ed1".$data1);
                if(!empty($data1)){
                    $data1 = getDateDisplay($data1, "-");
                    $data1_tmp = explode("-",$data1);
                    $data1 = $data1_tmp[1]."-".$data1_tmp[2];
                }
                $data2 = "";
                if(!empty($formulaireInputs['experiences'][$id])){
                    if($formulaireInputs['experiences'][$id]->DATE_FIN != "0000-00-00"){
                        $data2 = $formulaireInputs['experiences'][$id]->DATE_FIN;
                    }
                }elseif(!empty($cvr_exeperience[$id])){
                    $data2 = $cvr_exeperience[$id]['datefin'];
                }
//                print_t("ed2".$data2);
                if(!empty($data2)){
                    $data2 = getDateDisplay($data2, "-");
                    $data2_tmp = explode("-",$data2);
                    $data2 = $data2_tmp[1]."-".$data2_tmp[2];
                }

                $html .= "<div class=\"candidature_formrow\" >";
                $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EXP_START."";
                if($is_dates_comp){
                    $html .= " <span class=\"compulsorystar\">*</span>";
                }
                $html .=  "</div>";
                $html .=  "<div class=\"inputc\">";
                $html .=   "<input type=\"text\" id=\"candi_EX_EXP_START_".$id."\" name=\"candi_EX_EXP_START_".$id."\" value=\"".cleanInput($data1)."\" style=\"width:102px;\"/>";
                $html .=  "</div>";
                $html .=  "<div class=\"labelc\" style=\"width: 40px; text-align: center;\"> "._CANDIDATURE_EX_EXP_END." </div>";
                $html .=  "<div class=\"inputc\">";
                $html .=   "<input type=\"text\" id=\"candi_EX_EXP_END_".$id."\" name=\"candi_EX_EXP_END_".$id."\" value=\"".cleanInput($data2)."\" style=\"width:102px;\"/>";
                $html .=  "</div>";
                $html .=  "<div class=\"spacer\"></div>";
                $html .= "</div>";
            }

            if($is_function){
                $data = "";
                if(!empty($formulaireInputs['experiences'][$id])){
                    $data = $formulaireInputs['experiences'][$id]->INTITULE_POSTE;
                }elseif(!empty($cvr_exeperience[$id])){
                    $data = $cvr_exeperience[$id]['fonction'];
                }
                if(!empty($function_treelist)){
                    //MODE TREELIST
                    $treelist = new Treelist($function_treelist);
                    $data = str_replace("$$$$", "", $data);
                    $datatr = "";
                    $datatr_temps = explode("|",$data);
                    foreach($datatr_temps as $datatr_temp){
                        if(!empty($datatr_temp)){
                            $sql = "SELECT
                                        *
                                    FROM
                                        treelist
                                    WHERE
                                        T_ID_MOTCLE =:T_ID_MOTCLE
                                ";
                            $select = $conn->prepare($sql);
                            $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                            $select->execute();
                            $result = $select->fetchObject();
                            if($result){
                                if(!empty($result->VALEUR52)){
                                    $datatr .= $result->VALEUR52." / ";
                                }elseif(!empty($result->VALEUR42)){
                                    $datatr .= $result->VALEUR42." / ";
                                }elseif(!empty($result->VALEUR32)){
                                    $datatr .= $result->VALEUR32." / ";
                                }elseif(!empty($result->VALEUR22)){
                                    $datatr .= $result->VALEUR22." / ";
                                }
                            }
                        }
                    }
                    $treelist->setConn($conn);
                    $treelist->selectedIDs = $datatr_temps;
                    $treelist->isMultiple = $is_function_treelist_multi;
                    $treelist->idhtml = "treel_experience_".$id;
                    $treelist->idInputDisplay = "tr_candi_EX_EXPERIENCE_".$id;
                    $treelist->idInputHidden = "candi_EX_EXPERIENCE_".$id;
                    $treelist->buildChilds();

                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EXPERIENCE."";
                    if($is_function_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" onclick=\"$('#candi_EX_EXPERIENCE_".$id."_treelist').show();".$resizeIframeCode."\" id=\"tr_candi_EX_EXPERIENCE_".$id."\" name=\"tr_candi_EX_EXPERIENCE_".$id."\" value=\"".cleanInput($datatr)."\"/>";
                    $html .=   "<input type=\"hidden\" id=\"candi_EX_EXPERIENCE_".$id."\" name=\"candi_EX_EXPERIENCE_".$id."\" value=\"$$$$".$data."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                    $html .= "<div id=\"candi_EX_EXPERIENCE_".$id."_treelist\" style=\"display:none;padding-left:270px;\">";
                    $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_EX_EXPERIENCE_".$id."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
                    $html .= $treelist->renderHTML();
                    $html .= "</div>";
                }else{
                    //MODE NORMAL
                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EXPERIENCE."";
                    if($is_function_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" id=\"candi_EX_EXPERIENCE_".$id."\" name=\"candi_EX_EXPERIENCE_".$id."\" value=\"".cleanInput($data)."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                }
                
            }

            if($is_societe){
                $data = "";
                if(!empty($formulaireInputs['experiences'][$id])){
                    $data = $formulaireInputs['experiences'][$id]->RAISON_SOCIALE;
                }elseif(!empty($cvr_exeperience[$id])){
                    $data = $cvr_exeperience[$id]['entreprise'];
                }

                if(!empty($societe_treelist)){
                    //MODE TREELIST
                    $treelist = new Treelist($societe_treelist);
                    $data = str_replace("$$$$", "", $data);
                    $datatr = "";
                    $datatr_temps = explode("|",$data);
                    foreach($datatr_temps as $datatr_temp){
                        if(!empty($datatr_temp)){
                            $sql = "SELECT
                                        *
                                    FROM
                                        treelist
                                    WHERE
                                        T_ID_MOTCLE =:T_ID_MOTCLE
                                ";
                            $select = $conn->prepare($sql);
                            $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                            $select->execute();
                            $result = $select->fetchObject();
                            if($result){
                                if(!empty($result->VALEUR52)){
                                    $datatr .= $result->VALEUR52." / ";
                                }elseif(!empty($result->VALEUR42)){
                                    $datatr .= $result->VALEUR42." / ";
                                }elseif(!empty($result->VALEUR32)){
                                    $datatr .= $result->VALEUR32." / ";
                                }elseif(!empty($result->VALEUR22)){
                                    $datatr .= $result->VALEUR22." / ";
                                }
                            }
                        }
                    }
                    $treelist->setConn($conn);
                    $treelist->selectedIDs = $datatr_temps;
                    $treelist->isMultiple = $is_societe_treelist_multi;
                    $treelist->idhtml = "treel_company_".$id;
                    $treelist->idInputDisplay = "tr_candi_EX_EXP_COMPANY_".$id;
                    $treelist->idInputHidden = "candi_EX_EXP_COMPANY_".$id;
                    $treelist->buildChilds();

                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EXP_COMPANY."";
                    if($is_societe_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" onclick=\"$('#candi_EX_EXP_COMPANY_".$id."_treelist').show();".$resizeIframeCode."\" id=\"tr_candi_EX_EXP_COMPANY_".$id."\" name=\"tr_candi_EX_EXP_COMPANY_".$id."\" value=\"".cleanInput($datatr)."\"/>";
                    $html .=   "<input type=\"hidden\" id=\"candi_EX_EXP_COMPANY_".$id."\" name=\"candi_EX_EXP_COMPANY_".$id."\" value=\"$$$$".$data."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                    $html .= "<div id=\"candi_EX_EXP_COMPANY_".$id."_treelist\" style=\"display:none;padding-left:270px;\">";
                    $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_EX_EXP_COMPANY_".$id."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
                    $html .= $treelist->renderHTML();
                    $html .= "</div>";
                }else{
                    //MODE NORMAL
                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EXP_COMPANY."";
                    if($is_societe_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" id=\"candi_EX_EXP_COMPANY_".$id."\" name=\"candi_EX_EXP_COMPANY_".$id."\" value=\"".cleanInput($data)."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                }
                
                
            }

            if($is_add){
                $html .=  "<div class=\"experience_bloc_del\" style=\"\"><span onclick=\"delExp(".$id.");\">"._CANDIDATURE_EX_EXPERIENCES_DEL."</span></div>";
            }
            $html .= "</div>";
            $lastExpIndex = $id;
        }
        $html .= "</div>";
        
        $html .= "<input type=\"hidden\" id=\"candi_EX_EXP_LASTINDEX\" name=\"candi_EX_EXP_LASTINDEX\" value=\"".$lastExpIndex."\" />";

        if(true){
            $checkjs .= "var lastExperienceIndex = $('#candi_EX_EXP_LASTINDEX').val();";
            $checkjs .= " for(i=0;i<=lastExperienceIndex;i++){";
            if($is_dates && $is_dates_comp){
                $checkjs .= "if($('#candi_EX_EXP_START_'+i).val()!= undefined){";
                $checkjs .=  "if($('#candi_EX_EXP_START_'+i).val() == ''){";
                $checkjs .=   "alert('"._CANDIDATURE_EX_EXP_START_JSERROR."');";
                $checkjs .=   "$('#candi_EX_EXP_START_'+i).focus();";
                $checkjs .=   "return false;";
                $checkjs .=  "}else{";
                $checkjs .=   "var exp=new RegExp(\"^[0-9]{2}-[0-9]{4}$\",\"g\");";
                $checkjs .=   "if(!exp.test($('#candi_EX_EXP_START_'+i).val())){";
                $checkjs .=    "alert('"._CANDIDATURE_EX_EXP_START_JSERROR2."');";
                $checkjs .=    "$('#candi_EX_EXP_START_'+i).focus();";
                $checkjs .=    "return false;";
                $checkjs .=   "}";
                $checkjs .=  "}";
                $checkjs .= "}";

                $checkjs .= "if($('#candi_EX_ENPOSTE_'+i).val()== undefined  || !$('#candi_EX_ENPOSTE_'+i).attr('checked')){";
                $checkjs .=  "if($('#candi_EX_EXP_END_'+i).val()!= undefined){";
                $checkjs .=   "if($('#candi_EX_EXP_END_'+i).val() == ''){";
                $checkjs .=    "alert('"._CANDIDATURE_EX_EXP_END_JSERROR."');";
                $checkjs .=    "$('#candi_EX_EXP_END_'+i).focus();";
                $checkjs .=    "return false;";
                $checkjs .=   "}else{";
                $checkjs .=    "var exp=new RegExp(\"^[0-9]{2}-[0-9]{4}$\",\"g\");";
                $checkjs .=    "if(!exp.test($('#candi_EX_EXP_END_'+i).val())){";
                $checkjs .=     "alert('"._CANDIDATURE_EX_EXP_END_JSERROR2."');";
                $checkjs .=     "$('#candi_EX_EXP_END_'+i).focus();";
                $checkjs .=     "return false;";
                $checkjs .=    "}";
                $checkjs .=   "}";
                $checkjs .=  "}";
                $checkjs .= "}";
            }elseif($is_dates && !$is_dates_comp){
                //Controle des dates uniquement si saisie
                $checkjs .= "if($('#candi_EX_EXP_START_'+i).val()!= undefined){";
                $checkjs .=  "if($('#candi_EX_EXP_START_'+i).val() == ''){";
//                $checkjs .=   "alert('"._CANDIDATURE_EX_EXP_START_JSERROR."');";
//                $checkjs .=   "$('#candi_EX_EXP_START_'+i).focus();";
//                $checkjs .=   "return false;";
                $checkjs .=  "}else{";
                $checkjs .=   "var exp=new RegExp(\"^[0-9]{2}-[0-9]{4}$\",\"g\");";
                $checkjs .=   "if(!exp.test($('#candi_EX_EXP_START_'+i).val())){";
                $checkjs .=    "alert('"._CANDIDATURE_EX_EXP_START_JSERROR2."');";
                $checkjs .=    "$('#candi_EX_EXP_START_'+i).focus();";
                $checkjs .=    "return false;";
                $checkjs .=   "}";
                $checkjs .=  "}";
                $checkjs .= "}";

                $checkjs .= "if($('#candi_EX_ENPOSTE_'+i).val()== undefined  || !$('#candi_EX_ENPOSTE_'+i).attr('checked')){";
                $checkjs .=  "if($('#candi_EX_EXP_END_'+i).val()!= undefined){";
                $checkjs .=   "if($('#candi_EX_EXP_END_'+i).val() == ''){";
//                $checkjs .=    "alert('"._CANDIDATURE_EX_EXP_END_JSERROR."');";
//                $checkjs .=    "$('#candi_EX_EXP_END_'+i).focus();";
//                $checkjs .=    "return false;";
                $checkjs .=   "}else{";
                $checkjs .=    "var exp=new RegExp(\"^[0-9]{2}-[0-9]{4}$\",\"g\");";
                $checkjs .=    "if(!exp.test($('#candi_EX_EXP_END_'+i).val())){";
                $checkjs .=     "alert('"._CANDIDATURE_EX_EXP_END_JSERROR2."');";
                $checkjs .=     "$('#candi_EX_EXP_END_'+i).focus();";
                $checkjs .=     "return false;";
                $checkjs .=    "}";
                $checkjs .=   "}";
                $checkjs .=  "}";
                $checkjs .= "}";
            }
            if($is_function && $is_function_comp){
                if(!empty($function_treelist)){
                    //MODE TREELIST
                    $checkjs .= "if($('#candi_EX_EXPERIENCE_'+i).val()!= undefined){";
                    $checkjs .= "   var exp=new RegExp(\"[|]+\",\"g\");";
                    $checkjs .= "   if(!exp.test($('#candi_EX_EXPERIENCE_'+i).val())){";
                    $checkjs .= "       alert('"._CANDIDATURE_EX_EXPERIENCE_TR_JSERROR."');";
                    $checkjs .= "       $('#tr_candi_EX_EXPERIENCE_'+i).focus();";
                    $checkjs .= "       return false;";
                    $checkjs .= "   }";
                    $checkjs .= "}";
                }else{
                    //MODE NORMAL
                    $checkjs .= "if($('#candi_EX_EXPERIENCE_'+i).val()!= undefined){";
                    $checkjs .=  "if($('#candi_EX_EXPERIENCE_'+i).val() == ''){";
                    $checkjs .=   "alert('"._CANDIDATURE_EX_EXPERIENCE_JSERROR."');";
                    $checkjs .=   "$('#candi_EX_EXPERIENCE_'+i).focus();";
                    $checkjs .=   "return false;";
                    $checkjs .=  "}";
                    $checkjs .= "}";
                }

                
            }
            if($is_societe && $is_societe_comp){
                if(!empty($societe_treelist)){
                    //MODE TREELIST
                    $checkjs .= "if($('#candi_EX_EXP_COMPANY_'+i).val()!= undefined){";
                    $checkjs .= "   var exp=new RegExp(\"[|]+\",\"g\");";
                    $checkjs .= "   if(!exp.test($('#candi_EX_EXP_COMPANY_'+i).val())){";
                    $checkjs .= "       alert('"._CANDIDATURE_EX_EXP_COMPANY_TR_JSERROR."');";
                    $checkjs .= "       $('#tr_candi_EX_EXP_COMPANY_'+i).focus();";
                    $checkjs .= "       return false;";
                    $checkjs .= "   }";
                    $checkjs .= "}";
                }else{
                    //MODE NORMAL
                    $checkjs .= "if($('#candi_EX_EXP_COMPANY_'+i).val()!= undefined){";
                    $checkjs .=  "if($('#candi_EX_EXP_COMPANY_'+i).val() == ''){";
                    $checkjs .=   "alert('"._CANDIDATURE_EX_EXP_COMPANY_JSERROR."');";
                    $checkjs .=   "$('#candi_EX_EXP_COMPANY_'+i).focus();";
                    $checkjs .=   "return false;";
                    $checkjs .=  "}";
                    $checkjs .= "}";
                }
            }
            $checkjs .= "}";


        }

        if($is_add){
            $js .= "function addExpJS(){";
            $js .=  "var index_exp = $('#candi_EX_EXP_LASTINDEX').val();";
            $js .=  "var exphtml = \"\";";
            $js .=  "index_exp++;";
            //$js .=  "alert(index_exp);";
            
            
            $js .=  "exphtml += '<div id=\"experience_bloc_'+index_exp+'\" >';";
            if($is_enposte){
                $js .= "exphtml += '<div class=\"candidature_formrow\" >';";
                $js .= "exphtml += '<div class=\"labelc\">"._CANDIDATURE_EX_ENPOSTE."';";
                $js .= "exphtml += '</div>';";
                $js .= "exphtml += '<div class=\"inputc\">';";
                $js .= "exphtml += '<input type=\"checkbox\" id=\"candi_EX_ENPOSTE_'+index_exp+'\" name=\"candi_EX_ENPOSTE_'+index_exp+'\" value=\"1\" style=\"width:15px;\"/>';";
                $js .= "exphtml += '</div>';";
                $js .= "exphtml += '<div class=\"spacer\"></div>';";
                $js .= "exphtml += '</div>';";
            }
            if($is_dates){
                $js .= "exphtml += '<div class=\"candidature_formrow\" >';";
                $js .= "exphtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EXP_START."';";
                if($is_dates_comp){
                    $js .= "exphtml += ' <span class=\"compulsorystar\">*</span>';";
                }
                $js .= "exphtml += '</div>';";
                $js .= "exphtml += '<div class=\"inputc\">';";
                $js .= "exphtml += '<input type=\"text\" id=\"candi_EX_EXP_START_'+index_exp+'\" name=\"candi_EX_EXP_START_'+index_exp+'\" value=\"\" style=\"width:102px;\"/>';";
                $js .= "exphtml += '</div>';";
                $js .= "exphtml += '<div class=\"labelc\" style=\"width: 40px; text-align: center;\"> "._CANDIDATURE_EX_EXP_END." </div>';";
                $js .= "exphtml += '<div class=\"inputc\">';";
                $js .= "exphtml += '<input type=\"text\" id=\"candi_EX_EXP_END_'+index_exp+'\" name=\"candi_EX_EXP_END_'+index_exp+'\" value=\"\" style=\"width:102px;\"/>';";
                $js .= "exphtml += '</div>';";
                $js .= "exphtml += '<div class=\"spacer\"></div>';";
                $js .= "exphtml += '</div>';";
            }
            if($is_function){
                if(!empty($function_treelist)){
                    //MODE TREELIST
                    $js .= "exphtml += '<div class=\"candidature_formrow\" >';";
                    $js .= "exphtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EXPERIENCE."';";
                    if($is_function_comp){
                        $js .= "exphtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .= "exphtml += '</div>';";
                    $js .= "exphtml += '<div class=\"inputc\">';";
                    $js .= "exphtml += '<input type=\"text\" onclick=\"$(\'#candi_EX_EXPERIENCE_'+index_exp+'_treelist\').show();".$resizeIframeCode."\" id=\"tr_candi_EX_EXPERIENCE_'+index_exp+'\" name=\"tr_candi_EX_EXPERIENCE_'+index_exp+'\" value=\"\"/>';";
                    $js .= "exphtml += '<input type=\"hidden\" id=\"candi_EX_EXPERIENCE_'+index_exp+'\" name=\"candi_EX_EXPERIENCE_'+index_exp+'\" value=\"$$$$\"/>';";
                    $js .= "exphtml += '</div>';";
                    $js .= "exphtml += '<div class=\"spacer\"></div>';";
                    $js .= "exphtml += '</div>';";

                    $js .= "exphtml += '<div id=\"candi_EX_EXPERIENCE_'+index_exp+'_treelist\" style=\"display:none;padding-left:270px;\">';";
                    $js .= "exphtml += '<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$(\'#candi_EX_EXPERIENCE_'+index_exp+'_treelist\').hide();".$resizeIframeCode."\">x</span></div>';";
                    $js .= "exphtml += '<div id=\"candi_EX_EXPERIENCE_'+index_exp+'_treelistdiv\"></div>';";
                   
                    
                    $js .= "";

                    $js .= "exphtml += '</div>';";
                }else{
                    //MODE NORMAL
                    $js .= "exphtml += '<div class=\"candidature_formrow\" >';";
                    $js .= "exphtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EXPERIENCE."';";
                    if($is_function_comp){
                        $js .= "exphtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .= "exphtml += '</div>';";
                    $js .= "exphtml += '<div class=\"inputc\">';";
                    $js .= "exphtml += '<input type=\"text\" id=\"candi_EX_EXPERIENCE_'+index_exp+'\" name=\"candi_EX_EXPERIENCE_'+index_exp+'\" value=\"\"/>';";
                    $js .= "exphtml += '</div>';";
                    $js .= "exphtml += '<div class=\"spacer\"></div>';";
                    $js .= "exphtml += '</div>';";
                }
               
            }
            if($is_societe){
                if(!empty($societe_treelist)){
                    //MODE TREELIST
                    $js .= "exphtml += '<div class=\"candidature_formrow\" >';";
                    $js .= "exphtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EXP_COMPANY."';";
                    if($is_societe_comp){
                        $js .= "exphtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .= "exphtml += '</div>';";
                    $js .= "exphtml += '<div class=\"inputc\">';";
                    $js .= "exphtml += '<input type=\"text\" onclick=\"$(\'#candi_EX_EXP_COMPANY_'+index_exp+'_treelist\').show();".$resizeIframeCode."\" id=\"tr_candi_EX_EXP_COMPANY_'+index_exp+'\" name=\"tr_candi_EX_EXP_COMPANY_'+index_exp+'\" value=\"\"/>';";
                    $js .= "exphtml += '<input type=\"hidden\" id=\"candi_EX_EXP_COMPANY_'+index_exp+'\" name=\"candi_EX_EXP_COMPANY_'+index_exp+'\" value=\"$$$$\"/>';";
                    $js .= "exphtml += '</div>';";
                    $js .= "exphtml += '<div class=\"spacer\"></div>';";
                    $js .= "exphtml += '</div>';";

                    $js .= "exphtml += '<div id=\"candi_EX_EXP_COMPANY_'+index_exp+'_treelist\" style=\"display:none;padding-left:270px;\">';";
                    $js .= "exphtml += '<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$(\'#candi_EX_EXP_COMPANY_'+index_exp+'_treelist\').hide();".$resizeIframeCode."\">x</span></div>';";
                    $js .= "exphtml += '<div id=\"candi_EX_EXP_COMPANY_'+index_exp+'_treelistdiv\"></div>';";


                    $js .= "";

                    $js .= "exphtml += '</div>';";
                }else{
                    //MODE NORMAL
                    $js .= "exphtml += '<div class=\"candidature_formrow\" >';";
                    $js .= "exphtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EXP_COMPANY."';";
                    if($is_societe_comp){
                        $js .= "exphtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .= "exphtml += '</div>';";
                    $js .= "exphtml += '<div class=\"inputc\">';";
                    $js .= "exphtml += '<input type=\"text\" id=\"candi_EX_EXP_COMPANY_'+index_exp+'\" name=\"candi_EX_EXP_COMPANY_'+index_exp+'\" value=\"\"/>';";
                    $js .= "exphtml += '</div>';";
                    $js .= "exphtml += '<div class=\"spacer\"></div>';";
                    $js .= "exphtml += '</div>';";
                }
                
            }
            if($is_add){
                $js .=  "exphtml += '<div class=\"experience_bloc_del\" style=\"\"><span onclick=\"delExp('+index_exp+');\">"._CANDIDATURE_EX_EXPERIENCES_DEL."</span></div>';";
            }
            $js .=  "exphtml += '</div>';";
            $js .= "";
            $js .= "";
            
            $js .= "";
            $js .= "";
            $js .= "";
            $js .= "";
            $js .=  "$('#experience_bloc_dyna').prepend(exphtml);";
            $js .= "$('#candi_EX_EXP_LASTINDEX').val(index_exp);";
            if($is_function && !empty($function_treelist)){
                $functiontreelistmulti = "";
                if($is_function_treelist_multi){
                    $functiontreelistmulti = "&treelistismulti=1";
                }
                $js .= "$.get('treelistajax.php?treelistname=".$function_treelist."".$functiontreelistmulti."&treelistidhtml=treel_experience_'+index_exp+'&treelistidinputdisplay=tr_candi_EX_EXPERIENCE_'+index_exp+'&treelistidinputhidden=candi_EX_EXPERIENCE_'+index_exp, function(datartr){ $('#candi_EX_EXPERIENCE_'+index_exp+'_treelistdiv').html(datartr);} );";
            }
            if($is_societe && !empty($societe_treelist)){
                $societetreelistmulti = "";
                if($is_societe_treelist_multi){
                    $societetreelistmulti = "&treelistismulti=1";
                }
                $js .= "$.get('treelistajax.php?treelistname=".$societe_treelist."".$societetreelistmulti."&treelistidhtml=treel_company_'+index_exp+'&treelistidinputdisplay=tr_candi_EX_EXP_COMPANY_'+index_exp+'&treelistidinputhidden=candi_EX_EXP_COMPANY_'+index_exp, function(datartr){ $('#candi_EX_EXP_COMPANY_'+index_exp+'_treelistdiv').html(datartr);} );";
            }

            if(_IS_IFRAME_RESIZE){
                $js .= " resizeiframe();";
            } 

            $js .= "}";

            $js .= "function delExp(index){";
            $js .= "$('#experience_bloc_'+index).remove();";
            if(_IS_IFRAME_RESIZE){
                $js .= " resizeiframe();";
            } 
            $js .= "}";

        }

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }

    /*
     * Champ Formation
     */
    if($ref == "EX_FORMATIONS_BLOC"){
        $is_title = false;
        $is_add = false;
        $is_intitul = false;
        $is_intitul_comp = false;
        $intitul_treelist = "";
        $is_intitul_treelist_multi = false;
        $is_etabl = false;
        $is_etabl_comp = false;
        $etabl_treelist = "";
        $is_etabl_treelist_multi = false;
        $is_dates = false;
        $is_dates_comp = false;
        $is_niveau = false;
        $is_niveau_comp = false;
        $niveau_treelist = "";
        $is_niveau_treelist_multi = false;
        $nb_default = 1;

        $fos_options = unserialize($options);
        foreach($fos_options as $fo_option){
            if($fo_option == "title"){
                $is_title = true;
            }
            if($fo_option == "adddel"){
                $is_add = true;
            }
            if($fo_option == "intitul"){
                $is_intitul = true;
            }
            if($fo_option == "intitul_comp"){
                $is_intitul_comp = true;
            }
            if(preg_match('#intitul_treelist=(.+)#', $fo_option, $match)){
                $intitul_treelist = $match[1];
            }
            if($fo_option == "intitul_treelist_multi"){
                $is_intitul_treelist_multi = true;
            }
            
            if($fo_option == "etabl"){
                $is_etabl = true;
            }
            if($fo_option == "etabl_comp"){
                $is_etabl_comp = true;
            }
            if(preg_match('#etabl_treelist=(.+)#', $fo_option, $match)){
                $etabl_treelist = $match[1];
            }
            if($fo_option == "etabl_treelist_multi"){
                $is_etabl_treelist_multi = "checked=\"checked\"";
            }

            if($fo_option == "dates"){
                $is_dates = true;
            }
            if($fo_option == "dates_comp"){
                $is_dates_comp = true;
            }
            if($fo_option == "niveau"){
                $is_niveau = true;
            }
            if($fo_option == "niveau_comp"){
                $is_niveau_comp = true;
            }
            if(preg_match('#niveau_treelist=(.+)#', $fo_option, $match)){
                $niveau_treelist = $match[1];
            }
            if($fo_option == "niveau_treelist_multi"){
                $is_niveau_treelist_multi = "checked=\"checked\"";
            }
             if(preg_match('#nb=([0-9])#', $fo_option, $match)){
                $nb_default = $match[1];
            }
        }

        //si il y a des formation on ne prend pas en compte le nombre d'element pas default.
        $cvr_fo = array();

        if(count($formulaireInputs['formations'])){
            $nb_default = count($formulaireInputs['formations']);
        }elseif(isset($cvreaderdata['formations']['formation'])){
            
            if(isset($cvreaderdata['formations']['formation'][0])){
                $tempFormation = $cvreaderdata['formations']['formation'];
            }else{
                $tempFormation[0] = $cvreaderdata['formations']['formation'];
            }
            $cvrfoid = 0;
            foreach($tempFormation as $key=>$formation){                

                $cvr_fo[$cvrfoid]['dateObtention'] = $formation['dateObtention'];
                $cvr_fo[$cvrfoid]['intitule'] = utf8_decode($formation['intitule']);
                $cvr_fo[$cvrfoid]['etablissementVille'] = utf8_decode($formation['etablissementVille']);               

                $cvrfoid++;

            }

            $nb_default = count($cvr_fo);

        }
//        print_t($cvr_fo);
        $id = 0;
        if($is_title){
            $html .= "<div id=\"formation_bloc_top\" >";
            $html .=  _CANDIDATURE_EX_FORMATIONS_BLOC;
            $html .= "</div>";
        }
        if($is_add){
            $html .= "<div id=\"formation_bloc_add\" style=\"\">";
            $html .=  "<span onclick=\"addFoJS();\">"._CANDIDATURE_EX_FORMATIONS_ADD."</span>";
            $html .= "</div>";
        }

        $html .= "<div id=\"formation_bloc_dyna\" >";

        $lastFoIndex = 0;
        for($id = 0; $id < $nb_default; $id++){

            $html .= "<div id=\"formation_bloc_".$id."\" >";
            if($is_intitul){
                $data = "";
                if(!empty($formulaireInputs['formations'][$id])){
                    $data = $formulaireInputs['formations'][$id]->DIPLOME;
                }elseif(!empty($cvr_fo[$id])){
                    $data = $cvr_fo[$id]['intitule'];
                }

                if(!empty($intitul_treelist)){
                    //MODE TREELIST
                    $treelist = new Treelist($intitul_treelist);
                    $data = str_replace("$$$$", "", $data);
                    $datatr = "";
                    $datatr_temps = explode("|",$data);
                    foreach($datatr_temps as $datatr_temp){
                        if(!empty($datatr_temp)){
                            $sql = "SELECT
                                        *
                                    FROM
                                        treelist
                                    WHERE
                                        T_ID_MOTCLE =:T_ID_MOTCLE
                                ";
                            $select = $conn->prepare($sql);
                            $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                            $select->execute();
                            $result = $select->fetchObject();
                            if($result){
                                if(!empty($result->VALEUR52)){
                                    $datatr .= $result->VALEUR52." / ";
                                }elseif(!empty($result->VALEUR42)){
                                    $datatr .= $result->VALEUR42." / ";
                                }elseif(!empty($result->VALEUR32)){
                                    $datatr .= $result->VALEUR32." / ";
                                }elseif(!empty($result->VALEUR22)){
                                    $datatr .= $result->VALEUR22." / ";
                                }
                            }
                        }
                    }
                    $treelist->setConn($conn);
                    $treelist->selectedIDs = $datatr_temps;
                    $treelist->isMultiple = $is_intitul_treelist_multi;
                    $treelist->idhtml = "treel_fo_intitul_".$id;
                    $treelist->idInputDisplay = "tr_candi_FO_INTITUL_".$id;
                    $treelist->idInputHidden = "candi_FO_INTITUL_".$id;
                    $treelist->buildChilds();

                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EDUCATION."";
                    if($is_intitul_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" onclick=\"$('#candi_FO_INTITUL_".$id."_treelist').show();".$resizeIframeCode."\" id=\"tr_candi_FO_INTITUL_".$id."\" name=\"tr_candi_FO_INTITUL_".$id."\" value=\"".cleanInput($datatr)."\"/>";
                    $html .=   "<input type=\"hidden\" id=\"candi_FO_INTITUL_".$id."\" name=\"candi_FO_INTITUL_".$id."\" value=\"$$$$".$data."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                    $html .= "<div id=\"candi_FO_INTITUL_".$id."_treelist\" style=\"display:none;padding-left:270px;\">";
                    $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_FO_INTITUL_".$id."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
                    $html .= $treelist->renderHTML();
                    $html .= "</div>";
                    
                }else{
                    //MODE NORMAL
                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EDUCATION;
                    if($is_intitul_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" id=\"candi_FO_INTITUL_".$id."\" name=\"candi_FO_INTITUL_".$id."\" value=\"".cleanInput($data)."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                }


                
            }
            if($is_etabl){
                $data = "";
                if(!empty($formulaireInputs['formations'][$id])){
                    $data = $formulaireInputs['formations'][$id]->INSTITUT;
                }elseif(!empty($cvr_fo[$id])){
                    $data = $cvr_fo[$id]['etablissementVille'];
                }

                if(!empty($etabl_treelist)){
                    //MODE TREELIST
                    $treelist = new Treelist($etabl_treelist);
                    $data = str_replace("$$$$", "", $data);
                    $datatr = "";
                    $datatr_temps = explode("|",$data);
                    foreach($datatr_temps as $datatr_temp){
                        if(!empty($datatr_temp)){
                            $sql = "SELECT
                                        *
                                    FROM
                                        treelist
                                    WHERE
                                        T_ID_MOTCLE =:T_ID_MOTCLE
                                ";
                            $select = $conn->prepare($sql);
                            $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                            $select->execute();
                            $result = $select->fetchObject();
                            if($result){
                                if(!empty($result->VALEUR52)){
                                    $datatr .= $result->VALEUR52." / ";
                                }elseif(!empty($result->VALEUR42)){
                                    $datatr .= $result->VALEUR42." / ";
                                }elseif(!empty($result->VALEUR32)){
                                    $datatr .= $result->VALEUR32." / ";
                                }elseif(!empty($result->VALEUR22)){
                                    $datatr .= $result->VALEUR22." / ";
                                }
                            }
                        }
                    }
                    $treelist->setConn($conn);
                    $treelist->selectedIDs = $datatr_temps;
                    $treelist->isMultiple = $is_intitul_treelist_multi;
                    $treelist->idhtml = "treel_fo_etabl_".$id;
                    $treelist->idInputDisplay = "tr_candi_FO_ETABL_".$id;
                    $treelist->idInputHidden = "candi_FO_ETABL_".$id;
                    $treelist->buildChilds();

                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EDU_SCHOOL."";
                    if($is_etabl_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" onclick=\"$('#candi_FO_ETABL_".$id."_treelist').show();".$resizeIframeCode."\" id=\"tr_candi_FO_ETABL_".$id."\" name=\"tr_candi_FO_ETABL_".$id."\" value=\"".cleanInput($datatr)."\"/>";
                    $html .=   "<input type=\"hidden\" id=\"candi_FO_ETABL_".$id."\" name=\"candi_FO_ETABL_".$id."\" value=\"$$$$".$data."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                    $html .= "<div id=\"candi_FO_ETABL_".$id."_treelist\" style=\"display:none;padding-left:270px;\">";
                    $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_FO_ETABL_".$id."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
                    $html .= $treelist->renderHTML();
                    $html .= "</div>";
                }else{
                    //MODE NORMAL
                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EDU_SCHOOL;
                    if($is_etabl_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" id=\"candi_FO_ETABL_".$id."\" name=\"candi_FO_ETABL_".$id."\" value=\"".cleanInput($data)."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                }
                
            }
            if($is_dates){
                $data1 = "";
//                if(!empty($formulaireInputs['formations'][$id])){
//                    if($formulaireInputs['formations'][$id]->DATE_FORM != "0000-00-00"){
//                        $data1 = $formulaireInputs['formations'][$id]->DATE_FORM;
//                    }
//                }
////                print_t("fd1".$data1);
//                if(!empty($data1)){
//                    $data1 = getDateDisplay($data1, "-");
//                    $data1_tmp = explode("-",$data1);
//                    $data1 = $data1_tmp[1]."-".$data1_tmp[2];
//                }
                $data2 = "";
                if(!empty($formulaireInputs['formations'][$id])){
                    if(preg_match('#[0-9]{4}-[0-9]{2}-[0-9]{2}#', $formulaireInputs['formations'][$id]->DATE_FORM, $matches)){
                        if($matches[0] != "0000-00-00"){
                            $data2 = $matches[0];
                        }
                    }
                }elseif(!empty($cvr_fo[$id]['dateObtention'])){
                    if((int)$cvr_fo[$id]['dateObtention'] < 100){
                        if((int)$cvr_fo[$id]['dateObtention'] < 30){
                            if((int)$cvr_fo[$id]['dateObtention'] < 10){
                                $dtemp = "200".$cvr_fo[$id]['dateObtention'];
                            }else{
                                 $dtemp = "20".$cvr_fo[$id]['dateObtention'];
                            }
                        }else{
                            $dtemp = "19".$cvr_fo[$id]['dateObtention'];
                        }
                    }else{
                        $dtemp = $cvr_fo[$id]['dateObtention'];
                    }
                    
                    $data2 = "01-01-".$dtemp;
                }
//                print_t("fd2".$data2);
                if(!empty($data2)){
                    $data2 = getDateDisplay($data2, "-");
                    $data2_tmp = explode("-",$data2);
                    $data2 = $data2_tmp[2];
                }

                $html .= "<div class=\"candidature_formrow\" >";
                $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EDU_START;
                if($is_dates_comp){
                    $html .= " <span class=\"compulsorystar\">*</span>";
                }
                $html .=  "</div>";
//                $html .=  "<div class=\"inputc\">";
//                $html .=   "<input type=\"text\" id=\"candi_FO_FORMA_START_".$id."\" name=\"candi_FO_FORMA_START_".$id."\" value=\"".cleanInput($data1)."\" style=\"width:102px;\"/>";
//                $html .=  "</div>";
//                $html .=  "<div class=\"labelc\" style=\"width: 40px; text-align: center;\"> "._CANDIDATURE_EX_EDU_END." </div>";
                $html .=  "<div class=\"inputc\">";
                $html .=   "<input type=\"text\" id=\"candi_FO_FORMA_END_".$id."\" name=\"candi_FO_FORMA_END_".$id."\" value=\"".cleanInput($data2)."\" style=\"\"/>";
                $html .=  "</div>";
                $html .=  "<div class=\"spacer\"></div>";
                $html .= "</div>";
            }
            if($is_niveau){
                $data = "";
                if(!empty($formulaireInputs['formations'][$id])){
                    $data = $formulaireInputs['formations'][$id]->TYPOLOGIE_FORMATION;
                }

                if(!empty($niveau_treelist)){
                    //MODE TREELIST
                    $treelist = new Treelist($niveau_treelist);
                    $data = str_replace("$$$$", "", $data);
                    $datatr = "";
                    $datatr_temps = explode("|",$data);
                    foreach($datatr_temps as $datatr_temp){
                        if(!empty($datatr_temp)){
                            $sql = "SELECT
                                        *
                                    FROM
                                        treelist
                                    WHERE
                                        T_ID_MOTCLE =:T_ID_MOTCLE
                                ";
                            $select = $conn->prepare($sql);
                            $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                            $select->execute();
                            $result = $select->fetchObject();
                            if($result){
                                if(!empty($result->VALEUR52)){
                                    $datatr .= $result->VALEUR52." / ";
                                }elseif(!empty($result->VALEUR42)){
                                    $datatr .= $result->VALEUR42." / ";
                                }elseif(!empty($result->VALEUR32)){
                                    $datatr .= $result->VALEUR32." / ";
                                }elseif(!empty($result->VALEUR22)){
                                    $datatr .= $result->VALEUR22." / ";
                                }
                            }
                        }
                    }
                    $treelist->setConn($conn);
                    $treelist->selectedIDs = $datatr_temps;
                    $treelist->isMultiple = $is_niveau_treelist_multi;
                    $treelist->idhtml = "treel_fo_niveau_".$id;
                    $treelist->idInputDisplay = "tr_candi_FO_NIVEAU_".$id;
                    $treelist->idInputHidden = "candi_FO_NIVEAU_".$id;
                    $treelist->buildChilds();

                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EDU_LEVEL."";
                    if($is_niveau_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" onclick=\"$('#candi_FO_NIVEAU_".$id."_treelist').show();".$resizeIframeCode."\" id=\"tr_candi_FO_NIVEAU_".$id."\" name=\"tr_candi_FO_NIVEAU_".$id."\" value=\"".cleanInput($datatr)."\"/>";
                    $html .=   "<input type=\"hidden\" id=\"candi_FO_NIVEAU_".$id."\" name=\"candi_FO_NIVEAU_".$id."\" value=\"$$$$".$data."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                    $html .= "<div id=\"candi_FO_NIVEAU_".$id."_treelist\" style=\"display:none;padding-left:270px;\">";
                    $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_FO_NIVEAU_".$id."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
                    $html .= $treelist->renderHTML();
                    $html .= "</div>";
                }else{
                    //MODE NORMAL
                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_EDU_LEVEL;
                    if($is_niveau_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" id=\"candi_FO_NIVEAU_".$id."\" name=\"candi_FO_NIVEAU_".$id."\" value=\"".cleanInput($data)."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                }
                
            }

            if($is_add){
                $html .=  "<div class=\"formation_bloc_del\" style=\"\"><span onclick=\"delFo(".$id.");\">"._CANDIDATURE_EX_FORMATIONS_DEL."</span></div>";
            }
            
            $html .= "</div>";
            $lastFoIndex = $id;
        }
        $html .= "</div>";
        $html .= "<input type=\"hidden\" id=\"candi_FO_FORMA_LASTINDEX\" name=\"candi_FO_FORMA_LASTINDEX\" value=\"".$lastFoIndex."\" />";

        if(true){
            $checkjs .= "var lastFormationIndex = $('#candi_FO_FORMA_LASTINDEX').val();";
            $checkjs .= "for(i=0;i<=lastFormationIndex;i++){";
            if($is_intitul && $is_intitul_comp){
                if(!empty($intitul_treelist)){
                    //MODE TREELIST
                    $checkjs .= "if($('#candi_FO_INTITUL_'+i).val()!= undefined){";
                    $checkjs .= "   var exp=new RegExp(\"[|]+\",\"g\");";
                    $checkjs .= "   if(!exp.test($('#candi_FO_INTITUL_'+i).val())){";
                    $checkjs .= "       alert('"._CANDIDATURE_EX_EDUCATION_TR_JSERROR."');";
                    $checkjs .= "       $('#tr_candi_FO_INTITUL_'+i).focus();";
                    $checkjs .= "       return false;";
                    $checkjs .= "   }";
                    $checkjs .= "}";
                }else{
                    //MODE NORMAL
                    $checkjs .= "if($('#candi_FO_INTITUL_'+i).val()!= undefined){";
                    $checkjs .=  "if($('#candi_FO_INTITUL_'+i).val() == ''){";
                    $checkjs .=   "alert('"._CANDIDATURE_EX_EDUCATION_JSERROR."');";
                    $checkjs .=   "$('#candi_FO_INTITUL_'+i).focus();";
                    $checkjs .=   "return false;";
                    $checkjs .=  "}";
                    $checkjs .= "}";
                }
                
            }

            if($is_etabl && $is_etabl_comp){
                if(!empty($etabl_treelist)){
                    //MODE TREELIST
                    $checkjs .= "if($('#candi_FO_ETABL_'+i).val()!= undefined){";
                    $checkjs .= "   var exp=new RegExp(\"[|]+\",\"g\");";
                    $checkjs .= "   if(!exp.test($('#candi_FO_ETABL_'+i).val())){";
                    $checkjs .= "       alert('"._CANDIDATURE_EX_EDU_SCHOOL_TR_JSERROR."');";
                    $checkjs .= "       $('#tr_candi_FO_ETABL_'+i).focus();";
                    $checkjs .= "       return false;";
                    $checkjs .= "   }";
                    $checkjs .= "}";
                }else{
                    //MODE NORMAL
                    $checkjs .= "if($('#candi_FO_ETABL_'+i).val()!= undefined){";
                    $checkjs .=  "if($('#candi_FO_ETABL_'+i).val() == ''){";
                    $checkjs .=   "alert('"._CANDIDATURE_EX_EDU_SCHOOL_JSERROR."');";
                    $checkjs .=   "$('#candi_FO_ETABL_'+i).focus();";
                    $checkjs .=   "return false;";
                    $checkjs .=  "}";
                    $checkjs .= "}";
                }
                
            }

            if($is_dates && $is_dates_comp){
//                $checkjs .= "if($('#candi_FO_FORMA_START_'+i).val()!= undefined){";
//                $checkjs .=  "if($('#candi_FO_FORMA_START_'+i).val() == ''){";
//                $checkjs .=   "alert('"._CANDIDATURE_EX_EDU_START_JSERROR."');";
//                $checkjs .=   "$('#candi_FO_FORMA_START_'+i).focus();";
//                $checkjs .=   "return false;";
//                $checkjs .=  "}else{";
//                $checkjs .=   "var exp=new RegExp(\"^[0-9]{2}-[0-9]{4}$\",\"g\");";
//                $checkjs .=   "if(!exp.test($('#candi_FO_FORMA_START_'+i).val())){";
//                $checkjs .=    "alert('"._CANDIDATURE_EX_EDU_START_JSERROR2."');";
//                $checkjs .=    "$('#candi_FO_FORMA_START_'+i).focus();";
//                $checkjs .=    "return false;";
//                $checkjs .=   "}";
//                $checkjs .=  "}";
//                $checkjs .= "}";
                
                $checkjs .=  "if($('#candi_FO_FORMA_END_'+i).val()!= undefined){";
                $checkjs .=   "if($('#candi_FO_FORMA_END_'+i).val() == ''){";
                $checkjs .=    "alert('"._CANDIDATURE_EX_EDU_END_JSERROR."');";
                $checkjs .=    "$('#candi_FO_FORMA_END_'+i).focus();";
                $checkjs .=    "return false;";
                $checkjs .=   "}else{";
                $checkjs .=    "var exp=new RegExp(\"^[0-9]{4}$\",\"g\");";
                $checkjs .=    "if(!exp.test($('#candi_FO_FORMA_END_'+i).val())){";
                $checkjs .=     "alert('"._CANDIDATURE_EX_EDU_END_JSERROR2."');";
                $checkjs .=     "$('#candi_FO_FORMA_END_'+i).focus();";
                $checkjs .=     "return false;";
                $checkjs .=    "}";
                $checkjs .=   "}";
                $checkjs .=  "}";
               
            }elseif($is_dates && !$is_dates_comp){
                //Controle des dates uniquement si saisie
//                $checkjs .= "if($('#candi_FO_FORMA_START_'+i).val()!= undefined){";
//                $checkjs .=  "if($('#candi_FO_FORMA_START_'+i).val() == ''){";
//                $checkjs .=  "}else{";
//                $checkjs .=   "var exp=new RegExp(\"^[0-9]{2}-[0-9]{4}$\",\"g\");";
//                $checkjs .=   "if(!exp.test($('#candi_FO_FORMA_START_'+i).val())){";
//                $checkjs .=    "alert('"._CANDIDATURE_EX_EDU_START_JSERROR2."');";
//                $checkjs .=    "$('#candi_FO_FORMA_START_'+i).focus();";
//                $checkjs .=    "return false;";
//                $checkjs .=   "}";
//                $checkjs .=  "}";
//                $checkjs .= "}";

                $checkjs .=  "if($('#candi_FO_FORMA_END_'+i).val()!= undefined){";
                $checkjs .=   "if($('#candi_FO_FORMA_END_'+i).val() == ''){";
                $checkjs .=   "}else{";
                $checkjs .=    "var exp=new RegExp(\"^[0-9]{4}$\",\"g\");";
                $checkjs .=    "if(!exp.test($('#candi_FO_FORMA_END_'+i).val())){";
                $checkjs .=     "alert('"._CANDIDATURE_EX_EDU_END_JSERROR2."');";
                $checkjs .=     "$('#candi_FO_FORMA_END_'+i).focus();";
                $checkjs .=     "return false;";
                $checkjs .=    "}";
                $checkjs .=   "}";
                $checkjs .=  "}";
            }

            if($is_niveau && $is_niveau_comp){
                if(!empty($niveau_treelist)){
                    //MODE TREELIST
                    $checkjs .= "if($('#candi_FO_NIVEAU_'+i).val()!= undefined){";
                    $checkjs .= "   var exp=new RegExp(\"[|]+\",\"g\");";
                    $checkjs .= "   if(!exp.test($('#candi_FO_NIVEAU_'+i).val())){";
                    $checkjs .= "       alert('"._CANDIDATURE_EX_EDU_LEVEL_TR_JSERROR."');";
                    $checkjs .= "       $('#tr_candi_FO_NIVEAU_'+i).focus();";
                    $checkjs .= "       return false;";
                    $checkjs .= "   }";
                    $checkjs .= "}";
                }else{
                    //MODE NORMAL
                    $checkjs .= "if($('#candi_FO_NIVEAU_'+i).val()!= undefined){";
                    $checkjs .=  "if($('#candi_FO_NIVEAU_'+i).val() == ''){";
                    $checkjs .=   "alert('"._CANDIDATURE_EX_EDU_LEVEL_JSERROR."');";
                    $checkjs .=   "$('#candi_FO_NIVEAU_'+i).focus();";
                    $checkjs .=   "return false;";
                    $checkjs .=  "}";
                    $checkjs .= "}";
                }
                
            }

            $checkjs .= "}";
        }

         if($is_add){
            $js .= "function addFoJS(){";
            $js .=  "var index_fo = $('#candi_FO_FORMA_LASTINDEX').val();";
            $js .=  "var fohtml = \"\";";
            $js .=  "index_fo++;";

            $js .=  "fohtml += '<div id=\"formation_bloc_'+index_fo+'\" >';";
            if($is_intitul){
                if(!empty($intitul_treelist)){
                    ///MODE TREELIST
                    $js .= "fohtml += '<div class=\"candidature_formrow\" >';";
                    $js .= "fohtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EDUCATION."';";
                    if($is_intitul_comp){
                        $js .= "fohtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .= "fohtml += '</div>';";
                    $js .= "fohtml += '<div class=\"inputc\">';";
                    $js .= "fohtml += '<input type=\"text\" onclick=\"$(\'#candi_FO_INTITUL_'+index_fo+'_treelist\').show();".$resizeIframeCode."\" id=\"tr_candi_FO_INTITUL_'+index_fo+'\" name=\"tr_candi_FO_INTITUL_'+index_fo+'\" value=\"\"/>';";
                    $js .= "fohtml += '<input type=\"hidden\" id=\"candi_FO_INTITUL_'+index_fo+'\" name=\"candi_FO_INTITUL_'+index_fo+'\" value=\"$$$$\"/>';";
                    $js .= "fohtml += '</div>';";
                    $js .= "fohtml += '<div class=\"spacer\"></div>';";
                    $js .= "fohtml += '</div>';";

                    $js .= "fohtml += '<div id=\"candi_FO_INTITUL_'+index_fo+'_treelist\" style=\"display:none;padding-left:270px;\">';";
                    $js .= "fohtml += '<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$(\'#candi_FO_INTITUL_'+index_fo+'_treelist\').hide();".$resizeIframeCode."\">x</span></div>';";
                    $js .= "fohtml += '<div id=\"candi_FO_INTITUL_'+index_fo+'_treelistdiv\"></div>';";


                    $js .= "";

                    $js .= "fohtml += '</div>';";
                }else{
                    //MODE NORMAL
                    $js .= "fohtml += '<div class=\"candidature_formrow\" >';";
                    $js .=  "fohtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EDUCATION."';";
                    if($is_intitul_comp){
                        $js .= "fohtml +=  ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .=  "fohtml += '</div>';";
                    $js .=  "fohtml += '<div class=\"inputc\">';";
                    $js .=   "fohtml += '<input type=\"text\" id=\"candi_FO_INTITUL_'+index_fo+'\" name=\"candi_FO_INTITUL_'+index_fo+'\" value=\"\"/>';";
                    $js .=  "fohtml += '</div>';";
                    $js .=  "fohtml += '<div class=\"spacer\"></div>';";
                    $js .= "fohtml += '</div>';";
                }
                
            }

            if($is_etabl){
                if(!empty($etabl_treelist)){
                    //MODE TREELIST
                    $js .= "fohtml += '<div class=\"candidature_formrow\" >';";
                    $js .= "fohtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EDU_SCHOOL."';";
                    if($is_etabl_comp){
                        $js .= "fohtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .= "fohtml += '</div>';";
                    $js .= "fohtml += '<div class=\"inputc\">';";
                    $js .= "fohtml += '<input type=\"text\" onclick=\"$(\'#candi_FO_ETABL_'+index_fo+'_treelist\').show();".$resizeIframeCode."\" id=\"tr_candi_FO_ETABL_'+index_fo+'\" name=\"tr_candi_FO_ETABL_'+index_fo+'\" value=\"\"/>';";
                    $js .= "fohtml += '<input type=\"hidden\" id=\"candi_FO_ETABL_'+index_fo+'\" name=\"candi_FO_ETABL_'+index_fo+'\" value=\"$$$$\"/>';";
                    $js .= "fohtml += '</div>';";
                    $js .= "fohtml += '<div class=\"spacer\"></div>';";
                    $js .= "fohtml += '</div>';";

                    $js .= "fohtml += '<div id=\"candi_FO_ETABL_'+index_fo+'_treelist\" style=\"display:none;padding-left:270px;\">';";
                    $js .= "fohtml += '<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$(\'#candi_FO_ETABL_'+index_fo+'_treelist\').hide();".$resizeIframeCode."\">x</span></div>';";
                    $js .= "fohtml += '<div id=\"candi_FO_ETABL_'+index_fo+'_treelistdiv\"></div>';";


                    $js .= "";

                    $js .= "fohtml += '</div>';";
                }else{
                    $js .= "fohtml += '<div class=\"candidature_formrow\" >';";
                    $js .=  "fohtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EDU_SCHOOL."';";
                    if($is_etabl_comp){
                        $js .= "fohtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .=  "fohtml += '</div>';";
                    $js .=  "fohtml += '<div class=\"inputc\">';";
                    $js .=  "fohtml += '<input type=\"text\" id=\"candi_FO_ETABL_'+index_fo+'\" name=\"candi_FO_ETABL_'+index_fo+'\" value=\"\"/>';";
                    $js .=  "fohtml += '</div>';";
                    $js .=  "fohtml += '<div class=\"spacer\"></div>';";
                    $js .= "fohtml += '</div>';";
                }
                
            }

            if($is_dates){
                $js .= "fohtml += '<div class=\"candidature_formrow\" >';";
                $js .=  "fohtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EDU_START."';";
                if($is_dates_comp){
                    $js .= "fohtml += ' <span class=\"compulsorystar\">*</span>';";
                }
                $js .=  "fohtml += '</div>';";
//                $js .=  "fohtml += '<div class=\"inputc\">';";
//                $js .=   "fohtml += '<input type=\"text\" id=\"candi_FO_FORMA_START_'+index_fo+'\" name=\"candi_FO_FORMA_START_'+index_fo+'\" value=\"\" style=\"width:102px;\"/>';";
//                $js .=  "fohtml += '</div>';";
//                $js .=  "fohtml += '<div class=\"labelc\" style=\"width: 40px; text-align: center;\"> "._CANDIDATURE_EX_EDU_END." </div>';";
                $js .=  "fohtml += '<div class=\"inputc\">';";
                $js .=   "fohtml += '<input type=\"text\" id=\"candi_FO_FORMA_END_'+index_fo+'\" name=\"candi_FO_FORMA_END_'+index_fo+'\" value=\"\" style=\"\"/>';";
                $js .=  "fohtml += '</div>';";
                $js .=  "fohtml += '<div class=\"spacer\"></div>';";
                $js .= "fohtml += '</div>';";
            }

            if($is_niveau){
                if(!empty($niveau_treelist)){
                    //MODE TREELIST
                    $js .= "fohtml += '<div class=\"candidature_formrow\" >';";
                    $js .= "fohtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EDU_LEVEL."';";
                    if($is_niveau_comp){
                        $js .= "fohtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .= "fohtml += '</div>';";
                    $js .= "fohtml += '<div class=\"inputc\">';";
                    $js .= "fohtml += '<input type=\"text\" onclick=\"$(\'#candi_FO_NIVEAU_'+index_fo+'_treelist\').show();".$resizeIframeCode."\" id=\"tr_candi_FO_NIVEAU_'+index_fo+'\" name=\"tr_candi_FO_NIVEAU_'+index_fo+'\" value=\"\"/>';";
                    $js .= "fohtml += '<input type=\"hidden\" id=\"candi_FO_NIVEAU_'+index_fo+'\" name=\"candi_FO_NIVEAU_'+index_fo+'\" value=\"$$$$\"/>';";
                    $js .= "fohtml += '</div>';";
                    $js .= "fohtml += '<div class=\"spacer\"></div>';";
                    $js .= "fohtml += '</div>';";

                    $js .= "fohtml += '<div id=\"candi_FO_NIVEAU_'+index_fo+'_treelist\" style=\"display:none;padding-left:270px;\">';";
                    $js .= "fohtml += '<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$(\'#candi_FO_NIVEAU_'+index_fo+'_treelist\').hide();".$resizeIframeCode."\">x</span></div>';";
                    $js .= "fohtml += '<div id=\"candi_FO_NIVEAU_'+index_fo+'_treelistdiv\"></div>';";


                    $js .= "";

                    $js .= "fohtml += '</div>';";
                }else{
                    //MODE NORMAL
                    $js .= "fohtml += '<div class=\"candidature_formrow\" >';";
                    $js .=  "fohtml += '<div class=\"labelc\">"._CANDIDATURE_EX_EDU_LEVEL."';";
                    if($is_niveau_comp){
                        $js .= "fohtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .=  "fohtml += '</div>';";
                    $js .=  "fohtml += '<div class=\"inputc\">';";
                    $js .=   "fohtml += '<input type=\"text\" id=\"candi_FO_NIVEAU_'+index_fo+'\" name=\"candi_FO_NIVEAU_'+index_fo+'\" value=\"\"/>';";
                    $js .=  "fohtml += '</div>';";
                    $js .=  "fohtml += '<div class=\"spacer\"></div>';";
                    $js .= "fohtml += '</div>';";
                }
                
            }
            if($is_add){
                $js .=  "fohtml += '<div class=\"formation_bloc_del\" style=\"\"><span onclick=\"delFo('+index_fo+');\">"._CANDIDATURE_EX_FORMATIONS_DEL."</span></div>';";
            }

            $js .=  "fohtml += '</div>';";

            $js .= "$('#formation_bloc_dyna').prepend(fohtml);";
            $js .= "$('#candi_FO_FORMA_LASTINDEX').val(index_fo);";



            if($is_intitul && !empty($intitul_treelist)){
                $intitultreelistmulti = "";
                if($is_intitul_treelist_multi){
                    $intitultreelistmulti = "&treelistismulti=1";
                }
                $js .= "$.get('treelistajax.php?treelistname=".$intitul_treelist."".$intitultreelistmulti."&treelistidhtml=treel_fo_intitul_'+index_fo+'&treelistidinputdisplay=tr_candi_FO_INTITUL_'+index_fo+'&treelistidinputhidden=candi_FO_INTITUL_'+index_fo, function(datartr){ $('#candi_FO_INTITUL_'+index_fo+'_treelistdiv').html(datartr);} );";
            }

            if($is_etabl && !empty($etabl_treelist)){
                $etabltreelistmulti = "";
                if($is_etabl_treelist_multi){
                    $etabltreelistmulti = "&treelistismulti=1";
                }
                $js .= "$.get('treelistajax.php?treelistname=".$etabl_treelist."".$etabltreelistmulti."&treelistidhtml=treel_fo_etabl_'+index_fo+'&treelistidinputdisplay=tr_candi_FO_ETABL_'+index_fo+'&treelistidinputhidden=candi_FO_ETABL_'+index_fo, function(datartr){ $('#candi_FO_ETABL_'+index_fo+'_treelistdiv').html(datartr);} );";
            }

            if($is_niveau && !empty($niveau_treelist)){
                $niveautreelistmulti = "";
                if($is_niveau_treelist_multi){
                    $niveautreelistmulti = "&treelistismulti=1";
                }
                $js .= "$.get('treelistajax.php?treelistname=".$niveau_treelist."".$niveautreelistmulti."&treelistidhtml=treel_fo_niveau_'+index_fo+'&treelistidinputdisplay=tr_candi_FO_NIVEAU_'+index_fo+'&treelistidinputhidden=candi_FO_NIVEAU_'+index_fo, function(datartr){ $('#candi_FO_NIVEAU_'+index_fo+'_treelistdiv').html(datartr);} );";
            }

            if(_IS_IFRAME_RESIZE){
                $js .= " resizeiframe();";
            } 



            $js .= "}";

            $js .= "function delFo(index){";
            $js .= "$('#formation_bloc_'+index).remove();";
            if(_IS_IFRAME_RESIZE){
                $js .= " resizeiframe();";
            } 
            $js .= "}";
         }

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);

    }

    /*
     * Champ langue
     */
    if($ref == "EX_LANGUES_BLOC"){
        $is_title = false;
        $is_add = false;
        $is_langue = false;
        $is_langue_comp = false;
        $langue_treelist = "";
        $is_langue_treelist_multi = false;
        $is_niveau = false;
        $is_niveau_comp = false;
        $niveau_treelist = "";
        $is_niveau_treelist_multi = false;
        $nb_default = 1;


        $lg_options = unserialize($options);
        foreach($lg_options as $lg_option){
            if($lg_option == "title"){
                $is_title = true;
            }
            if($lg_option == "adddel"){
                $is_add = true;
            }
            if($lg_option == "langue"){
                $is_langue = true;
            }
            if($lg_option == "langue_comp"){
                $is_langue_comp = true;
            }
            if(preg_match('#langue_treelist=(.+)#', $lg_option, $match)){
                $langue_treelist = $match[1];
            }
            if($lg_option == "langue_treelist_multi"){
                $is_langue_treelist_multi = true;
            }
            if($lg_option == "niveau"){
                $is_niveau = true;
            }
            if($lg_option == "niveau_comp"){
                $is_niveau_comp = true;
            }
            if(preg_match('#niveau_treelist=(.+)#', $lg_option, $match)){
                $niveau_treelist = $match[1];
            }
            if($lg_option == "niveau_treelist_multi"){
                $is_niveau_treelist_multi = true;
            }
            if(preg_match('#nb=([0-9])#', $lg_option, $match)){
                $nb_default = $match[1];
            }
        }

        //si il y a des langues on ne prend pas en compte le nombre d'element pas default.
        if(count($formulaireInputs['langues'])){
            $nb_default = count($formulaireInputs['langues']);
        }elseif(isset($cvreaderdata['langues']['langue'])){

            if(isset($cvreaderdata['langues']['langue'][0])){
                $tempLg = $cvreaderdata['langues']['langue'];
            }else{
                $tempLg[0] = $cvreaderdata['langues']['langue'];
            }
            $cvrlgid = 0;
            foreach($tempLg as $key=>$lg){

                $cvr_lg[$cvrlgid]['langue'] = utf8_decode($lg['nom']);
                $cvr_lg[$cvrlgid]['niveau'] = utf8_decode($lg['niveau']);


                $cvrlgid++;

            }

            $nb_default = count($cvr_lg);

        }
        

        $id = 0;
        if($is_title){
            $html .= "<div id=\"langue_bloc_top\" >";
            $html .=  _CANDIDATURE_EX_LANGUES_BLOC;
            $html .= "</div>";
        }
        if($is_add){
            $html .= "<div id=\"langue_bloc_add\" style=\"\">";
            $html .=  "<span onclick=\"addLgJS();\">"._CANDIDATURE_EX_LANGUES_ADD."</span>";
            $html .= "</div>";
        }

        $html .= "<div id=\"langue_bloc_dyna\" >";

        $lastLgIndex = 0;
        for($id = 0; $id < $nb_default; $id++){
            $html .= "<div id=\"langue_bloc_".$id."\" >";

            if($is_langue){
                $data = "";
                if(!empty($formulaireInputs['langues'][$id])){
                    $data = $formulaireInputs['langues'][$id]->LANGUE_LIB;
                }elseif(!empty($cvr_lg[$id])){
                    $data = $cvr_lg[$id]['langue'];
                }

                if(!empty($langue_treelist)){
                    //MODE TREELIST
                    $treelist = new Treelist($langue_treelist);
                    $data = str_replace("$$$$", "", $data);
                    $datatr = "";
                    $datatr_temps = explode("|",$data);
                    foreach($datatr_temps as $datatr_temp){
                        if(!empty($datatr_temp)){
                            $sql = "SELECT
                                        *
                                    FROM
                                        treelist
                                    WHERE
                                        T_ID_MOTCLE =:T_ID_MOTCLE
                                ";
                            $select = $conn->prepare($sql);
                            $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                            $select->execute();
                            $result = $select->fetchObject();
                            if($result){
                                if(!empty($result->VALEUR52)){
                                    $datatr .= $result->VALEUR52." / ";
                                }elseif(!empty($result->VALEUR42)){
                                    $datatr .= $result->VALEUR42." / ";
                                }elseif(!empty($result->VALEUR32)){
                                    $datatr .= $result->VALEUR32." / ";
                                }elseif(!empty($result->VALEUR22)){
                                    $datatr .= $result->VALEUR22." / ";
                                }
                            }
                        }
                    }
                    $treelist->setConn($conn);
                    $treelist->selectedIDs = $datatr_temps;
                    $treelist->isMultiple = $is_langue_treelist_multi;
                    $treelist->idhtml = "treel_lg_langue_".$id;
                    $treelist->idInputDisplay = "tr_candi_LG_LANGUE_".$id;
                    $treelist->idInputHidden = "candi_LG_LANGUE_".$id;
                    $treelist->buildChilds();

                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_LANG."";
                    if($is_langue_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" onclick=\"$('#candi_LG_LANGUE_".$id."_treelist').show();".$resizeIframeCode."\" id=\"tr_candi_LG_LANGUE_".$id."\" name=\"tr_candi_LG_LANGUE_".$id."\" value=\"".cleanInput($datatr)."\"/>";
                    $html .=   "<input type=\"hidden\" id=\"candi_LG_LANGUE_".$id."\" name=\"candi_LG_LANGUE_".$id."\" value=\"$$$$".$data."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                    $html .= "<div id=\"candi_LG_LANGUE_".$id."_treelist\" style=\"display:none;padding-left:270px;\">";
                    $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_LG_LANGUE_".$id."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
                    $html .= $treelist->renderHTML();
                    $html .= "</div>";
                }else{
                    //MODE NORMAL
                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_LANG;
                    if($is_langue_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" id=\"candi_LG_LANGUE_".$id."\" name=\"candi_LG_LANGUE_".$id."\" value=\"".cleanInput($data)."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                }
                
            }

            if($is_niveau){
                $data = "";
                if(!empty($formulaireInputs['langues'][$id])){
                    $data = $formulaireInputs['langues'][$id]->LEVEL_LIB;
                }elseif(!empty($cvr_lg[$id])){
                    $data = $cvr_lg[$id]['niveau'];
                }
                if(!empty($niveau_treelist)){
                    //MODE TREELIST
                    $treelist = new Treelist($niveau_treelist);
                    $data = str_replace("$$$$", "", $data);
                    $datatr = "";
                    $datatr_temps = explode("|",$data);
                    foreach($datatr_temps as $datatr_temp){
                        if(!empty($datatr_temp)){
                            $sql = "SELECT
                                        *
                                    FROM
                                        treelist
                                    WHERE
                                        T_ID_MOTCLE =:T_ID_MOTCLE
                                ";
                            $select = $conn->prepare($sql);
                            $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                            $select->execute();
                            $result = $select->fetchObject();
                            if($result){
                                if(!empty($result->VALEUR52)){
                                    $datatr .= $result->VALEUR52." / ";
                                }elseif(!empty($result->VALEUR42)){
                                    $datatr .= $result->VALEUR42." / ";
                                }elseif(!empty($result->VALEUR32)){
                                    $datatr .= $result->VALEUR32." / ";
                                }elseif(!empty($result->VALEUR22)){
                                    $datatr .= $result->VALEUR22." / ";
                                }
                            }
                        }
                    }
                    $treelist->setConn($conn);
                    $treelist->selectedIDs = $datatr_temps;
                    $treelist->isMultiple = $is_niveau_treelist_multi;
                    $treelist->idhtml = "treel_lg_niveau_".$id;
                    $treelist->idInputDisplay = "tr_candi_LG_NIVEAU_".$id;
                    $treelist->idInputHidden = "candi_LG_NIVEAU_".$id;
                    $treelist->buildChilds();

                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_LANG_LEVEL."";
                    if($is_niveau_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" onclick=\"$('#candi_LG_NIVEAU_".$id."_treelist').show();\" id=\"tr_candi_LG_NIVEAU_".$id."\" name=\"tr_candi_LG_NIVEAU_".$id."\" value=\"".cleanInput($datatr)."\"/>";
                    $html .=   "<input type=\"hidden\" id=\"candi_LG_NIVEAU_".$id."\" name=\"candi_LG_NIVEAU_".$id."\" value=\"$$$$".$data."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                    $html .= "<div id=\"candi_LG_NIVEAU_".$id."_treelist\" style=\"display:none;padding-left:270px;\">";
                    $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_LG_NIVEAU_".$id."_treelist').hide();\">x</span></div>";
                    $html .= $treelist->renderHTML();
                    $html .= "</div>";
                }else{
                    //MODE NORMAL
                    $html .= "<div class=\"candidature_formrow\" >";
                    $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_LANG_LEVEL;
                    if($is_niveau_comp){
                        $html .= " <span class=\"compulsorystar\">*</span>";
                    }
                    $html .=  "</div>";
                    $html .=  "<div class=\"inputc\">";
                    $html .=   "<input type=\"text\" id=\"candi_LG_NIVEAU_".$id."\" name=\"candi_LG_NIVEAU_".$id."\" value=\"".cleanInput($data)."\"/>";
                    $html .=  "</div>";
                    $html .=  "<div class=\"spacer\"></div>";
                    $html .= "</div>";
                }
                
            }

            if($is_add){
                $html .=  "<div class=\"langue_bloc_del\" style=\"\"><span onclick=\"delLg(".$id.");\">"._CANDIDATURE_EX_LANGUES_DEL."</span></div>";
            }



            $html .= "</div>";
            $lastLgIndex = $id;
        }
        $html .= "</div>";
        $html .= "<input type=\"hidden\" id=\"candi_LG_LASTINDEX\" name=\"candi_LG_LASTINDEX\" value=\"".$lastLgIndex."\" />";

        if(true){
            
            $checkjs .= "var lastLangueIndex = $('#candi_LG_LASTINDEX').val();";
            $checkjs .= "for(i=0;i<=lastLangueIndex;i++){";

            if($is_langue && $is_langue_comp){
                if(!empty($langue_treelist)){
                    //MODE TREELIST 
                    $checkjs .= "if($('#candi_LG_LANGUE_'+i).val()!= undefined){";
                    $checkjs .= "   var exp=new RegExp(\"[|]+\",\"g\");";
                    $checkjs .= "   if(!exp.test($('#candi_LG_LANGUE_'+i).val())){";
                    $checkjs .= "       alert('"._CANDIDATURE_EX_LANG_TR_JSERROR."');";
                    $checkjs .= "       $('#tr_candi_LG_LANGUE_'+i).focus();";
                    $checkjs .= "       return false;";
                    $checkjs .= "   }";
                    $checkjs .= "}";
                }else{
                    $checkjs .= "if($('#candi_LG_LANGUE_'+i).val()!= undefined){";
                    $checkjs .=  "if($('#candi_LG_LANGUE_'+i).val() == ''){";
                    $checkjs .=   "alert('"._CANDIDATURE_EX_LANG_JSERROR."');";
                    $checkjs .=   "$('#candi_LG_LANGUE_'+i).focus();";
                    $checkjs .=   "return false;";
                    $checkjs .=  "}";
                    $checkjs .= "}";
                }
                
            }

            if($is_niveau && $is_niveau_comp){
                 if(!empty($niveau_treelist)){
                    //MODE TREELIST
                    $checkjs .= "if($('#candi_LG_NIVEAU_'+i).val()!= undefined){";
                    $checkjs .= "   var exp=new RegExp(\"[|]+\",\"g\");";
                    $checkjs .= "   if(!exp.test($('#candi_LG_NIVEAU_'+i).val())){";
                    $checkjs .= "       alert('"._CANDIDATURE_EX_LANG_LEVEL_TR_JSERROR."');";
                    $checkjs .= "       $('#tr_candi_LG_NIVEAU_'+i).focus();";
                    $checkjs .= "       return false;";
                    $checkjs .= "   }";
                    $checkjs .= "}";
                }else{
                    //MODE NORMAL
                    $checkjs .= "if($('#candi_LG_NIVEAU_'+i).val()!= undefined){";
                    $checkjs .=  "if($('#candi_LG_NIVEAU_'+i).val() == ''){";
                    $checkjs .=   "alert('"._CANDIDATURE_EX_LANG_LEVEL_JSERROR."');";
                    $checkjs .=   "$('#candi_LG_NIVEAU_'+i).focus();";
                    $checkjs .=   "return false;";
                    $checkjs .=  "}";
                    $checkjs .= "}";
                }
            }

            $checkjs .= "}";

        }



        if($is_add){
            $js .= "function addLgJS(){";
            $js .=  "var index_lg = $('#candi_LG_LASTINDEX').val();";
            $js .=  "var lghtml = \"\";";
            $js .=  "index_lg++;";

            $js .=  "lghtml += '<div id=\"langue_bloc_'+index_lg+'\" >';";
            if($is_langue){
                if(!empty($langue_treelist)){
                    //MODE TREELIST
                    $js .= "lghtml += '<div class=\"candidature_formrow\" >';";
                    $js .= "lghtml += '<div class=\"labelc\">"._CANDIDATURE_EX_LANG."';";
                    if($is_langue_comp){
                        $js .= "lghtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .= "lghtml += '</div>';";
                    $js .= "lghtml += '<div class=\"inputc\">';";
                    $js .= "lghtml += '<input type=\"text\" onclick=\"$(\'#candi_LG_LANGUE_'+index_lg+'_treelist\').show();".$resizeIframeCode."\" id=\"tr_candi_LG_LANGUE_'+index_lg+'\" name=\"tr_candi_LG_LANGUE_'+index_lg+'\" value=\"\"/>';";
                    $js .= "lghtml += '<input type=\"hidden\" id=\"candi_LG_LANGUE_'+index_lg+'\" name=\"candi_LG_LANGUE_'+index_lg+'\" value=\"$$$$\"/>';";
                    $js .= "lghtml += '</div>';";
                    $js .= "lghtml += '<div class=\"spacer\"></div>';";
                    $js .= "lghtml += '</div>';";

                    $js .= "lghtml += '<div id=\"candi_LG_LANGUE_'+index_lg+'_treelist\" style=\"display:none;padding-left:270px;\">';";
                    $js .= "lghtml += '<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$(\'#candi_LG_LANGUE_'+index_lg+'_treelist\').hide();".$resizeIframeCode."\">x</span></div>';";
                    $js .= "lghtml += '<div id=\"candi_LG_LANGUE_'+index_lg+'_treelistdiv\"></div>';";


                    $js .= "";

                    $js .= "lghtml += '</div>';";
                }else{
                    $js .= "lghtml += '<div class=\"candidature_formrow\" >';";
                    $js .=  "lghtml += '<div class=\"labelc\">"._CANDIDATURE_EX_LANG."';";
                    if($is_langue_comp){
                        $js .= " lghtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .=  "lghtml += '</div>';";
                    $js .=  "lghtml += '<div class=\"inputc\">';";
                    $js .=   "lghtml += '<input type=\"text\" id=\"candi_LG_LANGUE_'+index_lg+'\" name=\"candi_LG_LANGUE_'+index_lg+'\" value=\"\"/>';";
                    $js .=  "lghtml += '</div>';";
                    $js .=  "lghtml += '<div class=\"spacer\"></div>';";
                    $js .= "lghtml += '</div>';";
                }
                
            }

            if($is_niveau){
                if(!empty($niveau_treelist)){
                    //MODE TREELIST
                    $js .= "lghtml += '<div class=\"candidature_formrow\" >';";
                    $js .= "lghtml += '<div class=\"labelc\">"._CANDIDATURE_EX_LANG_LEVEL."';";
                    if($is_niveau_comp){
                        $js .= "lghtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .= "lghtml += '</div>';";
                    $js .= "lghtml += '<div class=\"inputc\">';";
                    $js .= "lghtml += '<input type=\"text\" onclick=\"$(\'#candi_LG_NIVEAU_'+index_lg+'_treelist\').show();".$resizeIframeCode."\" id=\"tr_candi_LG_NIVEAU_'+index_lg+'\" name=\"tr_candi_LG_NIVEAU_'+index_lg+'\" value=\"\"/>';";
                    $js .= "lghtml += '<input type=\"hidden\" id=\"candi_LG_NIVEAU_'+index_lg+'\" name=\"candi_LG_NIVEAU_'+index_lg+'\" value=\"$$$$\"/>';";
                    $js .= "lghtml += '</div>';";
                    $js .= "lghtml += '<div class=\"spacer\"></div>';";
                    $js .= "lghtml += '</div>';";

                    $js .= "lghtml += '<div id=\"candi_LG_NIVEAU_'+index_lg+'_treelist\" style=\"display:none;padding-left:270px;\">';";
                    $js .= "lghtml += '<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$(\'#candi_LG_NIVEAU_'+index_lg+'_treelist\').hide();".$resizeIframeCode."\">x</span></div>';";
                    $js .= "lghtml += '<div id=\"candi_LG_NIVEAU_'+index_lg+'_treelistdiv\"></div>';";


                    $js .= "";

                    $js .= "lghtml += '</div>';";
                }else{
                    //MODE NORMAL
                    $js .= "lghtml += '<div class=\"candidature_formrow\" >';";
                    $js .=  "lghtml += '<div class=\"labelc\">"._CANDIDATURE_EX_LANG_LEVEL."';";
                    if($is_niveau_comp){
                        $js .= "lghtml += ' <span class=\"compulsorystar\">*</span>';";
                    }
                    $js .=  "lghtml += '</div>';";
                    $js .=  "lghtml += '<div class=\"inputc\">';";
                    $js .=   "lghtml += '<input type=\"text\" id=\"candi_LG_NIVEAU_'+index_lg+'\" name=\"candi_LG_NIVEAU_'+index_lg+'\" value=\"\"/>';";
                    $js .=  "lghtml += '</div>';";
                    $js .=  "lghtml += '<div class=\"spacer\"></div>';";
                    $js .= "lghtml += '</div>';";
                }
                
            }


            if($is_add){
                $js .=  "lghtml += '<div class=\"formation_bloc_del\" style=\"\"><span onclick=\"delLg('+index_lg+');\">"._CANDIDATURE_EX_LANGUES_DEL."</span></div>';";
            }

            $js .= "lghtml += '</div>';";
            $js .= "$('#langue_bloc_dyna').prepend(lghtml);";
            $js .= "$('#candi_LG_LASTINDEX').val(index_lg);";
            if($is_langue && !empty($langue_treelist)){
                $languetreelistmulti = "";
                if($is_langue_treelist_multi){
                    $languetreelistmulti = "&treelistismulti=1";
                }
                $js .= "$.get('treelistajax.php?treelistname=".$langue_treelist."".$languetreelistmulti."&treelistidhtml=treel_lg_langue_'+index_lg+'&treelistidinputdisplay=tr_candi_LG_LANGUE_'+index_lg+'&treelistidinputhidden=candi_LG_LANGUE_'+index_lg, function(datartr){ $('#candi_LG_LANGUE_'+index_lg+'_treelistdiv').html(datartr);} );";
            }
            if($is_niveau && !empty($niveau_treelist)){
                $niveautreelistmulti = "";
                if($is_niveau_treelist_multi){
                    $niveautreelistmulti = "&treelistismulti=1";
                }
                $js .= "$.get('treelistajax.php?treelistname=".$niveau_treelist."".$niveautreelistmulti."&treelistidhtml=treel_lg_niveau_'+index_lg+'&treelistidinputdisplay=tr_candi_LG_NIVEAU_'+index_lg+'&treelistidinputhidden=candi_LG_NIVEAU_'+index_lg, function(datartr){ $('#candi_LG_NIVEAU_'+index_lg+'_treelistdiv').html(datartr);} );";
            }

            if(_IS_IFRAME_RESIZE){
                $js .= " resizeiframe();";
            }
            
            $js .=  "}";

            $js .= "function delLg(index){";
            $js .= "$('#langue_bloc_'+index).remove();";
            if(_IS_IFRAME_RESIZE){
                $js .= " resizeiframe();";
            } 
            $js .= "}";

        }

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }




    /*
     * Champ salaire fixe
     */
    if($ref == "SALARY_FIXE"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->SALAIRE_FIXE;
            if($data == "0"){
                $data = "";
            }
        }
        $html .= "<div class=\"candidature_formrow\" >";
        $html .=  "<div class=\"labelc\">"._CANDIDATURE_SALARY_FIXE."";
        if($isCompulsory == "1"){
            $html .= " <span class=\"compulsorystar\">*</span>";
        }
        $html .=  "</div>";
        $html .=  "<div class=\"inputc\">";
        $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
        $html .=  "</div>";
        $html .=  "<div class=\"spacer\"></div>";
        $html .= "</div>";
        $html .= "";

        if($isCompulsory == "1"){
           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
           $checkjs .=  "alert('"._CANDIDATURE_SALARY_FIXE_JSERROR."');";
           $checkjs .=  "$('#candi_".$ref."').focus();";
           $checkjs .=  "return false;";
           $checkjs .= "}";
        }
        //controle integer
        $checkjs .= "if($('#candi_".$ref."').val() != ''){";
        $checkjs .=  "if(checkInteger($('#candi_".$ref."').val())){";
        $checkjs .=   "alert('"._CANDIDATURE_SALARY_FIXE_JSERROR2."');";
        $checkjs .=   "$('#candi_".$ref."').focus();";
        $checkjs .=   "return false;";
        $checkjs .=  "}";
        $checkjs .= "}";

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }

    /*
     * Champ salaire variable
     */
    if($ref == "SALARY_VARIABLE"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->SALAIRE_VARIABLE;
            if($data == "0"){
                $data = "";
            }
        }

        $html .= "<div class=\"candidature_formrow\" >";
        $html .=  "<div class=\"labelc\">"._CANDIDATURE_SALARY_VARIABLE."";
        if($isCompulsory == "1"){
            $html .= " <span class=\"compulsorystar\">*</span>";
        }
        $html .=  "</div>";
        $html .=  "<div class=\"inputc\">";
        $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
        $html .=  "</div>";
        $html .=  "<div class=\"spacer\"></div>";
        $html .= "</div>";
        $html .= "";

        if($isCompulsory == "1"){
           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
           $checkjs .=  "alert('"._CANDIDATURE_SALARY_VARIABLE_JSERROR."');";
           $checkjs .=  "$('#candi_".$ref."').focus();";
           $checkjs .=  "return false;";
           $checkjs .= "}";
        }
        //controle integer
        $checkjs .= "if($('#candi_".$ref."').val() != ''){";
        $checkjs .=  "if(checkInteger($('#candi_".$ref."').val())){";
        $checkjs .=   "alert('"._CANDIDATURE_SALARY_VARIABLE_JSERROR2."');";
        $checkjs .=   "$('#candi_".$ref."').focus();";
        $checkjs .=   "return false;";
        $checkjs .=  "}";
        $checkjs .= "}";

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }

    /*
     * Champ salaire total
     */
    if($ref == "SALARY_TOTAL"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->SALAIRE;
            if($data == "0"){
                $data = "";
            }
        }
        $html .= "<div class=\"candidature_formrow\" >";
        $html .=  "<div class=\"labelc\">"._CANDIDATURE_SALARY_TOTAL."";
        if($isCompulsory == "1"){
            $html .= " <span class=\"compulsorystar\">*</span>";
        }
        $html .=  "</div>";
        $html .=  "<div class=\"inputc\">";
        $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
        $html .=  "</div>";
        $html .=  "<div class=\"spacer\"></div>";
        $html .= "</div>";
        $html .= "";

        if($isCompulsory == "1"){
           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
           $checkjs .=  "alert('"._CANDIDATURE_SALARY_TOTAL_JSERROR."');";
           $checkjs .=  "$('#candi_".$ref."').focus();";
           $checkjs .=  "return false;";
           $checkjs .= "}";
        }
        //controle integer
        $checkjs .= "if($('#candi_".$ref."').val() != ''){";
        $checkjs .=  "if(checkInteger($('#candi_".$ref."').val())){";
        $checkjs .=   "alert('"._CANDIDATURE_SALARY_TOTAL_JSERROR2."');";
        $checkjs .=   "$('#candi_".$ref."').focus();";
        $checkjs .=   "return false;";
        $checkjs .=  "}";
        $checkjs .= "}";

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }

    /*
     * Champ salaire avantage
     */
    if($ref == "SALARY_ADVANTAGE"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->SALAIRE_AVANTAGE;
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_SALARY_ADVANTAGE."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_SALARY_ADVANTAGE_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_SALARY_ADVANTAGE."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_SALARY_ADVANTAGE_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }




        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ sector
     */
    if($ref == "EX_SECTOR"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            if(_ADMEN_USE_ADVERT_LINES){
                $data = $formulaireInputs['personne']->SECTEUR;
            }else{
                $data = $formulaireInputs['personne']->LIBRE1;
            }
            
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_SECTOR."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_SECTOR_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_SECTOR."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_SECTOR_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }






        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }


    /*
     * Champ fonction
     */
    if($ref == "EX_FUNCTION"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->POSTE_OCCUPE;
        }

        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_FUNCTION."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_FUNCTION_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_FUNCTION."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_FUNCTION_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }






        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }













    /*
     * Champ disponibilité
     */
    if($ref == "EX_AVAILABILITY"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->CL1;
        }



        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_AVAILABILITY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_AVAILABILITY_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_AVAILABILITY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"".cleanInput($data)."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_AVAILABILITY_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }

//    /*
//     * Champ permis
//     */
//    if($ref == "EX_DRIVING_LICENCE"){
//        $html .= "<div class=\"candidature_formrow\" >";
//        $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_DRIVING_LICENCE."";
//        if($isCompulsory == "1"){
//            $html .= " <span class=\"compulsorystar\">*</span>";
//        }
//        $html .=  "</div>";
//        $html .=  "<div class=\"inputc\">";
//        $html .=   "<input type=\"text\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"\"/>";
//        $html .=  "</div>";
//        $html .=  "<div class=\"spacer\"></div>";
//        $html .= "</div>";
//        $html .= "";
//
//        if($isCompulsory == "1"){
//           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
//           $checkjs .=  "alert('"._CANDIDATURE_EX_DRIVING_LICENCE_JSERROR."');";
//           $checkjs .=  "$('#candi_".$ref."').focus();";
//           $checkjs .=  "return false;";
//           $checkjs .= "}";
//        }
//
//        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
//    }



    /*
     * Champ divers
     */
    if($ref == "EX_COMMENT"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->COMMENTAIRE;
        }
        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_COMMENT."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_COMMENT_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_COMMENT."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<textarea id=\"candi_".$ref."\" name=\"candi_".$ref."\" cols=\"\" rows=\"\" >".$data."</textarea>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_COMMENT_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }
        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }

    /*
     * Champ mobilité
     */
    if($ref == "EX_MOBILITY"){
        $data = "";
        if(!empty($formulaireInputs['personne'])){
            $data = $formulaireInputs['personne']->MOBILITE_TXT;
        }


        if($treelist){
            //MODE TREELIST
            $data = str_replace("$$$$", "", $data);
            $datatr = "";
            $datatr_temps = explode("|",$data);
            foreach($datatr_temps as $datatr_temp){
                if(!empty($datatr_temp)){
                    $sql = "SELECT
                                *
                            FROM
                                treelist
                            WHERE
                                T_ID_MOTCLE =:T_ID_MOTCLE
                        ";
                    $select = $conn->prepare($sql);
                    $select->bindParam(':T_ID_MOTCLE', $datatr_temp, PDO::PARAM_INT);
                    $select->execute();
                    $result = $select->fetchObject();
                    if($result){
                        if(!empty($result->VALEUR52)){
                            $datatr .= $result->VALEUR52." / ";
                        }elseif(!empty($result->VALEUR42)){
                            $datatr .= $result->VALEUR42." / ";
                        }elseif(!empty($result->VALEUR32)){
                            $datatr .= $result->VALEUR32." / ";
                        }elseif(!empty($result->VALEUR22)){
                            $datatr .= $result->VALEUR22." / ";
                        }
                    }
                }
            }
            $treelist = Treelist::cast($treelist);
            $treelist->setConn($conn);
            $treelist->selectedIDs = $datatr_temps;
            $treelist->isMultiple = $treelist_multi;
            $treelist->idhtml = "treel".$ref;
            $treelist->idInputDisplay = "tr_candi_".$ref;
            $treelist->idInputHidden = "candi_".$ref;
            $treelist->buildChilds();

            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_MOBILITY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input onclick=\"$('#candi_".$ref."_treelist').show();".$resizeIframeCode."\" type=\"text\" id=\"tr_candi_".$ref."\" name=\"tr_candi_".$ref."\" value=\"".cleanInput($datatr)."\"/>";
            $html .=   "<input type=\"hidden\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"$$$$".$data."\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";
            $html .= "<div id=\"candi_".$ref."_treelist\" style=\"display:none;padding-left:270px;\">";
            $html .= "<div style=\"text-align:right;\"><span style=\"cursor:pointer;\" onclick=\"$('#candi_".$ref."_treelist').hide();".$resizeIframeCode."\">x</span></div>";
            $html .= $treelist->renderHTML();
            $html .= "</div>";

            if($isCompulsory == "1"){

                $checkjs .= "var exp=new RegExp(\"[|]+\",\"g\");";
                $checkjs .= "if(!exp.test($('#candi_".$ref."').val())){";
                $checkjs .= "  alert('"._CANDIDATURE_EX_MOBILITY_TR_JSERROR."');";
                $checkjs .= "  $('#tr_candi_".$ref."').focus();";
                $checkjs .= "  return false;";
                $checkjs .= "}";

            }
        }else{
            //MODE NORMAL
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_MOBILITY."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<textarea id=\"candi_".$ref."\" name=\"candi_".$ref."\" cols=\"\" rows=\"\" >".cleanInput($data)."</textarea>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_MOBILITY_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
        }



        

        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
    }

//    /*
//     * Champ objectif
//     */
//    if($ref == "EX_OBJECTIVE"){
//        $html .= "<div class=\"candidature_formrow\" >";
//        $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_OBJECTIVE."";
//        if($isCompulsory == "1"){
//            $html .= " <span class=\"compulsorystar\">*</span>";
//        }
//        $html .=  "</div>";
//        $html .=  "<div class=\"inputc\">";
//        $html .=   "<textarea id=\"candi_".$ref."\" name=\"candi_".$ref."\" cols=\"\" rows=\"\" ></textarea>";
//        $html .=  "</div>";
//        $html .=  "<div class=\"spacer\"></div>";
//        $html .= "</div>";
//        $html .= "";
//
//        if($isCompulsory == "1"){
//           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
//           $checkjs .=  "alert('"._CANDIDATURE_EX_OBJECTIVE_JSERROR."');";
//           $checkjs .=  "$('#candi_".$ref."').focus();";
//           $checkjs .=  "return false;";
//           $checkjs .= "}";
//        }
//
//        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
//    }



//    /*
//     * Champ publication
//     */
//    if($ref == "EX_PUBLICATION"){
//        $html .= "<div class=\"candidature_formrow\" >";
//        $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_PUBLICATION."";
//        if($isCompulsory == "1"){
//            $html .= " <span class=\"compulsorystar\">*</span>";
//        }
//        $html .=  "</div>";
//        $html .=  "<div class=\"inputc\">";
//        $html .=   "<textarea id=\"candi_".$ref."\" name=\"candi_".$ref."\" cols=\"\" rows=\"\" ></textarea>";
//        $html .=  "</div>";
//        $html .=  "<div class=\"spacer\"></div>";
//        $html .= "</div>";
//        $html .= "";
//
//        if($isCompulsory == "1"){
//           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
//           $checkjs .=  "alert('"._CANDIDATURE_EX_PUBLICATION_JSERROR."');";
//           $checkjs .=  "$('#candi_".$ref."').focus();";
//           $checkjs .=  "return false;";
//           $checkjs .= "}";
//        }
//
//        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
//    }

//    /*
//     * Champ reference
//     */
//    if($ref == "EX_REFERENCES"){
//        $html .= "<div class=\"candidature_formrow\" >";
//        $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_REFERENCES."";
//        if($isCompulsory == "1"){
//            $html .= " <span class=\"compulsorystar\">*</span>";
//        }
//        $html .=  "</div>";
//        $html .=  "<div class=\"inputc\">";
//        $html .=   "<textarea id=\"candi_".$ref."\" name=\"candi_".$ref."\" cols=\"\" rows=\"\" ></textarea>";
//        $html .=  "</div>";
//        $html .=  "<div class=\"spacer\"></div>";
//        $html .= "</div>";
//        $html .= "";
//
//        if($isCompulsory == "1"){
//           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
//           $checkjs .=  "alert('"._CANDIDATURE_EX_REFERENCES_JSERROR."');";
//           $checkjs .=  "$('#candi_".$ref."').focus();";
//           $checkjs .=  "return false;";
//           $checkjs .= "}";
//        }
//
//        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
//    }


//    /*
//     * Champ competence
//     */
//    if($ref == "EX_SKILLS"){
//        $html .= "<div class=\"candidature_formrow\" >";
//        $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_SKILLS."";
//        if($isCompulsory == "1"){
//            $html .= " <span class=\"compulsorystar\">*</span>";
//        }
//        $html .=  "</div>";
//        $html .=  "<div class=\"inputc\">";
//        $html .=   "<textarea id=\"candi_".$ref."\" name=\"candi_".$ref."\" cols=\"\" rows=\"\" ></textarea>";
//        $html .=  "</div>";
//        $html .=  "<div class=\"spacer\"></div>";
//        $html .= "</div>";
//        $html .= "";
//
//        if($isCompulsory == "1"){
//           $checkjs .= "if($('#candi_".$ref."').val() == ''){";
//           $checkjs .=  "alert('"._CANDIDATURE_EX_SKILLS_JSERROR."');";
//           $checkjs .=  "$('#candi_".$ref."').focus();";
//           $checkjs .=  "return false;";
//           $checkjs .= "}";
//        }
//
//        return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
//    }


    if($formulaire == "profil"){
        //Maj profil

        /*
         * Champ cv
         */
        if($ref == "EX_CV"){
            $data = "";
            if(count($formulaireInputs['documents'])){
                foreach($formulaireInputs['documents'] as $document){
                    if($document->TITRE_DOC == "CV"){
                        $data = $document->PATHNAME;
                        $data_tmp = explode("/",$data);
                        $data = $data_tmp[count($data_tmp)-1];
                        break;
                    }
                }
            }
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_CV."";
            if($isCompulsory == "1" && $data != ""){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_old_".$ref."\" name=\"candi_old_".$ref."\" value=\"".cleanInput($data)."\" readonly=\"readonly\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";


            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_NEWCV."";
            if($isCompulsory == "1" && $data == ""){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"file\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1" && $data == ""){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_CV_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
            //controle extension
            $checkjs .= "if($('#candi_".$ref."').val() != ''){";
            $checkjs .=  "if(checkFileExtention($('#candi_".$ref."').val())){";
            $checkjs .=   "alert('"._CANDIDATURE_EX_CV_JSERROR2."');";
            $checkjs .=   "$('#candi_".$ref."').focus();";
            $checkjs .=   "return false;";
            $checkjs .=  "}";
            $checkjs .= "}";

            return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
        }

        /*
         * Champ lm
         */
        if($ref == "EX_LM"){
            $data = "";
            if(count($formulaireInputs['documents'])){
                foreach($formulaireInputs['documents'] as $document){
                    if($document->TITRE_DOC == "LM"){
                        $data = $document->PATHNAME;
                        $data_tmp = explode("/",$data);
                        $data = $data_tmp[count($data_tmp)-1];
                        break;
                    }
                }
            }
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_LM."";
            if($isCompulsory == "1" && $data != ""){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_old_".$ref."\" name=\"candi_old_".$ref."\" value=\"".cleanInput($data)."\" readonly=\"readonly\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";

            
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_NEWLM."";
            if($isCompulsory == "1" && $data == ""){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"file\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1" && $data == ""){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_LM_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }

            //controle extension
            $checkjs .= "if($('#candi_".$ref."').val() != ''){";
            $checkjs .=  "if(checkFileExtention($('#candi_".$ref."').val())){";
            $checkjs .=   "alert('"._CANDIDATURE_EX_LM_JSERROR2."');";
            $checkjs .=   "$('#candi_".$ref."').focus();";
            $checkjs .=   "return false;";
            $checkjs .=  "}";
            $checkjs .= "}";

            return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
        }
    }else{
        //Candidature
        
        /*
         * Champ cv
         */
        if($ref == "EX_CV"){

            if(!empty($cvreaderdata['etatCivil'])){
            $data = "";      
                       
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_CV."";
            if($isCompulsory == "1" && $data != ""){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"text\" id=\"candi_old_".$ref."\" name=\"candi_old_".$ref."\" value=\"".cleanInput($_SESSION['cvreaderfileuploadname'])."\" readonly=\"readonly\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";

            }


            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_CV."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"file\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1" && empty($cvreaderdata['etatCivil']) ){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_CV_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
            //controle extension
            $checkjs .= "if($('#candi_".$ref."').val() != ''){";
            $checkjs .=  "if(checkFileExtention($('#candi_".$ref."').val())){";
            $checkjs .=   "alert('"._CANDIDATURE_EX_CV_JSERROR2."');";
            $checkjs .=   "$('#candi_".$ref."').focus();";
            $checkjs .=   "return false;";
            $checkjs .=  "}";
            $checkjs .= "}";
            return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
            
        }
        

        /*
         * Champ lm
         */
        if($ref == "EX_LM"){
            $html .= "<div class=\"candidature_formrow\" >";
            $html .=  "<div class=\"labelc\">"._CANDIDATURE_EX_LM."";
            if($isCompulsory == "1"){
                $html .= " <span class=\"compulsorystar\">*</span>";
            }
            $html .=  "</div>";
            $html .=  "<div class=\"inputc\">";
            $html .=   "<input type=\"file\" id=\"candi_".$ref."\" name=\"candi_".$ref."\" value=\"\"/>";
            $html .=  "</div>";
            $html .=  "<div class=\"spacer\"></div>";
            $html .= "</div>";
            $html .= "";

            if($isCompulsory == "1"){
               $checkjs .= "if($('#candi_".$ref."').val() == ''){";
               $checkjs .=  "alert('"._CANDIDATURE_EX_LM_JSERROR."');";
               $checkjs .=  "$('#candi_".$ref."').focus();";
               $checkjs .=  "return false;";
               $checkjs .= "}";
            }
            //controle extension
            $checkjs .= "if($('#candi_".$ref."').val() != ''){";
            $checkjs .=  "if(checkFileExtention($('#candi_".$ref."').val())){";
            $checkjs .=   "alert('"._CANDIDATURE_EX_LM_JSERROR2."');";
            $checkjs .=   "$('#candi_".$ref."').focus();";
            $checkjs .=   "return false;";
            $checkjs .=  "}";
            $checkjs .= "}";

            return array('HTML'=>$html,'JS'=>$js,'CHECKJS'=>$checkjs);
        }
    }


    
}


/*
 * Génération du chaine de caractères aléatoire
 */
function randomstr($nbelement = 8){
    $characters = array(
        "A","B","C","D","E","F","G","H","J","K","L","M",
        "N","P","Q","R","S","T","U","V","W","X","Y","Z",
        "a","b","c","d","e","f","g","h","j","k","l","m",
        "n","p","q","r","s","t","u","v","w","x","y","z",
        "1","2","3","4","5","6","7","8","9",".",":");


    $random_chars = "";
    for($i=0;$i<=$nbelement;$i++){

        $x = mt_rand(0, count($characters)-1);
        $random_chars .= $characters[$x];

    }

    return $random_chars;

}

/*
 * Nétoyage des noms de fichier
 */
function cleanFileName($filename){
    $filename = strtolower($filename);
    $filename = str_replace(" ", "-", $filename);
    $filename = str_replace("/", "-", $filename);
    $filename = str_replace("é", "e", $filename);
    $filename = str_replace("è", "e", $filename);
    $filename = str_replace("ê", "e", $filename);
    $filename = str_replace("à", "a", $filename);
    $filename = str_replace("â", "a", $filename);
    $filename = str_replace("ç", "c", $filename);
    $filename = str_replace("ù", "u", $filename);
    $filename = str_replace(",", "-", $filename);
    $filename = str_replace("?", "-", $filename);
    $filename = str_replace("!", "-", $filename);
    $filename = str_replace(";", "-", $filename);
    $filename = str_replace(":", "-", $filename);
    $filename = str_replace("%", "-", $filename);


    return $filename;
}


/*
 * Controle de l'extension des fichiers
 */
function checkExtension($extension){
    if($extension == "pdf" || $extension == "PDF"){
        return true;
    }
    if($extension == "doc" || $extension == "DOC"){
        return true;
    }
    if($extension == "docx" || $extension == "DOCX"){
        return true;
    }
    if($extension == "rtf" || $extension == "RTF"){
        return true;
    }
    if($extension == "ppt" || $extension == "PPT"){
        return true;
    }
    return false;
}


/*
 * Supprime les doubles quotes pour eviter des problèmes dans les text inputs.
 */
function cleanInput($input){
    $input = str_replace('"', '', $input);

    return $input;
}


/*
 * Génération des variables de sessions pour la sécurité anti-spam
 */
function formulaire_secu_gen(){
    $_SESSION['secu_nb_1'] = 0;
    $_SESSION['secu_nb_2'] = 0;
   
    $_SESSION['secu_total'] = 0;

    $_SESSION['secu_nb_1'] = mt_rand(0, 12);
    $_SESSION['secu_nb_2'] = mt_rand(0, 12);
   
    $_SESSION['secu_total'] = $_SESSION['secu_nb_1'] + $_SESSION['secu_nb_2'];
    
}


function simplexml2array($xml) {
    if (get_class($xml) == 'SimpleXMLElement') {
       $attributes = $xml->attributes();
       foreach($attributes as $k=>$v) {
           if ($v) $a[$k] = (string) $v;
       }
       $x = $xml;
       $xml = get_object_vars($xml);
    }
    if (is_array($xml)) {
       if (count($xml) == 0) return (string) $x; // for CDATA
       foreach($xml as $key=>$value) {
           $r[$key] = simplexml2array($value);
       }
       if (isset($a)) $r['@'] = $a;    // Attributes
       return $r;
    }
    return (string) $xml;
}

?>