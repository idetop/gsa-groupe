//Créé

function selectAllCbx(action,id,size)
{
    for(i=0;i<size;i++)
    {
	var id_cbx=id+'_'+i;
	$('#'+id_cbx).attr('checked', action);
    }
}

function checkEmail(str) {
    var at="@"
    var dot="."
    var lat=str.indexOf(at)
    var lstr=str.length
    if (str.indexOf(at)==-1){
        alert("E-mail invalide")
        return false
    }
    if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
        alert("E-mail invalide");
        return false
    }

    if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
        alert("E-mail invalide");
        return false
    }

    if (str.indexOf(at,(lat+1))!=-1){
        alert("E-mail invalide");
        return false
    }

    if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
        alert("E-mail invalide");
        return false
    }

    if (str.indexOf(dot,(lat+2))==-1){
        alert("E-mail invalide");
        return false
    }

    if (str.indexOf(" ")!=-1){
        alert("E-mail invalide");
        return false
    }
    return true
}

function checkDate(s_date) {
	var re_date = /^\s*(\d{1,2})\-(\d{1,2})\-(\d{2,4})\s*$/;
	if (!re_date.exec(s_date)){            
            alert (alert_invalidDate + " '"+ s_date + "'.\n"+alert_acceptedFormat);
            return false;
        }
		
	var n_day = Number(RegExp.$1),n_month = Number(RegExp.$2),n_year = Number(RegExp.$3);

	if (n_year < 100)
		n_year += (n_year < this.a_tpl.centyear ? 2000 : 1900);
	if (n_month < 1 || n_month > 12){
            alert (alert_invalidMonth + " '" + n_month + "'.\n" + alert_allowedMonthRange);
            return false;
        }	
            
	var d_numdays = new Date(n_year, n_month, 0);
	if (n_day > d_numdays.getDate()){
            alert(alert_invalidDay + " '" + n_day + "'.\n" + alert_allowedDayRange + d_numdays.getDate() + ".");
            return false;
        }		 

	return true;
}

function getRandomNum(lbound, ubound) {
    return (Math.floor(Math.random() * (ubound - lbound)) + lbound);
}

function getRandomChar(){
    var numberChars = "0123456789";
    var lowerChars = "abcdefghijklmnopqrstuvwxyz";
    var upperChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var otherChars = "!@?";
    charSet =  lowerChars + numberChars + upperChars + otherChars;

    return charSet.charAt(getRandomNum(0, charSet.length));
}

function getRandomString(){
    rndstr = "";
    for (var idx = 1; idx <= 8; ++idx) {
        rndstr = rndstr + getRandomChar();
    }
    return rndstr;
}

function add_div_ajax(hidden,div,file) {
	
	//alert(hidden)
	var temp = $('#'+hidden).val();
	temp++;
	$('#'+hidden).val(temp);

	$.post(file, {'temp' : temp}, function(data){
		$('#'+div).append(data);
	});
}

function supp_div(id_div)
{
   $('#'+id_div).slideToggle('fast', function(){
	    $('#'+id_div).remove();
    });
}

function ville_ajax(lst_pays,txt_cp,name_div_ville,name_ville,class_texte,class_liste)
{
    var pays_id=$('#'+lst_pays).val();
    var div_ville=$('#'+name_div_ville);
    var cp=$('#'+txt_cp).val();

    $.post('act-offres.php?action=ajaxville', {'pays_id' : pays_id, 'cp' : cp, 'name_ville' : name_ville, 'class_texte' : class_texte, 'class_liste' : class_liste}, function(data){
	div_ville.html(data);
	$('#'+name_ville).focus();
    });

}






