<?php
// CONNEXION MYSQL
include('mysql/mysql.php');

#####################################################
# Envoi formulaire
#####################################################
if(isset($_REQUEST['submit_envoyer'])){
	$nom 	= addslashes($_REQUEST['nom']);
	$adr 	= addslashes($_REQUEST['adresse']);
	$cp 	= addslashes($_REQUEST['cp']);
	$ville 	= addslashes($_REQUEST['ville']);
	$pays 	= addslashes($_REQUEST['pays']);
	$email 	= addslashes($_REQUEST['email']);
	$tel 	= addslashes($_REQUEST['tel']);
	$fax 	= addslashes($_REQUEST['fax']);
	$site 	= addslashes($_REQUEST['site']);
	$msg 	= addslashes($_REQUEST['message']);
	$date  	= date('d/m/Y');
	// headers mail
    $dest       =  $email;
    $dest_gsa   =  'fsinabian@groupegtf.fr';

    $headers 	=  "From: " . stripslashes($nom) . "<" . $email . ">" . "\n";
    $headers 	.= "Reply-to: " . $email . "\n";
    $headers 	.= 'Content-Type: text/html; charset="windows-1258"' . "\n";
    $headers    .= 'Content-Transfer-Encoding: 8bit';
    $sujet 		=  'Nouveau message depuis votre site Portail';
    // message
    $message    = '
    <!DOCTYPE html PUBLIC "-W3CDTD XHTML 1.0 Transitional//EN" "http:www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http:www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
    <body style="font-family:Helvetica,Arial,sans-serif;text-align:left;margin:10px;font-size:12px">
        <center>
        <table width="700" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;border:1px solid #e3e3e3;font-size:12px">
            <tbody>
                <tr>
                    <td><img src="images/header-mail.jpg" /></td>
                </tr>
                <tr>
                    <td style="padding: 0px 60px;">
                        <h2 style="margin-top:35px;margin-bottom:15px;font-weight:normal;color:#828071">' . $sujet . '</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                    <br /><br />
                    <table cellspacing="0" cellpadding="6" width="580" style="margin-left:60px;font-size:14px">
                        <tbody>
                            <tr>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Formulaire rempli le</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;' . $date . '</td>
                            </tr>
                            <tr style="background-color: #F2F2F2;">
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Nom</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;' . stripslashes($nom).'</td>
                            </tr>
                            <tr>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Email</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:' . $email . '">' . $email . '</a></td>
                            </tr>
                            <tr style="background-color: #F2F2F2;">
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;T&eacute;l&eacute;phone</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;' . $tel . '</td>
                            </tr>
                            <tr>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Fax</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;' . $fax . '</td>
                            </tr>
                            <tr style="background-color: #F2F2F2;">
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Site internet</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;' . stripslashes($site) . '</td>
                            </tr>
							<tr>
								<td height="20" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;Message</td>
								<td height="20" valign="top"><div style="margin-left:15px">' . stripslashes($msg) . '</div></td>
							</tr>
                        </tbody>
                    </table>
                    <br /><br /><br />
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;background-color:#9E9E9E;height:35px;font-size:12px;color:#ffffff;">
                        GSA, soci&eacute;t&eacute; du Groupe GTF - 3, rue des 4 chemin&eacute;es, 92514 BOULOGNE CEDEX  |
						<a href="mailto:info@gsa.com" style="color: #ffffff;">info@gsa.com</a> | 0 147 619 631
                    </td>
                </tr>
            </tbody>
        </table>
        </center>
    </body>
    </html>';
    // envoi
    if (mail($dest, $sujet, $message, $headers)) {
        $success = '1';
        $alerte  = "Le groupe GSA a bien re&ccedil;u votre mail et vous en remercie.\n\nNous traiterons votre demande dans les meilleurs d&eacute;lais.\n";
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>GSA - Contact</title>
    <meta name="google-site-verification" content="cfM15zJRYHBtIL-8teSrrVgsNsNz42UCDbYARVLyP8w" />
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<meta name="description" content="GSA, une entreprise du groupe GTF." />
	<meta name="keywords" content="" />
	<link type="text/css" rel="stylesheet" href="css/styles.css" />
</head>

<body>

<?php include('header.php'); ?>

<!-- BANDEAU -->
<div id="wrapper">
<div id="bandeau_timeline" style="background:none">
	<div id="bloc_form">

		<!-- formulaire -->
		<form name="form_contact" action="" method="">
			<div style="float:left;margin-right:10px">
				<div class="label"><label>Nom/Soci&eacute;t&eacute;</label></div><input type="text" name="nom" id="nom" value="" />
				<br style="clear:both" /><br />
				<div class="label"><label>Adresse</label></div><input type="text" name="adresse" id="adresse" value="" />
				<br style="clear:both" /><br />
				<div class="label"><label>Code postal</label></div><input type="text" name="cp" id="cp" value="" style="width:70px;float:left" />
				<div class="label" style="margin-left:10px;width:35px"><label>Ville</label></div><input type="text" name="cp" id="cp" value="" style="width:175px;float:left" />
				<br style="clear:both" /><br />
				<div class="label"><label>Pays</label></div><input type="text" name="pays" id="pays" value="" />
				<br style="clear:both" /><br />
				<div class="label"><label>Email</label></div><input type="text" name="email" id="email" value="" />
				<br style="clear:both" /><br />
				<div class="label"><label>T&eacute;l&eacute;phone</label></div><input type="text" name="tel" id="tel" value="" />
				<br style="clear:both" /><br />
				<div class="label"><label>Fax</label></div><input type="text" name="fax" id="fax" value="" />
				<br style="clear:both" /><br />
				<div class="label"><label>Site Internet</label></div><input type="text" name="site" id="site" value="" />
			</div>
			<div style="float:left;margin-left:10px">
				<div class="label" style="margin-bottom:7px"><label>Votre message</label></div><br style="clear:both" />
				<textarea name="message" id="message" style="height:94px" onFocus="if(this.value=='Votre message ...'){this.value=''}">Votre message ...</textarea>
				<br style="clear:both" /><br />
				<input type="submit" name="submit_envoyer" id="submit_envoyer" value="ENVOYER" />
				<br style="clear:both" /><br />
				<iframe width="311" height="110" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=Gerard+Sinabian+Associes+-+GSA,+Boulogne-Billancourt&amp;aq=0&amp;oq=Gerard+Sinabian+Associes&amp;g=3+Rue+4+Chemin%C3%A9es,+92100+Boulogne-Billancourt&amp;ie=UTF8&amp;hq=Gerard+Sinabian+Associes+-+GSA,&amp;hnear=Boulogne-Billancourt,+Hauts-De-Seine,+%C3%8Ele-de-France&amp;t=m&amp;ll=48.833242,2.243356&amp;spn=0.022018,0.012217&amp;output=embed"></iframe>
			</div>
		</form>


	</div>
</div>

<?php include('footer.php'); ?>

</body>
</html>
