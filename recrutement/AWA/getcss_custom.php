<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors.php');
include_once('include/pdo.php');
include_once('include/framework.php');


$sql = "
        SELECT
            *
        FROM
            awa_designs

        WHERE
            active = '1'
    ";
$select = $conn->prepare($sql);
$select->execute();
$dba_design = null;
$dba_design = $select->fetchObject();
if($dba_design){
    
}else{
    exit();
}



$css = "";

/*
 * front CSS
 */
$css .= "#container980{";
$css .=  "width: 980px;";
$css .=  "background-color: transparent;";
$css .=  "margin: 0 auto;";
$css .= "}";

$css .= "#conteneur_head{";
$css .=  "width: 996px;";
$css .=  "background-repeat: no-repeat;";
$css .=  "margin: 0 auto;";
$css .= "clear: both;";
$css .= "padding-bottom: 20px;";
$css .= "padding-top: 10px;";
$css .= "}";

$css .= "#logo_head{";
$css .= "}";

$css .= "#subheader{";
$css .=  "width: 100%;";
$css .= "}";

$css .= "#languesdisplay{";
$css .=  "float: right;";
$css .= "}";

$css .= "#languesdisplay img{";
$css .=  "height: 17px;";
$css .=  "cursor: pointer;";
$css .=  "margin-left: 3px;";
$css .= "}";

$css .= ".spacer{";
$css .=  "clear: both;";
$css .=  "font-size: 0px;";
$css .= "}";

$css .= "#middle{";
$css .=  "width: 980px;";
$css .=  "background-image: url('images/bg-global.jpg');";
$css .=  "background-repeat: no-repeat;";
$css .= "background-position: 15px -50px;";
$css .= "min-height: 800px;";
$css .= "}";

$css .= "div.menu_element{";
$css .=  "padding-left: 10px;";
$css .= "}";

$css .= "div.contenttitle h1{";
$css .=  "font-size: 16px;";
$css .=  "padding: 0px 0px 0px 0px;";
$css .=  "margin: 0px 0px 0px 0px;";
$css .= "}";

$css .= "fieldset{";
$css .=  "border: none;";
$css .= "}";

$css .= "div.searchRow{";
$css .=  "padding-bottom: 4px;";
$css .= "}";

$css .= "div.label{";
$css .=  "float: left;";
$css .=  "width: 100px;";
$css .= "}";

$css .= "div.input{";
$css .=  "float: left;";
$css .= "}";

$css .= "div.input input{";
$css .=  "width: 200px;";
$css .= "}";

$css .= "div.input select{";
$css .=  "width: 205px;";
$css .= "}";

$css .= "div.offre-intro{";
$css .=  "padding-top: 15px;";
$css .=  "padding-bottom: 10px;";
$css .=  "padding-left: 4px;";
$css .=  "padding-right: 4px;";
$css .= "}";

$css .= "div.row1{";
$css .=  "font-size: 15px;";
$css .= "}";

$css .= "div.intitule h2{";
$css .= "font-size: 15px;";
$css .= "margin: 0px 0px 0px 0px;";
$css .= "padding: 0px 0px 0px 0px;";
$css .= "}";

$css .= "div.row2{";
$css .= "font-size: 14px;";
$css .= "}";

$css .= "#inituleFiche{";
$css .=  "float: left;";
$css .= "}";

$css .= "#refFiche{";
$css .= "float: right;";
$css .= "}";

$css .= "#detailOffreContent{";
$css .= "}";

$css .= "#ficheLeft{";
$css .=  "width: 420px;";
$css .=  "float: left;";
$css .= "}";


$css .= "#ficheRight{";
$css .=  "float: right;";
$css .=  "width: 280px;";
$css .=  "color: #A0A0A0;";
$css .= "}";


$css .= "#infocomp img{";
$css .=  "cursor: pointer;";
$css .= "}";

$css .= "#buttonsarea{";
$css .=  "margin: 0 auto;";
$css .= "width: 250px;";
$css .= "}";

$css .= "#buttonsarealist{";
$css .= "}";


$css .= "#rechercherbtn{";
$css .= "width: 130px;";
$css .= "font-weight: bold;";
$css .= "text-align: center;";
$css .= "cursor: pointer;";
$css .= "margin: 5px 0 0 20px;";
$css .= "background: #631E61;";
$css .= "color: white;";
$css .= "font-size: 14px;";
$css .= "padding: 2px 0;";
$css .= "}";

$css .= "#retour{";
$css .= "width: 100px;";
$css .= "font-weight: bold;";
$css .= "text-align: center;";
$css .= "cursor: pointer;";
$css .= "float: left;";
$css .= "}";

$css .= "#postuler{";
$css .=  "width: 100px;";
$css .=  "font-weight: bold;";
$css .=  "text-align: center;";
$css .=  "cursor: pointer;";
$css .= "float: right;";
$css .= "}";

$css .= "#footer{";
$css .= "width: 980px;";
$css .= "height: 100px;";
$css .= "}";


$css .= ".candidature_formrow{";
$css .=  "padding-bottom: 3px;";
$css .= "}";

$css .= ".inputc{";
$css .= "float: left;";
$css .= "}";

$css .= "#experience_bloc_add span{";
$css .= "cursor: pointer;";
$css .= "}";

$css .= "div.experience_bloc_del span{";
$css .= "cursor: pointer;";
$css .= "}";

$css .= "#formation_bloc_add span{";
$css .= "cursor: pointer;";
$css .= "}";

$css .= "div.formation_bloc_del span{";
$css .= "cursor: pointer;";
$css .= "}";

$css .= "#langue_bloc_add span{";
$css .=  "cursor: pointer;";
$css .= "}";

$css .= "div.langue_bloc_del span{";
$css .=  "cursor: pointer;";
$css .= "}";

$css .= "#btn_envoyer{";
$css .=  "width: 100px;";
$css .=  "font-weight: bold;";
$css .=  "text-align: center;";
$css .=  "cursor: pointer;";
$css .= "}";

$css .= "#btn_connexion{";
$css .=  "width: 100px;";
$css .=  "font-weight: bold;";
$css .=  "text-align: center;";
$css .=  "cursor: pointer;";
$css .=  "width: 130px;";
$css .=  "font-weight: bold;";
$css .=  "text-align: center;";
$css .=  "cursor: pointer;";
$css .=  "margin: 0px 0 0 3px;";
$css .=  "background: #631E61;";
$css .=  "color: white;";
$css .=  "font-size: 14px;";
$css .=  "padding: 2px 0;";
$css .= "}";

$css .= "#location_popup_menu{";
$css .=  "position: absolute;";
$css .=  "background-color: #ffffff;";
$css .=  "border: 1px solid #A0A0A0;";
$css .=  "width: 204px;";
$css .=  "top: 100px;";
$css .=  "left: 140px;";
$css .=  "font-size: 12px;";
$css .=  "display: none;";
$css .= "}";

$css .= "div.hoverbale:hover{";
$css .= "}";

$css .= ".spanexp{";
$css .=  "font-size: 15px;";
$css .=  "padding-right: 4px;";
$css .=  "padding-left: 2px;";
$css .=  "font-weight: bold;";
$css .=  "cursor: pointer;";
$css .= "}";

$css .= ".pays_title{";
$css .=  "font-weight: bold;";
$css .= "}";


$css .= ".pays_value{";
$css .=  "cursor: pointer;";
$css .= "}";

$css .= ".pays_content{";
$css .= "display: none;";
$css .= "}";

$css .= ".region_title{";
$css .= "padding-left: 12px;";
$css .= "}";

$css .= ".region_value{";
$css .= "cursor: pointer;";
$css .= "}";

$css .= ".region_content{";
$css .= "display: none;";
$css .= "}";

$css .= ".ville_title{";
$css .= "padding-left: 35px;";
$css .= "cursor: pointer;";
$css .= "}";

$css .= ".messageok{";
$css .= "text-align: center;";
$css .= "color: green;";
$css .= "padding-bottom: 10px;";
$css .= "}";

$css .= ".messagenotok{";
$css .=  "text-align: center;";
$css .=  "color: red;";
$css .=  "padding-bottom: 10px;";
$css .= "}";

$css .= "#pagination{";
$css .=  "text-align: center;";
$css .= "}";

$css .= ".pactive{";
$css .= "text-decoration: underline;";
$css .= "}";








/*
 * Body
 */
$css .= "body{";
$css .= "margin: 0;";
$css .= "padding: 0;";
$css .= "font-family: ".$dba_design->BODY_POLICE.";";
$css .= "height: 100%;";
$css .=  "color: ".$dba_design->BODY_COLOR.";";
$css .= "}";


/*
 * Header
 */
 /*
$css .= "#header{";
$css .=  "width: 980px;";
$css .=  "height: 70px;";
$css .=  "background-image: url('images/logo/".$dba_design->HEADER_LOGO."');";
$css .=  "background-repeat: no-repeat;";
$css .= "}";
*/


/*
 * Pathway
 */

$css .= "#pathway{";
$css .=  "color: ".$dba_design->PATHWAY_COLOR.";";
//$css .=  "height: 20px;";
$css .=  "padding-left: 10px;";
$css .=  "padding-bottom: 10px;";
$css .=  "font-size: 14px;";
$css .=  "float: left";
$css .= "}";
$css .= "#pathway a{";
$css .=  "color: ".$dba_design->PATHWAY_COLOR.";";
$css .=  "text-decoration: none;";
$css .= "}";
$css .= "#pathway a:hover{";
$css .=  "color: ".$dba_design->PATHWAY_COLOR_HOVER.";";
$css .= "}";


/*
 * Mot de passe oublié
 */
$css .= ".motdepasselink{";
$css .=  "color: ".$dba_design->PATHWAY_COLOR.";";
$css .=  "font-size: 12px;";
$css .=  "text-align: left;";
$css .=  "width: 355px;";
$css .=  "margin-left: 150px;";
$css .=  "padding: 2px 3px 15px;";
$css .= "}";
$css .= ".motdepasselink a{";
$css .=  "color: ".$dba_design->PATHWAY_COLOR.";";
$css .=  "text-decoration: none;";
$css .= "}";
$css .= ".motdepasselink a:hover{";
$css .=  "color: ".$dba_design->PATHWAY_COLOR_HOVER.";";
$css .= "}";



/*
 * Zone candidat
 */
$css .= "#subheader2{";
$css .=  "width: 100%;";
$css .=  "text-align: right;";
$css .=  "padding-bottom: 5px;";
$css .=  "font-size: 14px;";
$css .=  "color: ".$dba_design->CANDIDAT_AREA_COLOR.";";
$css .= "}";
$css .= "#subheader2 a{";
$css .=  "text-decoration: none;";
$css .=  "color: ".$dba_design->CANDIDAT_AREA_COLOR.";";
$css .= "}";
$css .= "#subheader2 a:hover{";
$css .=  "color: ".$dba_design->CANDIDAT_AREA_COLOR_HOVER.";";
$css .= "}";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";



/*
 * Menu
 */

$css .= "#menu{";
$css .=  "margin-top: 20px;  ";
$css .=  "width: 200px;  ";
$css .=  "float: left;";
$css .=  "color: ".$dba_design->MENU_COLOR.";";
$css .= "font-size: 15px;";
$css .= "line-height: 20px;";
$css .= "}";
$css .= ".retour_element{";
$css .=  "margin-bottom: 10px;  ";
$css .= "}";

$css .= "#menu a{";
$css .=  "color: ".$dba_design->MENU_COLOR.";";
$css .=  "text-decoration: none;";
$css .= "}";
$css .= "#menu a:hover{";
$css .=  "color: ".$dba_design->MENU_COLOR_HOVER.";";
$css .= "}";



/*
 * Contenu page commun
 */
$css .= "#content{";
$css .=  "float: left;";
//$css .=  "border-left: 2px solid ".$dba_design->CONTENT_VERTICAL_BAR.";";
$css .=  "width: 748px;";
$css .=  "padding-left: 20px;";
$css .=  "padding-right: 10px;";
$css .=  "position: relative;";
$css .= "}";

$css .=  "div.contenttitle{";
//$css .=  "border-bottom: 2px solid ".$dba_design->CONTENT_HORIZONTAL_BAR.";";
$css .=  "width: 100%;";
$css .=  "font-weight: bold;";
$css .=  "color: ".$dba_design->CONTENT_TITRE1_COLOR.";";
$css .=  "font-size: 16px;";
$css .= "margin: 20px 0 0;";
$css .= "padding: 0;";
$css .= "}";

$css .= ".buttonclass{";
$css .=  "border: 2px solid ".$dba_design->CONTENT_BUTTON_COLOR.";";
$css .=  "color: ".$dba_design->CONTENT_BUTTON_COLOR.";";
$css .= "}";




/*
 * Formulaires
 */
//title bloc color
//add color
//delete color
$css .= ".labeldesign{";
$css .= "color: ".$dba_design->CONTENT_LABEL_COLOR.";";
$css .= "font-size: 13px;";
$css .= "}";
$css .= ".inputdesign{";
$css .= "color: ".$dba_design->CONTENT_INPUT_COLOR.";";
$css .= "";
$css .= "}";
$css .= "span.compulsorystar{";
$css .=  "color: ".$dba_design->FORM_COMPULSORY_STAR_COLOR.";";
$css .= "}";
$css .= "";


/*
 * BLOC RECHERCHE
 * 
 */
$css .= ".blocRecherche{";
$css .= "background: white;";
$css .= "padding: 5px 10px;";
$css .= "margin: 0 0px;";
$css .= "padding-left: 250px;";
$css .= "background-image: url('images/bg-gradient.jpg');";
$css .= "background-position: top left;";
$css .= "background-repeat: no-repeat;";
$css .= "border: 0;";
$css .= "height: 220px;";
$css .= "margin-top: 8px;";
$css .= "}";



/*
 * Formulaire candidature
 */
$css .= ".labelc{";
$css .=  "float: left;";
$css .=  "width: 270px;";
$css .=  "color: ".$dba_design->FORM_LABEL_COLOR.";";
$css .= "}";
$css .= ".inputc input{";
$css .=  "width: 250px;";
$css .=  "color: ".$dba_design->FORM_INPUT_COLOR.";";
$css .= "}";
$css .= ".inputc textarea{";
$css .=  "width: 250px;";
$css .=  "color: ".$dba_design->FORM_INPUT_COLOR.";";
$css .= "}";
$css .= ".inputc select{";
$css .=  "width: 255px;";
$css .=  "color: ".$dba_design->FORM_INPUT_COLOR.";";
$css .= "}";
$css .= "#experience_bloc_top{";
$css .=  "font-weight: bold;";
$css .=  "color: ".$dba_design->FORM_BLOC_TITLE_COLOR.";";
$css .=  "padding-top: 5px;";
$css .= "}";
$css .= "#formation_bloc_top{";
$css .=  "font-weight: bold;";
$css .=  "color: ".$dba_design->FORM_BLOC_TITLE_COLOR.";";
$css .=  "padding-top: 5px;";
$css .= "}";
$css .= "#langue_bloc_top{";
$css .=  "font-weight: bold;";
$css .=  "color: ".$dba_design->FORM_BLOC_TITLE_COLOR.";";
$css .=  "padding-top: 5px;";
$css .= "}";
$css .= "#experience_bloc_add{";
$css .=  "text-align:right;";
$css .=  "width:455px;";
$css .=  "color: ".$dba_design->FORM_ADD_COLOR.";";
$css .= "}";
$css .= "#experience_bloc_add span:hover{";
$css .=  "color: ".$dba_design->FORM_ADD_HOVER_COLOR.";";
$css .= "}";
$css .= "#formation_bloc_add{";
$css .=  "text-align:right;";
$css .=  "width:455px;";
$css .=  "color: ".$dba_design->FORM_ADD_COLOR.";";
$css .= "}";
$css .= "#formation_bloc_add span:hover{";
$css .=  "color: ".$dba_design->FORM_ADD_HOVER_COLOR.";";
$css .= "}";
$css .= "#langue_bloc_add{";
$css .=  "text-align:right;";
$css .=  "width:455px;";
$css .=  "color: ".$dba_design->FORM_ADD_COLOR.";";
$css .= "}";
$css .= "#langue_bloc_add span:hover{";
$css .=  "color: ".$dba_design->FORM_ADD_HOVER_COLOR.";";
$css .= "}";
$css .= "div.experience_bloc_del{";
$css .=  "text-align:right;";
$css .=  "width:455px;";
$css .=  "color: ".$dba_design->FORM_DEL_COLOR.";";
$css .= "}";
$css .= "div.experience_bloc_del span:hover{";
$css .=  "color: ".$dba_design->FORM_DEL_COLOR_HOVER.";";
$css .= "}";
$css .= "div.formation_bloc_del{";
$css .=  "text-align:right;";
$css .=  "width:455px;";
$css .=  "color: ".$dba_design->FORM_DEL_COLOR.";";
$css .= "}";
$css .= "div.formation_bloc_del span:hover{";
$css .=  "color: ".$dba_design->FORM_DEL_COLOR_HOVER.";";
$css .= "}";
$css .= "div.langue_bloc_del{";
$css .=  "text-align:right;";
$css .=  "width:455px;";
$css .=  "color: ".$dba_design->FORM_DEL_COLOR.";";
$css .= "}";
$css .= "div.langue_bloc_del span:hover{";
$css .=  "color: ".$dba_design->FORM_DEL_COLOR_HOVER.";";
$css .= "}";


/*
 * Liste offres
 */
$css .= "div.intitule{";
$css .=  "float: left;";
$css .=  "font-family: ".$dba_design->DATA_POLICE.";";
$css .=  "color: ".$dba_design->CONTENT_LISTE_INTITULE_COLOR.";";
$css .= "}";
$css .= "div.intitule a{";
$css .=  "text-decoration: none;";
$css .=  "color: ".$dba_design->CONTENT_LISTE_INTITULE_COLOR.";";
$css .= "}";
$css .= "div.intitule a:hover{";
$css .=  "text-decoration: none;";
$css .=  "color: ".$dba_design->CONTENT_LISTE_INTITULE_COLOR_HOVER.";";
$css .= "}";
$css .= "div.datepublication{";
$css .=  "font-family: ".$dba_design->DATA_POLICE.";";
$css .=  "float: right;";
$css .=  "color: ".$dba_design->CONTENT_LISTE_DATE_COLOR.";";
$css .= "}";
$css .= "div.details{";
$css .=  "font-family: ".$dba_design->DATA_POLICE.";";
$css .=  "color: ".$dba_design->CONTENT_LISTE_CONTRAT_COLOR.";";
$css .=  "float: left;";
$css .= "}";
$css .= "div.localisation{";
$css .=  "font-family: ".$dba_design->DATA_POLICE.";";
$css .=  "color: ".$dba_design->CONTENT_LISTE_LOCATION_COLOR.";";
$css .=  "float: right;";
$css .= "}";
$css .= "div.row3{";
$css .=  "font-family: ".$dba_design->DATA_POLICE.";";
$css .=  "color: ".$dba_design->CONTENT_LISTE_DESC_COLOR.";";
$css .=  "font-size: 14px;";
$css .=  "padding-top: 5px;";
$css .=  "text-align: justify;";
$css .= "}";
$css .= "";
$css .= "#noresultmsg{";
$css .=  "color: ".$dba_design->CONTENT_LISTE_INTITULE_COLOR.";";
$css .=  "text-align: center;";
$css .=  "padding-top: 20px;";
$css .= "}";

$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";


/*
 * Pagination
 */
$css .= ".pagi{";
$css .=  "padding-left: 3px;";
$css .=  "padding-right: 3px;";
$css .=  "cursor: pointer;";
$css .=  "color: ".$dba_design->CONTENT_LISTE_PAGINATION_COLOR.";";
$css .= "}";
$css .= ".pagi a{";
$css .=  "text-decoration: none;";
$css .=  "color: ".$dba_design->CONTENT_LISTE_PAGINATION_COLOR.";";
$css .= "}";
$css .= ".pagi a:hover{";
$css .=  "color: ".$dba_design->CONTENT_LISTE_PAGINATION_COLOR_HOVER.";";
$css .=  "text-decoration: none;";
$css .= "}";
$css .= "";
$css .= "";


/*
 * Fiche offre
 */
$css .= "#desctext{";
$css .=  "font-family: ".$dba_design->DATA_POLICE.";";
$css .=  "color: ".$dba_design->CONTENT_FICHE_DESC_COLOR.";";
$css .=  "text-align: justify;";
$css .= "}";
$css .= "#mission_title{";
$css .=  "color: ".$dba_design->CONTENT_FICHE_MISSION_TITLE_COLOR.";";
$css .=  "font-weight: bold;";
$css .=  "margin: 5px 0 10px 0;";
$css .= "}";
$css .= "#mission_text{";
$css .=  "font-family: ".$dba_design->DATA_POLICE.";";
$css .=  "color: ".$dba_design->CONTENT_FICHE_MISSION_COLOR.";";
$css .=  "text-align: justify;";
$css .= "}";
$css .= "#nomEntreprise{";
$css .=  "width: 100%;";
$css .=  "text-align: center;";
$css .=  "padding: 5px;";
$css .=  "color: ".$dba_design->CONTENT_FICHE_CUSTOMER_TITLE_COLOR.";";
$css .=  "font-family: ".$dba_design->DATA_POLICE.";";
$css .= "}";
$css .= "#descriptionEntreprise{";
$css .=  "width: 260px;";
$css .=  "border: 1px solid ".$dba_design->CONTENT_FICHE_CUSTOMER_BORDER_COLOR.";";
$css .=  "font-family: ".$dba_design->DATA_POLICE.";";
$css .=  "padding-left: 6px;";
$css .=  "padding-right: 6px;";
$css .=  "padding-top: 4px;";
$css .=  "padding-bottom: 4px;";
$css .=  "color: ".$dba_design->CONTENT_FICHE_CUSTOMER_COLOR.";";
$css .= "}";
$css .= "#infocompTitre{";
$css .=  "width: 100%;";
$css .=  "text-align: center;";
$css .=  "font-weight: bold;";
$css .=  "padding: 5px;";
$css .=  "color: ".$dba_design->CONTENT_FICHE_INFO_TITLE_COLOR.";";
$css .= "}";
$css .= "#infocomp{";
$css .=  "width: 260px;";
$css .=  "border: 1px solid ".$dba_design->CONTENT_FICHE_INFO_BORDER_COLOR.";";
$css .=  "padding-left: 6px;";
$css .=  "padding-right: 6px;";
$css .=  "padding-top: 4px;";
$css .=  "padding-bottom: 4px;";
$css .=  "color: ".$dba_design->CONTENT_FICHE_INFO_COLOR.";";
$css .=  "font-family: ".$dba_design->DATA_POLICE.";";
$css .= "}";



/*
 * Footer
 */
$css .= "#footer{";
$css .=  "height: 20px;";
$css .=  "margin-bottom: 24px;";
$css .=  "clear: both;";
$css .=  "width: 100%;";
$css .=  "font-family: Arial,Helvetica,sans-serif;";
$css .=  "font-size: 11px;";
$css .=  "text-align: center;";
$css .=  "padding: 10px 0;";
$css .=  "line-height: 20px;";
$css .=  "background: #631E61;";
$css .=  "letter-spacing: 1px;";
$css .=  "color: ".$dba_design->FOOTER_COLOR.";";
$css .= "}";
$css .= "#footer a{";
$css .=  "color: ".$dba_design->FOOTER_COLOR.";";
$css .=  "text-decoration: none;";
$css .= "}";
$css .= "#footer a:hover{";
$css .=  "color: ".$dba_design->FOOTER_HOVER_COLOR.";";
$css .= "}";
$css .= "";
$css .= "";



/*
 * Google map
 */
$css .= "#bgpopup_localisation{";
$css .=  "z-index: 2000;";
$css .=  "display: none;";
$css .=  "background: #000000;";
$css .=  "position: absolute;";
$css .=  "top: 0;";
$css .=  "left: 0;";
$css .=  "opacity: 0.75;";
$css .=  "filter:alpha(opacity=75);";
$css .= "}";

$css .= "#popup_localisation{";
$css .=  "display: none;";
$css .=  "position: absolute;";
$css .=  "z-index: 2005;";
$css .=  "background-color: #ffffff;";
$css .=  "border: 4px solid #EEEEEE;";
$css .= "}";

$css .= "#popup_localisation_header{";
$css .=  "height: 20px;";
$css .=  "background-color: #EEEEEE;";
$css .=  "padding-left: 3px;";
$css .=  "color: #005599;";
$css .=  "font-weight: bold;";
$css .= "}";

$css .= "#map_canvas{";
$css .=  "height:300px;";
$css .= "}";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";
$css .= "";


/* FORM LOGIN 
 * 
 */
$css .= "#form-login{";
$css .=  "background: rgb(255,255,255); ";/* Old browsers */
$css .=  "background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(229,229,229,1) 100%); ";/* FF3.6+ */
$css .=  "background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(100%,rgba(229,229,229,1))); ";/* Chrome,Safari4+ */
$css .=  "background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%); ";/* Chrome10+,Safari5.1+ */
$css .=  "background: -o-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%); ";/* Opera 11.10+ */
$css .=  "background: -ms-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%); ";/* IE10+ */
$css .=  "background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%); ";/* W3C */
$css .=  "filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 ); ";/* IE6-9 */
$css .=  "margin: 15px 20px 50px 10px;";
$css .=  "padding: 10px 20px;";
$css .=  "-moz-box-shadow: 0px 0px 7px 2px #CCCCCC;";
$css .=  "-webkit-box-shadow: 0px 0px 7px 2px #CCC;";
$css .=  "-o-box-shadow: 0px 0px 7px 2px #CCCCCC;";
$css .=  "box-shadow: 0px 0px 7px 2px #CCC;";
$css .=  "-moz-border-radius: 15px;";
$css .=  "-webkit-border-radius: 15px;";
$css .=  "border-radius: 15px;";
$css .= "}";

header("Content-type: text/css");
$css .= "";
$css .= "";
$css .= "";
echo $css;

?>
