<?php
// CONNEXION MYSQL
include('../mysql/mysql.php');

$pole = 'medical';

#####################################################
# Envoi formulaire
#####################################################
if(isset($_REQUEST['submit_envoyer'])){
	$nom 	= addslashes($_REQUEST['nom']);
	$prenom = addslashes($_REQUEST['prenom']);
	$societe = addslashes($_REQUEST['societe']);
	$email 	= addslashes($_REQUEST['email']);
	$msg 	= addslashes($_REQUEST['msg']);
	$tel 	= addslashes($_REQUEST['tel']);
	$date  	= date('d/m/Y');
	// headers mail
    $dest       =  $email;
    $dest_gsa   =  'pchaule@gsa-medical.fr';
	$path_img	= 'http://gsa-groupe.fr/';


    $headers 	=  "From: ".stripslashes($prenom." ".$nom)."<".$email.">"."\n";
    $headers 	.= "Reply-to: ".$email."\n";
    $headers 	.= 'Content-Type: text/html; charset="windows-1258"'."\n";
    $headers    .= 'Content-Transfer-Encoding: 8bit';

    $headers2 	=  "From: GSA Healthcare<".$dest_gsa.">"."\n";
    $headers2 	.= "Reply-to: ".$dest_gsa."\n";
    $headers2 	.= 'Content-Type: text/html; charset="windows-1258"'."\n";
    $headers2    .= 'Content-Transfer-Encoding: 8bit';

    $sujet 		=  'Nouveau message depuis le site GSA Healthcare';
    // message
    $message    = '
    <!DOCTYPE html PUBLIC "-W3CDTD XHTML 1.0 Transitional//EN" "http:www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http:www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
    <body style="font-family:Helvetica,Arial,sans-serif;text-align:left;margin:10px;font-size:12px">
        <center>
        <table width="700" cellspacing="0" cellpadding="0" style="text-align:left;font-family:Helvetica,Arial,sans-serif;border:1px solid #e3e3e3;font-size:12px">
            <tbody>
                <tr>
                    <td><img src="'.$path_img.'images/header-mail.jpg" /></td>
                </tr>
                <tr>
                    <td style="padding: 0px 60px;">
                        <h2 style="margin-top:35px;margin-bottom:15px;font-weight:normal;color:#828071">'.$sujet.'</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                    <br /><br />
                    <table cellspacing="0" cellpadding="6" width="580" style="margin-left:60px;font-size:14px">
                        <tbody>
                            <tr>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Formulaire rempli le</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;'.$date.'</td>
                            </tr>
                            <tr style="background-color: #F2F2F2;">
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Nom</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;'.stripslashes($prenom." ".$nom).'</td>
                            </tr>
                            <tr style="background-color: #F2F2F2;">
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Soci&eacute;t&eacute;</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;'.stripslashes($societe).'</td>
                            </tr>
                            <tr>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Email</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:'.$email.'">'.$email.'</a></td>
                            </tr>
                            <tr style="background-color: #F2F2F2;">
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;T&eacute;l&eacute;phone</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;'.$tel.'</td>
                            </tr>
							<tr>
								<td height="20" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;Message</td>
								<td height="20" valign="top"><div style="margin-left:15px">'.nl2br(stripslashes($msg)).'</div></td>
							</tr>
                        </tbody>
                    </table>
                    <br /><br /><br />
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;background-color:#9E9E9E;height:35px;font-size:12px;color:#ffffff;">
                        GSA, soci&eacute;t&eacute; du Groupe GTF - 3, rue des 4 chemin&eacute;es, 92514 BOULOGNE CEDEX  | 0 147 619 631
                    </td>
                </tr>
            </tbody>
        </table>
        </center>
    </body>
    </html>';
    // message
    $msg_conf    = '
    <!DOCTYPE html PUBLIC "-W3CDTD XHTML 1.0 Transitional//EN" "http:www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http:www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
    <body style="font-family:Helvetica,Arial,sans-serif;text-align:left;margin:10px;font-size:12px">
        <center>
        <table width="700" cellspacing="0" cellpadding="0" style="text-align:left;font-family:Helvetica,Arial,sans-serif;border:1px solid #e3e3e3;font-size:12px">
            <tbody>
                <tr>
                    <td><img src="'.$path_img.'images/header-mail.jpg" /></td>
                </tr>
                <tr>
                    <td style="padding: 0px 60px;">
                        <p>
                            <br />GSA Healthcare a bien re&ccedil;u votre mail et vous en remercie. Nous traiterons votre demande dans les meilleurs d&eacute;lais.<br /><br />
                            Vous trouverez ci-apr&egrave;s les informations saisies :
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                    <br /><br />
                    <table cellspacing="0" cellpadding="6" width="580" style="margin-left:60px;font-size:14px">
                        <tbody>
                            <tr>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Formulaire rempli le</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;'.$date.'</td>
                            </tr>
                            <tr style="background-color: #F2F2F2;">
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Nom</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;'.stripslashes($prenom." ".$nom).'</td>
                            </tr>
                            <tr style="background-color: #F2F2F2;">
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Soci&eacute;t&eacute;</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;'.stripslashes($societe).'</td>
                            </tr>
                            <tr>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;Email</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:'.$email.'">'.$email.'</a></td>
                            </tr>
                            <tr style="background-color: #F2F2F2;">
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;T&eacute;l&eacute;phone</td>
                                <td height="20">&nbsp;&nbsp;&nbsp;&nbsp;'.$tel.'</td>
                            </tr>
							<tr>
								<td height="20" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;Message</td>
								<td height="20" valign="top"><div style="margin-left:15px">'.nl2br(stripslashes($msg)).'</div></td>
							</tr>
                        </tbody>
                    </table>
                    <br /><br /><br />
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;background-color:#9E9E9E;height:35px;font-size:12px;color:#ffffff;">
                        GSA, soci&eacute;t&eacute; du Groupe GTF - 3, rue des 4 chemin&eacute;es, 92514 BOULOGNE CEDEX  | 0 147 619 631
                    </td>
                </tr>
            </tbody>
        </table>
        </center>
    </body>
    </html>';
    // envoi
    if(mail($dest_gsa,$sujet,$message,$headers)) {
		if(mail($dest,"GSA Healthcare a bien re&ccedil;u votre message.",$msg_conf,$headers2)){
            $success    = '1';
            $alerte     = "GSA Healthcare a bien re&ccedil;u votre mail et vous en remercie.\n\nNous traiterons votre demande dans les meilleurs d&eacute;lais.\n";
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Portail de GSA, une entreprise du groupe GTF</title>
    <meta name="google-site-verification" content="cfM15zJRYHBtIL-8teSrrVgsNsNz42UCDbYARVLyP8w" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="description" content="GSA, une entreprise du groupe GTF." />
<meta name="keywords" content="" />
<script type="text/javascript" src="js/swfobject.js"></script>
<!--
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
-->

<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/coda-slider-2.0.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.coda-slider-2.0.js"></script>
 <script type="text/javascript">
$().ready(function() {
   $('#coda-slider-1').codaSlider({
	   dynamicArrows: false,
	   dynamicTabs: false,
	   autoSlide: true,
	   autoSlideInterval: 4000,
	   autoSlideStopWhenClicked: true
   });
});
</script>

<link type="text/css" rel="stylesheet" href="css/styles.css" />
</head>

<body class="type" style="background-image: url('images/fond-contact.jpg');">



<?php include('../header.php'); ?>

<!-- BANDEAU -->
<div id="wrapper">
	<div id="bandeau2">

		<a href="contact.php" class="contact">contacter GSA m&eacute;dical</a>
		<a href="index.php" class="retour-accueil">Retour Accueil</a>
		<a href="http://gsa-groupe.fr" class="gsa-groupe">Visiter GSA Groupe</a>
		<img src="images/contact-titre.png" />

		<div class="map">
            <iframe width="300" height="220" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=Gerard+Sinabian+Associes+-+GSA,+Boulogne-Billancourt&amp;aq=0&amp;oq=Gerard+Sinabian+Associes&amp;g=3+Rue+4+Chemin%C3%A9es,+92100+Boulogne-Billancourt&amp;ie=UTF8&amp;hq=Gerard+Sinabian+Associes+-+GSA,&amp;hnear=Boulogne-Billancourt,+Hauts-De-Seine,+%C3%8Ele-de-France&amp;t=m&amp;ll=48.833242,2.243356&amp;spn=0.022018,0.012217&amp;output=embed"></iframe>
		</div>
		<div class="carte-visite"><br />
            GSA M&eacute;dical<br /><br />
			<?php info_poles($pole,'nom'); ?><br />
			<?php info_poles($pole,'poste'); ?><br />
			<?php info_poles($pole,'tel'); ?><br />
			<?php info_poles($pole,'mobile'); ?><br />
			<a href="mailto:<?php info_poles($pole,'email'); ?>"><?php info_poles($pole,'email'); ?></a><br />
			<?php info_poles($pole,'adresse'); ?><br />
			<?php info_poles($pole,'cp'); ?> <?php info_poles($pole,'ville'); ?>
			<br /><br />
			<a href="http://g.co/maps/pwnct" target="_blank">> Agrandir le plan</a>
		</div>
	</div>

	<!-- CONTENU -->

	<div id="contenu">
		<!-- colonne de gauche : menu -->
		<div id="type-colgauche">
			<ul>
				<li class="interne"><a href="conseil-strategie-operationnelle.html">Conseil en strat&eacute;gie op&eacute;rationnelle</a></li>
				<li class="interne"><a href="qualite-certification.html">Qualit&eacute; et Certification</a></li>
				<li class="interne"><a href="methodologie.html">M&eacute;thodologie et Mode d'Emploi</a></li>
				<li class="interne"><a href="reseaux-exclusifs.html">R&eacute;seaux exclusifs</a></li>
				<li class="interne"><a href="reseaux-multi-produits.html">R&eacute;seaux multi-produits</a></li>
				<li><a href="medecins.html">M&eacute;decins g&eacute;n&eacute;ralistes et sp&eacute;cialit&eacute;s m&eacute;dicales</a></li>
				<li><a href="../index.html">&bullet; Recrutement<br />&bullet; M&eacute;dical <br />&bullet; Pharma <br /></a></li>
				<!---->
			</ul>
		</div>
		<!-- colonne centrale : contenu principal -->
		<div id="type-central">
			<div class="formulaire">
				<form name="contact" action="" method="post">
				 	<span>
				 		<label>Nom</label>
				 		<input type="text" name="nom" />
				 	</span>
				 	<span class="b">
				 		<label>Pr&eacute;nom</label>
				 		<input type="text" name="prenom" />
				 	</span>
				 	<span>
				 		<label>Soci&eacute;t&eacute;</label>
				 		<input type="text" name="societe" />
				 	</span>
				 	<span class="b">
				 		<label>Adresse E-mail</label>
				 		<input type="text" name="email" />
				 	</span>
				 	<span>
				 		<label>Num&eacute;ro de t&eacute;l&eacute;phone</label>
				 		<input type="text" name="tel" />
				 	</span>
				 	<span class="b">
				 		<label>Tapez ici votre message</label><br />
				 		<textarea name="msg"></textarea>
				 	</span>
				 	<span>
				 		<input type="submit" name="submit_envoyer" value="ENVOYER" />
				 	</span>
				</form>
			</div>
		</div>
		<!-- colonne droite : contenu annexe -->
		<div id="type-coldroite">
			<h3>La charte de d&eacute;ontologie</h3>
			<p>
			GSA M&eacute;dical est signataire de la charte de d&eacute;ontologie des membres de l'organisation professionnelle de la prestation de services aux industries de sant&eacute;.<br />
			<a href="Certificat_visitemedicale_GSA_nov2011.pdf" target="_blank" style="text-decoration:none; color: #222222;"> > Consulter la certification</a>
			</p>
			<br />
			<img src="images/illus-coldroite.jpg" />
		</div>
	</div>
</div>

<?php include('../footer.php'); ?>

<?php if(isset($_REQUEST['submit_envoyer'])){ ?><script type="text/javascript">alert('<?= $alerte; ?>');</script><?php } ?>

</body>
</html>
