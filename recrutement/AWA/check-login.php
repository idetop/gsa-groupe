<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');

//print_t($_POST);
Debug::d_echo("acces ", 2,"check-login.php");
Debug::d_print_r($_GET, 1,"GET","check-login.php");
Debug::d_print_r($_POST, 1,"POST","check-login.php");
Debug::d_print_r($_SESSION, 1,"SESSION","check-login.php");

$source = "";
if(!empty($_GET['source'])){
    $source = $_GET['source'];
}


if(count($_POST)){
    if(!empty($_POST['login']) && !empty($_POST['pass'])){

        $sql = "
            SELECT
                ID,
                ID_PERSONNE_WEB,
                LOGIN
            FROM
                awa_candidats
            WHERE
                `LOGIN`=:login
                AND `PASSWORD` = AES_ENCRYPT('".$_POST['pass']."','admen')
            ";

        $select = $conn->prepare($sql);
        $select->bindParam(':login', $_POST['login'], PDO::PARAM_STR);
        $select->execute();
        $candidat = null;
        $candidat = $select->fetchObject();
        if($candidat){
//            echo "ok";
//            print_t($candidat);
            $_SESSION['awa_candidat_id'] = $candidat->ID;
            $_SESSION['awa_candidat_login'] = $candidat->LOGIN;
            Debug::d_echo("login ok ".$candidat->ID, 2,"check-login.php");
        }else{
//            echo "pas ok";
            Debug::d_echo("login failed ".$_POST['login'], 2,"check-login.php");
        }
    }
}
//print_t($_SESSION);
//echo "Location: http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language']);

if(!empty($_GET['oid']) && $candidat){
    $sql = "
        SELECT
            an.LIBELLE,
            an.ID_ANNONCE
            
        FROM
            annonces AS an
        INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION
        INNER JOIN societes AS so ON so.ID_SOCIETE = mi.ID_SOCIETE

        WHERE
            an.ID_ANNONCE =:idannonce
            AND an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )
    ";


    $sql45 = "
        SELECT
            an.LIBELLE,
            an.ID_ANNONCE
           
        FROM
            tannonces AS an
        INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION
        INNER JOIN societes AS so ON so.ID_SOCIETE = mi.ID_SOCIETE
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_ANNONCE =:idannonce
            AND al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )
    ";

    if(_ADMEN_USE_ADVERT_LINES){
        $sql = $sql45;
    }

    $select = $conn->prepare($sql);
    $select->bindParam(':idannonce', $_GET['oid'], PDO::PARAM_INT);
    $select->execute();

    $annonce = null;
    $annonce = $select->fetchObject();

    if($annonce){
        header("Location: "._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_CANDIDATURE_OFFRE_".$_SESSION['awa_language'])."?id=".$annonce->ID_ANNONCE."&source=".$source);
    }else{
        Debug::d_echo("annonce non trouvée (bad ref, unactive...) return 404", 2,"fiche.php");
        header('HTTP/1.0 404 Not Found');
        exit();
    }     

}else{
    if(!empty($_GET['oid'])){
        header("Location: http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language'])."?oid=".$_GET['oid']."&source=".$source);
    }else{
        header("Location: http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language']));
    }
    
}

?>
