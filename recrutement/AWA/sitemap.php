<?php
include_once('include/config.php');
//include_once('include/display_errors.php');
include_once('include/pdo.php');
include_once('include/framework.php');

$sql = "
        SELECT
            an.REFERENCE,
            an.LIBELLE,
            an.TEXTE_ANNONCE,
            an.DATE_DEBUT,
            an.DESCRCUSTOMER,
            an.DESCRASSIGNMENT,
            an.ID_ANNONCE,
            mi.PAYS,
            mi.REGION,
            mi.ZIP,
            mi.CITY,
            mi.SALARY_FROM,
            mi.SALARY_TO,
            mi.TYPECONTRAT,
            mi.ID_MISSION

        FROM
            annonces AS an
        INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION

        WHERE
            an.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( an.DATE_DEBUT <= '".date("Y-m-d")."' OR  an.DATE_DEBUT IS NULL OR an.DATE_DEBUT = '0000-00-00' )
            AND ( an.DATE_FIN >= '".date("Y-m-d")."' OR an.DATE_FIN IS NULL OR an.DATE_FIN = '0000-00-00' )
            

    ";


$sql45 = "
        SELECT
            al.REF2 AS REFERENCE,
            an.LIBELLE,
            an.TEXTE_ANNONCE,
            an.DATE_DEBUT,
            an.DESCRCUSTOMER,
            an.DESCRASSIGNMENT,
            an.ID_ANNONCE,
            mi.PAYS,
            mi.REGION,
            mi.ZIP,
            mi.CITY,
            mi.SALARYMIN AS SALARY_FROM,
            mi.SALARYMAX AS SALARY_TO,
            mi.TYPECONTRAT,
            mi.ID_MISSION

        FROM
            tannonces AS an
        INNER JOIN missions AS mi ON mi.ID_MISSION = an.ID_MISSION
        INNER JOIN advert_lines AS al ON al.ID_ANNONCE = an.ID_ANNONCE

        WHERE
            al.ID_SUPPORT = '"._CONFIG_SUPPORT_ID."'
            AND mi.ETAT = '1'
            AND ( al.DATE_BEGIN <= '".date("Y-m-d")."' OR  al.DATE_BEGIN IS NULL OR al.DATE_BEGIN = '0000-00-00' )
            AND ( al.DATE_END >= '".date("Y-m-d")."' OR al.DATE_END IS NULL OR al.DATE_END = '0000-00-00' )


    ";

if(_ADMEN_USE_ADVERT_LINES){
    $sql = $sql45;
}

$select = $conn->prepare($sql);
$select->execute();

while($row = $select->fetchObject()){
    $annonces[] = $row;
}

$xml = "";

$xml .="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
$xml .="<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.84\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.google.com/schemas/sitemap/0.84  http://www.google.com/schemas/sitemap/0.84/sitemap.xsd\">";
foreach($annonces as $annonce){
    $xml .="<url>";
    $xml .="<loc>http://"._CONFIG_DOMAIN_NAME.rewriteAnnonceUrl($annonce->ID_ANNONCE,$annonce->LIBELLE)."</loc>";
    $xml .="<lastmod>".$annonce->DATE_DEBUT."</lastmod>";
    $xml .="<changefreq>weekly</changefreq>";
    $xml .="<priority>0.5</priority>";
    $xml .="</url>";
}

$xml .="</urlset>";
Header("content-type: application/xml");
echo $xml;
?>