<div id="container980">
    <div id="header"></div>
    <div id="subheader">
        <div id="pathway">
            <?php echo $templateDatas['pathway']; ?>
        </div>
        <div id="languesdisplay">
            <?php echo $templateDatas['flags']; ?>
        </div>
        <div class="spacer"></div>
    </div>
    <?php echo $templateDatas['candidatArea']; ?>
    <div id="middle">
        <div id="menu">
             <?php echo $templateDatas['menu']; ?>
        </div>
        <div id="content">
            <div class="contenttitle"><h1><?php echo _ESPACECANDIDAT_TITLE; ?></h1></div>

            <div style="height: 20px;"></div>
            <div style="">
                <?php echo _ESPACECANDIDAT_INTRO_TEXT; ?>
            </div>
            <div style="height: 20px;"></div>
            <div style="padding-left: 150px;">
                <form id="login_form" action="check-login.php?oid=<?php echo $backToOfferId; ?>&source=<?php echo $source; ?>" method="post">
                    <fieldset>
                        <div class="candidature_formrow" >
                            <div class="label labeldesign" style="width: 150px;"><?php echo _ESPACECANDIDAT_LOGIN_FORM_EMAIL_LABEL; ?> <span class="compulsorystar">*</span></div>
                            <div class="input inputdesign">
                                <input type="text" id="login" name="login" value="<?php echo $templateDatas['emailPreselect']; ?>"/>
                            </div>
                            <div class="spacer"></div>
                        </div>
                        <div class="candidature_formrow" >
                            <div class="label labeldesign" style="width: 150px;"><?php echo _ESPACECANDIDAT_LOGIN_FORM_PASS_LABEL; ?> <span class="compulsorystar">*</span></div>
                            <div class="input inputdesign">
                                <input type="password" id="pass" name="pass" value=""/>
                            </div>
                            <div class="spacer"></div>
                        </div>
                        <div  class="motdepasselink"><a href="<?php echo $templateDatas['forgetPasswordButtonLink']; ?>" title=""><?php echo _ESPACECANDIDAT_MDP_OUBLI_LINK; ?></a></div>
                        <div style="padding-top: 35px;padding-left: 150px;">
                            <div id="btn_connexion" class="buttonclass" onclick="$('#login_form').submit();"><?php echo _ESPACECANDIDAT_LOGIN_FORM_SUBMIT; ?></div>
                        </div>

                    </fieldset>
                </form>
            </div>
            


            <div style="height: 30px;"></div>
        </div>
        <div class="spacer"></div>
    </div>
    <div id="footer">
         <?php echo $templateDatas['footerText']; ?>
    </div>
</div>