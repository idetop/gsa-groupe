
    <!-- Bouton nouveau -->
    <div id="body_btn_nouveau">
        <!--<a href="javascript:;" class="retour_off" title="Page précédente"><img src="src/icons/arrow_left.png"></a>-->
        <a href="<?= $site; ?>" class="link_nouveau" target="_blank">
            <div class="btn_nouveau"><span class="span_nouveau">Accéder au site <font color="#b9001f">&raquo;</font></span></div>
        </a>
    </div>
    <div class="clear"></div>
</div>

<div id="content">
    <!-- Colonne gauche -->
    <?php include($b_templates.'side_left.php'); ?>

    <!-- Colonne droite -->
    <div class="colonne_right">

        <div id="title_line"><span class="span_title_line">Accueil</span></div>
        <table class="table_right" width="760" cellpadding="6" cellspacing="0">
            <tbody>
                <tr>
                    <td>
						<div style="text-align:center">
                            <?php if($statut == 'gsa'){ ?>
							<div id="icons" class="vtip" title="Gérer les accès administrateurs" onClick="window.location='index.php?page=user'"><img src="src/iKon/usergold.png"><p><br />Administrateurs<br /></p></div>
							<div id="icons" class="vtip" title="Gérer le contenu de la page Qui sommes-nous" onClick="window.location='index.php?page=qui-sommes-nous&part=qui'"><img src="src/iKon/sketchesiq2.png"><p>Qui sommes-nous ?</p></div>
                            <?php } ?>
                            <?php if($statut == 'formation' || $statut == 'gsa'){ ?>
							<div id="icons" class="vtip" title="Gérer le contenu des pages GSA Formation" onClick="window.location='index.php?page=gsa-formation&part=accueil'"><img src="src/iKon/formation.png"><p><br />GSA Formation<br /></p></div>
                            <?php } ?>
                            <?php if($statut == 'pharma' || $statut == 'gsa'){ ?>
							<div id="icons" class="vtip" title="Gérer le contenu des pages GSA Pharma" onClick="window.location='index.php?page=gsa-pharma&part=accueil'"><img src="src/iKon/pharma.png"><p><br />GSA Pharma<br /></p></div>
                            <?php } ?>
                            <?php if($statut == 'recrutement' || $statut == 'gsa'){ ?>
							<div id="icons" class="vtip" title="Gérer le contenu des pages GSA Recrutement" onClick="window.location='index.php?page=gsa-recrutement&part=accueil'"><img src="src/iKon/recrutement.png"><p>GSA Recrutement</p></div>
                            <?php } ?>
                            <?php if($statut == 'medical' || $statut == 'gsa'){ ?>
							<div id="icons" class="vtip" title="Gérer le contenu des pages GSA Médical" onClick="window.location='index.php?page=gsa-medical&part=accueil'"><img src="src/iKon/medical.png"><p><br />GSA Médical<br /></p></div>
                            <?php } ?>
							<div style="clear:both;"></div>
						</div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
</div>

