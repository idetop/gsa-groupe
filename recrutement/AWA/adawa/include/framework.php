<?php
//Créé
@include_once 'pdo.php';

//langue d'affichage par défaut 
if(empty($_SESSION['langue'] )){
    $_SESSION['langue'] = "fr";
}
if(!empty($_GET['adawalg'])){
    if($_GET['adawalg'] == "fr"){
        $_SESSION['langue'] = "fr";
    }elseif($_GET['adawalg'] == "en"){
        $_SESSION['langue'] = "en";
    }
}
//chargement du fichier de langue associé
if(is_file("../../langues/framework_".$_SESSION['langue'].".php")){
    include_once "../../langues/framework_".$_SESSION['langue'].".php";
}elseif(is_file("../langues/framework_".$_SESSION['langue'].".php")){
    include_once "../langues/framework_".$_SESSION['langue'].".php";
}else{
    include_once "langues/framework_".$_SESSION['langue'].".php";
}
if(is_file("../../langues/adawa_".$_SESSION['langue'].".php")){
    include_once "../../langues/adawa_".$_SESSION['langue'].".php";
}elseif(is_file("../langues/adawa_".$_SESSION['langue'].".php")){
    include_once "../langues/adawa_".$_SESSION['langue'].".php";
}else{
    include_once "langues/adawa_".$_SESSION['langue'].".php";
}




function print_t($var){
    echo "<pre>\n";
	print_r($var);
    echo "</pre>\n";
}

function format_msWord($txt){
    $txt = str_replace("œ","oe",$txt);
    $txt = str_replace("&","&amp;",$txt);
    $txt = str_replace("–","-",$txt);
    $txt = str_replace("…","...",$txt);
    $txt = str_replace("“","\"", $txt);
    $txt = str_replace("”","\"", $txt);
    $txt = trim(str_replace("’","'",$txt));

    return $txt;
}



function test_session_boe($session_id,$session_login,$redirect,$conn){
    $auth_session = false;


    if(isset($_SESSION['wrong_session'])){
	$_SESSION['wrong_session'] = "";
	unset($_SESSION['wrong_session']);
    }

    if(!empty($session_id) && !empty($session_login)){
	$req = "SELECT
		    ID
		FROM
		    awa_recruteurs
		WHERE
		    LOGIN LIKE :email
                    AND ID = :id
                    AND ACTIVE = 1
                ";
        $select = $conn->prepare($req);
        $select->bindParam(':email', $session_login, PDO::PARAM_STR);
        $select->bindParam(':id', $session_id, PDO::PARAM_INT);
	$select->execute();
        $selectResult = null;
        $selectResult = $select->fetchObject();
	
	if($selectResult){
	    $auth_session=true;
        }
    }

    if(!$auth_session){
	$_SESSION['wrong_session'] = true;
	header('Location:'.$redirect);
    }
}

/*
 * Retourne une date au format SQL (YYYY-MM-DD)
 * Accepte en entrée DD-MM-YYYY ou DD/MM/YYYY
 */
function getEnglishSQLDate($datefr){
    $temp = array();
    str_replace("/", "-", $datefr);
    $temp = explode("-",$datefr);
    if(count($temp) != 3){
        exit("BAD DATE FORMAT");
    }
    if($temp[2] > 1900){
        return $temp[2]."-".$temp[1]."-".$temp[0];
    }elseif($temp[0] > 1900){
        return $temp[0]."-".$temp[1]."-".$temp[2];
    }elseif($temp[2] == 0 && $temp[1] == 0 && $temp[0] == 0){
        return "0000-00-00";
    }else{
        exit("BAD DATE FORMAT");
    }
}

function getDateDisplay($date_sql,$separateur){
    $temp=array();
    $temp=explode("-",$date_sql);
    if(count($temp) != 3){
        exit("BAD DATE FORMAT");
    }
    if($temp[2] > 1900){
        return $temp[0].$separateur.$temp[1].$separateur.$temp[2];
    }elseif($temp[0] > 1900){
        return $temp[2].$separateur.$temp[1].$separateur.$temp[0];
    }elseif($temp[2] == 0 && $temp[1] == 0 && $temp[0] == 0){
        return "00-00-0000";
    }else{
        exit("BAD DATE FORMAT2");
    }
}


function simplexml2array($xml) {
    if (get_class($xml) == 'SimpleXMLElement') {       
       $attributes = $xml->attributes();
       foreach($attributes as $k=>$v) {
           if ($v) $a[$k] = (string) $v;
       }
       $x = $xml;
       $xml = get_object_vars($xml);
    }
    if (is_array($xml)) {
       if (count($xml) == 0) return (string) $x; // for CDATA
       foreach($xml as $key=>$value) {           
           $r[$key] = simplexml2array($value);
       }
       if (isset($a)) $r['@'] = $a;    // Attributes
       return $r;
    }
    return (string) $xml;
}


/*
 * ENVOI DE MAIL
 * Permet d'envoyer un mail de vase avec le bon formatage du header
 * Mail envoyer en text/plain et en ISO
 * INPUT =>
 *          $emailSrc : email de l'emeteur
 *          $emailDst : email de destination
 *          $objet : Contenu de l'objet du mail
 *          $message : contenu du message du mail
 */
function sendmail($emailSrc,$emailDst,$objet,$message){
    $debug = false;
    if(!empty ($emailSrc) && !empty ($emailDst) && !empty ($objet) && !empty ($message)){
        $to       = $emailDst;
        $from     = $emailSrc;

        $subject  = $objet;
        $message  = $message;        

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";
        $headers .= 'Content-Transfer-Encoding: 8bit' . "\r\n";
        $headers .= 'From: ' . $from . "\r\n" ;
        $headers .= 'Reply-To: '. $from . "\r\n" ;

        if(mail($to, utf8_decode($subject), utf8_decode($message), $headers,'-f'.$from)){
            if($debug){
                print($headers."<br/>");
                print($to."<br/><br/>");
                print($subject."<br/><br/>");
                print($message."<br/>");
            }else{
                return 1;
            }
        }else{
            return 0;
        }
    }
}


/*
 * Gestion du contenu de la zone des drapeaux pour le changement de langue
 */
function displayFlags(){
    $returnhtml = "";
    $returnhtml .= "<a href=\"?adawalg=fr\" title=\"\"><img src=\""._CONFIG_ROOTFOLDER."images/fr.png\" alt=\"\"/></a>";
    $returnhtml .= "<a href=\"?adawalg=en\" title=\"\"><img src=\""._CONFIG_ROOTFOLDER."images/uk.png\" alt=\"\"/></a>";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";
    $returnhtml .= "";

    return $returnhtml;
}


?>
