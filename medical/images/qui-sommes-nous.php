<?php include('controllers/qui-sommes-nous.php'); ?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>GSA - Membre de l'OPPSIS</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<meta name="description" content="GSA, une entreprise du groupe GTF." />
	<meta name="keywords" content="" />
	
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/swfobject.js"></script>
	
    <link type="text/css" rel="stylesheet" href="css/styles.css" />
    
	<!-- Plugin QUI SOMMES-NOUS -->
    <link rel="stylesheet" href="css/coda-slider-2.0.css" type="text/css" media="screen" />
    <script type="text/javascript" src="js/jquery.coda-slider-2.0.js"></script>
	
	<!-- Plugin HISTORIQUE -->
    <script src="js/timeline/js/jquery.timelinr-0.9.4.js" type="text/javascript"></script>
	<link rel="stylesheet" href="js/timeline/css/style_v.css" type="text/css" media="screen" />

    <script type="text/javascript">
    $().ready(function() {
		// Init slider QUI SOMMES-NOUS
       $('#coda-slider-1').codaSlider({
           dynamicArrows: false,
           dynamicTabs: true,
           autoSlide: true,
           autoSlideInterval: 8000,
           autoSlideStopWhenClicked: true
       });
        // Init slider HISTORIQUE
        $().timelinr({
            orientation: 'vertical',
            issuesSpeed: 300,
            datesSpeed: 100,
            issuesSpeed: 500,
            arrowKeys: 'false',
            startAt: 1,
            autoPlay: 'true',
            autoPlayDirection: 'forward',
            autoPlayPause: 4000
        });
    });
    </script>
</head>

<body>
    
<?php include('header.php'); ?>
	
<!-- BANDEAU -->
<div id="wrapper">
    <div id="bandeau_oppsis" class="qui">
        <div class="carou_qui">
            <div id="coda-nav-left-1" class="btn gauche prev">
                <img src="images/fleche-gauche.png" />
            </div>
            <div id="slide-container">
                <div class="coda-slider-wrapper">
                    <div class="coda-slider preload" id="coda-slider-1"><?php slides_qui(); ?></div>
                </div>
            </div>
            <div id="coda-nav-right-1" class="btn droite next">
                <img src="images/fleche-droite.png" class="next" />
            </div>
        </div>
    </div>
</div>
	
<div id="wrapper">
	<!-- CONTENU -->
	<div id="contenu" style="margin-top:-25px;">
	
		<!-- colonne gauche -->
		<div class="article_left qui">
			<img src="images/titre-colonne1.gif" />
			<?php ambition(); ?>
		</div>
		
		<!-- colonne droite -->
		<div class="article_right qui">
			<img src="images/titre-colonne2.gif" />
			<div id="les-chiffres">
				<div id="chiffres1">
					<span class="rond6">56</span>
					<span>millions<br />d'euros<br />de chiffre<br />d'affaires</span>
				</div>
				<div id="chiffres2">
					<span class="effectifs">457</span>
					<span>Collaborateurs en France et � l'international</span>	
				</div>
				<div id="chiffres1">
					<span class="rond6" style="padding: 10px 25px;">&nbsp;4&nbsp;</span>
					<span style="font-size:26px;"><br />p�les<br />d'expertise<br /></span>
				</div>
			</div>
		</div>
		
		<div id="equipe"><?php equipe(); ?></div>
        
	</div>
	
	
	<div id="bandeau_timeline">
		<div id="timeline">
			<!-- left -->
			<ul id="dates"><?php slides_story('left'); ?></ul>
			<!-- content -->
			<ul id="issues"><?php slides_story('content'); ?></ul>
		</div>
		<br style="clear: left" />
	</div>
	
</div>

<?php include('footer.php'); ?>

</body>
</html>