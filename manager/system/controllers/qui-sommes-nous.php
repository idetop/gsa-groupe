<?php
####################################################################################################################
# QUI SOMMES-NOUS
####################################################################################################################

if($part == 'qui'){

    #########################
    # AJOUTER
    #########################
    if($_REQUEST['action'] == 'add'){
        $sql_last       = mysqli_query($connexion, "SELECT * FROM gsa_about_slider ORDER BY ordre DESC LIMIT 0,1");
        $data_last      = mysqli_fetch_array($sql_last);
        $val            = $data_last['ordre']+1;
        $val_max        = $val;
    }
    if(! empty($_REQUEST['submit_add'])){
        $ordre_old      = $_REQUEST['ordre_old'];
        $ordre_new      = $_REQUEST['ordre'];
        $texte          = addslashes($_REQUEST['texte']);
        // insert
        $insert         = mysqli_query($connexion, "INSERT INTO gsa_about_slider (texte,ordre) VALUES ('".$texte."','".$ordre_new."')");
        // update ordre
        if($ordre_old != $ordre_new){
            $sql_last      = mysqli_query($connexion, "SELECT * FROM gsa_about_slider ORDER BY id DESC LIMIT 0,1");
            $data_last     = mysqli_fetch_array($sql_last);
            $update_ordre1 = mysqli_query($connexion, "UPDATE gsa_about_slider SET ordre = '".$ordre_old."' WHERE ordre = '".$ordre_new."'");
            $update_ordre2 = mysqli_query($connexion, "UPDATE gsa_about_slider SET ordre = '".$ordre_new."' WHERE id = '".$data_last['id']."'");
        }
        $retour         = "success";
        $alerte         = "Le nouveau texte a bien été enregistré.";
    }

    #########################
    # UPDATE
    #########################
    if(! empty($_REQUEST['update'])){
        $id_update      = $_REQUEST['id_update'];
        $ordre_old      = $_REQUEST['ordre_old'];
        $ordre_new      = $_REQUEST['ordre'];
        $texte          = addslashes($_REQUEST['texte']);
        // update texte
        $update_texte   = mysqli_query($connexion, "UPDATE gsa_about_slider SET texte = '".$texte."',ordre = '".$ordre_old."' WHERE id = '".$id_update."'");
        // update ordre
        if($ordre_old != $ordre_new){
            $update_ordre1 = mysqli_query($connexion, "UPDATE gsa_about_slider SET ordre = '".$ordre_old."' WHERE ordre = '".$ordre_new."'");
            $update_ordre2 = mysqli_query($connexion, "UPDATE gsa_about_slider SET ordre = '".$ordre_new."' WHERE id = '".$id_update."'");
        }
        $retour         = "success";
        $alerte         = "Le texte à bien été mis à jour.";
    }

    #########################
    # SUPPRIMER
    #########################
    if(! empty($_REQUEST['id_del'])){
        $id_del         = $_REQUEST['id_del'];
        // recherche ordre du slide
        $sql_ordre      = mysqli_query($connexion, "SELECT * FROM gsa_about_slider WHERE id = '".$id_del."'");
        $data_ordre     = mysqli_fetch_array($sql_ordre);
        $ordre          = $data_ordre['ordre'];
        // suppression du slide
        $delete         = mysqli_query($connexion, "DELETE FROM gsa_about_slider WHERE id = '".$id_del."'");
        // actualisation des ordres
        $select_ordre   = mysqli_query($connexion, "SELECT * FROM gsa_about_slider WHERE ordre > '".$ordre."'");
        while($row_ordre = mysqli_fetch_array($select_ordre)) {
            $ordre_new  = $row_ordre['ordre']-1;
            $update     = mysqli_query($connexion, "UPDATE gsa_about_slider SET ordre = '".$ordre_new."' WHERE id ='".$row_ordre['id']."'");
        }
        // ok
        $retour         = "success";
        $alerte         = "Le texte à bien été supprimé.";
    }

    #########################
    # EDITER
    #########################
    if(! empty($_REQUEST['id'])){
        $sql_edit       = mysqli_query($connexion, "SELECT * FROM gsa_about_slider WHERE id = '".$_REQUEST['id']."'");
        $data_edit      = mysqli_fetch_array($sql_edit);
        $val            = $data_edit['ordre'];
        // find dernier slide
        $sql_last       = mysqli_query($connexion, "SELECT * FROM gsa_about_slider ORDER BY ordre DESC LIMIT 0,1");
        $data_last      = mysqli_fetch_array($sql_last);
        $val_max        = $data_last['ordre'];
        $cursor_moins   = "pointer";
        $cursor_plus    = "pointer";
        $disabled_moins = ''; if($data_edit['ordre'] == '1') { $disabled_moins = 'disabled'; $cursor_moins = "no-drop"; }
        $disabled_plus  = ''; if($data_edit['ordre'] == $data_last['ordre']) { $disabled_plus = 'disabled'; $cursor_plus = "no-drop"; }
    }

    #########################
    # liste des slides
    #########################
    function liste_slides_qui(){
        $i = 0;
        $sql = mysqli_query($connexion, "SELECT * FROM gsa_about_slider ORDER BY ordre ASC");
        while($row = mysqli_fetch_array($sql)){
            $color = ++$i % 2 ? '#ffffff':'#EAEAEA';
            echo '
            <tr class="tr_content" style="background:'.$color.'">
                <td class="td_content" valign="top" style="text-align:center;font-size:14px"><p><strong>'.$row['ordre'].'</strong></p></td>
                <td class="td_content" valign="top" style="text-align:justify">'.$row['code_postal'].' '.stripslashes($row['texte']).'</td>
                <td width="60" valign="top" style="padding:5px;">
                    <form name="del_fiche" method="post" action="" style="margin-top:17px">
                        <input type="hidden" name="id_del" value="'.$row['id'].'">
                        <input id="submit_del" class="vtip" title="Supprimer" name="del" type="submit" value="" style="float:right" onClick="return confirm(\'\nEtes-vous sur de vouloir supprimer ce slide ?\n\n\');"><br /><br />
                        <a class="vtip" title="Editer" href="index.php?page='.$_REQUEST['page'].'&part='.$_REQUEST['part'].'&action=edit&id='.$row['id'].'">
                            <input id="submit_accept" type="button" value="" style="float:right;background:url(\'src/icons/page_white_edit.png\') no-repeat;">
                        </a>
                    </form>
                </td>
            </tr>';
        }
    }

}


####################################################################################################################
# NOTRE AMBITION
####################################################################################################################

if($part == 'ambition'){

    #########################
    # UPDATE
    #########################
    if(! empty($_REQUEST['update'])){
        $texte          = addslashes($_REQUEST['texte']);
        $update_texte   = mysqli_query($connexion, "UPDATE gsa_about_ambition SET texte = '".$texte."' WHERE id = '1'");
        $retour         = "success";
        $alerte         = "Le texte à bien été mis à jour.";
    }

    #########################
    # EDITER
    #########################
    $sql_edit       = mysqli_query($connexion, "SELECT * FROM gsa_about_ambition WHERE id = '1'");
    $data_edit      = mysqli_fetch_array($sql_edit);

}



####################################################################################################################
# HOMMES CLES
####################################################################################################################

if($part == 'hommes'){

    #########################
    # AJOUTER
    #########################
    if($_REQUEST['action'] == 'add'){
        $sql_last       = mysqli_query($connexion, "SELECT * FROM gsa_about_equipe ORDER BY ordre DESC LIMIT 0,1");
        $data_last      = mysqli_fetch_array($sql_last);
        $val            = $data_last['ordre']+1;
        $val_max        = $val;
    }
    if(! empty($_REQUEST['submit_add'])){
        $nom            = addslashes($_REQUEST['nom']);
        $poste          = addslashes($_REQUEST['poste']);
        $pole           = addslashes($_REQUEST['pole']);
        $statut         = addslashes($_REQUEST['statut']);
        $sql_last       = mysqli_query($connexion, "SELECT * FROM gsa_about_equipe WHERE statut = '".$statut."' ORDER BY id DESC LIMIT 0,1");
        $data_last      = mysqli_fetch_array($sql_last);
        $ordre          = $data_last['ordre']+1;
        $photo          = "";
        // upload photo
        if(! empty($_FILES['photo']['name'])){
            // renomme
            $extension  = pathinfo($_FILES['photo']['name'],PATHINFO_EXTENSION);
            $name       = 'GSA-'.rand(10,9999999999);
            $photo      = $name.".".$extension;
            upload_photo($photo,$_FILES['photo']['tmp_name'],'../images/','150');
        }
        // insert
        $insert         = mysqli_query($connexion, "INSERT INTO gsa_about_equipe (nom,poste,pole,photo,statut,ordre) VALUES
        ('".$nom."','".$poste."','".$pole."','".$photo."','".$statut."','".$ordre."')");
        $retour         = "success";
        $alerte         = "Le nouveau poste a bien été enregistré.";
    }

    #########################
    # UPDATE
    #########################
    if(! empty($_REQUEST['update'])){
        $id_update      = $_REQUEST['id_update'];
        $ordre_old      = $_REQUEST['ordre_old'];
        $ordre_new      = $_REQUEST['ordre'];
        $nom            = addslashes($_REQUEST['nom']);
        $poste          = addslashes($_REQUEST['poste']);
        $pole           = addslashes($_REQUEST['pole']);
        $statut         = addslashes($_REQUEST['statut']);
        // update infos
        $update_poste   = mysqli_query($connexion, "UPDATE gsa_about_equipe SET nom = '".$nom."',poste = '".$poste."',pole = '".$pole."',statut = '".$statut."',ordre = '".$ordre_old."' WHERE id = '".$id_update."'");
        // update ordre
        if($ordre_old != $ordre_new){
            $update_ordre1 = mysqli_query($connexion, "UPDATE gsa_about_equipe SET ordre = '".$ordre_old."' WHERE ordre = '".$ordre_new."'");
            $update_ordre2 = mysqli_query($connexion, "UPDATE gsa_about_equipe SET ordre = '".$ordre_new."' WHERE statut = '".$statut."' AND id = '".$id_update."'");
        }
        // update photo
        if(! empty($_FILES['photo']['name'])){
            // renomme
            $extension  = pathinfo($_FILES['photo']['name'],PATHINFO_EXTENSION);
            $name       = 'GSA-'.rand(10,9999999999);
            $photo      = $name.".".$extension;
            upload_photo($photo,$_FILES['photo']['tmp_name'],'../images/','150');
            $update_photo = mysqli_query($connexion, "UPDATE gsa_about_equipe SET photo = '".$photo."' WHERE id = '".$id_update."'");
        }
        $retour         = "success";
        $alerte         = "Le poste a bien été mis à jour.";
    }

    #########################
    # SUPPRIMER
    #########################
    if(! empty($_REQUEST['id_del'])){
        $id_del         = $_REQUEST['id_del'];
        $statut_del     = $_REQUEST['statut_del'];
        // recherche ordre du slide
        $sql_ordre      = mysqli_query($connexion, "SELECT * FROM gsa_about_equipe WHERE id = '".$id_del."' AND statut = '".$statut_del."'");
        $data_ordre     = mysqli_fetch_array($sql_ordre);
        $ordre          = $data_ordre['ordre'];
        // suppression du slide
        $delete         = mysqli_query($connexion, "DELETE FROM gsa_about_equipe WHERE id = '".$id_del."'");
        // actualisation des ordres
        $select_ordre   = mysqli_query($connexion, "SELECT * FROM gsa_about_equipe WHERE statut = '".$statut_del."' AND ordre > '".$ordre."'");
        while($row_ordre = mysqli_fetch_array($select_ordre)) {
            $ordre_new  = $row_ordre['ordre']-1;
            $update     = mysqli_query($connexion, "UPDATE gsa_about_equipe SET ordre = '".$ordre_new."' WHERE id ='".$row_ordre['id']."'");
        }
        // ok
        $retour         = "success";
        $alerte         = "Le poste à bien été supprimé.";
    }

    #########################
    # EDITER
    #########################
    if(! empty($_REQUEST['id'])){
        $sql_edit       = mysqli_query($connexion, "SELECT * FROM gsa_about_equipe WHERE id = '".$_REQUEST['id']."'");
        $data_edit      = mysqli_fetch_array($sql_edit);
        $val            = $data_edit['ordre'];
        // find dernier poste
        $sql_last       = mysqli_query($connexion, "SELECT * FROM gsa_about_equipe ORDER BY ordre DESC LIMIT 0,1");
        $data_last      = mysqli_fetch_array($sql_last);
        $val_max        = $data_last['ordre'];
        $cursor_moins   = "pointer";
        $cursor_plus    = "pointer";
        $disabled_moins = ''; if($data_edit['ordre'] == '1') { $disabled_moins = 'disabled'; $cursor_moins = "no-drop"; }
        $disabled_plus  = ''; if($data_edit['ordre'] == $data_last['ordre']) { $disabled_plus = 'disabled'; $cursor_plus = "no-drop"; }
        // options statut
        function options_statut($statut_reg){
            $array_statut = array('Mettre en avant sur fond blanc','Liste normale sur fond neutre');
            for($i=0;$i<=1;$i++){
                $statut = $i+1;
                $selected="";
                if($statut == $statut_reg){
                    $selected="selected=selected";
                }
                echo '<option value="'.$statut.'" '.$selected.'>'.$array_statut[$i].'</option>';
            }
        }
    }

    #########################
    # liste des postes
    #########################
    function liste_postes(){
        $sql = mysqli_query($connexion, "SELECT * FROM gsa_about_equipe ORDER BY statut,ordre ASC");
        while($row = mysqli_fetch_array($sql)){
            $color = ++$i % 2 ? '#ffffff':'#EAEAEA';
            echo '
            <tr class="tr_content" style="background:'.$color.'">
                <td class="td_content" valign="top">
                    <a href="index.php?page='.$_REQUEST['page'].'&part='.$_REQUEST['part'].'&action=edit&id='.$row['id'].'"><img src="../images/'.$row['photo'].'" width="80" /></a>
                </td>
                <td class="td_content" valign="top" style="text-align:left">
                    <b>'.stripslashes($row['nom']).'</b>
                    <p>'.stripslashes($row['poste']).'<br />'.stripslashes($row['pole']).'</p>
                </td>
                <td width="60" valign="top" style="padding:5px;">
                    <form name="del_fiche" method="post" action="" style="margin-top:17px">
                        <input type="hidden" name="id_del" value="'.$row['id'].'">
                        <input type="hidden" name="statut_del" value="'.$row['statut'].'">
                        <input id="submit_del" class="vtip" title="Supprimer" name="del" type="submit" value="" style="float:right" onClick="return confirm(\'\nEtes-vous sur de vouloir supprimer ce poste ?\n\n\');"><br /><br />
                        <a class="vtip" title="Editer" href="index.php?page='.$_REQUEST['page'].'&part='.$_REQUEST['part'].'&action=edit&id='.$row['id'].'">
                            <input id="submit_accept" type="button" value="" style="float:right;background:url(\'src/icons/page_white_edit.png\') no-repeat;">
                        </a>
                    </form>
                </td>
            </tr>';
        }
    }

}



####################################################################################################################
# HISTORIQUE
####################################################################################################################

if($part == 'historique'){

    #########################
    # AJOUTER
    #########################
    if(! empty($_REQUEST['submit_add'])){
        $annee          = addslashes($_REQUEST['annee']);
        $texte          = addslashes($_REQUEST['texte']);
        // insert
        $insert         = mysqli_query($connexion, "INSERT INTO gsa_about_historique (nom,poste,pole,photo,statut,ordre) VALUES
        ('".$nom."','".$poste."','".$pole."','".$photo."','".$statut."','".$ordre."')");
        $retour         = "success";
        $alerte         = "La nouvelle date a bien été enregistrée.";
    }

    #########################
    # UPDATE
    #########################
    if(! empty($_REQUEST['update'])){
        $id_update      = $_REQUEST['id_update'];
        $annee          = addslashes($_REQUEST['annee']);
        $texte          = addslashes($_REQUEST['texte']);
        // update date
        $update_poste   = mysqli_query($connexion, "UPDATE gsa_about_historique SET annee = '".$annee."',texte = '".$texte."' WHERE id = '".$id_update."'");
        $retour         = "success";
        $alerte         = "Le date a bien été mise à jour.";
    }

    #########################
    # SUPPRIMER
    #########################
    if(! empty($_REQUEST['id_del'])){
        $id_del         = $_REQUEST['id_del'];
        // suppression du slide
        $delete         = mysqli_query($connexion, "DELETE FROM gsa_about_historique WHERE id = '".$id_del."'");
        $retour         = "success";
        $alerte         = "La date à bien été supprimée.";
    }

    #########################
    # EDITER
    #########################
    if(! empty($_REQUEST['id'])){
        $sql_edit       = mysqli_query($connexion, "SELECT * FROM gsa_about_historique WHERE id = '".$_REQUEST['id']."'");
        $data_edit      = mysqli_fetch_array($sql_edit);
    }

    #########################
    # liste des dates
    #########################
    function liste_dates(){
        $sql = mysqli_query($connexion, "SELECT * FROM gsa_about_historique ORDER BY annee");
        while($row = mysqli_fetch_array($sql)){
            $color = ++$i % 2 ? '#ffffff':'#EAEAEA';
            echo '
            <tr class="tr_content" style="background:'.$color.'">
                <td class="td_content" valign="top">
                    <input type="text" class="text" name="annee" value="'.stripslashes($row['annee']).'" style="width:50px;text-align:center" />
                </td>
                <td class="td_content" valign="top" style="text-align:left;">
                    <input type="text" class="text" name="texte" value="'.stripslashes($row['texte']).'" style="width:450px;margin-right:10px" />
                    <input type="submit" name="update" value="Enregistrer" style="cursor:pointer;padding:2px 10px" />
                </td>
                <td width="60" style="padding:5px;">
                    <form name="del_fiche" method="post" action="">
                        <input type="hidden" name="id_del" value="'.$row['id'].'">
                        <input id="submit_del" class="vtip" title="Supprimer" name="del" type="submit" value="" style="float:right" onClick="return confirm(\'\nEtes-vous sur de vouloir supprimer cette date ?\n\n\');">
                    </form>
                </td>
            </tr>';
        }
    }

}
?>
