<?php
include_once "../include/display_errors_bo.php";
@session_start();

Debug::d_echo("acces ", 2,"tbl-formulaireco.php");
Debug::d_print_r($_GET, 1,"GET","tbl-formulaireco.php");
Debug::d_print_r($_POST, 1,"POST","tbl-formulaireco.php");
Debug::d_print_r($_SESSION, 1,"SESSION","tbl-formulaireco.php");


include_once "../../include/config.php";
@include_once '../include/pdo.php';
include_once "../include/framework.php";
include_once "../include/tableau.php";

$starttime = microtime();


/*
 * Variables communes
 */
$title = _CONF_FORM_CANDI_OFFRE_PAGE_TITLE;
$tableauName = "formulaireco";
$filename = "tbl-formulaireco.php";
$numberOfRowsPerPage = 50;
//$numberOfRows = 0;
$pageNumber = 1;
$debugMode = 0;
$orderColId = null;
$order = null;
$criteriasArray = array();
$messageToDisplay = null;


/*
 * Control d'accès
 */
if(empty($_SESSION['boe_user_id'])){
    $_SESSION['boe_user_id'] = null;
}
if(empty ($_SESSION['boe_user_email'])){
    $_SESSION['boe_user_email'] = null;
}
test_session_boe($_SESSION['boe_user_id'], $_SESSION['boe_user_email'], "../", $conn);

/*
 * Traitement des données d'entrées ($_GET)
 */
//print_t($_GET);

if(isset($_GET['pageNumber'])){
    if((int)$_GET['pageNumber'] != 0){
        $pageNumber = (int)$_GET['pageNumber'];
    }
}

if(isset($_GET['numberOfRowsPerPage'])){
    if((int)$_GET['numberOfRowsPerPage'] != 0){
        $numberOfRowsPerPage = (int)$_GET['numberOfRowsPerPage'];
    }
}

if(isset($_GET['orderColId'])){
    if((int)$_GET['orderColId'] != 0){
        $orderColId = (int)$_GET['orderColId'];
    }
}

if(isset($_GET['order'])){
    if($_GET['order'] == "up"){
        $order = "up";
    }else{
        $order = "down";
    }
}

if(isset($_GET['msg'])){
   $messageToDisplay = $_GET['msg'];
}

$criteriaId = 0;
while(isset($_GET['crit'.$criteriaId])){
    $criteriasArray[$criteriaId] = $_GET['crit'.$criteriaId];
    $criteriaId++;
}



/*
 * Init tableau
 */
$tableau = new Tableau($tableauName, $filename);
$tableau->setNumberOfRowsPerPage($numberOfRowsPerPage);
$tableau->setPageNumber($pageNumber);
$tableau->tableTitle = $title;
$tableau->messageToDisplay = $messageToDisplay;


/*
 * Définition des boutons
 */
$tblbutton = new TblButton();
$tblbutton->buttonLabel = _CONF_FORM_CANDI_OFFRE_TBL_ADD_BTN;
$tblbutton->altText = _CONF_FORM_CANDI_OFFRE_TBL_ADD_ALT;
$tblbutton->buttonpicture = "../images/folder_new.png";
$tblbutton->popupMode = true;
$tblbutton->needMultipleSelection = false;
$tblbutton->link = "v-add-elementco.php?";
$tblbutton->popupWidth = 450;
$tblbutton->popupTitle = _CONF_FORM_CANDI_OFFRE_TBL_ADD_POPUP_TITLE;
$tableau->addTblButton($tblbutton);




/*
 * Définition des critères de filtrage
 */
////Filtre 0 LISTE
//$fcriteria0 = new filterCriteria("clientType","list","Type de client");
//$fcriteria0->addListRow("", "Tous");
//$sqlfilter = "SELECT id, texte_".$_SESSION['langue']." as texte FROM typesClients";
//$selectfi = $conn->prepare($sqlfilter);
//$selectfi->execute();
//while($row = $selectfi->fetchObject()){
//    $fcriteria0->addListRow($row->id, $row->texte);
//}
//if(isset($criteriasArray[0]))$fcriteria0->selectedValue = $criteriasArray[0];
//$tableau->addFilterCriteria($fcriteria0);
//
////Filtre 1 LISTE
//$fcriteria1 = new filterCriteria("pays","list","Pays");
//$fcriteria1->addListRow("", "Tous");
//$sqlpays = "SELECT id, texte_".$_SESSION['langue']." as texte FROM paysInscription ";
//$selectpa = $conn->prepare($sqlpays);
//$selectpa->execute();
//while($row = $selectpa->fetchObject()){
//    $fcriteria1->addListRow($row->id, $row->texte);
//}
//if(isset($criteriasArray[1]))$fcriteria1->selectedValue = $criteriasArray[1];
//$tableau->addFilterCriteria($fcriteria1);
//
////Filtre 2 DATE
//$fcriteria2 = new filterCriteria("datec","date","Date crÃ©ation");
//if(isset($criteriasArray[2]))$fcriteria2->selectedValue = $criteriasArray[2];
//$tableau->addFilterCriteria($fcriteria2);




/*
 * Définition des columns du tableau
 */
$colheader = array();
//Checkbox
//$colheader[]= array("content"=>"",
//                    "align"=>"",
//                    "dbaColumnName"=>"",
//                    "orderable"=>"0",
//                    "type"=>"checkbox");
//
$colheader[]= array("content"=>_CONF_FORM_CANDI_OFFRE_TBL_COL_ELEMENT,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

//
$colheader[]= array("content"=>_CONF_FORM_CANDI_OFFRE_TBL_COL_POSITION,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

$colheader[]= array("content"=>_CONF_FORM_CANDI_OFFRE_TBL_COL_OBLI,
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

$colheader[]= array("content"=>"Treelist",
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");

//$colheader[]= array("content"=>"Options",
//                    "align"=>"",
//                    "dbaColumnName"=>"",
//                    "orderable"=>"0",
//                    "type"=>"custom");

$colheader[]= array("content"=>"",
                    "align"=>"",
                    "dbaColumnName"=>"",
                    "orderable"=>"0",
                    "type"=>"custom");


//CrÃ©ation des columns
$tableau->setHeaderRow($colheader);

//dÃ©finition de l'ordre sur la column selectionnÃ©e
if($orderColId != null){
    $tableau->setOrderedColumn($orderColId, $order);
}





/*
 * Requete sql des Ã©lÃ©ments Ã  afficher
 */
//Traitement de l'ordre d'affichage
$orderSql = "";
$orderByTBLName = $tableau->getOrderSQLColumnName();
//GÃ©nÃ©ration du order sql
if($orderByTBLName){
    if($orderSql == ""){
        $orderSql = "ORDER BY `".$orderByTBLName."`";
        if($order == "up"){
            $orderSql .= " ASC";
        }else{
            $orderSql .= " DESC";
        }
    }
}

//Traitement manuel des filtres


/*
 * RequÃªte SQL de comptage
 */
//RÃ©cupÃ©ration de l'id client
$sqlcount = "   SELECT
                    COUNT(fo.ID)
                    
                FROM
                    `awa_formulaireco_composition` AS fo
                
                ".$tableau->getFilterWhereSQL("")."
            ";
//print_t($sqlcount) ;
$select = $conn->prepare($sqlcount);
$select->execute();
$tmp  = $select->fetch();
$nbOfSQLrows = $tmp[0];
//Envoi dans le tableau le nombre de row dans la dba
$tableau->setNumberOfSQLRows($nbOfSQLrows);
//Calcul de la condition limit pour le requete sql
$limit = ($pageNumber - 1) * $numberOfRowsPerPage;

//Requete principal
$sql = "SELECT
            `fo`.*,
            fe.LANGUAGE_KEY
            
        FROM
            `awa_formulaireco_composition` AS `fo`
        INNER JOIN awa_formulaire_elements AS fe ON fe.id = fo.ID_FORMULAIRE_ELEMENT

        ".$tableau->getFilterWhereSQL("")."
        GROUP BY `fo`.`ID`

        ORDER BY fo.POSITION ASC
        LIMIT ".$limit.",".$numberOfRowsPerPage;

//print_t($sql);
$select = $conn->prepare($sql);
//$select->bindParam(':recruid', $_SESSION['boe_user_id'], PDO::PARAM_INT);
$select->execute();


/*
 * Remplissage du tableau
 */
while($row = $select->fetchObject()){
    
    $colrow = array();

    $colrow[]= array("content"=>"<a class=\"orange\" href=\"".$tableau->getAjaxPopupLink('v-add-elementco.php?element_id='.$row->ID,450,_CONF_FORM_CANDI_OFFRE_TBL_COL_ELEMENT_ACT_POPUP)."\" title=\""._CONF_FORM_CANDI_OFFRE_TBL_COL_ELEMENT_ACT_ALT."\">".constant($row->LANGUAGE_KEY)."</a>","align"=>"");
    $colrow[]= array("content"=>$row->POSITION,"align"=>"");
    if($row->COMPULSORY == "1"){
        $imcompulsory = "<img src=\"../images/check.png\" alt=\""._CONF_FORM_CANDI_OFFRE_TBL_COL_OBLI_ALT."\" />";
    }else{
        $imcompulsory = "<img src=\"../images/uncheck.png\" alt=\""._CONF_FORM_CANDI_OFFRE_TBL_COL_OBLI_ALT."\" />";
    }
    $colrow[]= array("content"=>$imcompulsory,"align"=>"");
    $colrow[]= array("content"=>$row->VALEUR12,"align"=>"");
    $colrow[]= array("content"=>"<a href=\"javascript:actionAjax_".$tableau->tablename."('act-formulaireco.php?action=del&id=".$row->ID."','".urlencode(constant($row->LANGUAGE_KEY)." "._CONF_FORM_CANDI_OFFRE_TBL_COL_DEL_RETURN)."','"._CONF_FORM_CANDI_OFFRE_TBL_COL_DEL_ASK." ".constant($row->LANGUAGE_KEY)."');\" title=\""._CONF_FORM_CANDI_OFFRE_TBL_COL_DEL_ALT."\"><img src='../images/trash.png'/ height='16' title=\""._CONF_FORM_CANDI_OFFRE_TBL_COL_DEL_ALT."\" alt=\""._CONF_FORM_CANDI_OFFRE_TBL_COL_DEL_ALT."\"/></a>","align"=>"");
    
    $tableau->addRow($colrow);
}


/*
 * Affichage du tableau
 */ 
echo $tableau->displayHTML();
//print_t($_SESSION[]);
//print_t($fcriteria1);
//print_t($tableau);

//print_t($_SESSION);


$endtime = microtime();
//echo($endtime - $starttime);
?>