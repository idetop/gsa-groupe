<?php
define("_PATWHAY_HOME","Accueil");

define("_MENU_HOME","Accueil");
define("_MENU_OFFRE","Les offres");
define("_MENU_ESPACECANDIDAT","Espace candidats");
define("_MENU_CANDIDATURESPONTANEE","Candidature spontanée");

define("_CANDIDATURE_OFFRE_TITLE","Candidature");

define("_CANDIDATURE_SPONTANE_TITLE","Candidature spontanée");
define("_CANDIDATURE_EX_NAME","Nom");
define("_CANDIDATURE_EX_NAME_JSERROR","Veuillez saisir un nom");
define("_CANDIDATURE_EX_NAME_TR_JSERROR","Veuillez sélectionner un nom");
define("_CANDIDATURE_EX_FIRST_NAME","Prénom");
define("_CANDIDATURE_EX_FIRST_NAME_TR_JSERROR","Veuillez sélectionner un prénom");
define("_CANDIDATURE_EX_CIVILITY","Civilité");
define("_CANDIDATURE_EX_CIVILITY_DEFAULT","Sélectionnez une civilité");
define("_CANDIDATURE_EX_CIVILITY_M","Monsieur");
define("_CANDIDATURE_EX_CIVILITY_MM","Madame");
define("_CANDIDATURE_EX_CIVILITY_ML","Mademoiselle");
define("_CANDIDATURE_EX_CIVILITY_JSERROR","Veuillez sélectionner une civilité");
define("_CANDIDATURE_EX_ADDRESS","Adresse");
define("_CANDIDATURE_EX_ADDRESS_JSERROR","Veuillez saisir une adresse");
define("_CANDIDATURE_EX_ADDRESS_TR_JSERROR","Veuillez sélectionner une adresse");
define("_CANDIDATURE_EX_ZIP","Code postal");
define("_CANDIDATURE_EX_ZIP_JSERROR","Veuillez saisir un code postal");
define("_CANDIDATURE_EX_ZIP_TR_JSERROR","Veuillez sélectionner un code postal");
define("_CANDIDATURE_EX_CITY","Ville");
define("_CANDIDATURE_EX_CITY_JSERROR","Veuillez saisir une ville");
define("_CANDIDATURE_EX_CITY_TR_JSERROR","Veuillez sélectionner une ville");
define("_CANDIDATURE_EX_PROVINCE","Région");
define("_CANDIDATURE_EX_PROVINCE_JSERROR","Veuillez saisir une région");
define("_CANDIDATURE_EX_PROVINCE_TR_JSERROR","Veuillez sélectionner une région");
define("_CANDIDATURE_EX_COUNTRY","Pays");
define("_CANDIDATURE_EX_COUNTRY_JSERROR","Veuillez saisir un pays");
define("_CANDIDATURE_EX_COUNTRY_TR_JSERROR","Veuillez sélectionner un pays");
define("_CANDIDATURE_EX_PHONE_PERSO","Téléphone");
define("_CANDIDATURE_EX_PHONE_PERSO_JSERROR","Veuillez saisir un numéro de téléphone personnel");
define("_CANDIDATURE_EX_PHONE_PERSO_TR_JSERROR","Veuillez sélectionner un numéro de téléphone personnel");
define("_CANDIDATURE_EX_PHONE_MOBILE","Mobile");
define("_CANDIDATURE_EX_PHONE_MOBILE_JSERROR","Veuillez saisir un numéro de mobile");
define("_CANDIDATURE_EX_PHONE_MOBILE_TR_JSERROR","Veuillez sélectionner un numéro de mobile");
define("_CANDIDATURE_EX_PHONE_PRO","Téléphone pro");
define("_CANDIDATURE_EX_PHONE_PRO_JSERROR","Veuillez saisir un numéro de téléphone professionnel");
define("_CANDIDATURE_EX_PHONE_PRO_TR_JSERROR","Veuillez sélectionner un numéro de téléphone professionnel");
define("_CANDIDATURE_EX_FAX","Fax");
define("_CANDIDATURE_EX_FAX_JSERROR","Veuillez saisir un numéro de fax");
define("_CANDIDATURE_EX_FAX_TR_JSERROR","Veuillez sélectionner un numéro de fax");
define("_CANDIDATURE_EX_BIRTH_DATE","Date de naissance (dd-mm-yyyy)");
define("_CANDIDATURE_EX_BIRTH_DATE_JSERROR","Veuillez saisir une date de naissance");
define("_CANDIDATURE_EX_BIRTH_DATE_JSERROR2","Veuillez saisir la date au format dd-mm-yyyy");
define("_CANDIDATURE_EX_MARITAL_STATUS","Situation personnelle");
define("_CANDIDATURE_EX_MARITAL_STATUS_JSERROR","Veuillez saisir une situation personnelle");
define("_CANDIDATURE_EX_MARITAL_STATUS_TR_JSERROR","Veuillez sélectionner une situation personnelle");
define("_CANDIDATURE_EX_CHILDREN_COUNT","Nombre d'enfant");
define("_CANDIDATURE_EX_CHILDREN_COUNT_JSERROR","Veuillez saisir un nombre d\'enfants");
define("_CANDIDATURE_EX_CHILDREN_COUNT_JSERROR2","Veuillez saisir uniquement des chiffres pour le nombre d\'enfants");
define("_CANDIDATURE_EX_NATIONALITY","Nationnalité");
define("_CANDIDATURE_EX_NATIONALITY_JSERROR","Veuillez saisir une nationnalité");
define("_CANDIDATURE_EX_NATIONALITY_TR_JSERROR","Veuillez sélectionner une nationnalité");
define("_CANDIDATURE_EX_EMAIL_PRO","Email pro");
define("_CANDIDATURE_EX_EMAIL_PRO_JSERROR","Veuillez saisir une adresse email professionnelle");
define("_CANDIDATURE_EX_EMAIL_PRO_JSERROR2","Veuillez saisir une adresse email professionnelle valide");
define("_CANDIDATURE_EX_EMAIL_PERSO","Email perso");
define("_CANDIDATURE_EX_EMAIL_JSERROR","Veuillez saisir une adresse e-mail valide");

define("_CANDIDATURE_EX_EMAIL_PERSO_JSERROR","Veuillez saisir une adresse email personnelle");
define("_CANDIDATURE_EX_EMAIL_PERSO_JSERROR2","Veuillez saisir une adresse email personnelle valide");

define("_CANDIDATURE_EX_EXPERIENCES_BLOC","Expériences");
define("_CANDIDATURE_EX_EXPERIENCES_ADD","Ajouter une expérience");
define("_CANDIDATURE_EX_EXPERIENCES_DEL","Supprimer");
define("_CANDIDATURE_EX_ENPOSTE","En poste");
define("_CANDIDATURE_EX_EXP_START","Date début (mm-yyyy)");
define("_CANDIDATURE_EX_EXP_START_JSERROR","Veuillez saisir une date de début");
define("_CANDIDATURE_EX_EXP_START_JSERROR2","Veuillez saisir la date au format mm-yyyy");
define("_CANDIDATURE_EX_EXP_END","à");
define("_CANDIDATURE_EX_EXP_END_JSERROR","Veuillez saisir une date de fin");
define("_CANDIDATURE_EX_EXP_END_JSERROR2","Veuillez saisir la date au format mm-yyyy");
define("_CANDIDATURE_EX_EXPERIENCE","Fonction");
define("_CANDIDATURE_EX_EXPERIENCE_JSERROR","Veuillez saisir une fonction");
define("_CANDIDATURE_EX_EXPERIENCE_TR_JSERROR","Veuillez sélectionner une fonction");
define("_CANDIDATURE_EX_EXP_COMPANY","Société");
define("_CANDIDATURE_EX_EXP_COMPANY_JSERROR","Veuillez saisir une société");
define("_CANDIDATURE_EX_EXP_COMPANY_TR_JSERROR","Veuillez sélectionner une société");

define("_CANDIDATURE_SALARY_FIXE","Salaire fixe");
define("_CANDIDATURE_SALARY_FIXE_JSERROR","Veuillez saisir le salaire fixe");
define("_CANDIDATURE_SALARY_FIXE_JSERROR2","Veuillez saisir uniquement des chiffres pour le salaire fixe");
define("_CANDIDATURE_SALARY_VARIABLE","Salaire variable");
define("_CANDIDATURE_SALARY_VARIABLE_JSERROR","Veuillez saisir le salaire variable");
define("_CANDIDATURE_SALARY_VARIABLE_JSERROR2","Veuillez saisir uniquement des chiffres pour le salaire variable");
define("_CANDIDATURE_SALARY_TOTAL","Salaire total");
define("_CANDIDATURE_SALARY_TOTAL_JSERROR","Veuillez saisir le salaire total");
define("_CANDIDATURE_SALARY_TOTAL_JSERROR2","Veuillez saisir uniquement des chiffres pour le salaire total");
define("_CANDIDATURE_SALARY_ADVANTAGE","Avantage");
define("_CANDIDATURE_SALARY_ADVANTAGE_JSERROR","Veuillez saisir un avantage");
define("_CANDIDATURE_SALARY_ADVANTAGE_TR_JSERROR","Veuillez sélectionner un avantage");

define("_CANDIDATURE_EX_FORMATIONS_BLOC","Formations");
define("_CANDIDATURE_EX_FORMATIONS_ADD","Ajouter une formation");
define("_CANDIDATURE_EX_FORMATIONS_DEL","Supprimer");
define("_CANDIDATURE_EX_EDUCATION","Intitulé");
define("_CANDIDATURE_EX_EDUCATION_JSERROR","Veuillez saisir un intitulé");
define("_CANDIDATURE_EX_EDUCATION_TR_JSERROR","Veuillez sélectionner un intitulé");
define("_CANDIDATURE_EX_EDU_SCHOOL","Etablissement");
define("_CANDIDATURE_EX_EDU_SCHOOL_JSERROR","Veuillez saisir un établissement");
define("_CANDIDATURE_EX_EDU_SCHOOL_TR_JSERROR","Veuillez sélectionner un établissement");
define("_CANDIDATURE_EX_EDU_START","Date fin (yyyy)");
define("_CANDIDATURE_EX_EDU_START_JSERROR","Veuillez saisir une date de début");
define("_CANDIDATURE_EX_EDU_START_JSERROR2","Veuillez saisir la date au format mm-yyyy");
define("_CANDIDATURE_EX_EDU_END","à");
define("_CANDIDATURE_EX_EDU_END_JSERROR","Veuillez saisir une date de fin");
define("_CANDIDATURE_EX_EDU_END_JSERROR2","Veuillez saisir la date au format yyyy");
define("_CANDIDATURE_EX_EDU_LEVEL","Niveau du dipôme");
define("_CANDIDATURE_EX_EDU_LEVEL_JSERROR","Veuillez saisir un niveau");
define("_CANDIDATURE_EX_EDU_LEVEL_TR_JSERROR","Veuillez sélectionner un niveau");

define("_CANDIDATURE_EX_LANGUES_BLOC","Langues");
define("_CANDIDATURE_EX_LANGUES_ADD","Ajouter une langue");
define("_CANDIDATURE_EX_LANGUES_DEL","Supprimer");
define("_CANDIDATURE_EX_LANG","Langue");
define("_CANDIDATURE_EX_LANG_JSERROR","Veuillez saisir une langue");
define("_CANDIDATURE_EX_LANG_TR_JSERROR","Veuillez sélectionner une langue");
define("_CANDIDATURE_EX_LANG_LEVEL","Niveau");
define("_CANDIDATURE_EX_LANG_LEVEL_JSERROR","Veuillez saisir un niveau");
define("_CANDIDATURE_EX_LANG_LEVEL_TR_JSERROR","Veuillez sélectionner un niveau");

define("_CANDIDATURE_EX_AVAILABILITY","Disponibilité");
define("_CANDIDATURE_EX_AVAILABILITY_JSERROR","Veuillez saisir une disponibilité");
define("_CANDIDATURE_EX_AVAILABILITY_TR_JSERROR","Veuillez sélectionner une disponibilité");
define("_CANDIDATURE_EX_DRIVING_LICENCE","Permis");
define("_CANDIDATURE_EX_DRIVING_LICENCE_JSERROR","Veuiller saisir un permis");
define("_CANDIDATURE_EX_COMMENT","Commentaires");
define("_CANDIDATURE_EX_COMMENT_JSERROR","Veuillez saisir un commentaire");
define("_CANDIDATURE_EX_COMMENT_TR_JSERROR","Veuillez sélectionner un commentaire");
define("_CANDIDATURE_EX_MOBILITY","Mobilités");
define("_CANDIDATURE_EX_MOBILITY_JSERROR","Veuillez saisir une mobilité");
define("_CANDIDATURE_EX_MOBILITY_TR_JSERROR","Veuillez sélectionner une mobilité");
define("_CANDIDATURE_EX_OBJECTIVE","Objectifs");
define("_CANDIDATURE_EX_OBJECTIVE_JSERROR","Veuillez saisir un objectif");
define("_CANDIDATURE_EX_PUBLICATION","Publications");
define("_CANDIDATURE_EX_PUBLICATION_JSERROR","Veuillez saisir une publication");
define("_CANDIDATURE_EX_REFERENCES","Références");
define("_CANDIDATURE_EX_REFERENCES_JSERROR","Veuillez saisir une référence");
define("_CANDIDATURE_EX_SKILLS","Compétences");
define("_CANDIDATURE_EX_SKILLS_JSERROR","Veuillez saisir une compétence");

define("_CANDIDATURE_EX_CV","CV");
define("_CANDIDATURE_EX_NEWCV","Nouveau CV");
define("_CANDIDATURE_EX_CV_JSERROR","Veuillez sélectionner un CV");
define("_CANDIDATURE_EX_CV_JSERROR2","Veuillez sélectionner un CV au format : pdf, doc, docx, rtf ou ppt");
define("_CANDIDATURE_EX_LM","Lettre de motivation");
define("_CANDIDATURE_EX_NEWLM","Nouvelle lettre de motivation");
define("_CANDIDATURE_EX_LM_JSERROR","Veuillez sélectionner une lettre de motivation");
define("_CANDIDATURE_EX_LM_JSERROR2","Veuillez sélectionner une lettre de motivation au format : pdf, doc, docx, rtf ou ppt");

define("_CANDIDATURE_NEW_PASSWORD","Nouveau mot de passe");
define("_CANDIDATURE_NEW_PASSWORD_COPY","Confirmation nouveau mot de passe");
define("_CANDIDATURE_NEW_PASSWORD_JSERROR","Les mots de passe ne sont pas identiques");

define("_CANDIDATURE_SECU_LABEL","Veuillez saisir le résultat de la somme ci-dessous");
define("_CANDIDATURE_SECU_ERROR","Veuillez saisir le résultat de la somme");
define("_CANDIDATURE_SECU_ERROR2","Résultat érroné");

define("_CANDIDATURE_EMAIL_PRESENCE_ERROR","Pour continuer vous devez vous connecter avec votre e-mail et mot de passe.\\nOu sinon, vous devez changer l\'e-mail dans le formulaire");
define("_CANDIDATURE_EMAIL_PRESENCE_ASKTOCONNECT","Voulez-vous vous connecter ?");

define("_CANDIDATURE_EX_SECTOR","Secteur");
define("_CANDIDATURE_EX_SECTOR_JSERROR","Veuillez saisir un secteur");
define("_CANDIDATURE_EX_SECTOR_TR_JSERROR","Veuillez sélectionner un secteur");
define("_CANDIDATURE_EX_FUNCTION","Fonction");
define("_CANDIDATURE_EX_FUNCTION_JSERROR","Veuillez saisir une fonction");
define("_CANDIDATURE_EX_FUNCTION_TR_JSERROR","Veuillez sélectionner une fonction");

define("_CANDIDATURE_EX_CUSTOM","Champ custom");

define("_CANDIDATURE_CS_SUBMIT","ENVOYER");

define("_OFFRE_LISTE_TITLE","Liste des offres");
define("_OFFRE_LISTE_RECHERCHE_TITLE","Recherche avancée");
define("_OFFRE_LISTE_RECHERCHE_FONCTION_LABEL","Fonction");
define("_OFFRE_LISTE_RECHERCHE_FONCTION_DEFAULT","Toutes les fonctions");
define("_OFFRE_LISTE_RECHERCHE_SECTEUR_LABEL","Secteur");
define("_OFFRE_LISTE_RECHERCHE_SECTEUR_DEFAULT","Tous les secteurs");
define("_OFFRE_LISTE_RECHERCHE_LOCATION_LABEL","Lieu");
define("_OFFRE_LISTE_RECHERCHE_KEYWORD_LABEL","Mots clés");
define("_OFFRE_LISTE_RECHERCHE_SUBMIT","RECHERCHER");
define("_OFFRE_LISTE_NORESULT_MSG","Il n'y aucun résultat. Choisissez d'autres critères");
define("_OFFRE_LISTE_PAGINATION_DEBUT","première");
define("_OFFRE_LISTE_PAGINATION_PREC","précédant");
define("_OFFRE_LISTE_PAGINATION_SUIV","suivant");
define("_OFFRE_LISTE_PAGINATION_FIN","dernière");
define("_OFFRE_LISTE_SALARY_TO","à");

define("_OFFRE_FICHE_REF_LABEL","Réf");
define("_OFFRE_FICHE_INFORMATION_COMPLEMENTAIRE_LABEL","Informations complémentaires");
define("_OFFRE_FICHE_LIEU_LABEL","Lieu");
define("_OFFRE_FICHE_CONTRAT_LABEL","Type contrat");
define("_OFFRE_FICHE_SALAIRE_LABEL","Salaire");
define("_OFFRE_FICHE_POSTE_LABEL","Poste");
define("_OFFRE_FICHE_LOCALISER_ALT","Localiser");
define("_OFFRE_FICHE_DATEPARUTION_LABEL","Date de parution");

define("_OFFRE_FICHE_RETOUR_BTN","RETOUR");
define("_OFFRE_FICHE_POSTULER_BTN","POSTULER");


define("_ESPACECANDIDAT_TITLE","Espace candidat");
define("_ESPACECANDIDAT_INTRO_TEXT","Vous n'êtes pas actuellement identifié. Veuillez saisir votre email et mot de passe pour accéder à votre espace candidat.");
define("_ESPACECANDIDAT_LOGIN_FORM_EMAIL_LABEL","Email");
define("_ESPACECANDIDAT_LOGIN_FORM_EMAIL_JSERROR","Veuillez saisir votre email");
define("_ESPACECANDIDAT_LOGIN_FORM_PASS_LABEL","Mot de passe");
define("_ESPACECANDIDAT_LOGIN_FORM_PASS_JSERROR","Veuillez saisir votre mot de passe");
define("_ESPACECANDIDAT_LOGIN_FORM_SUBMIT","CONNEXION");
define("_ESPACECANDIDAT_MDP_OUBLI_LINK","Mot de passe oublié ?");

define("_ESPACECANDIDAT_PROFIL_TITLE","Votre profil");
define("_ESPACECANDIDAT_PROFIL_SAVED_MESSAGE","Votre profil a été sauvegardé");

define("_ESPACECANDIDAT_BONJOUR","Bonjour");
define("_ESPACECANDIDAT_DECONNEXION","vous déconnecter");

define("_MENTIONLEGAL_TITLE","Mentions légales");

define("_HOMEPAGE_TITLE","Accueil");

define("_CANDIDATURE_FIN_TITLE","Candidature fin ");

define("_MDPOUBLIE_TITLE","Mot de passe oublié");
define("_MDPOUBLIE_INTRO_TEXT","Vous avez oublié votre mot de passe? Veuillez saisir votre adresse e-mail dans le formulaire ci-dessous.<br/>Un e-mail vous sera envoyé avec votre mot de passe.");
define("_MDPOUBLIE_SEND_BTN","ENVOYER");
define("_MDPOUBLIE_MESSAGE_OK","Mail envoyé avec succès");
define("_MDPOUBLIE_MESSAGE_NOK","E-mail non reconnu");

define("_FOOTER_MENTIONLEGAL_LINK_TEXT","Mentions légales");
define("_FOOTER_MENTIONLEGAL_LINK_ALT","Mentions légales");

define("_FOOTER_COPYRIGHT","Copyright 2010");


define("_CVREADER_UPLOAD_INTROTEXT","Veuillez uploader votre CV. Il sera utilisé pour préremplir le formulaire de candidature.<br/>Si vous ne souhaitez pas utiliser cette fonctionnalité. Cliquez sur le bouton 'Envoyer' sans sélectionner de fichier.");
define("_CVREADER_UPLOAD_LABEL","Uploadez votre CV");
define("_CVREADER_UPLOAD_BTN","Envoyer");


/*
 * envoi de mail
 */

define("_EMAIL_OBJET_CANDIDATURE_SPONTANEE","AWA : Candidature spontanée");
define("_EMAIL_OBJET_CANDIDATURE_OFFRE","AWA : Candidature offre"); //+ ref=$refAnnonce
define("_EMAIL_OBJET_MAJ_CANDIDAT_PROFIL","AWA : Maj candidat");

define("_CANDIDATURE_EX_FIRST_NAME_JSERROR","Veuillez saisir un prénom");
?>
