<?php
//Créé 

//ini_set('error_log','1');
//error_reporting(E_ALL);
//error_reporting(E_ALL | E_STRICT | E_PARSE);
//ini_set('display_startup_errors','1');
//ini_set('display_errors','1');
//

define("_LOGS_FOLDER_PATHWAY","/homez.218/gsagroup/www/recrutement/AWA/log/");


$date=date('Y-m-d');
$path=_LOGS_FOLDER_PATHWAY;
$filename='AWA_FO_ERROR_'.$date.'.txt';


if(!file_exists($path.$filename))
{
    touch($path.$filename);
}

ini_set('error_log',$path.$filename);

//@include_once 'pdo.php';
//
class Debug {

    static $levelToDisplay = 0;    //0 actions+datas+sql //1 actions+datas //2 actions

    static public function d_echo($str,$level,$file = "",$row = "") {
        if($level >= Debug::$levelToDisplay){
            $date=date('Y-m-d');
            $path=_LOGS_FOLDER_PATHWAY;
            $filename='AWA_FO_DEBUG_'.$date.'.txt';

            $output ="[".date('Y-m-d D : H.i.s')."] | ".$_SERVER['REMOTE_ADDR']." | ".session_id()." | ".$file." | ".$row." | ".$str;
            $output.="\n";



            $file=fopen($path.$filename,'a');
                fwrite($file, utf8_decode($output));
            fclose($file);
        }
    }

    static public function d_print_r($var,$level,$prefix="",$file = "",$row = "") {
        if($level >= Debug::$levelToDisplay){
            $date=date('Y-m-d');
            $path=_LOGS_FOLDER_PATHWAY.'details/';
//            $filename='AWA_FO_DEBUG_'.$date.'.txt';
            $filename = $_SERVER['REMOTE_ADDR'].'-'.session_id().'.txt';

            $output="[".date('Y-m-d D : H.i.s')."] ".$file." | ".$row." |".$prefix."\n".print_r($var,true);
            $output.="\n";



            $file=fopen($path.$filename,'a');
                fwrite($file, utf8_decode($output));
            fclose($file);
        }

    }

    static public function d_sql_error($select,$level,$prefix="",$file = "",$row = "") {
        if($level >= Debug::$levelToDisplay){
            $date=date('Y-m-d');
            $path=_LOGS_FOLDER_PATHWAY.'details/';
//            $filename='AWA_FO_DEBUG_'.$date.'.txt';
            $filename = $_SERVER['REMOTE_ADDR'].'-'.session_id().'.txt';

            $output="[".date('Y-m-d D : H.i.s')."] ".$file." | ".$row." |".$prefix."\n".print_r($select->errorInfo(),true);
            $output.="\n";



            $file=fopen($path.$filename,'a');
                fwrite($file, utf8_decode($output));
            fclose($file);
        }
    }

}


?>
