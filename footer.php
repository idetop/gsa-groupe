<!-- FOOTER -->
<div id="footer">
	<div id="ss-footer">
		<div class="bloc_pole">
			<a href="http://gsa-groupe.fr" class="retour-gsa">
			<img src="images/spacer-footer.gif" />
		</a>
			<div class="blocs-footer">
				<a href="http://gsa-medical.fr" class="lien_pole"><img src="images/carre-medical2.jpg" alt="" /></a>
				<a href="http://gsa-pharma.fr" class="lien_pole"><img src="images/carre-pharma2.jpg" alt="" /></a>
				<a href="http://gsa-formation.fr" class="lien_pole"><img src="images/carre-formation2.jpg" alt="" /></a>
				<a href="http://gsa-recrutement.fr" class="lien_pole"><img src="images/carre-recrutement2.jpg" alt="" /></a>
			</div>
		</div>
		<div>
            <span><a href="http://maps.google.fr/maps?q=Gerard+Sinabian+Associes+-+GSA,+Boulogne-Billancourt&hl=fr&ie=UTF8&sll=50.972242,1.493266&sspn=1.400924,3.56781&oq=gsa+boulogn&hq=Gerard+Sinabian+Associes+-+GSA,&hnear=Boulogne-Billancourt,+Hauts-De-Seine,+%C3%8Ele-de-France&t=m&z=14&iwloc=A" target="_blank">plan d'acc&egrave;s</a></span>
			<h2>Contact</h2>
			<p style="color:#6D6D6D">
				<?php info_poles($pole,'tel'); ?><br />
				<?php info_poles($pole,'adresse'); ?><br />
				<?php info_poles($pole,'cp'); ?> <?php info_poles($pole,'ville'); ?>
			</p>
		</div>
		<br style="clear: left" /><br /><br />
		<p class="lien_footer">
			<a href="http://<?= $_SERVER['SERVER_NAME']; ?>">Accueil</a>&nbsp;&nbsp;/&nbsp;&nbsp;
			<a href="http://gsa-groupe.fr">GSA Groupe GTF</a>&nbsp;&nbsp;/&nbsp;&nbsp;
			<a href="http://gsa-medical.fr">Vente et Promotion m&eacute;dicale</a>&nbsp;&nbsp;/&nbsp;&nbsp;
			<a href="http://gsa-formation.fr">Espace Management et Formation</a>&nbsp;&nbsp;/&nbsp;&nbsp;
			<a href="http://gsa-recrutement.fr">Espace Recrutement</a>&nbsp;&nbsp;/&nbsp;&nbsp;
			<a href="http://gsa-recrutement.fr/AWA/espace-candidat.htm" target="_blank">Espace Candidat</a>&nbsp;&nbsp;/&nbsp;&nbsp;
            <a href="http://gsa-groupe.fr/mentions.php" target="_blank">Mentions L&eacute;gales</a>
		</p>
		<br /><br />
	</div>
</div>

<?php
if (str_replace("www.","",$_SERVER['SERVER_NAME']) == 'gsa-groupe.fr')      { $account = 1; }
if (str_replace("www.","",$_SERVER['SERVER_NAME']) == 'gsa-formation.fr')   { $account = 2; }
if (str_replace("www.","",$_SERVER['SERVER_NAME']) == 'gsa-pharma.fr')      { $account = 3; }
if (str_replace("www.","",$_SERVER['SERVER_NAME']) == 'gsa-recrutement.fr') { $account = 4; }
if (str_replace("www.","",$_SERVER['SERVER_NAME']) == 'gsa-medical.fr')     { $account = 5; }
?>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-29344630-<?= $account; ?>']);
    _gaq.push(['_trackPageview']);
    (function(){
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>

