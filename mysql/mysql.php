<?php

// General stuff
// should not be here,
// but this was the only file that
// is called throughout the app

// Composer
require __DIR__ . '/../vendor/autoload.php';

// Load environment configuration
$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

// Debug
if (getenv('APP_DEBUG') == true) {
    error_reporting(E_ALL & ~E_NOTICE);
    @ini_set('display_errors', 'on');
    define('_PS_DEBUG_SQL_', true);
} else {
    @ini_set('display_errors', 'off');
    define('_PS_DEBUG_SQL_', false);
}

// Database connexion
// ==================

/**
 * Connect to the database
 * @return Object|string|null The object representing the connection to the MySQL Server
 */
function databaseConnect()
{
    // Define connection as a static variable, to avoid connecting more than once
    static $connection;

    // Try and connect to the database, if a connection has not been established yet
    if (! isset($connection)) {
        $connection = mysqli_connect(getenv('DB_HOST'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'), getenv('DB_DATABASE'));
    }

    // If connection was not successful, handle the error
    if (! $connection) {
        return mysqli_connect_error();
    }

    return $connection;
}

// Necessary to convert from old `mysql` PHP code

$connexion = databaseConnect();
mysqli_query($connexion, "SET NAMES 'utf8'");

/**
 * A replacement for missing `mysqli_result`
 * @param  [type]  $res   [description]
 * @param  [type]  $row   [description]
 * @param  integer $field [description]
 * @return [type]         [description]
 */
function mysqli_result($res, $row, $field = 0)
{
    $res->data_seek($row);

    $datarow = $res->fetch_array();

    return $datarow[$field];
}


##################################
# infos pole
##################################
function info_poles($poleload, $rowinfo)
{
    global $connexion;

    if (! empty($poleload)) {
        $sql = mysqli_query($connexion, "SELECT * FROM gsa_admin WHERE pole = '" . $poleload . "'");
    } else {
        $sql = mysqli_query($connexion, "SELECT * FROM gsa_admin WHERE pole = 'gsa'");
    }
    $dat = mysqli_fetch_array($sql);
    echo stripslashes($dat[$rowinfo]);
}

