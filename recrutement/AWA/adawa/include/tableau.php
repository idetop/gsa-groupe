<?php
/* é
 * Class de gestion de tableau html
 */

class Column{
    
    var $columnName = null;             //Le nom de la column
    var $columnType = null;             //le type, "checkbox" pour liste de checkbox et "custom" pour le reste

    var $headerContent = null;          //Titre de la colonne affiché
    var $rowsContent = array();         //Le contenu des chaques lignes de la colonne
    var $rowsId = 0;

    var $alignHeader = "center";        //center;left;right
    var $alignRow = "center";           //center;left;right

    var $orderable = false;             //True si on veut gérer un tri sur la colonne, ATTENTION lié à la DBA
    var $order = "none";                //up;down;none

    var $dbaColumnName = null;          //le nom de la variable dans la DBA (nom de colonne ou alias)

    

    
    public function  __construct($columnName = null) {
        $this->columnName = $columnName;
    }
    
    public function setHeaderContent($content = null){
        if($content != ""){
            $this->headerContent = $content;
        }
    }

    public function setHeaderAlign($align = null){
        if($align == "left"){
            $this->alignHeader = "left";
            $this->alignRow = "left";
        }elseif($align == "right"){
            $this->alignHeader = "right";
            $this->alignRow = "right";
        }
    }

    public function setDBAColumnName($name = null){
        if($name != null && $name != ""){
            $this->dbaColumnName = $name;
        }
    }

    public function setOrderable($value = null){
        if($value == "1"){
            $this->orderable=true;
        }
    }

    public function setOrder($order){
        if($order == "up"){
            $this->order = "up";
        }else if($order == "down"){
            $this->order = "down";
        }
    }

    public function setType($type){
        if($type == "checkbox"){
            $this->columnType = "checkbox";
        }else {
            $this->columnType = "custom";
        }
    }
    
    public function setRowAlign($align = null){
        if($align == "left"){           
            $this->alignRow = "left";
        }elseif($align == "right"){
            $this->alignRow = "right";
        }elseif($align == "center"){
            $this->alignRow = "center";
        }
    }   

    public function addRow($content = null){
        $this->rowsContent[] = $content;       
    }

    static public function cast(Column $object) {
        return $object;
    }
    
}


class Tableau{
    
    var $tablename = "";                        //Nom du tableau, necessaire au bon fonctionnement du JS
    var $tableTitle = null;                     //Texte afficher en titre
    
    var $columns = array();                     //Liste des colonnes du tableau
    var $numberOfColumn = 0;                    //Nombre de colonnes du tableau

    var $numberOfRow = 0;                       //Nombre de ligne effective dans le tableau
    var $headerIsSet = false;                   //Normalement toujours à true lors de la création du tableau

    var $footerIsSet = true;                    //Activation de la partie pour la pagination

    var $CSSTitleFontStyle = "arial";           //Style pour le titre du tableau en mode normal
    var $CSSTitleFontSize = "22px";             //Style pour le titre du tableau en mode normal
    var $CSSTitleFontWeight = "bold";           //Style pour le titre du tableau en mode normal
    var $CSSTitleFontColor = "#424DA6";         //Style pour le titre du tableau en mode normal
    var $CSSTitleTop = "20px";                  //Style pour le titre du tableau en mode normal
    var $CSSTitleLeft= "0px";                   //Style pour le titre du tableau en mode normal

    var $CSSTitleFontStyle_c = "arial";         //Style pour le titre du tableau en mode compact
    var $CSSTitleFontSize_c = "20px";           //Style pour le titre du tableau en mode compact
    var $CSSTitleFontWeight_c = "bold";         //Style pour le titre du tableau en mode compact
    var $CSSTitleFontColor_c = "#00A8DD";       //Style pour le titre du tableau en mode compact
    var $CSSTitleTop_c = "5px";                //Style pour le titre du tableau en mode compact
    var $CSSTitleLeft_c = "0px";                //Style pour le titre du tableau en mode compact
   

    var $CSSFiltersLabelFontStyle ="arial";
    var $CSSFiltersLabelFontSize ="11px";
    var $CSSFiltersLabelColor ="#666666";
    var $CSSFiltersLabelFontWeight ="bold";

    var $CSSFiltersInputFontStyle ="arial";
    var $CSSFiltersInputFontSize ="11px";
    var $CSSFiltersInputColor ="#000000";
    var $CSSFiltersInputFontWeight ="normal";

    var $CSSFiltersSelectFontStyle ="arial";
    var $CSSFiltersSelectFontSize ="11px";
    var $CSSFiltersSelectColor ="#000000";
    var $CSSFiltersSelectFontWeight ="normal";

    var $CSSFiltersSearchFontStyle ="arial";
    var $CSSFiltersSearchFontSize ="11px";
    var $CSSFiltersSearchColor ="#000000";
    var $CSSFiltersSearchFontWeight ="normal";

    var $tableBorderSize = 0;                       //Largeur de la bordure du tableau HTML (laisser à 0 ) en px
    var $tableCellSpacing = 0;                      //Marge entre les cellules (laisser à 0) en px
    var $tableCellPadding = 2;                      //Marge dans les cellules en px

    var $tableWidth = "99%";                        //Occupation de l'espace du tableau par rapport à son conteneur

    var $CSSHeaderBGColor = "#efefef";              //Couleur de fond du header
    var $CSSRow1BGColor = "#f7f7f7";                //Couleur de fond des lignes du tableau
    var $CSSRow2BGColor = "#f3f3f3";                //Couleur de fond alternative des lignes du tableau
    var $CSSRowOverBGColor = "#D9EEFF";             //Couleur de fond en mouse over

    var $CSSTableBorder = true;
    var $CSSTableBorderWidth = 1;                   //px
    var $CSSTableBorderColor = "#cecece";

    var $CSSHeaderFontStyle = "arial";              //Style de police du header du tableau
    var $CSSHeaderFontSize = "14px";                //Taille police du header du tableau
    var $CSSHeaderFontWeight = "bold";              //Largeur police du header du tableau
    var $CSSHeaderFontColor = "#424DA6";
    
    var $CSSRowsFontStyle = "arial";                //Style de police du reste du tableau, pagination incluse
    var $CSSRowsFontSize = "11px";                  //Taille police du reste du tableau, pagination incluse
    var $CSSRowsFontColor = "#424DA6";

    var $CSSRowFooterFontColor = "#666666";

    var $numberOfRowsPerPage = 10;                  //Nombre de lignes par page par défault
    var $numberOfSQLRows = 0;                       //Nombre de lignes totales venant de la DBA
    var $pageNumber = 1;                            //Numéro de la page en cour de visualisation
    var $numberOfPages = 1;                         //Nombre total de page

    var $ajaxMode = true;                           //Toujours true maintenant

    var $filename = null;                           //Nom du fichier php d'appel du tableau

    var $orderColumnId = null;                      //Numéro de la colone ou il faut appliquer un tri

    var $filterCriterias = array();                 //liste des critères de filtrages
    var $numberOfVisibleCriterias = 0;
    
    var $tblButtons = array();                      //Liste des boutons
    var $compactButtons = false;

    var $messageToDisplay = null;

    var $parentPopupId = null;


    
    public function  __construct($tablename = "", $filename = null) {
        $this->tablename = $tablename;
        if($filename != null){
            $this->filename = $filename;
        }
    }

    /*
     * input : array =>
     *                  content
     *                  align [*center;left;right]
     *                  dbaColumnName
     *                  orderable [*0;1]
     *                  type [*custom;checkbox]
     */
    public function setHeaderRow($row = array()){
        $this->numberOfColumn = count($row);
       
        foreach($row as $key=>$value){
            $this->columns[$key] = new Column();
            if(isset($value["content"])){
                $this->columns[$key]->setHeaderContent($value["content"]);
            }
            if(isset($value["align"])){
                $this->columns[$key]->setHeaderAlign($value["align"]);
            }
            if(isset($value["dbaColumnName"])){
                $this->columns[$key]->setDBAColumnName($value["dbaColumnName"]);
            }
            if(isset($value["orderable"])){
                $this->columns[$key]->setOrderable($value["orderable"]);
            }
            if(isset($value["type"])){
                $this->columns[$key]->setType($value["type"]);
            }
            
        }
        $this->headerIsSet = true;        
    }

    public function setNumberOfRowsPerPage($number = 0){
        if((int)$number != 0){
            $this->numberOfRowsPerPage = $number;           
        }
    }

    public function setNumberOfSQLRows($number = 0){
        if((int)$number != 0){
            $this->numberOfSQLRows = $number;
        }
    }

    public function setPageNumber($number = 0){
        if((int)$number != 0){
            $this->pageNumber = $number;
        }
    }


    /*
     * input : array =>
     *                  content
     */
    public function addRow($row = array()){      
        foreach($row as $key=>$value){
            if(isset($value["content"])){
                $this->columns[$key]->addRow($value["content"]);
            }            
        }
        $this->numberOfRow++;
    }

    public function calculateNumberOfPage(){
        $this->numberOfPages = ceil(($this->numberOfSQLRows / $this->numberOfRowsPerPage));
    }

    public function setOrderedColumn($colId = null,$order = null){
        if($colId != null && $order != null){
            $this->columns[$colId-1]->setOrder($order);
            $this->orderColumnId = $colId;
        }
    }

    public function getOrderSQLColumnName(){
        if($this->orderColumnId != null){
            return $this->columns[$this->orderColumnId-1]->dbaColumnName;
        }else{
            return  false;
        }
    }

    public function addFilterCriteria(filterCriteria $criteria){
        $this->filterCriterias[] = $criteria;
        if($criteria->criteriaType != "hidden") $this->numberOfVisibleCriterias++;
    }

    public function getFilterWhereSQL($where = ""){
        foreach($this->filterCriterias as $criteria){
            $criteria  = filterCriteria::cast($criteria);
            if($criteria->selectedValue != null && $criteria->criteriaDBAName != null){
                if($criteria->criteriaType == "list"){
                    if($where!= ""){
                        $where .= " AND ".$criteria->criteriaDBAName." ".$criteria->customCondition." '".$criteria->selectedValue."' ";
                    }else{
                        $where = " WHERE ".$criteria->criteriaDBAName." ".$criteria->customCondition." '".$criteria->selectedValue."' ";
                    }
                }elseif ($criteria->criteriaType == "date"){
                    $temp = array();
                    $temp = explode(":",$criteria->selectedValue);
                    if($temp[0]){
                        if($where!= ""){
                            $where .= " AND ".$criteria->criteriaDBAName." >= '".getEnglishSQLDate($temp[0])."' ";
                        }else{
                            $where = " WHERE ".$criteria->criteriaDBAName." >= '".getEnglishSQLDate($temp[0])."' ";
                        }
                    }
                    if($temp[1]){
                        if($where!= ""){
                            $where .= " AND ".$criteria->criteriaDBAName." <= '".getEnglishSQLDate($temp[1])."' ";
                        }else{
                            $where = " WHERE ".$criteria->criteriaDBAName." <= '".getEnglishSQLDate($temp[1])."' ";
                        }
                    }                    
                }elseif($criteria->criteriaType == "text"){
                    if($where!= ""){
                        $where .= " AND ".$criteria->criteriaDBAName." LIKE '%".$criteria->selectedValue."%' ";
                    }else{
                        $where = " WHERE ".$criteria->criteriaDBAName." LIKE '%".$criteria->selectedValue."%' ";
                    }
                }elseif($criteria->criteriaType == "hidden"){
                    if($where!= ""){
                        $where .= " AND ".$criteria->criteriaDBAName." ".$criteria->customCondition." '".$criteria->selectedValue."' ";
                    }else{
                        $where = " WHERE ".$criteria->criteriaDBAName." ".$criteria->customCondition." '".$criteria->selectedValue."' ";
                    }
                }
            }
        }
        return $where;
    }

    public function addTblButton($tblbutton){
        $this->tblButtons[] = $tblbutton;
    }


    public function getAjaxPopupLink($url,$popupWidth,$popupTitle){
        if(!$this->parentPopupId){
            return "javascript:openPopup_".$this->tablename."('".$url."',".$popupWidth.",'".$popupTitle."');";
        }else{
            return "javascript:openPopup2_".$this->tablename."('".$url."',".$popupWidth.",'".$popupTitle."','".$this->parentPopupId."');";
        }
        
    }
//    public function getAjaxPopupLink2($url,$popupWidth,$popupTitle,$parentPopupId){
//        return "javascript:openPopup_".$this->tablename."('".$url."',".$popupWidth.",'".$popupTitle."','".$parentPopupId."');";
//    }

    public function getAjaxPopupLinkMini($url,$popupWidth,$popupTitle,$ancre){
        if(!$this->parentPopupId){
            return "javascript:openPopupMini_".$this->tablename."('".$url."',".$popupWidth.",'".$popupTitle."','".$ancre."');";
        }else{
            return "javascript:openPopupMini2_".$this->tablename."('".$url."',".$popupWidth.",'".$popupTitle."','".$ancre."','".$this->parentPopupId."');";
        }
    }


    
    public function displayHTML(){
        $this->calculateNumberOfPage();

        $html = "<div style='width:100%;border:0px solid #00AA00;margin:0 auto;'>";
        /*
         * CODE JAVASCRIPT
         */
        if($this->ajaxMode){
            $html .= "<script type='text/javascript' >";
            $html .= "";

            /*
             * FONCTION DE REFRESH DE BASE DU TABLEAU
             * permet de recharger le tableau a partir de l'url définit
             * INPUT=>
             *          url : url avec tout les arguments necessaire au rafraichissement du tableau
             * 
             */
            $html .= "function refresh_".$this->tablename."(url){";            
            
	    $criteraJS = "";
            foreach($this->filterCriterias as $criteriaKey=>$criteria){
                if($criteria->criteriaType == "date"){
                    $criteraJS .= "&crit".$criteriaKey."='+$('#".$criteria->criteriaType."_".$criteria->criteriaName."_du_".$this->tablename."').val()+':'+$('#".$criteria->criteriaType."_".$criteria->criteriaName."_au_".$this->tablename."').val()+'";
                }else{
                    $criteraJS .= "&crit".$criteriaKey."='+$('#".$criteria->criteriaType."_".$criteria->criteriaName."_".$this->tablename."').val()+'";
                }
            }
            if($criteraJS != ""){
                $criteraJS = "+'".$criteraJS."'";
            }
            $html .= " $('#tableau_".$this->tablename."').load(url".$criteraJS.");";
            $html .= "}";
            
            /*
             * CHANGEMENT DU NOMBRE DE LIGNE PAR PAGE
             * permet de changer le nombre de ligne par page du tableau
             */
            $html .= "function changeNumberOfRowsPerPage_".$this->tablename."(){";
            if($this->orderColumnId != null){                
                $html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber=1&orderColId=".$this->orderColumnId."&order=".$this->columns[$this->orderColumnId-1]->order."');";
            }else{
                $html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber=1');";
            }
            $html .= "}";
            
            /*
             * CHANGE L'ORDRE DU TABLEAU
             * lorsqu'on clique sur une fleche de changement d'ordre dans le tableau
             * on appel cette fonction
             * INPUT=>
             *          columnId : numero de la column ou il faut appliquer l'ordre
             *          order : le sens de l'ordre 
             */
            $html .= "function changeOrder_".$this->tablename."(columnId,order){";
            $html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber=1&orderColId='+columnId+'&order='+order+'');";
            $html .= "}";

            /*
             * CHANGEMENT DE PAGE DU TABLEAU
             * Lors que le curseur sort du input du numéro de page, on appel cette fonction
             * qui récupe la valeur saisie et charge la page correspondante
             */
            $html .= "function changePageDirect_".$this->tablename."(){";
            if($this->orderColumnId != null){
                $html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber='+$('#pageNumber_".$this->tablename."').val()+'&orderColId=".$this->orderColumnId."&order=".$this->columns[$this->orderColumnId-1]->order."');";
            }else{
                $html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber='+$('#pageNumber_".$this->tablename."').val());";
            }
            $html .= "}";

            /*
             * SUBMIT DU FORMULAIRE DE FILTRATE
             */
            $html .= "function filterSubmit_".$this->tablename."(){";
            if($this->orderColumnId != null){
                $html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber=1&orderColId=".$this->orderColumnId."&order=".$this->columns[$this->orderColumnId-1]->order."');";
            }else{
                $html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber=1');";
            }
            $html .= "}";

            /*
             * OUVERTURE POPUP
             * permet d'ouvrir un popup
             *
             * INPUT=>
             *          url : url de chargement dans le popup
             *          pwidth : largeur du popup en pixel, il faut juste indiquer la valeur sans le px
             *          title : titre dans le popup
             *          resetScroll : si = 1, on remonte le scroll a 0, necessaire pour corriger un probleme
             *                          après le rechargement du popup après un popup long (voir très long).
             *
             */
            $html .= "function openPopup_".$this->tablename."(url,pwidth,title,resetScroll){";
            $html .= "  doc_height = $(document).height();";
            $html .= "  doc_width = $(document).width();";
            $html .= "  win_height = $(window).height();";
            $html .= "  win_width = $(window).width();";
            $html .= "  if(resetScroll == 1){
                            $(window).scrollTop(0);                           
                    }";
            $html .= "  win_scrolltop = $(window).scrollTop();";                       
            $html .= "  popup_width = pwidth;";
            $html .= "  popup_left = (win_width - popup_width)/2;";
            $html .= "  $('#popupdiv_content_".$this->tablename."').load(url);"; 
            $html .= "  $('#popupbg_".$this->tablename."').css('height',doc_height+'px');";
            $html .= "  $('#popupbg_".$this->tablename."').css('width',doc_width+'px');";
            $html .= "  $('#popupbg_".$this->tablename."').fadeIn('fast');";
            $html .= "  $('#popupdiv_title_".$this->tablename."').html(title);";
            $html .= "  $('#popupdiv_".$this->tablename."').css('width',popup_width+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').css('top',win_scrolltop+50+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').css('left',popup_left+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').fadeIn('fast', 
                                function(){                                
                                    window.setTimeout(function(){                                        
                                        doc_height = $(document).height();
                                        $('#popupbg_".$this->tablename."').css('height',doc_height+'px');
                                    }, 800);
                                   
                                }
                        );";            
            $html .= "}";

            /*
             * OUVERTURE POPUP AVEC PARENT POPUP
             * permet d'ouvrir un popup avec deja un parent popup ouvert
             *
             * INPUT=>
             *          url : url de chargement dans le popup
             *          pwidth : largeur du popup en pixel, il faut juste indiquer la valeur sans le px
             *          title : titre dans le popup
             *          parentPopupId : le nom du tableau parent au popup parent de ce popup
             *
             */
            $html .= "function openPopup2_".$this->tablename."(url,pwidth,title,parentPopupId){";
            $html .= "  doc_height = $('#popupdiv_'+parentPopupId).height();";
            $html .= "  doc_width = $('#popupdiv_'+parentPopupId).width();";
            $html .= "  win_height = $(window).height();";
            $html .= "  win_width = $('#popupdiv_'+parentPopupId).width();";//On prend la largeur du popup au lieu de la fenetre
            $html .= "  win_scrolltop = $(window).scrollTop();";            
            $html .= "  popup_width = pwidth;";
            $html .= "  popup_left = (win_width - popup_width)/2;";            
            $html .= "  $('#popupdiv_content_".$this->tablename."').load(url);";
            $html .= "  $('#popupbg_".$this->tablename."').css('height',doc_height+'px');";
            $html .= "  $('#popupbg_".$this->tablename."').css('width',doc_width+'px');";
            $html .= "  $('#popupbg_".$this->tablename."').fadeIn('fast');";
            $html .= "  $('#popupdiv_title_".$this->tablename."').html(title);";
            $html .= "  $('#popupdiv_".$this->tablename."').css('width',popup_width+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').css('top',win_scrolltop+50+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').css('left',popup_left+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').fadeIn('fast');";
            $html .= "}";

            /*
             * OUVERTURE POPUP MINI AVEC PARENT POPUP
             * permet d'ouvrir un mini popup avec deja un parent popup ouvert
             *
             * INPUT=>
             *          url : url de chargement dans le popup
             *          pwidth : largeur du popup en pixel, il faut juste indiquer la valeur sans le px
             *          title : titre dans le popup
             *          ancre :  id du span qui sert de point de repere a l'ouverture du popup
             *          
             */
            $html .= "function openPopupMini_".$this->tablename."(url,pwidth,title,ancre){";
            $html .= "  doc_height = $(document).height();";
            $html .= "  doc_width = $(document).width();";
            $html .= "  win_height = $(window).height();";
            $html .= "  win_width = $(window).width();";
            $html .= "  win_scrolltop = $(window).scrollTop();";
            $html .= "  popup_width = pwidth;";
            $html .= "  popup_left = $('#'+ancre).position().left;";
            $html .= "  popup_top = $('#'+ancre).position().top;";
            $html .= "  if(popup_left + popup_width > doc_width){";
            $html .= "      popup_left = popup_left - ((popup_left + popup_width)-doc_width)-30;";
            $html .= "  }";           
            $html .= "  $('#popupdiv_content_".$this->tablename."').load(url);";
            $html .= "  $('#popupbg_".$this->tablename."').css('height',doc_height+'px');";
            $html .= "  $('#popupbg_".$this->tablename."').css('width',doc_width+'px');";
            $html .= "  $('#popupbg_".$this->tablename."').fadeIn('fast');";
            $html .= "  $('#popupdiv_title_".$this->tablename."').html(title);";
            $html .= "  $('#popupdiv_".$this->tablename."').css('width',popup_width+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').css('top',popup_top+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').css('left',popup_left+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').fadeIn('fast');";           
            $html .= "}";

            /*
             * OUVERTURE POPUP MINI AVEC PARENT POPUP
             * permet d'ouvrir un mini popup avec deja un parent popup ouvert
             *
             * INPUT=>
             *          url : url de chargement dans le popup
             *          pwidth : largeur du popup en pixel, il faut juste indiquer la valeur sans le px
             *          title : titre dans le popup
             *          ancre :  id du span qui sert de point de repere a l'ouverture du popup
             *          parentPopupId : le nom du tableau parent au popup parent de ce popup
             */
            $html .= "function openPopupMini2_".$this->tablename."(url,pwidth,title,ancre,parentPopupId){";
            $html .= "  doc_height = $('#popupdiv_'+parentPopupId).height();";
            $html .= "  doc_width = $('#popupdiv_'+parentPopupId).width();";
            $html .= "  win_height = $(window).height();";
            $html .= "  win_width = $(window).width();";
            $html .= "  win_scrolltop = $(window).scrollTop();";
            $html .= "  popup_width = pwidth;";
            $html .= "  popup_left = $('#'+ancre).position().left;";
            $html .= "  popup_top = $('#'+ancre).position().top;";
            $html .= "  if(popup_left + popup_width > doc_width){";
            $html .= "      popup_left = popup_left - ((popup_left + popup_width)-doc_width)-30;";
            $html .= "  }";
            $html .= "  $('#popupdiv_content_".$this->tablename."').load(url);";
            $html .= "  $('#popupbg_".$this->tablename."').css('height',doc_height+'px');";
            $html .= "  $('#popupbg_".$this->tablename."').css('width',doc_width+'px');";
            $html .= "  $('#popupbg_".$this->tablename."').fadeIn('fast');";
            $html .= "  $('#popupdiv_title_".$this->tablename."').html(title);";
            $html .= "  $('#popupdiv_".$this->tablename."').css('width',popup_width+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').css('top',popup_top+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').css('left',popup_left+'px');";
            $html .= "  $('#popupdiv_".$this->tablename."').fadeIn('fast');";
            //$html .= "  alert($('#posi_50').position().top);";
            //$html .= "    $('select').css('display','none');";
            $html .= "}";

            /*
             * OUVERTURE POPUP AVEC MULTIPLE SELECTION
             * Permet d'envoyer au popup la selection de check box dans le tableau
             *
             * INPUT=>
             *          url : url de chargement dans le popup
             *          pwidth : largeur du popup en pixel, il faut juste indiquer la valeur sans le px
             *          title : titre dans le popup
             */
            $html .= "function openPopupMS_".$this->tablename."(url,pwidth,title){";
            $html .= "  doc_height = $(document).height();";
            $html .= "  doc_width = $(document).width();";
            $html .= "  win_height = $(window).height();";
            $html .= "  win_width = $(window).width();";
            $html .= "  win_scrolltop = $(window).scrollTop();";
            $html .= "  popup_width = pwidth;";
            $html .= "  popup_left = (win_width - popup_width)/2;";
            $html .= "  selection = 'chkms=';";
            $html .= "  numberOfSelection = 0;";
            $html .= "  for(i=0;i<".$this->numberOfRow.";i++){";
            $html .= "      if ($('#chkms_".$this->tablename."_'+i+':checked').val() !== undefined) {";
            $html .= "          selection += $('#chkms_".$this->tablename."_'+i+':checked').val() + ';';";
            $html .= "          numberOfSelection++;";
            $html .= "      }";
            $html .= "  }";
            $html .= "  if(numberOfSelection == 0){";
            $html .= "      alert(\""._FW_TABLEAU_ERROR_SELECTATLEAST1."\");";
            $html .= "  }else{";            
            $html .= "      $('#popupdiv_content_".$this->tablename."').load(url+'&'+selection);";
            $html .= "      $('#popupbg_".$this->tablename."').css('height',doc_height+'px');";
            $html .= "      $('#popupbg_".$this->tablename."').css('width',doc_width+'px');";
            $html .= "      $('#popupbg_".$this->tablename."').fadeIn('fast');";
            $html .= "      $('#popupdiv_title_".$this->tablename."').html(title);";
            $html .= "      $('#popupdiv_".$this->tablename."').css('width',popup_width+'px');";
            $html .= "      $('#popupdiv_".$this->tablename."').css('top',win_scrolltop+50+'px');";
            $html .= "      $('#popupdiv_".$this->tablename."').css('left',popup_left+'px');";
            $html .= "      $('#popupdiv_".$this->tablename."').fadeIn('fast');";
            $html .= "  }";
            $html .= "}";
            
            /*
             * FERMETURE DU POPUP
             * On masque le popup et on vide le contenu html du la parti content.
             * 
             * INPUT =>
             *          message : message de retour dans le tableau parent
             *                    peut rester vide ("")
             *
             * Si on possède un popup parent, on ruse pour redimensionner le BG
             * black pour qu'il n'y ai pas de bordure blanche en dessous si
             * le popup parent devait etre plus long après la fermeture du popup enfant.
             */
            $html .= "function closePopup_".$this->tablename."(message){";
            $html .= "$('#popupdiv_".$this->tablename."').fadeOut('fast');";
            $html .= "$('#popupbg_".$this->tablename."').fadeOut('fast');";
            $html .= "document.getElementById('popupdiv_content_".$this->tablename."').innerHTML = '';";            
            if($this->orderColumnId != null){
                
		$html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber='+$('#pageNumber_".$this->tablename."').val()+'&orderColId=".$this->orderColumnId."&order=".$this->columns[$this->orderColumnId-1]->order."&msg=' + message);";
            }else{
                
		$html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber='+$('#pageNumber_".$this->tablename."').val()+'&msg='+message);";
            }
            if($this->parentPopupId){
                $html .= "window.setTimeout(function(){
                                        doc_height = $(document).height();
                                        $('#popupbg_".$this->parentPopupId."').css('height',doc_height+'px');
                                    }, 800);";
            }
            $html .= "}";
            
            /*
             * SELECTION DE TOUTES LES CHECKBOX
             * Permet de selectionner toutes les checkboxs du tableau
             */
            $html .= "function chkSelection_".$this->tablename."(){";
            $html .= "  checkval = false;";
            $html .= "  if($('#chkAll_".$this->tablename."'+':checked').val() != undefined){";
            $html .= "      checkval = true;";
            $html .= "  }";
            $html .= "  for(i=0;i<".$this->numberOfRow.";i++){";
            $html .= "      $('#chkms_".$this->tablename."_'+i).attr('checked', checkval);";
            $html .= "  }";
            $html .= "}";
            
            /*
             * FUNCTION DE CONFIRMATION D'ACTION
             * Permet de confirmer une action
             * INPUT =>
             *          url : url de destination de l'action si OK
             *          message : message de retour sur le tableau
             *                    peut rester vide ("")
             *          messageConfirm : Message dans le dialog de confirmation
             */
            $html .= "function actionAjax_".$this->tablename."(url,message,messageConfirm){";
            $html .= "  if(!(messageConfirm == '' || messageConfirm == null)){";
            $html .= "      var answer = confirm(messageConfirm);";
            $html .= "  }else{";
            $html .= "      var answer = true;";
            $html .= "  }";            
            $html .= "  if(answer){";
            $html .= "      $.get(url);";
            if($this->orderColumnId != null){
                $html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber='+$('#pageNumber_".$this->tablename."').val()+'&orderColId=".$this->orderColumnId."&order=".$this->columns[$this->orderColumnId-1]->order."&msg=' + message);";
            }else{
                $html .= "  refresh_".$this->tablename."('".$this->filename."?numberOfRowsPerPage='+$('#numberOfRowsPerPage_".$this->tablename."').val()+'&pageNumber='+$('#pageNumber_".$this->tablename."').val()+'&msg='+message);";
            }              
            $html .= "  }";
            $html .= "}";

            $html .= "</script>";            

        }else {

        }
        
        /*
         * CODE CSS
         */
        $html .= "<style type=\"text/css\">";
        $html .= "";

        //CSS pour le titre du tableau (div)
        $html .= "#titlebutton_".$this->tablename." {";
        $html .= "  height:70px; ";
        $html .= "  margin:0 auto;";
        $html .= "  border:0px solid #cecece;";
        $html .= "  width:99%;";
        $html .= "  position:relative;";
        $html .= "}";
        $html .= "#title_".$this->tablename." {";
        $html .= "  color:".$this->CSSTitleFontColor.";";
        $html .= "  font-size:".$this->CSSTitleFontSize.";";
        $html .= "  font-weight:".$this->CSSTitleFontWeight.";";
        $html .= "  font-family:".$this->CSSTitleFontStyle.";";
        $html .= "  position:absolute;";
        $html .= "  top:".$this->CSSTitleTop.";";
        $html .= "  left:".$this->CSSTitleLeft.";";
        $html .= "}";
        $html .= "div.btnBlock_".$this->tablename." {";
        $html .= "  border:0px solid #000000;    ";
        $html .= "  width:60px;";
        $html .= "  height:70px;";
        $html .= "  float:right;";
        $html .= "  margin-left:5px;";
        $html .= "  margin-right:5px;";
        $html .= "  margin-top:5px;";
        $html .= "  font-size:11px;";
        $html .= "  text-align: center;";       
        $html .= "}";
        $html .= "div.btnPict_".$this->tablename." {";
        $html .= "  border:0px solid #000000;  ";
        $html .= "  width:40px;";
        $html .= "  height:40px;";
        $html .= "  margin-left:10px;";
        $html .= "  margin-bottom:5px;";
        $html .= "}";

        //Mode compact
        $html .= "#titlebutton_".$this->tablename."_compact {";
        $html .= "  height:40px; ";
        $html .= "  margin:0 auto;";
        $html .= "  border:0px solid #cecece;";
        $html .= "  width:99%;";
        $html .= "  position:relative;";
        $html .= "}";
        $html .= "#title_".$this->tablename."_compact {";
        $html .= "  color:".$this->CSSTitleFontColor_c.";";
        $html .= "  font-size:".$this->CSSTitleFontSize_c.";";
        $html .= "  font-weight:".$this->CSSTitleFontWeight_c.";";
        $html .= "  font-family:".$this->CSSTitleFontStyle_c.";";
        $html .= "  position:absolute;";
        $html .= "  top:".$this->CSSTitleTop_c.";";
        $html .= "  left:".$this->CSSTitleLeft_c.";";
        $html .= "}";
        $html .= "div.btnBlock_".$this->tablename."_compact{";
        $html .= "  border:0px solid #000000;    ";
        $html .= "  width:30px;";
        $html .= "  height:35px;";
        $html .= "  float:right;";
        $html .= "  margin-left:2px;";
        $html .= "  margin-right:2px;";
        $html .= "  margin-top:2px;";
        $html .= "  font-size:11px;";
        $html .= "  text-align: center;";
        $html .= "}";
        $html .= "div.btnPict_".$this->tablename."_compact {";
        $html .= "  border:0px solid #000000;  ";
        $html .= "  width:20px;";
        $html .= "  height:20px;";
        $html .= "  margin-left:5px;";
        $html .= "  margin-top:5px;";
        $html .= "}";

        //CSS pour les elements de filtrage
        $html .= "#filters_".$this->tablename." fieldset {";
        $html .= "  border:none;";
        $html .= "}";        
        $html .= "#filters_".$this->tablename." label {";
        $html .= "  font-size:".$this->CSSFiltersLabelFontSize.";";
        $html .= "  font-family:".$this->CSSFiltersLabelFontStyle.";";
        $html .= "  color:".$this->CSSFiltersLabelColor.";";
        $html .= "  font-weight:".$this->CSSFiltersLabelFontWeight.";";
        $html .= "  margin-right:6px;";
        $html .= "  margin-left:10px;";        
        $html .= "  display:inline;";
        $html .= "  float:none;";
        $html .= "}";
        $html .= "#filters_".$this->tablename." input {";
        $html .= "  font-size:".$this->CSSFiltersInputFontSize.";";
        $html .= "  font-family:".$this->CSSFiltersInputFontStyle.";";
        $html .= "  color:".$this->CSSFiltersInputColor.";";
        $html .= "  font-weight:".$this->CSSFiltersInputFontWeight.";";       
        $html .= "}";        
        $html .= "#filters_".$this->tablename." select {";
        $html .= "  font-size:".$this->CSSFiltersSelectFontSize.";";
        $html .= "  font-family:".$this->CSSFiltersSelectFontStyle.";";
        $html .= "  color:".$this->CSSFiltersSelectColor.";";
        $html .= "  font-weight:".$this->CSSFiltersSelectFontWeight.";";
        $html .= "}";
        $html .= "#filters_".$this->tablename." input.search {";
        $html .= "  font-size:".$this->CSSFiltersSearchFontSize.";";
        $html .= "  font-family:".$this->CSSFiltersSearchFontStyle.";";
        $html .= "  color:".$this->CSSFiltersSearchColor.";";
        $html .= "  font-weight:".$this->CSSFiltersSearchFontWeight.";";
        $html .= "}";

        //CSS zone message
//        $html .= "#message_".$this->tablename." {";
//        $html .= "  text-align:center;";
//        $html .= "  font-family:".$this->CSSFiltersSearchFontStyle.";";
//        $html .= "  font-weight:bold;";
//        $html .= "  font-size:15px;";
//        $html .= "  margin-bottom:10px;";
//        $html .= "}";
//        $html .= "#message_".$this->tablename.".msg_error{";
//        $html .= "  color:#FF0000;";
//        $html .= "}";
//         $html .= "#message_".$this->tablename.".msg_ok{";
//        $html .= "  color:green;";
//        $html .= "}";
        
        
        //CSS pour le tableau de données
        $html .= "#table_".$this->tablename."{";
        $html .= "  border-collapse:collapse;";
        $html .= "}";
        $html .= "#table_".$this->tablename." td{";
        $html .= "  border: ".$this->CSSTableBorderWidth."px solid ".$this->CSSTableBorderColor.";";
        $html .= "  font-size:".$this->CSSRowsFontSize.";";
        $html .= "  font-family:".$this->CSSRowsFontStyle.";";
        $html .= "  color:".$this->CSSRowsFontColor.";";
        $html .= "}";
        $html .= "#table_".$this->tablename." td.tbl_header_".$this->tablename." {";        
        $html .= "  font-size:".$this->CSSHeaderFontSize.";";
        $html .= "  font-weight:".$this->CSSHeaderFontWeight.";";
        $html .= "  color:".$this->CSSHeaderFontColor.";";
        $html .= "  font-family:'".$this->CSSHeaderFontStyle."';";
        $html .= "  background: url(\"../images/bg_popup_menu.png\") repeat-x;";
        $html .= "  background-position:top;";
        $html .= "  background-color:".$this->CSSHeaderBGColor.";";
        $html .= "  vertical-align:middle;";
        $html .= "  text-align:center;";
        $html .= "}";

        $html .= "#table_".$this->tablename." td.tbl_footer_".$this->tablename." {";
        $html .= "  border-bottom:0px;";
        $html .= "  border-left:0px;";
        $html .= "  border-right:0px;";
        $html .= "  padding-top:10px;";
        $html .= "  color:".$this->CSSRowFooterFontColor.";";
        $html .= "}";

        $html .= "#table_".$this->tablename." td.tbl_footer_".$this->tablename." img{";
        $html .= "  position:relative;";
        $html .= "  top:2px;";
        $html .= "}";


        $html .= "#table_".$this->tablename." input{";
        $html .= "  font-size:".$this->CSSRowsFontSize.";";
        $html .= "  font-family:".$this->CSSRowsFontStyle.";";
        $html .= "  width:15px;";
        $html .= "}";        
        $html .= "#table_".$this->tablename." select{";
        $html .= "  font-size:".$this->CSSRowsFontSize.";";
        $html .= "  font-family:".$this->CSSRowsFontStyle.";";
        $html .= "  margin-left:10px;";
        $html .= "  margin-right:0px;";       
        $html .= "}";        
        $html .= "#table_".$this->tablename." fieldset {";
        $html .= "  border:none;";
        $html .= "}";        
        $html .= "</style>";

        /*
         * CODE HTML TITRE ET BOUTONS
         */
        if($this->tableTitle != null || count($this->tblButtons) >= 1){
            
            if(!$this->compactButtons){
                $html .= "<div id=\"titlebutton_".$this->tablename."\" >";
                $html .= "<div id=\"title_".$this->tablename."\" >".$this->tableTitle."</div>";
                foreach($this->tblButtons as $keybtn=>$tblButton){
                    $tblButton = TblButton::cast($tblButton);
                    $html .= "<div class=\"btnBlock_".$this->tablename."\" >";
                    $html .= "<div class=\"btnPict_".$this->tablename."\" >";
                    if($tblButton->buttonpicture){
                        if($tblButton->needMultipleSelection){
                            if($tblButton->popupMode){
                                $html .= "<a href=\"javascript:openPopupMS_".$this->tablename."('".$tblButton->link."',".$tblButton->popupWidth.",'".$tblButton->popupTitle."');\" title=\"".$tblButton->altText."\">";
                            }else{
                                $html .= "<a href=\"".$tblButton->link."\" title=\"".$tblButton->altText."\">";
                            }
                        }else{
                            if(($tblButton->needInput && $tblButton->inputOk) || !$tblButton->needInput){
                                if($tblButton->popupMode){
                                    $html .= "<a href=\"".$this->getAjaxPopupLink($tblButton->link,$tblButton->popupWidth,$tblButton->popupTitle)."\" title=\"".$tblButton->altText."\">";
                                }else{
                                    $html .= "<a href=\"".$tblButton->link."\" title=\"".$tblButton->altText."\">";
                                }
                            }else{
                                if($tblButton->messageIfInputNotOk){
                                    $html .= "<a href=\"javascript:alert('".$tblButton->messageIfInputNotOk."');\" title=\"".$tblButton->altText."\">";
                                }
                            }
                        }

                        $html .= "<img src=\"".$tblButton->buttonpicture."\" alt=\"".$tblButton->altText."\" title=\"".$tblButton->altText."\" width=\"40\" height=\"40\" border=\"0\"/>";
                        $html .= "</a>";
                    }
                    $html .= "</div>";

                    $html .= "";
                    if($tblButton->needMultipleSelection){
                        if($tblButton->popupMode){
                            $html .= "<a href=\"javascript:openPopupMS_".$this->tablename."('".$tblButton->link."',".$tblButton->popupWidth.",'".$tblButton->popupTitle."');\" title=\"".$tblButton->altText."\">";
                        }else{
                            $html .= "<a href=\"".$tblButton->link."\" title=\"".$tblButton->altText."\">";
                        }
                    }else{
                       if(($tblButton->needInput && $tblButton->inputOk) || !$tblButton->needInput){
                                if($tblButton->popupMode){
                                    $html .= "<a href=\"".$this->getAjaxPopupLink($tblButton->link,$tblButton->popupWidth,$tblButton->popupTitle)."\" title=\"".$tblButton->altText."\">";
                                }else{
                                    $html .= "<a href=\"".$tblButton->link."\" title=\"".$tblButton->altText."\">";
                                }
                            }else{
                                if($tblButton->messageIfInputNotOk){
                                    $html .= "<a href=\"javascript:alert('".$tblButton->messageIfInputNotOk."');\" title=\"".$tblButton->altText."\">";
                                }
                            }
                    }
                    $html .= $tblButton->buttonLabel;
                    $html .= "</a>";
                    $html .= "</div>";
                }
                $html .= "</div>";
            }else{
                //ZONE BOUTONS COMPACT
                $html .= "<div id=\"titlebutton_".$this->tablename."_compact\" >";
                $html .= "<div id=\"title_".$this->tablename."_compact\" >".$this->tableTitle."</div>";
                foreach($this->tblButtons as $keybtn=>$tblButton){
                    $tblButton = TblButton::cast($tblButton);
                    $html .= "<div class=\"btnBlock_".$this->tablename."_compact\" >";
                    $html .= "<div class=\"btnPict_".$this->tablename."_compact\" >";
                    if($tblButton->buttonpicture){
                        if($tblButton->needMultipleSelection){
                            if($tblButton->popupMode){
                                $html .= "<a href=\"javascript:openPopupMS_".$this->tablename."('".$tblButton->link."',".$tblButton->popupWidth.",'".$tblButton->popupTitle."');\" title=\"".$tblButton->altText."\">";
                            }else{
                                $html .= "<a href=\"".$tblButton->link."\" title=\"".$tblButton->altText."\">";
                            }
                        }else{
                            if(($tblButton->needInput && $tblButton->inputOk) || !$tblButton->needInput){
                                if($tblButton->popupMode){
                                    $html .= "<a href=\"".$this->getAjaxPopupLink($tblButton->link,$tblButton->popupWidth,$tblButton->popupTitle)."\" title=\"".$tblButton->altText."\">";
                                }else{
                                    $html .= "<a href=\"".$tblButton->link."\" title=\"".$tblButton->altText."\">";
                                }
                            }else{
                                if($tblButton->messageIfInputNotOk){
                                    $html .= "<a href=\"javascript:alert('".$tblButton->messageIfInputNotOk."');\" title=\"".$tblButton->altText."\">";
                                }
                            }
                        }

                        $html .= "<img src=\"".$tblButton->buttonpicture."\" alt=\"".$tblButton->altText."\" title=\"".$tblButton->altText."\" width=\"20\" height=\"20\" border=\"0\"/>";
                        $html .= "</a>";
                    }
                    $html .= "</div>";

                    $html .= "";
//                    if($tblButton->needMultipleSelection){
//                        if($tblButton->popupMode){
//                            $html .= "<a href=\"javascript:openPopupMS_".$this->tablename."('".$tblButton->link."',".$tblButton->popupWidth.",'".$tblButton->popupTitle."');\" title=\"".$tblButton->altText."\">";
//                        }else{
//                            $html .= "<a href=\"".$tblButton->link."\" title=\"".$tblButton->altText."\">";
//                        }
//                    }else{
//                       if(($tblButton->needInput && $tblButton->inputOk) || !$tblButton->needInput){
//                                if($tblButton->popupMode){
//                                    $html .= "<a href=\"javascript:openPopup_".$this->tablename."('".$tblButton->link."',".$tblButton->popupWidth.",'".$tblButton->popupTitle."');\" title=\"".$tblButton->altText."\">";
//                                }else{
//                                    $html .= "<a href=\"".$tblButton->link."\" title=\"".$tblButton->altText."\">";
//                                }
//                            }else{
//                                if($tblButton->messageIfInputNotOk){
//                                    $html .= "<a href=\"javascript:alert('".$tblButton->messageIfInputNotOk."');\" title=\"".$tblButton->altText."\">";
//                                }
//                            }
//                    }
//                    $html .= $tblButton->buttonLabel;
//                    $html .= "</a>";
                    $html .= "</div>";
                }
                $html .= "</div>";
            }
            
        }
        /*
         * CODE HTML FILTER
         */
         if(count($this->filterCriterias)){
             //Si on a des filtres visible (non hidden) on affiche le tout
            if($this->numberOfVisibleCriterias != 0){
                $html .= "<div id=\"filters_".$this->tablename."\" style='border:0px solid #000000; width:99%;margin:0 auto;margin-bottom:10px;'>";
                $html .= "<form action=\"\" method=\"get\" id=\"filtersForm_".$this->tablename."\" name=\"filtersForm_".$this->tablename."\" onsubmit=\"filterSubmit_".$this->tablename."();return false;\">";
                $html .= "<fieldset>";
                //$html .= "<div style='border:1px solid #000000;'>";
                foreach($this->filterCriterias as $criteriaKey=>$criteria){
                    $criteria = filterCriteria::cast($criteria);
                    if($criteria->criteriaType != "hidden"){
                        if($criteriaKey == 0){
                            $marginLeft = "margin-left:0px;";
                        }else{
                            $marginLeft = "";
                        }
                        $html .= "<div style='float:left;margin-bottom:6px;margin-top:2px;'>";
                        if($criteria->criteriaType == "list"){
                            $html .= "<label style=\"".$marginLeft."\">".$criteria->criteriaLabel."</label>";
                            $html .= "<select name=\"".$criteria->criteriaType."_".$criteria->criteriaName."_".$this->tablename."\" id=\"".$criteria->criteriaType."_".$criteria->criteriaName."_".$this->tablename."\" onchange=\"filterSubmit_".$this->tablename."();\">";
                            foreach($criteria->listRows as $selectrow){
                                $selected = "";
                                if($criteria->selectedValue == $selectrow[0]){
                                    $selected = "selected=\"selected\"";
                                }
                                $html .= "<option value=\"".$selectrow[0]."\" ".$selected.">".$selectrow[1]."</option>";
                            }
                            $html .= "</select>";
                        }
                        if($criteria->criteriaType == "date"){
                            $html .= "<label style=\"".$marginLeft."\">".$criteria->criteriaLabel;
                            if($criteria->selectedValue != null){
                                $values = explode(":",$criteria->selectedValue);
                            }else{
                                $values[0]= "";
                                $values[1]="";
                            }
                            $html .= " du </label>";
                            $html .= " <input type\"text\" id=\"".$criteria->criteriaType."_".$criteria->criteriaName."_du_".$this->tablename."\" name=\"".$criteria->criteriaType."_".$criteria->criteriaName."_du_".$this->tablename."\" value=\"".$values[0]."\"/ size=\"10\">";
                            $html .= "<span id=\"spancalendar_du_".$this->tablename."\"></span>";
                            $html .= "<script language=\"JavaScript\"> new tcal ({'formname': 'filtersForm_".$this->tablename."','controlname': '".$criteria->criteriaType."_".$criteria->criteriaName."_du_".$this->tablename."'},null,'spancalendar_du_".$this->tablename."');</script>";
                            //$html .= " <script language=\"javascript\" type=\"text/javascript\">new tcal ({'controlname' : '".$criteria->criteriaType."_".$criteria->criteriaName."_au_".$this->tablename."'});</script> ";
                            $html .= "<label> au </label>";
                            $html .= "<input type\"text\" id=\"".$criteria->criteriaType."_".$criteria->criteriaName."_au_".$this->tablename."\" name=\"".$criteria->criteriaType."_".$criteria->criteriaName."_au_".$this->tablename."\" value=\"".$values[1]."\" size=\"10\"/>";
                            $html .= "<span id=\"spancalendar_au_".$this->tablename."\"></span>";
                            $html .= "<script language=\"JavaScript\"> new tcal ({'formname': 'filtersForm_".$this->tablename."','controlname': '".$criteria->criteriaType."_".$criteria->criteriaName."_au_".$this->tablename."'},null,'spancalendar_au_".$this->tablename."');</script>";
                        }
                        if($criteria->criteriaType == "text"){
                            $html .= "<label style=\"".$marginLeft."\">".$criteria->criteriaLabel."</label>";
                            $html .= " <input type\"text\" id=\"".$criteria->criteriaType."_".$criteria->criteriaName."_".$this->tablename."\" name=\"".$criteria->criteriaType."_".$criteria->criteriaName."_".$this->tablename."\" value=\"".$criteria->selectedValue."\"/ size=\"10\">";
                        }
                        $html .= "</div>";
                    }else{
                        $html .= "<input type\"hidden\" id=\"".$criteria->criteriaType."_".$criteria->criteriaName."_".$this->tablename."\" name=\"".$criteria->criteriaType."_".$criteria->criteriaName."_".$this->tablename."\" value=\"".$criteria->selectedValue."\" />";
                    }
                }
                $html .= "<input class=\"search\" type=\"submit\" name=\"submit\" value=\""._FW_TABLEAU_FILTRAGE_SUBMITTEXT."\"/ style=\"float:left;margin-left:10px; \">";
                $html .= "</fieldset>";
                $html .= "</form>";
                $html .= "</div>";
            }else{
                //Si on a que des filtres hidden on place juste le formulaire sans mise en page
                $html .= "<form action=\"\" method=\"get\" id=\"filtersForm_".$this->tablename."\" name=\"filtersForm_".$this->tablename."\" onsubmit=\"filterSubmit_".$this->tablename."();return false;\">";
                $html .= "<fieldset>";
                foreach($this->filterCriterias as $criteriaKey=>$criteria){
                    $criteria = filterCriteria::cast($criteria);
                    $html .= "<input type=\"hidden\" id=\"".$criteria->criteriaType."_".$criteria->criteriaName."_".$this->tablename."\" name=\"".$criteria->criteriaType."_".$criteria->criteriaName."_".$this->tablename."\" value=\"".$criteria->selectedValue."\" />";
                }
                $html .= "</fieldset>";
                $html .= "</form>";
            }       
         }

         
         /*
         * CODE HTML ZONE MESSAGE
         */
         //$this->messageToDisplay ="-e-test";
         if($this->messageToDisplay){
            $strhead = substr($this->messageToDisplay, 0,3);
            if($strhead == "-e-"){
                $strmessage = substr($this->messageToDisplay, 3);
                $strmessageClass = "msg_error";
            }else{
                $strmessage = $this->messageToDisplay;
                $strmessageClass = "msg_ok";
            }
            $html .= "<div id=\"message_".$this->tablename."\" class=\"".$strmessageClass."\">".$strmessage."</div>";
         }
        

        /*
         * CODE HTML TABLE
         */
        $tableParam  = "";
        $tableParam .= " width='".$this->tableWidth."' ";
        $tableParam .= " border='".$this->tableBorderSize."' ";
        $tableParam .= " cellpadding='".$this->tableCellPadding."' ";
        $tableParam .= " cellspacing='".$this->tableCellSpacing."' ";
        $tableParam .= " id='table_".$this->tablename."'";
        $tableParam .= " align=\"center\"";
        $tabledCSS = "margin:0 auto;";
        $tableParam .= " style='".$tabledCSS."' ";
        $html .= "<table ".$tableParam." >\n";
        $html .= "";
        
        /*
         * Génération du header du tableau
         */
        if($this->headerIsSet){
            $html .= "<tr>";
            foreach ($this->columns as $colid=>$column){
                $column = Column::cast($column);
                $tdParam  = " align='".$column->alignHeader."' ";                
                $html .= "<td ".$tdParam." class=\"tbl_header_".$this->tablename."\">";
                if($column->columnType == "checkbox"){
                    $html .= "<input type=\"checkbox\" id=\"chkAll_".$this->tablename."\" onclick=\"chkSelection_".$this->tablename."();\" value=\"1\"/>";
                }else{
                    $html .= "".$column->headerContent."";
                }                
                if($column->orderable && $column->dbaColumnName && $column->columnType == "custom"){
                    if($column->order == "up"){
                        $html .= " <a href='javascript:changeOrder_".$this->tablename."(".($colid+1).",\"down\")' title=\""._FW_TABLEAU_TRIEDOWN_ALT."\"><img src=\"../images/downarrow.png\" alt=\"\" border=\"0\"></a> ";
                    }elseif($column->order == "down"){
                        $html .= " <a href='javascript:changeOrder_".$this->tablename."(".($colid+1).",\"up\")' title=\""._FW_TABLEAU_TRIEUP_ALT."\"><img src=\"../images/uparrow.png\" alt=\"\" border=\"0\"></a>";
                    }else{
                        $html .= " <a href='javascript:changeOrder_".$this->tablename."(".($colid+1).",\"up\")' title=\""._FW_TABLEAU_TRIEUP_ALT."\"><img src=\"../images/uparrow.png\" alt=\"\" border=\"0\"></a>";
                        $html .= "<a href='javascript:changeOrder_".$this->tablename."(".($colid+1).",\"down\")' title=\""._FW_TABLEAU_TRIEDOWN_ALT."\"><img src=\"../images/downarrow.png\" alt=\"\" border=\"0\"></a> ";
                    }          
                }
                $html .= "</td>";
                $html .= "";
                $html .= "";
                $html .= "";
            }
            $html .= "</tr>\n";
        }
        
         /*
         * Génération du contenu du tableau
         */
        if($this->numberOfRow > 0){
            $rowColorSwitch = 1;
            for($i=0;$i < $this->numberOfRow; $i++){
                $html .= "<tr>";
                foreach ($this->columns as $colid=>$column){
                    $column = Column::cast($column);
                    $tdParam = "";
                    $tdParam .= " align='".$column->alignRow."' ";
                    $tdParam .= " class='row_".$i."_".$this->tablename."' ";                   
                    $tdParam .= " onmouseover='$(\".row_".$i."_".$this->tablename."\").css(\"background-color\",\"".$this->CSSRowOverBGColor."\")'";
                    $tdCSS = "";
                    if($rowColorSwitch == 1){
                        $tdCSS .= "background-color:".$this->CSSRow1BGColor.";";
                        $tdParam .= " onmouseout='$(\".row_".$i."_".$this->tablename."\").css(\"background-color\",\"".$this->CSSRow1BGColor."\")'";
                    }else{
                        $tdCSS .= "background-color:".$this->CSSRow2BGColor.";";
                        $tdParam .= " onmouseout='$(\".row_".$i."_".$this->tablename."\").css(\"background-color\",\"".$this->CSSRow2BGColor."\")'";
                    }
                    $tdParam .= " style='".$tdCSS."' ";                    
                    $html .= "<td ".$tdParam.">";
                    if($column->columnType == "custom"){
                        $html .= "".$column->rowsContent[$i]."";
                    }elseif($column->columnType == "checkbox"){
                         $html .= "<input type=\"checkbox\"/ name=\"chkms_".$this->tablename."\" id=\"chkms_".$this->tablename."_".$i."\" value=\"".$column->rowsContent[$i]."\">";
                    }
                    

                    $html .= "</td>";
                    $html .= "";
                    
                }
                $html .= "</tr>\n";
                if($rowColorSwitch == 1){                        
                    $rowColorSwitch = 2;
                }else{
                    $rowColorSwitch = 1;
                }   
            }
        }
        
        /*
         * Génération du système de pagination
         */
        if($this->footerIsSet){
            //Mode ajax
            $html .= "<tr>";
            $tdParam = "";
            $tdParam .= "align='right' ";
            $tdParam .= "colspan='".$this->numberOfColumn."'";           
            $html .= "<td ".$tdParam." class=\"tbl_footer_".$this->tablename."\" style=''>";
            $html .= "<div style='float:left; margin-left:8px;'> ";
            if($this->orderColumnId != null){
                $html .= "<a href='javascript:refresh_".$this->tablename."(\"".$this->filename."?numberOfRowsPerPage=".$this->numberOfRowsPerPage."&amp;pageNumber=".$this->pageNumber."&amp;orderColId=".$this->orderColumnId."&amp;order=".$this->columns[$this->orderColumnId-1]->order."\")' title=\""._FW_TABLEAU_REFRESH_ALT."\"><img src='../images/refresh.png'/ height='14'></a>";
            }else{
                $html .= "<a href='javascript:refresh_".$this->tablename."(\"".$this->filename."?numberOfRowsPerPage=".$this->numberOfRowsPerPage."&amp;pageNumber=".$this->pageNumber."\")' title=\""._FW_TABLEAU_REFRESH_ALT."\"><img src='../images/refresh.png'/ height='14' ></a>";
            }
            $html .= "</div>";
            $html .= "<form action='' method='get' id='paging-".$this->tablename."' onsubmit='changePageDirect_".$this->tablename."();return false;'><fieldset>";
            if($this->pageNumber > 1){
                if($this->orderColumnId != null){
                    $html .= "<a href='javascript:refresh_".$this->tablename."(\"".$this->filename."?pageNumber=1&amp;numberOfRowsPerPage=".$this->numberOfRowsPerPage."&amp;orderColId=".$this->orderColumnId."&amp;order=".$this->columns[$this->orderColumnId-1]->order."\")' title=\""._FW_TABLEAU_PAGINATION_DEBUT_ALT."\"><img src='../images/deb_actif.png'/ height='12'></a>";
                }else{
                    $html .= "<a href='javascript:refresh_".$this->tablename."(\"".$this->filename."?pageNumber=1&amp;numberOfRowsPerPage=".$this->numberOfRowsPerPage."\")' title=\""._FW_TABLEAU_PAGINATION_DEBUT_ALT."\"><img src='../images/deb_actif.png'/ height='12'></a>";
               }
            }else{
                $html .= "<img src='../images/deb_inactif.png'/ height='12'>";
            }
            $html .= " ";
            if($this->pageNumber > 1){
                if($this->orderColumnId != null){
                    $html .= "<a href='javascript:refresh_".$this->tablename."(\"".$this->filename."?pageNumber=".($this->pageNumber-1)."&amp;numberOfRowsPerPage=".$this->numberOfRowsPerPage."&amp;orderColId=".$this->orderColumnId."&amp;order=".$this->columns[$this->orderColumnId-1]->order."\")' title=\""._FW_TABLEAU_PAGINATION_PREC_ALT."\"><img src='../images/prec_actif.png'/ height='12'></a> ";
                }else{
                    $html .= "<a href='javascript:refresh_".$this->tablename."(\"".$this->filename."?pageNumber=".($this->pageNumber-1)."&amp;numberOfRowsPerPage=".$this->numberOfRowsPerPage."\")' title=\""._FW_TABLEAU_PAGINATION_PREC_ALT."\"><img src='../images/prec_actif.png'/ height='12'></a> ";
                }
            }else{
                $html .= "<img src='../images/prec_inactif.png'/ height='12'>";
            }
            $html .= " ";
            $html .= "page <input type='text' name='pageNumber_".$this->tablename."' id='pageNumber_".$this->tablename."' size='2' value='".$this->pageNumber."' /> of ".$this->numberOfPages."";
            $html .= " ";
            if($this->pageNumber < $this->numberOfPages){
                if($this->orderColumnId != null){
                    $html .= "<a href='javascript:refresh_".$this->tablename."(\"".$this->filename."?pageNumber=".($this->pageNumber+1)."&amp;numberOfRowsPerPage=".$this->numberOfRowsPerPage."&amp;orderColId=".$this->orderColumnId."&amp;order=".$this->columns[$this->orderColumnId-1]->order."\")' title=\""._FW_TABLEAU_PAGINATION_SUIVANT_ALT."\"><img src='../images/suiv_actif.png'/ height='12'></a>";
                }else{
                    $html .= "<a href='javascript:refresh_".$this->tablename."(\"".$this->filename."?pageNumber=".($this->pageNumber+1)."&amp;numberOfRowsPerPage=".$this->numberOfRowsPerPage."\")' title=\""._FW_TABLEAU_PAGINATION_SUIVANT_ALT."\"><img src='../images/suiv_actif.png'/ height='12'></a>";
                }
            }else{
                $html .= "<img src='../images/suiv_inactif.png'/ height='12'>";
            }
            $html .= " ";
            if($this->pageNumber != $this->numberOfPages){
                if($this->orderColumnId != null){
                    $html .= "<a href='javascript:refresh_".$this->tablename."(\"".$this->filename."?pageNumber=".$this->numberOfPages."&amp;numberOfRowsPerPage=".$this->numberOfRowsPerPage."&amp;orderColId=".$this->orderColumnId."&amp;order=".$this->columns[$this->orderColumnId-1]->order."\")' title=\""._FW_TABLEAU_PAGINATION_FIN_ALT."\"><img src='../images/fin_actif.png'/ height='12'></a> ";
                }else{
                    $html .= "<a href='javascript:refresh_".$this->tablename."(\"".$this->filename."?pageNumber=".$this->numberOfPages."&amp;numberOfRowsPerPage=".$this->numberOfRowsPerPage."\")' title=\""._FW_TABLEAU_PAGINATION_FIN_ALT."\"><img src='../images/fin_actif.png'/ height='12'></a> ";
                }
            }else{
                $html .= "<img src='../images/fin_inactif.png'/ height='12'>";
            }
            $html .= " ";
            $html .= "<select name='numberOfRowsPerPage_".$this->tablename."' onchange='changeNumberOfRowsPerPage_".$this->tablename."()' id='numberOfRowsPerPage_".$this->tablename."'>";
            $selected = "";
            if($this->numberOfRowsPerPage == 3) $selected = "selected='selected'";
            $html .= "<option value='3' ".$selected.">3</option>";
            $selected = "";
            if($this->numberOfRowsPerPage == 10) $selected = "selected='selected'";
            $html .= "<option value='10' ".$selected.">10</option>";
            $selected = "";
            if($this->numberOfRowsPerPage == 20) $selected = "selected='selected'";
            $html .= "<option value='20' ".$selected.">20</option>";
            $selected = "";
            if($this->numberOfRowsPerPage == 30) $selected = "selected='selected'";
            $html .= "<option value='30' ".$selected.">30</option>";
            $selected = "";
            if($this->numberOfRowsPerPage == 40) $selected = "selected='selected'";
            $html .= "<option value='40' ".$selected.">40</option>";
            $selected = "";
            if($this->numberOfRowsPerPage == 50) $selected = "selected='selected'";
            $html .= "<option value='50' ".$selected.">50</option>";
            $html .= "</select>";
            $html .= "<input type=\"hidden\" value=\"".$this->orderColumnId."\" name=\"orderid_".$this->tablename."\" id=\"orderid_".$this->tablename."\"/>";
            
            $html .= "</fieldset></form>";
           
            $html .= "</td>";
            $html .= "</tr>";

        }
        $html .= "</table>";
        $html .= "</div>";

        /*
         * Génération du système de popup
         */
        //div pour le voile noir en arrière plant du popup        
        $tdParam = "";        
        $tdCSS = "";
        $tdCSS .= "z-index: 2000;";
        $tdCSS .= "display: block;";
//        $tdCSS .= "height: 500px;";
//        $tdCSS .= "width: 100%;";
        $tdCSS .= "background: #000000;";
        $tdCSS .= "position: absolute;";
        $tdCSS .= "top: 0;";
        $tdCSS .= "left: 0;";
//        $tdCSS .= "height: auto !important;";
//        $tdCSS .= "height: 100%;";
//        $tdCSS .= "min-height: 100%;";
        //$tdCSS .= "-moz-opacity:0.75;";
        //$tdCSS .= "-khtml-opacity: 0.75;";
        $tdCSS .= "opacity: 0.75;";
        $tdCSS .= "filter:alpha(opacity=75);";
        $tdParam .= " style=\"".$tdCSS."\" ";
        $html .= "<div id=\"popupbg_".$this->tablename."\" ".$tdParam."></div>";
        //div conteneur du popup
        $tdParam = "";
        $tdCSS = "";
        $tdCSS .= "z-index: 2005;";
        $tdCSS .= "display: none;";
        $tdCSS .= "position: absolute;";
//        $tdCSS .= "background-color: #ffffff;";
//        $tdCSS .= "border: 1px solid #000000;";
//        $tdCSS .= "width: 400px;";
//        //$tdCSS .= "margin: 0 auto;";
//        $tdCSS .= "top: 10%;";
//        $tdCSS .= "left: 25%;";
        $tdParam .= " style=\"".$tdCSS."\" ";
        $html .= "<div class=\"popup\" id=\"popupdiv_".$this->tablename."\" ".$tdParam.">";
        //div pour le header du popup
        $tdParam = "";
//        $tdCSS = "";
//        $tdCSS .= "text-align: right;";
//        $tdCSS .= "border-bottom: 1px solid #000000;";
//        $tdCSS .= "";
//        $tdCSS .= "";
//        $tdParam .= " style=\"".$tdCSS."\" ";
        $html .= "<div class=\"popup_menu\" id=\"popupdiv_menu_".$this->tablename."\" ".$tdParam.">";
        $html .= "<div class =\"popup_title\" id=\"popupdiv_title_".$this->tablename."\"></div>";
        $html .= "<div class =\"popup_close\" id=\"popupdiv_close_".$this->tablename."\"><a href=\"javascript:closePopup_".$this->tablename."('');\"><img src=\"../images/img_close.png\" alt=\"\" title=\"\"/></a></div>";
        $html .= "</div>";
//div pour le contenu du popup
        $tdParam = "";
//        $tdCSS = "";
//        $tdParam .= " style=\"".$tdCSS."\" ";
        $html .= "<div class=\"popup_content\" id=\"popupdiv_content_".$this->tablename."\" ".$tdParam."></div>";
        //fin div du conteneur du popup
        $html .= "</div>";

        
        return $html;
    }

    static public function cast(Tableau $object) {
        return $object;
    }

}



class filterCriteria{

    var $criteriaName = null;           //Nom du critère
    var $criteriaLabel = null;          //Label du critère, texte affiché à gauche du input

    var $criteriaType = null;           //text,list,date,hidden
    var $criteriaDBAName = null;        //Nom de l'element dans la DBA (alias)

    var $selectedValue = null;          //Valeur selectionné dans le cas d'une liste

    var $listRows = array();            //Listes des élements dans le cas d'une liste

    var $onNextLine = false;            //Gestion d'un saut de ligne pour ne pas couper un critère en 2 lignes en cas de largeur insufisante

    var $customCondition = "=";         //=; >; <=; >; >=; uniquement pour les listes et les filtres hiddens
    /*
     * input : array =>
     *                  name
     *                  label
     *                  type
     */
    public function  __construct($name = null, $type = null, $label = null, $criteriaDBAName = null){
        if($name != null && $name != ""){
            $this->criteriaName = $name;
        }else{
            exit(_FW_TABLEAU_ERROR_NEEDVALIDCRITERIANAME);
        }
        if($type != null && $type != ""){
            $this->criteriaType = $type;
        }else{
            exit(_FW_TABLEAU_ERROR_NEEDVALIDCRITERIATYPE);
        }
        if($type != null){
            $this->criteriaLabel = $label;
        }else{
            exit(_FW_TABLEAU_ERROR_NEEDVALIDCRITERIALABEL);
        }
        if($criteriaDBAName != null){
            $this->criteriaDBAName = $criteriaDBAName;
        }
    }

    public function addListRow($value,$label){
        $this->listRows[] = array($value,$label);
    }

    static public function cast(filterCriteria $object) {
        return $object;    
    }

    
}



class TblButton{

    var $buttonLabel = null;
    var $popupMode = false;    
    var $needMultipleSelection = false;
    var $buttonpicture = null;
    var $link = null;
    var $altText = null;
    var $popupWidth = 400;
    var $popupTitle = "";

    var $needInput = false;
    var $inputOk = false;
    var $messageIfInputNotOk = null;

    
    public function  __construct($name = null){
         
    }

    static public function cast(TblButton $object) {
        return $object;
    }
}



?>
