<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');
include_once('include/Treelist-class.php');

Debug::d_echo("acces ", 2,"candidature-spontanee.php");
Debug::d_print_r($_GET, 1,"GET","candidature-spontanee.php");
Debug::d_print_r($_POST, 1,"POST","candidature-spontanee.php");
Debug::d_print_r($_SESSION, 1,"SESSION","candidature-spontanee.php");


$ID_PERSONNE_WEB = "";
if(!empty($_SESSION['awa_candidat_id']) && !empty($_SESSION['awa_candidat_login'])){
    $sql = "SELECT
                personnes.ID_PERSONNE_WEB
            FROM
                awa_candidats
            INNER JOIN personnes ON personnes.ID_PERSONNE_WEB = awa_candidats.ID_PERSONNE_WEB

            WHERE
                awa_candidats.ID =:ID
                AND awa_candidats.LOGIN =:LOGIN
    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID', $_SESSION['awa_candidat_id'], PDO::PARAM_INT);
    $select->bindParam(':LOGIN', $_SESSION['awa_candidat_login'], PDO::PARAM_STR);
    $select->execute();
    $candidat = null;
    $candidat = $select->fetchObject();
    if($candidat){
        
        $ID_PERSONNE_WEB = $candidat->ID_PERSONNE_WEB;
        Debug::d_echo("chargement profil candidat id personne web ".$ID_PERSONNE_WEB, 2,"candidature-spontanee.php");
    }else{
        $_SESSION['awa_candidat_id'] = "";
        $_SESSION['awa_candidat_login'] = "";
    }
}


$personne = null;
$experiences = array();
$formations =  array();
$langues = array();
$documents = array();

if(!empty($ID_PERSONNE_WEB)){
    //personne
    $sql = "SELECT
                personnes.*
            FROM
                personnes

            WHERE
                personnes.ID_PERSONNE_WEB =:ID_PERSONNE_WEB

    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    $personne = null;
    $personne = $select->fetchObject();
    if($personne){
        
    }
    //experiences
    $sql = "SELECT
                experiences_professionnelles.*
            FROM
                experiences_professionnelles

            WHERE
                experiences_professionnelles.ID_PERSONNE_WEB =:ID_PERSONNE_WEB

    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    while($row = $select->fetchObject()){
        $experiences[] = $row;
    }

    //formations
    $sql = "SELECT
                histo_formations.*
            FROM
                histo_formations

            WHERE
                histo_formations.ID_PERSONNE_WEB =:ID_PERSONNE_WEB

    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    while($row = $select->fetchObject()){
        $formations[] = $row;
    }


    //langues
    $sql = "SELECT
                parler_langues.*
            FROM
                parler_langues

            WHERE
                parler_langues.ID_PERSONNE_WEB =:ID_PERSONNE_WEB

    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    while($row = $select->fetchObject()){
        $langues[] = $row;
    }

    //documents
    $sql = "SELECT
                documents.ID_PERSONNE_WEB,
                documents.TITRE_DOC,
                documents.PATHNAME
            FROM
                documents

            WHERE
                documents.ID_PERSONNE_WEB =:ID_PERSONNE_WEB

    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    while($row = $select->fetchObject()){
        $documents[] = $row;
    }
}

$contenu = null;
$_SESSION['cvreaderfileuploadtemp'] = "";
$_SESSION['cvreaderfileuploadname'] = "";
if(count($_FILES) && _USE_CVREADER){

    $date=date('Y-m-d H:i:s');
    $login = _CVREADER_LOGIN;
    $mdp = _CVREADER_PASS;
    $lang = "fr";

    $allow_format = array('application/msword', 'application/octet-stream', 'application/pdf','application/octetstream', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'text/plain','application/force-download','application/vnd.ms-word','image/pjpeg','image/jpeg');
    $allow_extension= array('doc','docx','txt','pdf','jpg');
    $tab_test_type =explode(".",$_FILES['uploadforcvr']['name']);
    $last_case=sizeof($tab_test_type)-1;

    $exten_test=$tab_test_type[$last_case];
    $exten_test = strtolower($exten_test);
    $cv_name = "";
    for($i=0 ; $i<sizeof($tab_test_type)-1 ; $i++) {
        $cv_name.= $tab_test_type[$i];
    }
    $cv_name.= ".".strtolower($exten_test);
    $_FILES['uploadforcvr']['name']=$cv_name;

    if (empty($_FILES['uploadforcvr']['name']) || !in_array($_FILES['uploadforcvr']['type'], $allow_format) || !in_array($exten_test, $allow_extension)  )  {
        $uperror_cv="Le format du document n'est pas pris en compte actuellement!";
    } else  {
        // Désactivation du cache WSDL
        ini_set('soap.wsdl_cache_enabled', 0);

        $cv = utf8_encode($_FILES['uploadforcvr']['name']);
        $cv = ereg_replace('[^0-9a-zA-Z_.]','',$cv);
        list($name_st, $exten_st) = explode(".",$cv);
        //echo $exten_st;
        $dir_cv = "candidatfiles/tempcvupload/";

        $cv = session_id().".".$exten_st;
        $cv2 =  $cv;
        move_uploaded_file($_FILES['uploadforcvr']['tmp_name'], $dir_cv.$cv);
        chmod($dir_cv.$cv,0777);
        $_SESSION['cvreaderfileuploadtemp'] = $dir_cv.$cv;
        $_SESSION['cvreaderfileuploadname'] = $cv_name;
        
        $fhandle = fopen ($dir_cv.$cv, 'rb');
        //$fhandle = fopen ($_FILES['cv_upload']['tmp_name'], 'rb');
        $fcontent = fread ($fhandle, filesize($dir_cv.$cv));
        fclose($fhandle);


        $oldFile = $dir_cv.$cv;
        $cv64 = base64_encode($fcontent);

        $time_start = microtime(true);

        try {
            $client = new SoapClient('http://www.cvreader.fr/cv_reader/cvreader.wsdl');
            $cv = $client->__soapCall('sendCV',array('cv_name'=>$cv, 'cv_content'=>$cv64));
            $response = $client->__soapCall('cvreader', array('cv_name'=>$cv, 'login'=>$login, 'mdp'=>$mdp, 'lang'=>$lang, 'url'=>$_SERVER['SERVER_NAME']));
         
            $xml2 = simplexml_load_string($response->contenu_xml);

            $contenu = simplexml2array($xml2);

            if($response->type_doc=='CV') {

            } else  {
                    $poperror_type_doc="Le document à analyser n'est pas un CV !";
            }
            //echo $popup.";";
        } catch (Exception $e) { echo $e; }; // Gestion des erreurs
    }

}




$sql = "SELECT
            `fo`.COMPULSORY,
            fe.LANGUAGE_KEY,
            fe.REF,
            fo.OPTIONS,
            fo.VALEUR12,
            fo.IS_MULTIPLE

        FROM
            `awa_formulairecs_composition` AS `fo`
        INNER JOIN awa_formulaire_elements AS fe ON fe.id = fo.ID_FORMULAIRE_ELEMENT

        
        GROUP BY `fo`.`ID`

        ORDER BY fo.POSITION ASC
        ";

//print_t($sql);
$select = $conn->prepare($sql);
$select->execute();

$formulaire_elements = array();
while($row = $select->fetchObject()){
    $formulaire_elements[] = $row;
}
$formulaire_JS = "";
$formulaire_CHECKJS = "";
$formulaire_HTML = "";

$formulaireInputs['personne'] = $personne;
$formulaireInputs['experiences'] = $experiences;
$formulaireInputs['formations'] = $formations;
$formulaireInputs['langues'] = $langues;
$formulaireInputs['documents'] = $documents;


foreach($formulaire_elements as $formulaire_element){
    $treelist = null;
    $treelist_multi = false;
    if(!empty($formulaire_element->VALEUR12)){
        $treelist = new Treelist($formulaire_element->VALEUR12);
    }
    if(!empty($formulaire_element->IS_MULTIPLE)){
        $treelist_multi = true;
    }
    $elementReturn = array();
    $elementReturn = getFormulaireHTMLJSelement($formulaire_element->REF,$formulaire_element->COMPULSORY,$formulaire_element->OPTIONS,$formulaireInputs,$treelist,$treelist_multi,$conn,$contenu,"spontane");
    $formulaire_JS .= $elementReturn['JS'];
    $formulaire_CHECKJS .= $elementReturn['CHECKJS'];
    $formulaire_HTML .= $elementReturn['HTML'];
}


formulaire_secu_gen();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title><?php echo _CANDIDATURE_SPONTANE_TITLE; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content=""/>
        <meta name="keywords" content=""/>
        <?php  if(!_USE_CUSTOM_CSS){ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?>getcss_custom.php" rel="stylesheet" type="text/css" />
        <?php }else{ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?><?php echo _TEMPLATE_FOLDER_NAME; ?>/custom.css" rel="stylesheet" type="text/css" />
        <?php } ?>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/framework.js"></script>
        <script type="text/javascript" >
            $.ajaxSettings.cache = false;
            $(document).ready(function(){
                <?php if(_IS_IFRAME_RESIZE){ ?>
                resizeiframe();
                <?php } ?>
            });
            
            function changeLanguage(language){
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-change_language.php", { 'language': language});
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-get_url.php",
                    { 'pageref':'candidaturespontanee', 'langue':language },
                    function(data){
                        window.location=data;
                    }
                );
            }

            <?php echo $formulaire_JS; ?>

            function checkForm(){
                if($('#emailpresence').val() != "0"){
                    alert('<?php echo _CANDIDATURE_EMAIL_PRESENCE_ERROR; ?>');
                    if(confirm('<?php echo _CANDIDATURE_EMAIL_PRESENCE_ASKTOCONNECT; ?>')){
                        var emailToCheck = "";
                        if($('#candi_EX_EMAIL_PERSO').val() != undefined && $('#candi_EX_EMAIL_PERSO').val() != ""){
                            emailToCheck = $('#candi_EX_EMAIL_PERSO').val();
                        }else if($('#candi_EX_EMAIL_PRO').val() != undefined && $('#candi_EX_EMAIL_PRO').val() != ""){
                            emailToCheck = $('#candi_EX_EMAIL_PRO').val();
                        }
                        window.location='<?php echo "http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language'])."?email="; ?>'+ encodeURIComponent(emailToCheck) ;
                    }
                    return false;
                }
                <?php if(empty($ID_PERSONNE_WEB)){ ?>
                if($('#candi_secu').val() == ""){
                    alert('<?php echo _CANDIDATURE_SECU_ERROR; ?>');
                    $('#candi_secu').focus();
                    return false;
                }else{
                    if($('#candi_secu').val() != '<?php echo $_SESSION['secu_total']; ?>'){

                        alert('<?php echo _CANDIDATURE_SECU_ERROR2; ?>');
                        return false;
                    }
                }
                <?php } ?>

                <?php echo $formulaire_CHECKJS; ?>
                        
                $('#candidature_form').submit();
            }

            function checkEmailDba(id){
                <?php
                if(empty($ID_PERSONNE_WEB)){
                    ?>
                    var emailToCheck = "";
                    if($('#candi_EX_EMAIL_PERSO').val() != undefined && $('#candi_EX_EMAIL_PERSO').val() != ""){
                        emailToCheck = $('#candi_EX_EMAIL_PERSO').val();
                    }else if($('#candi_EX_EMAIL_PRO').val() != undefined && $('#candi_EX_EMAIL_PRO').val() != ""){
                        emailToCheck = $('#candi_EX_EMAIL_PRO').val();
                    }

                    if(emailToCheck != ""){
                        $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-check_email.php",
                            { 'email': emailToCheck},
                            function(data){
                                if(data == "1"){
                                    if($('#emailpresence').val() != "1"){
                                        alert('<?php echo _CANDIDATURE_EMAIL_PRESENCE_ERROR; ?>');
                                        if(confirm('<?php echo _CANDIDATURE_EMAIL_PRESENCE_ASKTOCONNECT; ?>')){
                                            window.location='<?php echo "http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_ESPACE_CANDIDAT_".$_SESSION['awa_language'])."?email="; ?>'+ encodeURIComponent(emailToCheck) ;
                                        }
                                    }
                                    $('#emailpresence').val('1');
                                }else{
                                    $('#emailpresence').val('0');
                                }
                        });
                    }
                    <?php
                }
                ?>
            }
            
        </script>
    </head>
    <body>
        <?php
        $templateDatas = array();

        $templateDatas['pathway'] = "";
        $pathway = array();
        $pathway[0]['text'] = _CANDIDATURE_SPONTANE_TITLE;
        $pathway[0]['url'] = null;
        $templateDatas['pathway'] =  displayPathway($pathway);

        $templateDatas['header'] = "";
        $templateDatas['header'] = displayHeader();

        $templateDatas['candidatArea'] = "";
        $templateDatas['candidatArea'] = displayCandidatArea($conn);

        $templateDatas['menu'] = "";
        $templateDatas['menu'] = displayMenu();


        //Footer
        $templateDatas['footerText'] = "";
        $templateDatas['footerText'] = displayFooter();

        $templateDatas['displayCVReaderUpload'] = false;
        if(_USE_CVREADER && empty($_FILES) && empty($ID_PERSONNE_WEB)){
            $templateDatas['displayCVReaderUpload'] = true;
        }
        
        ?>
        <?php include(_TEMPLATE_FOLDER_NAME."/tpl_candidature-spontanee.php"); ?>
    </body>
</html>
<?php

//print_t($_SESSION);
//echo "<pre>";
//print_r($fonctions);
//echo "</pre>";
//echo "<pre>";
//print_r($secteurs);
//echo "</pre>";
?>