<?php
include_once "../include/display_errors_bo.php";
@session_start();

Debug::d_echo("acces ", 2,"v-add-elementpo.php");
Debug::d_print_r($_GET, 1,"GET","v-add-elementpo.php");
Debug::d_print_r($_POST, 1,"POST","v-add-elementpo.php");
Debug::d_print_r($_SESSION, 1,"SESSION","v-add-elementpo.php");

include_once "../../include/config.php";
@include_once '../include/pdo.php';
include_once "../include/framework.php";
include_once "../include/tableau.php";

$starttime = microtime();

/*
 * Variables communes
 */
$errorMsg = "";
$returnMsg = "";
$formsubmit = false;        //true après enregistrement du formulaire
$element_id = null;         //null si add et non null si edit
$edit = false;              //true si edit, false si add


/*
 * Control d'accès
 */
if(empty($_SESSION['boe_user_id'])){
    $_SESSION['boe_user_id'] = null;
}
if(empty ($_SESSION['boe_user_email'])){
    $_SESSION['boe_user_email'] = null;
}
test_session_boe($_SESSION['boe_user_id'], $_SESSION['boe_user_email'], "../", $conn);


/*
 * Traitement des données d'entrées ($_GET)
 */
if(!empty($_GET['element_id'])){
    $element_id = $_GET['element_id'];
    Debug::d_echo("edition formulaire profil ".$element_id, 2,"v-add-elementpo.php");
}else{
    if(!count($_POST)){
        Debug::d_echo("ajout formulaire profil ", 2,"v-add-elementpo.php");
    }
}

$formulairecs_id = "";
$formulairecs_typeid = "";
$formulairecs_position = "";
$formulairecs_compulsory = "";
$formulairecs_treelist = "";
$formulairecs_treelist_multi = "";
$formulairecs_options = "";
if(!empty($element_id)){
    $sql = "
        SELECT
            `fo`.*
            
        FROM
            `awa_formulairepo_composition` AS `fo`
        WHERE
            fo.ID =:idfo

        ";

    $select = $conn->prepare($sql);
    $select->bindParam(':idfo', $element_id, PDO::PARAM_INT);
    $select->execute();
    $formulairecs = null;
    $formulairecs = $select->fetchObject();
    if($formulairecs){
        
        $formulairecs_id = $formulairecs->ID;
        $formulairecs_typeid = $formulairecs->ID_FORMULAIRE_ELEMENT;
        $formulairecs_position = $formulairecs->POSITION;
        $formulairecs_compulsory = $formulairecs->COMPULSORY;
        $formulairecs_treelist = $formulairecs->VALEUR12;
        $formulairecs_treelist_multi = $formulairecs->IS_MULTIPLE;
        $formulairecs_options = $formulairecs->OPTIONS;
    }
    
    
}
if(count($_POST)){
//   print_t($_POST);
   if(!empty($_POST['id'])){
       //Update
       $optionselement = "";
       if($_POST['fcs_type'] == 3){
           $civi_options = array();
           if(!empty($_POST['fcs_civi_m'])){
                $civi_options[] = '_CANDIDATURE_EX_CIVILITY_M';
           }
           if(!empty($_POST['fcs_civi_mm'])){
                $civi_options[] = '_CANDIDATURE_EX_CIVILITY_MM';
           }
           if(!empty($_POST['fcs_civi_ml'])){
                $civi_options[] = '_CANDIDATURE_EX_CIVILITY_ML';
           }
           $optionselement = serialize($civi_options);

       }elseif($_POST['fcs_type'] == 19){
            //option expérience
            $exp_options = array();
            if(!empty($_POST['fcs_exp_title'])){
                $exp_options[] = 'title';
            }
            if(!empty($_POST['fcs_exp_adddel'])){
                $exp_options[] = 'adddel';
            }
            if(!empty($_POST['fcs_exp_enposte'])){
                $exp_options[] = 'enposte';
            }
            if(!empty($_POST['fcs_exp_dates'])){
                $exp_options[] = 'dates';
            }
            if(!empty($_POST['fcs_exp_dates_comp'])){
                $exp_options[] = 'dates_comp';
            }
            if(!empty($_POST['fcs_exp_fonction'])){
                $exp_options[] = 'fonction';
            }
            if(!empty($_POST['fcs_exp_fonction_comp'])){
                $exp_options[] = 'fonction_comp';
            }
            if(!empty($_POST['fcs_exp_function_treelist'])){
                 $exp_options[] = 'function_treelist='.$_POST['fcs_exp_function_treelist'];
            }
            if(!empty($_POST['fcs_exp_function_treelist_multi'])){
                 $exp_options[] = 'function_treelist_multi';
            }
            if(!empty($_POST['fcs_exp_societe'])){
                $exp_options[] = 'societe';
            }
            if(!empty($_POST['fcs_exp_societe_comp'])){
                $exp_options[] = 'societe_comp';
            }
            if(!empty($_POST['fcs_exp_societe_treelist'])){
                $exp_options[] = 'societe_treelist='.$_POST['fcs_exp_societe_treelist'];
            }
            if(!empty($_POST['fcs_exp_societe_treelist_multi'])){
                $exp_options[] = 'societe_treelist_multi';
            }
            if(!empty($_POST['fcs_exp_nb'])){
                $exp_options[] = 'nb='.$_POST['fcs_exp_nb'];
            }
            $optionselement = serialize($exp_options);
        }elseif($_POST['fcs_type'] == 20){
            //option formation
            $exp_options = array();
            if(!empty($_POST['fcs_fo_title'])){
                $exp_options[] = 'title';
            }
            if(!empty($_POST['fcs_fo_adddel'])){
                $exp_options[] = 'adddel';
            }
            if(!empty($_POST['fcs_fo_intitul'])){
                $exp_options[] = 'intitul';
            }
            if(!empty($_POST['fcs_fo_intitul_comp'])){
                $exp_options[] = 'intitul_comp';
            }
            if(!empty($_POST['fcs_fo_intitul_treelist'])){
                $exp_options[] = 'intitul_treelist='.$_POST['fcs_fo_intitul_treelist'];
            }
            if(!empty($_POST['fcs_fo_intitul_treelist_multi'])){
                $exp_options[] = 'intitul_treelist_multi';
            }
            if(!empty($_POST['fcs_fo_etabl'])){
                $exp_options[] = 'etabl';
            }
            if(!empty($_POST['fcs_fo_etabl_comp'])){
                $exp_options[] = 'etabl_comp';
            }
            if(!empty($_POST['fcs_fo_etabl_treelist'])){
                $exp_options[] = 'etabl_treelist='.$_POST['fcs_fo_etabl_treelist'];
            }
            if(!empty($_POST['fcs_fo_etabl_treelist_multi'])){
                $exp_options[] = 'etabl_treelist_multi';
            }
            if(!empty($_POST['fcs_fo_dates'])){
                $exp_options[] = 'dates';
            }
            if(!empty($_POST['fcs_fo_dates_comp'])){
                $exp_options[] = 'dates_comp';
            }
            if(!empty($_POST['fcs_fo_niveau'])){
                $exp_options[] = 'niveau';
            }
            if(!empty($_POST['fcs_fo_niveau_comp'])){
                $exp_options[] = 'niveau_comp';
            }
            if(!empty($_POST['fcs_fo_niveau_treelist'])){
                $exp_options[] = 'niveau_treelist='.$_POST['fcs_fo_niveau_treelist'];
            }
            if(!empty($_POST['fcs_fo_niveau_treelist_multi'])){
                $exp_options[] = 'niveau_treelist_multi';
            }
            if(!empty($_POST['fcs_fo_nb'])){
                $exp_options[] = 'nb='.$_POST['fcs_fo_nb'];
            }
            $optionselement = serialize($exp_options);
        }elseif($_POST['fcs_type'] == 22){
            //option formation
            $exp_options = array();
            if(!empty($_POST['fcs_lg_title'])){
                $exp_options[] = 'title';
            }
            if(!empty($_POST['fcs_lg_adddel'])){
                $exp_options[] = 'adddel';
            }
            if(!empty($_POST['fcs_lg_langue'])){
                $exp_options[] = 'langue';
            }
            if(!empty($_POST['fcs_lg_langue_comp'])){
                $exp_options[] = 'langue_comp';
            }
            if(!empty($_POST['fcs_lg_langue_treelist'])){
                $exp_options[] = 'langue_treelist='.$_POST['fcs_lg_langue_treelist'];
            }
            if(!empty($_POST['fcs_lg_langue_treelist_multi'])){
                $exp_options[] = 'langue_treelist_multi';
            }

            if(!empty($_POST['fcs_lg_niveau'])){
                $exp_options[] = 'niveau';
            }
            if(!empty($_POST['fcs_lg_niveau_comp'])){
                $exp_options[] = 'niveau_comp';
            }
            if(!empty($_POST['fcs_lg_niveau_treelist'])){
                $exp_options[] = 'niveau_treelist='.$_POST['fcs_lg_niveau_treelist'];
            }
            if(!empty($_POST['fcs_lg_niveau_treelist_multi'])){
                $exp_options[] = 'niveau_treelist_multi';
            }
            if(!empty($_POST['fcs_lg_nb'])){
                $exp_options[] = 'nb='.$_POST['fcs_lg_nb'];
            }
            $optionselement = serialize($exp_options);
        }elseif($_POST['fcs_type'] == 21){
            //option empty row
            $exp_options = array();

            if(!empty($_POST['fcs_rt_text'])){
                $exp_options[] = 'rt_text='.$_POST['fcs_rt_text'];
            }

            $optionselement = serialize($exp_options);
        }elseif($_POST['fcs_type'] == 40){
            //option custom
            $exp_options = array();

            if(!empty($_POST['fcs_custom_key'])){
                $exp_options[] = 'custom_key='.$_POST['fcs_custom_key'];
            }
            if(!empty($_POST['fcs_custom_traduction'])){
                $exp_options[] = 'custom_traduction='.$_POST['fcs_custom_traduction'];
            }
            if(!empty($_POST['fcs_custom_compulsory'])){
                $exp_options[] = 'compulsory';
            }
            if(!empty($_POST['fcs_custom_traductionc'])){
                $exp_options[] = 'custom_traductionc='.$_POST['fcs_custom_traductionc'];
            }
            if(!empty($_POST['fcs_custom_dba'])){
                $exp_options[] = 'custom_dba='.$_POST['fcs_custom_dba'];
            }

            $optionselement = serialize($exp_options);
        }
       
        $sql = "
            UPDATE
                awa_formulairepo_composition
            SET
                ID_FORMULAIRE_ELEMENT =:ID_FORMULAIRE_ELEMENT ,
                POSITION =:POSITION ,
                COMPULSORY =:COMPULSORY ,
                VALEUR12 =:VALEUR12 ,
                IS_MULTIPLE =:IS_MULTIPLE ,
                OPTIONS =:options
            WHERE
                ID =:idfo

            ";

        $select = $conn->prepare($sql);
        $select->bindParam(':idfo', $_POST['id'], PDO::PARAM_INT);
        $select->bindParam(':ID_FORMULAIRE_ELEMENT', $_POST['fcs_type'], PDO::PARAM_INT);
        $select->bindParam(':POSITION', $_POST['fcs_position'], PDO::PARAM_INT);
        $select->bindParam(':COMPULSORY', $_POST['fcs_comp'], PDO::PARAM_INT);
        $select->bindParam(':VALEUR12', $_POST['fcs_treelist'], PDO::PARAM_STR);
        $select->bindParam(':IS_MULTIPLE', $_POST['fcs_treelist_multi'], PDO::PARAM_INT);
        $select->bindParam(':options', $optionselement, PDO::PARAM_STR);
        $select->execute();
        Debug::d_echo("sauvegarde formulaire profil ".$_POST['id'], 2,"v-add-elementpo.php");
        echo "<script type='text/javascript' >";
        echo " closePopup_formulairepo('".urlencode(_CONF_FORM_CANDI_OFFRE_ADD_RETURN_MSG2)."')";
        echo "</script>";
   }else{
       //Insert
       $optionselement = "";
       if($_POST['fcs_type'] == 3){
           //option civilité
           $civi_options = array();
           if(!empty($_POST['fcs_civi_m'])){
                $civi_options[] = '_CANDIDATURE_EX_CIVILITY_M';
           }
           if(!empty($_POST['fcs_civi_mm'])){
                $civi_options[] = '_CANDIDATURE_EX_CIVILITY_MM';
           }
           if(!empty($_POST['fcs_civi_ml'])){
                $civi_options[] = '_CANDIDATURE_EX_CIVILITY_ML';
           }
           $optionselement = serialize($civi_options);

        }elseif($_POST['fcs_type'] == 19){
            //option expérience
            $exp_options = array();
            if(!empty($_POST['fcs_exp_title'])){
                $exp_options[] = 'title';
            }
            if(!empty($_POST['fcs_exp_adddel'])){
                $exp_options[] = 'adddel';
            }
            if(!empty($_POST['fcs_exp_enposte'])){
                $exp_options[] = 'enposte';
            }
            if(!empty($_POST['fcs_exp_dates'])){
                $exp_options[] = 'dates';
            }
            if(!empty($_POST['fcs_exp_dates_comp'])){
                $exp_options[] = 'dates_comp';
            }
            if(!empty($_POST['fcs_exp_fonction'])){
                $exp_options[] = 'fonction';
            }
            if(!empty($_POST['fcs_exp_fonction_comp'])){
                $exp_options[] = 'fonction_comp';
            }
            if(!empty($_POST['fcs_exp_function_treelist'])){
                 $exp_options[] = 'function_treelist='.$_POST['fcs_exp_function_treelist'];
            }
            if(!empty($_POST['fcs_exp_function_treelist_multi'])){
                 $exp_options[] = 'function_treelist_multi';
            }
            if(!empty($_POST['fcs_exp_societe'])){
                $exp_options[] = 'societe';
            }
            if(!empty($_POST['fcs_exp_societe_comp'])){
                $exp_options[] = 'societe_comp';
            }
            if(!empty($_POST['fcs_exp_societe_treelist'])){
                $exp_options[] = 'societe_treelist='.$_POST['fcs_exp_societe_treelist'];
            }
            if(!empty($_POST['fcs_exp_societe_treelist_multi'])){
                $exp_options[] = 'societe_treelist_multi';
            }
            if(!empty($_POST['fcs_exp_nb'])){
                $exp_options[] = 'nb='.$_POST['fcs_exp_nb'];
            }
            $optionselement = serialize($exp_options);

        }elseif($_POST['fcs_type'] == 20){
            //option formation
            $exp_options = array();
            if(!empty($_POST['fcs_fo_title'])){
                $exp_options[] = 'title';
            }
            if(!empty($_POST['fcs_fo_adddel'])){
                $exp_options[] = 'adddel';
            }
            if(!empty($_POST['fcs_fo_intitul'])){
                $exp_options[] = 'intitul';
            }
            if(!empty($_POST['fcs_fo_intitul_comp'])){
                $exp_options[] = 'intitul_comp';
            }
            if(!empty($_POST['fcs_fo_intitul_treelist'])){
                $exp_options[] = 'intitul_treelist='.$_POST['fcs_fo_intitul_treelist'];
            }
            if(!empty($_POST['fcs_fo_intitul_treelist_multi'])){
                $exp_options[] = 'intitul_treelist_multi';
            }
            if(!empty($_POST['fcs_fo_etabl'])){
                $exp_options[] = 'etabl';
            }
            if(!empty($_POST['fcs_fo_etabl_comp'])){
                $exp_options[] = 'etabl_comp';
            }
            if(!empty($_POST['fcs_fo_etabl_treelist'])){
                $exp_options[] = 'etabl_treelist='.$_POST['fcs_fo_etabl_treelist'];
            }
            if(!empty($_POST['fcs_fo_etabl_treelist_multi'])){
                $exp_options[] = 'etabl_treelist_multi';
            }
            if(!empty($_POST['fcs_fo_dates'])){
                $exp_options[] = 'dates';
            }
            if(!empty($_POST['fcs_fo_dates_comp'])){
                $exp_options[] = 'dates_comp';
            }
            if(!empty($_POST['fcs_fo_niveau'])){
                $exp_options[] = 'niveau';
            }
            if(!empty($_POST['fcs_fo_niveau_comp'])){
                $exp_options[] = 'niveau_comp';
            }
            if(!empty($_POST['fcs_fo_niveau_treelist'])){
                $exp_options[] = 'niveau_treelist='.$_POST['fcs_fo_niveau_treelist'];
            }
            if(!empty($_POST['fcs_fo_niveau_treelist_multi'])){
                $exp_options[] = 'niveau_treelist_multi';
            }
            if(!empty($_POST['fcs_fo_nb'])){
                $exp_options[] = 'nb='.$_POST['fcs_fo_nb'];
            }
            $optionselement = serialize($exp_options);
        }elseif($_POST['fcs_type'] == 22){
            //option formation
            $exp_options = array();
            if(!empty($_POST['fcs_lg_title'])){
                $exp_options[] = 'title';
            }
            if(!empty($_POST['fcs_lg_adddel'])){
                $exp_options[] = 'adddel';
            }
            if(!empty($_POST['fcs_lg_langue'])){
                $exp_options[] = 'langue';
            }
            if(!empty($_POST['fcs_lg_langue_comp'])){
                $exp_options[] = 'langue_comp';
            }
            if(!empty($_POST['fcs_lg_langue_treelist'])){
                $exp_options[] = 'langue_treelist='.$_POST['fcs_lg_langue_treelist'];
            }
            if(!empty($_POST['fcs_lg_langue_treelist_multi'])){
                $exp_options[] = 'langue_treelist_multi';
            }
            if(!empty($_POST['fcs_lg_niveau'])){
                $exp_options[] = 'niveau';
            }
            if(!empty($_POST['fcs_lg_niveau_comp'])){
                $exp_options[] = 'niveau_comp';
            }
            if(!empty($_POST['fcs_lg_niveau_treelist'])){
                $exp_options[] = 'niveau_treelist='.$_POST['fcs_lg_niveau_treelist'];
            }
            if(!empty($_POST['fcs_lg_niveau_treelist_multi'])){
                $exp_options[] = 'niveau_treelist_multi';
            }
            if(!empty($_POST['fcs_lg_nb'])){
                $exp_options[] = 'nb='.$_POST['fcs_lg_nb'];
            }
            $optionselement = serialize($exp_options);
        }elseif($_POST['fcs_type'] == 21){
            //option formation
            $exp_options = array();

            if(!empty($_POST['fcs_rt_text'])){
                $exp_options[] = 'rt_text='.$_POST['fcs_rt_text'];
            }

            $optionselement = serialize($exp_options);
        }elseif($_POST['fcs_type'] == 40){
            //option custom
            $exp_options = array();

            if(!empty($_POST['fcs_custom_key'])){
                $exp_options[] = 'custom_key='.$_POST['fcs_custom_key'];
            }
            if(!empty($_POST['fcs_custom_traduction'])){
                $exp_options[] = 'custom_traduction='.$_POST['fcs_custom_traduction'];
            }
            if(!empty($_POST['fcs_custom_compulsory'])){
                $exp_options[] = 'compulsory';
            }
            if(!empty($_POST['fcs_custom_traductionc'])){
                $exp_options[] = 'custom_traductionc='.$_POST['fcs_custom_traductionc'];
            }
            if(!empty($_POST['fcs_custom_dba'])){
                $exp_options[] = 'custom_dba='.$_POST['fcs_custom_dba'];
            }

            $optionselement = serialize($exp_options);
        }
        
        $sql = "
            INSERT INTO
                awa_formulairepo_composition
            SET
                ID_FORMULAIRE_ELEMENT =:ID_FORMULAIRE_ELEMENT ,
                POSITION =:POSITION ,
                COMPULSORY =:COMPULSORY,
                VALEUR12 =:VALEUR12 ,
                IS_MULTIPLE =:IS_MULTIPLE ,
                OPTIONS =:options

            ";

        $select = $conn->prepare($sql);
        $select->bindParam(':ID_FORMULAIRE_ELEMENT', $_POST['fcs_type'], PDO::PARAM_INT);
        $select->bindParam(':POSITION', $_POST['fcs_position'], PDO::PARAM_INT);
        $select->bindParam(':COMPULSORY', $_POST['fcs_comp'], PDO::PARAM_INT);
        $select->bindParam(':VALEUR12', $_POST['fcs_treelist'], PDO::PARAM_STR);
        $select->bindParam(':IS_MULTIPLE', $_POST['fcs_treelist_multi'], PDO::PARAM_INT);
        $select->bindParam(':options', $optionselement, PDO::PARAM_STR);
        $select->execute();
        Debug::d_echo("sauvegarde formulaire profil ", 2,"v-add-elementpo.php");

        echo "<script type='text/javascript' >";
        echo " closePopup_formulairepo('".urlencode(_CONF_FORM_CANDI_OFFRE_ADD_RETURN_MSG)."')";
        echo "</script>";
        
   }
}
?>
<script type="text/javascript" >
    function checkForm(){
        if($('#fcs_type').val() == ""){
            alert("<?php echo _CONF_FORM_CANDI_OFFRE_ADD_TYPEELEMENT_JS_ERROR; ?>");
            return false;
        }

        submitFormAjax();
    }
    
    function submitFormAjax(){
        $.ajax({
            "type": "POST",
            "url": "v-add-elementpo.php",
            "cache": false,
            "data": $('#form_entreprise').serialize(),
            "success": function(msg){
                $('#popupdiv_content_formulairepo').html(msg);
            }
        });
    }

    function typecheck(){
        if($('#fcs_type').val() == '3'){
            $('.fcscivi').show();
        }else{
            $('.fcscivi').hide();
        }

        if($('#fcs_type').val() == '19'){
            $('.fcsexp').show();
        }else{
            $('.fcsexp').hide();
        }

        if($('#fcs_type').val() == '20'){
            $('.fcsfo').show();
        }else{
            $('.fcsfo').hide();
        }

        if($('#fcs_type').val() == '22'){
            $('.fcslg').show();
        }else{
            $('.fcslg').hide();
        }

        if($('#fcs_type').val() == '21'){
            $('.fcsrt').show();
        }else{
            $('.fcsrt').hide();
        }

        if($('#fcs_type').val() == '40'){
            $('.fcscustom').show();
        }else{
            $('.fcscustom').hide();
        }




        //TREELIST AFFICHAGE
        if($('#fcs_type').val() == '13'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '17'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '18'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '19'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '20'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '21'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '22'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '23'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '24'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '25'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '37'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '35'){
            $('.trdisp').hide();
        }else if($('#fcs_type').val() == '36'){
            $('.trdisp').hide();
        }else{
            $('.trdisp').show();
        }
    }

</script>
<form enctype="multipart/form-data" onsubmit="checkForm();return false;" action="" method="post" id="form_entreprise">
    <fieldset>
        <input type="hidden" value="<?php echo $formulairecs_id; ?>" id="id" name="id">
         <div class="formrow">
            <label for="fcs_type" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_TYPEELEMENT_LABEL; ?><span class="compulsoryFieldStar">*</span></label>
            <select class="fs12 w150"  style="width: 250px;" id="fcs_type" name="fcs_type" onchange="typecheck();">
                <option value=""><?php echo _CONF_FORM_CANDI_OFFRE_ADD_TYPEELEMENT_VAL_DEFAULT; ?></option>
                <?php
                $sqlpays = "SELECT ID, LANGUAGE_KEY FROM awa_formulaire_elements ORDER BY `ORDER` ASC";
                $selectpa = $conn->prepare($sqlpays);
                $selectpa->execute();
                while($row = $selectpa->fetchObject()){
                    $selected = "";
                    if($row->ID == $formulairecs_typeid){
                        $selected = "selected=\"selected\"";
                    }
                    echo "<option value=\"".$row->ID."\" ".$selected.">".constant($row->LANGUAGE_KEY)."</option>";
                }
                ?>
            </select>
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="fcs_position" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_POSITION_LABEL; ?></label>
            <input type="text" value="<?php echo $formulairecs_position; ?>" style="width: 245px;" class="fs12 w150" id="fcs_position" name="fcs_position">
            <div class="spacer"></div>
        </div>
        <div class="formrow">
            <label for="fcs_comp" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_OBLI_LABEL; ?></label>
            <select class="fs12 w150"  style="width: 250px;" id="fcs_comp" name="fcs_comp" onchange="">
                <option value="0" <?php if($formulairecs_compulsory == "0"){ echo "selected=\"selected\"";} ?>><?php echo _CONF_FORM_CANDI_OFFRE_ADD_OBLI_VAL_0; ?></option>
                <option value="1" <?php if($formulairecs_compulsory == "1"){ echo "selected=\"selected\"";} ?>><?php echo _CONF_FORM_CANDI_OFFRE_ADD_OBLI_VAL_1; ?></option>
            </select>
            <div class="spacer"></div>
        </div>
        <?php
        $hideTreelistcls = "";
        if($formulairecs_typeid == "13"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "17"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "18"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "19"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "20"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "21"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "22"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "23"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "24"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "25"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "37"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "35"){
            $hideTreelistcls = "display:none;";
        }elseif($formulairecs_typeid == "36"){
            $hideTreelistcls = "display:none;";
        }

        ?>
        <div class="formrow trdisp" style="<?php echo $hideTreelistcls; ?>">
            <label for="fcs_treelist" class="w150">Treelist</label>
            <input type="text" value="<?php echo $formulairecs_treelist; ?>" style="width: 245px;" class="fs12 w150" id="fcs_treelist" name="fcs_treelist">
            <div class="spacer"></div>
        </div>
        <div class="formrow trdisp" style="<?php echo $hideTreelistcls; ?>">
            <label for="fcs_comp" class="w150">Treelist multiple</label>
            <select class="fs12 w150"  style="width: 250px;" id="fcs_treelist_multi" name="fcs_treelist_multi" onchange="">
                <option value="0" <?php if($formulairecs_treelist_multi == "0"){ echo "selected=\"selected\"";} ?>><?php echo _CONF_FORM_CANDI_OFFRE_ADD_OBLI_VAL_0; ?></option>
                <option value="1" <?php if($formulairecs_treelist_multi == "1"){ echo "selected=\"selected\"";} ?>><?php echo _CONF_FORM_CANDI_OFFRE_ADD_OBLI_VAL_1; ?></option>
            </select>
            <div class="spacer"></div>
        </div>
        <div style="height: 20px;"></div>
        <?php
        $selected_m = "";
        $selected_mm = "";
        $selected_ml = "";
        $civi_display = "display:none;";
        if($formulairecs_typeid == 3){
            $civi_options = unserialize($formulairecs_options);
            $civi_display = "display:block;";
            
            foreach($civi_options as $civi_option){
                if($civi_option == "_CANDIDATURE_EX_CIVILITY_M"){
                    $selected_m = "checked=\"checked\"";
                }
                if($civi_option == "_CANDIDATURE_EX_CIVILITY_MM"){
                    $selected_mm = "checked=\"checked\"";
                }
                if($civi_option == "_CANDIDATURE_EX_CIVILITY_ML"){
                    $selected_ml = "checked=\"checked\"";
                }
            }
        }
        ?>
        <div class="formrow fcscivi"  style="<?php echo $civi_display; ?>">
            <label for="fcs_civi_m" class="w150"><?php echo _CANDIDATURE_EX_CIVILITY_M; ?></label>
            <input type="checkbox" value="m" style="width: 20px;" class="fs12 w150" id="fcs_civi_m" name="fcs_civi_m" <?php echo $selected_m; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcscivi" style="<?php echo $civi_display; ?>">
            <label for="fcs_civi_mm" class="w150"><?php echo _CANDIDATURE_EX_CIVILITY_MM; ?></label>
            <input type="checkbox" value="mm" style="width: 20px;" class="fs12 w150" id="fcs_civi_mm" name="fcs_civi_mm" <?php echo $selected_mm; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcscivi" style="<?php echo $civi_display; ?>">
            <label for="fcs_civi_ml" class="w150"><?php echo _CANDIDATURE_EX_CIVILITY_ML; ?></label>
            <input type="checkbox" value="ml" style="width: 20px;" class="fs12 w150" id="fcs_civi_ml" name="fcs_civi_ml" <?php echo $selected_ml; ?>/>
            <div class="spacer"></div>
        </div>

        <?php
        $is_title = "";
        $is_add = "";
        $is_enposte = "";
        $is_dates = "";
        $is_dates_comp = "";
        $is_function = "";
        $is_function_comp = "";
        $function_treelist = "";
        $is_function_treelist_multi = "";
        $is_societe = "";
        $is_societe_comp = "";
        $societe_treelist = "";
        $is_societe_treelist_multi = "";
        $nb_default = "";
        $exp_display = "display:none;";

        if($formulairecs_typeid == 19){
            $exp_options = unserialize($formulairecs_options);
            $exp_display = "display:block;";

            foreach($exp_options as $exp_option){
                if($exp_option == "title"){
                    $is_title = "checked=\"checked\"";
                }
                if($exp_option == "adddel"){
                    $is_add = "checked=\"checked\"";
                }
                if($exp_option == "enposte"){
                    $is_enposte = "checked=\"checked\"";
                }
                if($exp_option == "dates"){
                    $is_dates = "checked=\"checked\"";
                }
                if($exp_option == "dates_comp"){
                    $is_dates_comp = "checked=\"checked\"";
                }
                if($exp_option == "fonction"){
                    $is_function = "checked=\"checked\"";
                }
                if($exp_option == "fonction_comp"){
                    $is_function_comp = "checked=\"checked\"";
                }
                if(preg_match('#function_treelist=(.+)#', $exp_option, $match)){
                    $function_treelist = $match[1];
                }
                if($exp_option == "function_treelist_multi"){
                    $is_function_treelist_multi = "checked=\"checked\"";
                }
                if($exp_option == "societe"){
                    $is_societe = "checked=\"checked\"";
                }
                if($exp_option == "societe_comp"){
                    $is_societe_comp = "checked=\"checked\"";
                }
                if(preg_match('#societe_treelist=(.+)#', $exp_option, $match)){
                    $societe_treelist = $match[1];
                }
                if($exp_option == "societe_treelist_multi"){
                    $is_societe_treelist_multi = "checked=\"checked\"";
                }
                if(preg_match('#nb=([0-9])#', $exp_option, $match)){
                    $nb_default = $match[1];
                }
            }

        }
        ?>
        <div class="formrow fcsexp" style="<?php echo $exp_display; ?>">
            <label for="fcs_exp_title" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_EXP_TITREBLOCK_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_title" name="fcs_exp_title" <?php echo $is_title; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsexp" style="<?php echo $exp_display; ?>">
            <label for="fcs_exp_adddel" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_EXP_ADDDEL_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_adddel" name="fcs_exp_adddel" <?php echo $is_add; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsexp" style="<?php echo $exp_display; ?>">
            <label for="fcs_exp_enposte" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_EXP_ENPOSTE_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_enposte" name="fcs_exp_enposte" <?php echo $is_enposte; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsexp" style="<?php echo $exp_display; ?>">
            <label for="fcs_exp_dates" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_EXP_DATES_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_dates" name="fcs_exp_dates" <?php echo $is_dates; ?>/>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_dates_comp" name="fcs_exp_dates_comp" <?php echo $is_dates_comp; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsexp" style="<?php echo $exp_display; ?>">
            <label for="fcs_exp_fonction" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_EXP_FUNCTION_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_fonction" name="fcs_exp_fonction" <?php echo $is_function; ?>/>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_fonction_comp" name="fcs_exp_fonction_comp" <?php echo $is_function_comp; ?>/>
            <input type="text" value="<?php echo $function_treelist; ?>" style="width: 130px;" class="fs12 w150" id="fcs_exp_function_treelist" name="fcs_exp_function_treelist" />
            <input type="checkbox" <?php echo $is_function_treelist_multi; ?> value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_function_treelist_multi" name="fcs_exp_function_treelist_multi" />
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsexp" style="<?php echo $exp_display; ?>">
            <label for="fcs_exp_societe" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_EXP_SOCIETE_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_societe" name="fcs_exp_societe" <?php echo $is_societe; ?>/>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_societe_comp" name="fcs_exp_societe_comp" <?php echo $is_societe_comp; ?>/>
            <input type="text" value="<?php echo $societe_treelist; ?>" style="width: 130px;" class="fs12 w150" id="fcs_exp_societe_treelist" name="fcs_exp_societe_treelist" />
            <input type="checkbox" <?php echo $is_societe_treelist_multi; ?> value="1" style="width: 20px;" class="fs12 w150" id="fcs_exp_societe_treelist_multi" name="fcs_exp_societe_treelist_multi" />
            <div class="spacer"></div>
        </div>

        <div class="formrow fcsexp" style="<?php echo $exp_display; ?>">
            <label for="fcs_exp_nb" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_EXP_NB_LABEL; ?></label>
            <input type="text" value="<?php echo $nb_default; ?>" style="width: 20px;" class="fs12 w150" id="fcs_exp_nb" name="fcs_exp_nb" />
            <div class="spacer"></div>
        </div>


        <?php
        $is_title = "";
        $is_add = "";
        $is_intitul = "";
        $is_intitul_comp = "";
        $intitul_treelist = "";
        $is_intitul_treelist_multi = "";
        $is_etabl = "";
        $is_etabl_comp = "";
        $etabl_treelist = "";
        $is_etabl_treelist_multi = "";
        $is_dates = "";
        $is_dates_comp = "";
        $is_niveau = "";
        $is_niveau_comp = "";
        $niveau_treelist = "";
        $is_niveau_treelist_multi = "";
        $fo_display = "display:none;";

        if($formulairecs_typeid == 20){
            $fo_options = unserialize($formulairecs_options);
            $fo_display = "display:block;";

            foreach($fo_options as $fo_option){
                if($fo_option == "title"){
                    $is_title = "checked=\"checked\"";
                }

                if($fo_option == "adddel"){
                    $is_add = "checked=\"checked\"";
                }

                if($fo_option == "intitul"){
                    $is_intitul = "checked=\"checked\"";
                }
                if($fo_option == "intitul_comp"){
                    $is_intitul_comp = "checked=\"checked\"";
                }
                if(preg_match('#intitul_treelist=(.+)#', $fo_option, $match)){
                    $intitul_treelist = $match[1];
                }
                if($fo_option == "intitul_treelist_multi"){
                    $is_intitul_treelist_multi = "checked=\"checked\"";
                }

                if($fo_option == "etabl"){
                    $is_etabl = "checked=\"checked\"";
                }
                if($fo_option == "etabl_comp"){
                    $is_etabl_comp = "checked=\"checked\"";
                }
                if(preg_match('#etabl_treelist=(.+)#', $fo_option, $match)){
                    $etabl_treelist = $match[1];
                }
                if($fo_option == "etabl_treelist_multi"){
                    $is_etabl_treelist_multi = "checked=\"checked\"";
                }

                if($fo_option == "dates"){
                    $is_dates = "checked=\"checked\"";
                }
                if($fo_option == "dates_comp"){
                    $is_dates_comp = "checked=\"checked\"";
                }
                
                if($fo_option == "niveau"){
                    $is_niveau = "checked=\"checked\"";
                }
                if($fo_option == "niveau_comp"){
                    $is_niveau_comp = "checked=\"checked\"";
                }
                if(preg_match('#niveau_treelist=(.+)#', $fo_option, $match)){
                    $niveau_treelist = $match[1];
                }
                if($fo_option == "niveau_treelist_multi"){
                    $is_niveau_treelist_multi = "checked=\"checked\"";
                }

                if(preg_match('#nb=([0-9])#', $fo_option, $match)){
                    $nb_default = $match[1];
                }
                
            }
        }

        ?>
        <div class="formrow fcsfo" style="<?php echo $fo_display; ?>">
            <label for="fcs_fo_title" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_FO_TITREBLOCK_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_title" name="fcs_fo_title" <?php echo $is_title; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsfo" style="<?php echo $fo_display; ?>">
            <label for="fcs_fo_adddel" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_FO_ADDDEL_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_adddel" name="fcs_fo_adddel" <?php echo $is_add; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsfo" style="<?php echo $fo_display; ?>">
            <label for="fcs_fo_intitul" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_FO_INTITULE_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_intitul" name="fcs_fo_intitul" <?php echo $is_intitul; ?>/>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_intitul_comp" name="fcs_fo_intitul_comp" <?php echo $is_intitul_comp; ?>/>
            <input type="text" value="<?php echo $intitul_treelist; ?>" style="width: 130px;" class="fs12 w150" id="fcs_fo_intitul_treelist" name="fcs_fo_intitul_treelist" />
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_intitul_treelist_multi" name="fcs_fo_intitul_treelist_multi" <?php echo $is_intitul_treelist_multi; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsfo" style="<?php echo $fo_display; ?>">
            <label for="fcs_fo_etabl" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_FO_ETABL_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_etabl" name="fcs_fo_etabl" <?php echo $is_etabl; ?>/>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_etabl_comp" name="fcs_fo_etabl_comp" <?php echo $is_etabl_comp; ?>/>
            <input type="text" value="<?php echo $etabl_treelist; ?>" style="width: 130px;" class="fs12 w150" id="fcs_fo_etabl_treelist" name="fcs_fo_etabl_treelist" />
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_etabl_treelist_multi" name="fcs_fo_etabl_treelist_multi" <?php echo $is_etabl_treelist_multi; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsfo" style="<?php echo $fo_display; ?>">
            <label for="fcs_fo_dates" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_FO_DATE_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_dates" name="fcs_fo_dates" <?php echo $is_dates; ?>/>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_dates_comp" name="fcs_fo_dates_comp" <?php echo $is_dates_comp; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsfo" style="<?php echo $fo_display; ?>">
            <label for="fcs_fo_niveau" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_FO_LEVEL_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_niveau" name="fcs_fo_niveau" <?php echo $is_niveau; ?>/>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_niveau_comp" name="fcs_fo_niveau_comp" <?php echo $is_niveau_comp; ?>/>
            <input type="text" value="<?php echo $niveau_treelist; ?>" style="width: 130px;" class="fs12 w150" id="fcs_fo_niveau_treelist" name="fcs_fo_niveau_treelist" />
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_fo_niveau_treelist_multi" name="fcs_fo_niveau_treelist_multi" <?php echo $is_niveau_treelist_multi; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcsfo" style="<?php echo $fo_display; ?>">
            <label for="fcs_fo_nb" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_FO_NB_LABEL; ?></label>
            <input type="text" value="<?php echo $nb_default; ?>" style="width: 20px;" class="fs12 w150" id="fcs_fo_nb" name="fcs_fo_nb" />
            <div class="spacer"></div>
        </div>



        <?php
        $is_title = "";
        $is_add = "";
        $is_niveau = "";
        $is_niveau_comp = "";
        $niveau_treelist = "";
        $is_niveau_treelist_multi = "";
        $is_langue = "";
        $is_langue_comp = "";
        $langue_treelist = "";
        $is_langue_treelist_multi = "";
        $lg_display = "display:none;";

        if($formulairecs_typeid == 22){
            $lg_options = unserialize($formulairecs_options);
            $lg_display = "display:block;";
            foreach($lg_options as $lg_option){
                if($lg_option == "title"){
                    $is_title = "checked=\"checked\"";
                }

                if($lg_option == "adddel"){
                    $is_add = "checked=\"checked\"";
                }
                if($lg_option == "langue"){
                    $is_langue = "checked=\"checked\"";
                }
                if($lg_option == "langue_comp"){
                    $is_langue_comp = "checked=\"checked\"";
                }
                if(preg_match('#langue_treelist=(.+)#', $lg_option, $match)){
                    $langue_treelist = $match[1];
                }
                if($lg_option == "langue_treelist_multi"){
                    $is_langue_treelist_multi = "checked=\"checked\"";
                }
                

                if($lg_option == "niveau"){
                    $is_niveau = "checked=\"checked\"";
                }
                if($lg_option == "niveau_comp"){
                    $is_niveau_comp = "checked=\"checked\"";
                }
                if(preg_match('#niveau_treelist=(.+)#', $lg_option, $match)){
                    $niveau_treelist = $match[1];
                }
                if($lg_option == "niveau_treelist_multi"){
                    $is_niveau_treelist_multi = "checked=\"checked\"";
                }

                if(preg_match('#nb=([0-9])#', $lg_option, $match)){
                    $nb_default = $match[1];
                }
            }

        }

        ?>
        <div class="formrow fcslg" style="<?php echo $lg_display; ?>">
            <label for="fcs_lg_title" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_LG_TITREBLOCK_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_lg_title" name="fcs_lg_title" <?php echo $is_title; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcslg" style="<?php echo $lg_display; ?>">
            <label for="fcs_lg_adddel" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_LG_ADDDEL_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_lg_adddel" name="fcs_lg_adddel" <?php echo $is_add; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcslg" style="<?php echo $lg_display; ?>">
            <label for="fcs_lg_langue" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_LG_LANGUE_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_lg_langue" name="fcs_lg_langue" <?php echo $is_langue; ?>/>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_lg_langue_comp" name="fcs_lg_langue_comp" <?php echo $is_langue_comp; ?>/>
            <input type="text" value="<?php echo $langue_treelist; ?>" style="width: 130px;" class="fs12 w150" id="fcs_lg_langue_treelist" name="fcs_lg_langue_treelist" />
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_lg_langue_treelist_multi" name="fcs_lg_langue_treelist_multi" <?php echo $is_langue_treelist_multi; ?>/>
            <div class="spacer"></div>
        </div>
        <div class="formrow fcslg" style="<?php echo $lg_display; ?>">
            <label for="fcs_lg_niveau" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_LG_LEVEL_LABEL; ?></label>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_lg_niveau" name="fcs_lg_niveau" <?php echo $is_niveau; ?>/>
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_lg_niveau_comp" name="fcs_lg_niveau_comp" <?php echo $is_niveau_comp; ?>/>
            <input type="text" value="<?php echo $niveau_treelist; ?>" style="width: 130px;" class="fs12 w150" id="fcs_lg_niveau_treelist" name="fcs_lg_niveau_treelist" />
            <input type="checkbox" value="1" style="width: 20px;" class="fs12 w150" id="fcs_lg_niveau_treelist_multi" name="fcs_lg_niveau_treelist_multi" <?php echo $is_niveau_treelist_multi; ?>/>
            <div class="spacer"></div>
        </div>
         <div class="formrow fcslg" style="<?php echo $lg_display; ?>">
            <label for="fcs_lg_nb" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_LG_NB_LABEL; ?></label>
            <input type="text" value="<?php echo $nb_default; ?>" style="width: 20px;" class="fs12 w150" id="fcs_lg_nb" name="fcs_lg_nb" />
            <div class="spacer"></div>
        </div>

        <?php
        $rowText = "";
        $rt_display = "display:none;";

        if($formulairecs_typeid == 21){

            $rt_options = unserialize($formulairecs_options);
            $rt_display = "display:block;";
            foreach($rt_options as $rt_option){
                if(preg_match('#rt_text=(.+)#', $rt_option, $match)){
                    $rowText = $match[1];
                }
            }

        }

        ?>
        <div class="formrow fcsrt" style="<?php echo $rt_display; ?>">
            <label for="fcs_rt_text" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_RT_TEXT_LABEL; ?></label>
            <input type="text" value="<?php echo $rowText; ?>" style="width:245px;" class="fs12 w150" id="fcs_rt_text" name="fcs_rt_text" />
            <div class="spacer"></div>
        </div>


        <?php
        $custom_key = "";
        $custom_translation = "";
        $is_compulsory = "";
        $custom_translationc = "";
        $custom_dba = "";
        $custom_display = "display:none;";

        if($formulairecs_typeid == 40){

            $custom_options = unserialize($formulairecs_options);
            $custom_display = "display:block;";
            foreach($custom_options as $custom_option){
                if(preg_match('#custom_key=(.+)#', $custom_option, $match)){
                    $custom_key = $match[1];
                }
                if(preg_match('#custom_traduction=(.+)#', $custom_option, $match)){
                    $custom_translation = $match[1];
                }
                if($custom_option == "compulsory"){
                    $is_compulsory = "checked=\"checked\"";
                }
                if(preg_match('#custom_traductionc=(.+)#', $custom_option, $match)){
                    $custom_translationc = $match[1];
                }
                if(preg_match('#custom_dba=(.+)#', $custom_option, $match)){
                    $custom_dba = $match[1];
                }
            }

        }

        ?>
        <div class="formrow fcscustom" style="<?php echo $custom_display; ?>">
            <label for="fcs_custom_key" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_CUSTOM_KEY_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $custom_key; ?>" style="width:245px;" class="fs12 w150" id="fcs_custom_key" name="fcs_custom_key" />
            <div class="spacer"></div>
        </div>
        <div class="formrow fcscustom" style="<?php echo $custom_display; ?>">
            <label for="fcs_custom_traduction" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_CUSTOM_TRANSLATION_LABEL; ?> <span class="compulsoryFieldStar">*</span></label>
            <input type="text" value="<?php echo $custom_translation; ?>" style="width:245px;" class="fs12 w150" id="fcs_custom_traduction" name="fcs_custom_traduction" />
            <div class="spacer"></div>
        </div>
        <div class="formrow fcscustom" style="<?php echo $custom_display; ?>">
            <label for="fcs_custom_compulsory" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_CUSTOM_COMPULSORY_LABEL; ?> </label>
            <input type="checkbox" value="1" <?php echo $is_compulsory; ?>  style="width:30px;" class="fs12 w150" id="fcs_custom_compulsory" name="fcs_custom_compulsory" />
            <div class="spacer"></div>
        </div>
        <div class="formrow fcscustom" style="<?php echo $custom_display; ?>">
            <label for="fcs_custom_traductionc" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_CUSTOM_TRANSLATION_ERRORMSG; ?> </label>
            <input type="text" value="<?php echo $custom_translationc; ?>" style="width:245px;" class="fs12 w150" id="fcs_custom_traductionc" name="fcs_custom_traductionc" />
            <div class="spacer"></div>
        </div>
        <div class="formrow fcscustom" style="<?php echo $custom_display; ?>">
            <label for="fcs_custom_dba" class="w150"><?php echo _CONF_FORM_CANDI_OFFRE_ADD_CUSTOM_DBA_LABEL; ?></label>
            <input type="text" value="<?php echo $custom_dba; ?>" style="width:245px;" class="fs12 w150" id="fcs_custom_dba" name="fcs_custom_dba" />
            <div class="spacer"></div>
        </div>


           
        <div style="height: 20px;"></div>
        <div class="center">
            <input type="submit" name="sbm" id="sbm" value="<?php echo _CONF_FORM_CANDI_OFFRE_ADD_SUBMIT_BTN; ?>"/>
        </div>
    </fieldset>
</form>