<?php

define("_CANDIDATURE_ROW_EMPTY","Ligne vide (espace vertical)");
define("_CANDIDATURE_EX_NAME","Nom");
define("_CANDIDATURE_EX_FIRST_NAME","Prénom");
define("_CANDIDATURE_EX_CIVILITY","Civilité");
define("_CANDIDATURE_EX_CIVILITY_DEFAULT","Sélectionnez une civilité");
define("_CANDIDATURE_EX_CIVILITY_M","Monsieur");
define("_CANDIDATURE_EX_CIVILITY_MM","Madame");
define("_CANDIDATURE_EX_CIVILITY_ML","Mademoiselle");
define("_CANDIDATURE_EX_ADDRESS","Addresse");
define("_CANDIDATURE_EX_ZIP","Code postal");
define("_CANDIDATURE_EX_CITY","Ville");
define("_CANDIDATURE_EX_PROVINCE","Région");
define("_CANDIDATURE_EX_COUNTRY","Pays");
define("_CANDIDATURE_EX_PHONE_PERSO","Téléphone");
define("_CANDIDATURE_EX_PHONE_MOBILE","Mobile");
define("_CANDIDATURE_EX_PHONE_PRO","Téléphone pro");
define("_CANDIDATURE_EX_FAX","Fax");
define("_CANDIDATURE_EX_BIRTH_DATE","Date de naissance");
define("_CANDIDATURE_EX_MARITAL_STATUS","Situation personnelle");
define("_CANDIDATURE_EX_CHILDREN_COUNT","Nombre d'enfant");
define("_CANDIDATURE_EX_NATIONALITY","Nationnalité");
define("_CANDIDATURE_EX_EMAIL_PRO","Email pro");
define("_CANDIDATURE_EX_EMAIL_PERSO","Email perso");

define("_CANDIDATURE_EX_EXPERIENCES_BLOC","Expériences");
define("_CANDIDATURE_EX_ENPOSTE","En poste");
define("_CANDIDATURE_EX_EXP_START","Date début");
define("_CANDIDATURE_EX_EXP_END","Date fin");
define("_CANDIDATURE_EX_EXPERIENCE","Fonction");
define("_CANDIDATURE_EX_EXP_COMPANY","Société");

define("_CANDIDATURE_SALARY_FIXE","Salaire fixe");
define("_CANDIDATURE_SALARY_VARIABLE","Salaire variable");
define("_CANDIDATURE_SALARY_TOTAL","Salaire total");
define("_CANDIDATURE_SALARY_ADVANTAGE","Avantage");

define("_CANDIDATURE_EX_FORMATIONS_BLOC","Formations");
define("_CANDIDATURE_EX_EDUCATION","Intitulé");
define("_CANDIDATURE_EX_EDU_SCHOOL","Etablissement");
define("_CANDIDATURE_EX_EDU_START","Date début");
define("_CANDIDATURE_EX_EDU_END","Date fin");
define("_CANDIDATURE_EX_EDU_LEVEL","Niveau du dipôme");

define("_CANDIDATURE_EX_LANGUES_BLOC","Langues");
define("_CANDIDATURE_EX_LANG","Langue");
define("_CANDIDATURE_EX_LANG_LEVEL","Niveau");

define("_CANDIDATURE_EX_AVAILABILITY","Disponibilité");
define("_CANDIDATURE_EX_DRIVING_LICENCE","Permis");
define("_CANDIDATURE_EX_COMMENT","Commentaires");
define("_CANDIDATURE_EX_MOBILITY","Mobilités");
define("_CANDIDATURE_EX_OBJECTIVE","Objectifs");
define("_CANDIDATURE_EX_PUBLICATION","Publications");
define("_CANDIDATURE_EX_REFERENCES","Références");
define("_CANDIDATURE_EX_SKILLS","Compétences");

define("_CANDIDATURE_EX_CV","CV");
define("_CANDIDATURE_EX_LM","Lettre de motivation");
define("_CANDIDATURE_NEW_PASSWORD","Nouveau mot de passe");
define("_CANDIDATURE_EX_SECTOR","Secteur");
define("_CANDIDATURE_EX_FUNCTION","Fonction");

define("_CANDIDATURE_EX_CUSTOM","Champ custom");


define("_MENU_ADMINISTRATION","Administration");                            // ici mettre \'
define("_MENU_ADMINISTRATION_CONF_SYS","Configuration système");            // ici mettre \'
define("_MENU_ADMINISTRATION_DESIGN_FO","Design FO");                       // ici mettre \'
define("_MENU_ADMINISTRATION_CANDIDATURE_OFFRE","Candidature offre");       // ici mettre \'
define("_MENU_ADMINISTRATION_CANDIDATURE_SPN","Candidature spontanée");     // ici mettre \'
define("_MENU_ADMINISTRATION_PROFIL","Profil candidat");                    // ici mettre \'


define("_MENU_MAILING","Mailing candidats");                            // ici mettre \'
define("_MENU_MAILING_RELANCE","Mails relance");                        // ici mettre \'
define("_MENU_MAILING_CANDIDATURE","Mails candidature");                // ici mettre \'
define("_MENU_MAILING_CANDIDATURE_SPN","Mails candidature spontanée");  // ici mettre \'
define("_MENU_MAILING_PROFIL","Mails maj profil");                      // ici mettre \'

define("_MENU_LOGOFF","Déconnexion");                            // ici mettre \'


define("_LOGIN_PAGE_TITLE","ADAWA");
define("_LOGIN_BLOC_TITLE","ADAWA : Login");
define("_LOGIN_FORM_LOGIN_LABEL","Login");
define("_LOGIN_FORM_MDP_LABEL","Mot de passe");
define("_LOGIN_FORM_SUBMIT_BTN","Connexion");   //pas de "
define("_LOGIN_FORM_ERROR_EXPIRE","Session expirée");
define("_LOGIN_FORM_ERROR_BAD_LOGINMDP","Mauvais login ou mot de passe");
define("_LOGIN_FORM_ERROR_EMPTY_LOGINMDP","Login ou mot de passe vide");


define("_CONF_SYS_PAGE_TITLE","Configuration du système");
define("_CONF_SYS_SAVED_MSG","Configuration sauvegardée");
define("_CONF_SYS_JS_REO_ERROR","Veuillez saisir un email destinataire pour les candidatures-offres");  // \"
define("_CONF_SYS_JS_REOS_ERROR","Veuillez saisir un email émetteur pour les candidatures-offres");
define("_CONF_SYS_JS_RES_ERROR","Veuillez saisir un email destinataire pour les candidatures-spontanées");
define("_CONF_SYS_JS_RESS_ERROR","Veuillez saisir un email émetteur pour les candidatures-spontanées");
define("_CONF_SYS_JS_REM_ERROR","Veuillez saisir un email destinataire pour les maj de profils");
define("_CONF_SYS_JS_REMS_ERROR","Veuillez saisir un email émetteur pour les maj de profils");
define("_CONF_SYS_JS_RERS_ERROR","Veuillez saisir un email émetteur pour les relances des candidats");
define("_CONF_SYS_JS_RSH_ERROR","Veuillez saisir un host pour le serveur SMTP");
define("_CONF_SYS_JS_RSHP_ERROR","Veuillez saisir un port pour le serveur SMTP");

define("_CONF_SYS_FORM_REO_LABEL","Email dst candidature offre");
define("_CONF_SYS_FORM_REOS_LABEL","Email src candidature offre");
define("_CONF_SYS_FORM_RENO_LABEL","Nom src candidature offre");
define("_CONF_SYS_FORM_RES_LABEL","Email dst candidature spontanée");
define("_CONF_SYS_FORM_RESS_LABEL","Email src candidature spontanée");
define("_CONF_SYS_FORM_RENS_LABEL","Nom src candidature spontanée");
define("_CONF_SYS_FORM_REM_LABEL","Email dst maj profil");
define("_CONF_SYS_FORM_REMS_LABEL","Email src maj profil");
define("_CONF_SYS_FORM_REMNS_LABEL","Nom src maj profil");
define("_CONF_SYS_FORM_RERS_LABEL","Email src relance candidat");
define("_CONF_SYS_FORM_RERNS_LABEL","Nom src relance candidat");
define("_CONF_SYS_FORM_REH_LABEL","Host SMTP");
define("_CONF_SYS_FORM_RESP_LABEL","Port SMTP ( 25 465 )");
define("_CONF_SYS_FORM_RSA_LABEL","Authentification SMTP");
define("_CONF_SYS_FORM_RSA_VAL_0","Non");
define("_CONF_SYS_FORM_RSA_VAL_1","Oui");

define("_CONF_SYS_FORM_RSU_LABEL","User SMTP");
define("_CONF_SYS_FORM_RSP_LABEL","Pass SMTP");
define("_CONF_SYS_FORM_RSS_LABEL","Sécurité connexion SMTP");
define("_CONF_SYS_FORM_RSS_VAL_DEFAULT","Aucune");
define("_CONF_SYS_FORM_RSS_VAL_SSL","SSL");
define("_CONF_SYS_FORM_RSS_VAL_TLS","TLS");

define("_CONF_SYS_FORM_SUBMIT_BTN","Sauvegarder");  // pas de "



define("_CONF_DESIGN_PAGE_TITLE","Design du FO");
define("_CONF_DESIGN_TBL_ADD_BTN","Ajouter");
define("_CONF_DESIGN_TBL_ADD_BTN_ALT","Ajouter un élément");
define("_CONF_DESIGN_TBL_ADD_BTN_POPUP_TITLE","Ajout d\'un élément");   // \'
define("_CONF_DESIGN_TBL_COL_TITRE","Titre");
define("_CONF_DESIGN_TBL_COL_TITRE_ACT_POPUP","Editer le design");      // \'
define("_CONF_DESIGN_TBL_COL_TITRE_ACT_ALT","Editez le design");    //pas de "
define("_CONF_DESIGN_TBL_COL_DATE_CREATION","Date création");
define("_CONF_DESIGN_TBL_COL_DATE_UPDATE","Date update");
define("_CONF_DESIGN_TBL_COL_ACTIF","Actif");
define("_CONF_DESIGN_TBL_COL_ACTIF_ACT_ALT","Activer"); //pas de "
define("_CONF_DESIGN_TBL_COL_DEL_ACT_ALT","Effacer"); //pas de "
define("_CONF_DESIGN_TBL_COL_DEL_ACT_ASK","Voulez vous vraiment effacer"); //pas de '
define("_CONF_DESIGN_TBL_COL_DEL_ACT_RETURN","effacé");

define("_CONF_DESIGN_ADD_JS_TITRE_ERROR","Veuillez saisir un titre");   // \"
define("_CONF_DESIGN_ADD_JS_HEADER_LOGO_ERROR","Veuillez sélectionner un logo");   // \"
define("_CONF_DESIGN_ADD_JS_BODY_POLICE_ERROR","Veuillez saisir la police par défaut");   // \"
define("_CONF_DESIGN_ADD_JS_BODY_COLOR_ERROR","Veuillez saisir la couleur par défaut");   // \"
define("_CONF_DESIGN_ADD_JS_DATA_POLICE_ERROR","Veuillez saisir la police des données");   // \"
define("_CONF_DESIGN_ADD_JS_PATHWAY_COLOR_ERROR","Veuillez saisir la couleur du chemin de fer");   // \"
define("_CONF_DESIGN_ADD_JS_PATHWAY_COLOR_HOVER_ERROR","Veuillez saisir la couleur du chemin de fer (hover)");   // \"
define("_CONF_DESIGN_ADD_JS_CANDIDAT_AREA_COLOR_ERROR","Veuillez saisir la couleur de la zone de l'utilisateur connecté");   // \"
define("_CONF_DESIGN_ADD_JS_CANDIDAT_AREA_COLOR_HOVER_ERROR","Veuillez saisir la couleur de la zone de l'utilisateur connecté (hover)");   // \"
define("_CONF_DESIGN_ADD_JS_MENU_COLOR_ERROR","Veuillez saisir la couleur du menu");   // \"
define("_CONF_DESIGN_ADD_JS_MENU_COLOR_HOVER_ERROR","Veuillez saisir la couleur du menu (hover)");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_VERTICAL_BAR_ERROR","Veuillez saisir la couleur de la barre verticale");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_HORIZONTAL_BAR_ERROR","Veuillez saisir la couleur de la barre de titre");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_TITRE1_COLOR_ERROR","Veuillez saisir la couleur du titre");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_BUTTON_COLOR_ERROR","Veuillez saisir la couleur des boutons");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_LABEL_COLOR_ERROR","Veuillez saisir la couleur des labels");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_INPUT_COLOR_ERROR","Veuillez saisir la couleur des inputs");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_LISTE_INTITULE_COLOR_ERROR","Veuillez saisir la couleur des intitulés de la liste");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_LISTE_INTITULE_COLOR_HOVER_ERROR","Veuillez saisir la couleur des intitulés de la liste (hover)");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_LISTE_DATE_COLOR_ERROR","Veuillez saisir la couleur des dates de la liste");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_LISTE_CONTRAT_COLOR_ERROR","Veuillez saisir la couleur des contrats de la liste");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_LISTE_LOCATION_COLOR_ERROR","Veuillez saisir la couleur de la location de la liste");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_LISTE_DESC_COLOR_ERROR","Veuillez saisir la couleur de la description de la liste");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_LISTE_PAGINATION_COLOR","Veuillez saisir la couleur de la pagination");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_LISTE_PAGINATION_COLOR_HOVER","Veuillez saisir la couleur de la pagination (hover)");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_FICHE_DESC_COLOR_ERROR","Veuillez saisir la couleur de la description de la fiche");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_FICHE_MISSION_TITLE_COLOR_ERROR","Veuillez saisir la couleur du titre de la mission de la fiche");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_FICHE_MISSION_COLOR_ERROR","Veuillez saisir la couleur de la mission de la fiche");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_FICHE_CUSTOMER_TITLE_COLOR_ERROR","Veuillez saisir la couleur du nom du client de la fiche");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_FICHE_CUSTOMER_COLOR_ERROR","Veuillez saisir la couleur de la description client de la fiche");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_FICHE_CUSTOMER_BORDER_COLOR_ERROR","Veuillez saisir la couleur de la bordure du bloc client de la fiche");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_FICHE_INFO_TITLE_COLOR_ERROR","Veuillez saisir la couleur du titre des infos de la fiche");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_FICHE_INFO_COLOR_ERROR","Veuillez saisir la couleur de la description des infos de la fiche");   // \"
define("_CONF_DESIGN_ADD_JS_CONTENT_FICHE_INFO_BORDER_COLOR_ERROR","Veuillez saisir la couleur de la bordure des infos de la fiche");   // \"
define("_CONF_DESIGN_ADD_JS_FORM_COMPULSORY_STAR_COLOR_ERROR","Veuillez saisir la couleur de l'étoile obligatoire");   // \"
define("_CONF_DESIGN_ADD_JS_FORM_LABEL_COLOR_ERROR","Veuillez saisir la couleur des labels du formulaire de candidature");   // \"
define("_CONF_DESIGN_ADD_JS_FORM_INPUT_COLOR_ERROR","Veuillez saisir la couleur des inputs du formulaire de candidature");   // \"
define("_CONF_DESIGN_ADD_JS_FORM_BLOC_TITLE_COLOR_ERROR","Veuillez saisir la couleur des titres des blocs du formulaire de candidature");   // \"
define("_CONF_DESIGN_ADD_JS_FORM_ADD_COLOR_ERROR","Veuillez saisir la couleur des ajouts du formulaire de candidature");   // \"
define("_CONF_DESIGN_ADD_JS_FORM_ADD_HOVER_COLOR_ERROR","Veuillez saisir la couleur des ajouts du formulaire de candidature (hover)");   // \"
define("_CONF_DESIGN_ADD_JS_FORM_DEL_COLOR_ERROR","Veuillez saisir la couleur des suppression du formulaire de candidature");   // \"
define("_CONF_DESIGN_ADD_JS_FOOTER_COLOR_ERROR","Veuillez saisir la couleur du pied de page");   // \"
define("_CONF_DESIGN_ADD_JS_FOOTER_HOVER_COLOR_ERROR","Veuillez saisir la couleur du pied de page (hover)");   // \"


define("_CONF_DESIGN_ADD_TITRE_LABEL","Titre");
define("_CONF_DESIGN_ADD_ACTIF_LABEL","Actif");
define("_CONF_DESIGN_ADD_ACTIF_VAL_0","Non");
define("_CONF_DESIGN_ADD_ACTIF_VAL_1","Oui");
define("_CONF_DESIGN_ADD_design_HEADER_LOGO_LABEL","Logo");
define("_CONF_DESIGN_ADD_design_HEADER_LOGO_N_LABEL","Nouveau logo ( 980px * 70px )");
define("_CONF_DESIGN_ADD_design_BODY_POLICE_LABEL","Police par défaut");
define("_CONF_DESIGN_ADD_design_BODY_COLOR_LABEL","Couleur par défaut");
define("_CONF_DESIGN_ADD_design_DATA_POLICE_LABEL","Police données");
define("_CONF_DESIGN_ADD_design_PATHWAY_COLOR_LABEL","Couleur du chemin de fer");
define("_CONF_DESIGN_ADD_design_PATHWAY_COLOR_HOVER_LABEL","Couleur du chemin de fer (hover)");
define("_CONF_DESIGN_ADD_design_CANDIDAT_AREA_COLOR_LABEL","Couleur de la zone candidat");
define("_CONF_DESIGN_ADD_design_CANDIDAT_AREA_COLOR_HOVER_LABEL","Couleur de la zone candidat (hover)");
define("_CONF_DESIGN_ADD_design_MENU_COLOR_LABEL","Couleur du menu");
define("_CONF_DESIGN_ADD_design_MENU_COLOR_HOVER_LABEL","Couleur du menu (hover)");
define("_CONF_DESIGN_ADD_design_CONTENT_VERTICAL_BAR_LABEL","Couleur barre vertical");
define("_CONF_DESIGN_ADD_design_CONTENT_HORIZONTAL_BAR_LABEL","Couleur barre titre");
define("_CONF_DESIGN_ADD_design_CONTENT_TITRE1_COLOR_LABEL","Couleur titre");
define("_CONF_DESIGN_ADD_design_CONTENT_BUTTON_COLOR_LABEL","Couleur button formulaire");
define("_CONF_DESIGN_ADD_design_CONTENT_LABEL_COLOR_LABEL","Couleur label");
define("_CONF_DESIGN_ADD_design_CONTENT_INPUT_COLOR_LABEL","Couleur input");
define("_CONF_DESIGN_ADD_design_CONTENT_LISTE_INTITULE_COLOR_LABEL","Liste - Couleur intitulé");
define("_CONF_DESIGN_ADD_design_CONTENT_LISTE_INTITULE_COLOR_HOVER_LABEL","Liste - Couleur intitulé (hover)");
define("_CONF_DESIGN_ADD_design_CONTENT_LISTE_DATE_COLOR_LABEL","Liste - Couleur date");
define("_CONF_DESIGN_ADD_design_CONTENT_LISTE_CONTRAT_COLOR_LABEL","Liste - Couleur contrat");
define("_CONF_DESIGN_ADD_design_CONTENT_LISTE_LOCATION_COLOR_LABEL","Liste - Couleur location");
define("_CONF_DESIGN_ADD_design_CONTENT_LISTE_DESC_COLOR_LABEL","Liste - Couleur description");
define("_CONF_DESIGN_ADD_design_CONTENT_LISTE_PAGINATION_COLOR","Liste - Couleur pagination");
define("_CONF_DESIGN_ADD_design_CONTENT_LISTE_PAGINATION_COLOR_HOVER","Liste - Couleur pagination (hover)");
define("_CONF_DESIGN_ADD_design_CONTENT_FICHE_DESC_COLOR_LABEL","Fiche - Couleur description");
define("_CONF_DESIGN_ADD_design_CONTENT_FICHE_MISSION_TITLE_COLOR_LABEL","Fiche - Couleur titre mission");
define("_CONF_DESIGN_ADD_design_CONTENT_FICHE_MISSION_COLOR_LABEL","Fiche - Couleur mission");
define("_CONF_DESIGN_ADD_design_CONTENT_FICHE_CUSTOMER_TITLE_COLOR_LABEL","Fiche - Couleur nom société");
define("_CONF_DESIGN_ADD_design_CONTENT_FICHE_CUSTOMER_COLOR_LABEL","Fiche - Couleur description société");
define("_CONF_DESIGN_ADD_design_CONTENT_FICHE_CUSTOMER_BORDER_COLOR_LABEL","Fiche - Couleur bordure bloc société");
define("_CONF_DESIGN_ADD_design_CONTENT_FICHE_INFO_TITLE_COLOR_LABEL","Fiche - Couleur titre informations");
define("_CONF_DESIGN_ADD_design_CONTENT_FICHE_INFO_COLOR_LABEL","Fiche - Couleur informations");
define("_CONF_DESIGN_ADD_design_CONTENT_FICHE_INFO_BORDER_COLOR_LABEL","Fiche - Couleur bordure bloc informations");
define("_CONF_DESIGN_ADD_design_FORM_COMPULSORY_STAR_COLOR_LABEL","Form - Couleur étoile obligatoire");
define("_CONF_DESIGN_ADD_design_FORM_LABEL_COLOR_LABEL","Form - Couleur label");
define("_CONF_DESIGN_ADD_design_FORM_INPUT_COLOR_LABEL","Form - Couleur input");
define("_CONF_DESIGN_ADD_design_FORM_BLOC_TITLE_COLOR_LABEL","Form - Couleur titre bloc");
define("_CONF_DESIGN_ADD_design_FORM_ADD_COLOR_LABEL","Form - Couleur ajout");
define("_CONF_DESIGN_ADD_design_FORM_ADD_HOVER_COLOR_LABEL","Form - Couleur ajout (hover)");
define("_CONF_DESIGN_ADD_design_FORM_DEL_COLOR_LABEL","Form - Couleur suppression");
define("_CONF_DESIGN_ADD_design_FORM_DEL_COLOR_HOVER_LABEL","Form - Couleur suppression (hover)");
define("_CONF_DESIGN_ADD_design_FOOTER_COLOR_LABEL","Couleur pied de page");
define("_CONF_DESIGN_ADD_design_FOOTER_HOVER_COLOR_LABEL","Couleur pied de page (hover)");
define("_CONF_DESIGN_ADD_SUBMIT_BTN","Sauvegarder"); //pas de "

define("_CONF_DESIGN_SAVE_MSG","Design sauvegardé"); // \'



define("_CONF_FORM_CANDI_OFFRE_PAGE_TITLE","Formulaire candidature");
define("_CONF_FORM_CANDI_OFFRE_TBL_ADD_BTN","Ajouter");
define("_CONF_FORM_CANDI_OFFRE_TBL_ADD_ALT","Ajouter un élément");
define("_CONF_FORM_CANDI_OFFRE_TBL_ADD_POPUP_TITLE","Ajout d\'un élement"); // \'
define("_CONF_FORM_CANDI_OFFRE_TBL_COL_ELEMENT","Elément");

define("_CONF_FORM_CANDI_OFFRE_TBL_COL_ELEMENT_ACT_ALT","Editez l'élement"); // pas de "
define("_CONF_FORM_CANDI_OFFRE_TBL_COL_ELEMENT_ACT_POPUP","Editer l\'élement"); // \'
define("_CONF_FORM_CANDI_OFFRE_TBL_COL_POSITION","Position");
define("_CONF_FORM_CANDI_OFFRE_TBL_COL_OBLI","Obligatoire");
define("_CONF_FORM_CANDI_OFFRE_TBL_COL_OBLI_ALT","Obligatoire");
define("_CONF_FORM_CANDI_OFFRE_TBL_COL_DEL_ASK","Voulez vous vraiment effacer"); // \'
define("_CONF_FORM_CANDI_OFFRE_TBL_COL_DEL_ALT","Effacer");
define("_CONF_FORM_CANDI_OFFRE_TBL_COL_DEL_RETURN","effacé");

define("_CONF_FORM_CANDI_OFFRE_ADD_TYPEELEMENT_LABEL","Type d'élement");
define("_CONF_FORM_CANDI_OFFRE_ADD_TYPEELEMENT_JS_ERROR","Veuillez sélectionner un type"); // pas "
define("_CONF_FORM_CANDI_OFFRE_ADD_TYPEELEMENT_VAL_DEFAULT","Choisir");
define("_CONF_FORM_CANDI_OFFRE_ADD_POSITION_LABEL","Position");
define("_CONF_FORM_CANDI_OFFRE_ADD_OBLI_LABEL","Obligatoire");
define("_CONF_FORM_CANDI_OFFRE_ADD_OBLI_VAL_0","Non");
define("_CONF_FORM_CANDI_OFFRE_ADD_OBLI_VAL_1","Oui");

define("_CONF_FORM_CANDI_OFFRE_ADD_EXP_TITREBLOCK_LABEL","Titre bloc");
define("_CONF_FORM_CANDI_OFFRE_ADD_EXP_ADDDEL_LABEL","Ajout/suppression");
define("_CONF_FORM_CANDI_OFFRE_ADD_EXP_ENPOSTE_LABEL","En poste");
define("_CONF_FORM_CANDI_OFFRE_ADD_EXP_DATES_LABEL","Dates");
define("_CONF_FORM_CANDI_OFFRE_ADD_EXP_FUNCTION_LABEL","Fonction");
define("_CONF_FORM_CANDI_OFFRE_ADD_EXP_SOCIETE_LABEL","Société");
define("_CONF_FORM_CANDI_OFFRE_ADD_EXP_NB_LABEL","Nombre d'expériences");

define("_CONF_FORM_CANDI_OFFRE_ADD_FO_TITREBLOCK_LABEL","Titre bloc");
define("_CONF_FORM_CANDI_OFFRE_ADD_FO_ADDDEL_LABEL","Ajout/suppression");
define("_CONF_FORM_CANDI_OFFRE_ADD_FO_INTITULE_LABEL","Intitulé");
define("_CONF_FORM_CANDI_OFFRE_ADD_FO_ETABL_LABEL","Etablissement");
define("_CONF_FORM_CANDI_OFFRE_ADD_FO_DATE_LABEL","Dates");
define("_CONF_FORM_CANDI_OFFRE_ADD_FO_LEVEL_LABEL","Niveau");
define("_CONF_FORM_CANDI_OFFRE_ADD_FO_NB_LABEL","Nombre de formations");

define("_CONF_FORM_CANDI_OFFRE_ADD_LG_TITREBLOCK_LABEL","Titre bloc");
define("_CONF_FORM_CANDI_OFFRE_ADD_LG_ADDDEL_LABEL","Ajout/suppression");
define("_CONF_FORM_CANDI_OFFRE_ADD_LG_LANGUE_LABEL","Langue");
define("_CONF_FORM_CANDI_OFFRE_ADD_LG_LEVEL_LABEL","Niveau");
define("_CONF_FORM_CANDI_OFFRE_ADD_LG_NB_LABEL","Nombre de langues");

define("_CONF_FORM_CANDI_OFFRE_ADD_RT_TEXT_LABEL","Texte key");

define("_CONF_FORM_CANDI_OFFRE_ADD_CUSTOM_KEY_LABEL","Email key");
define("_CONF_FORM_CANDI_OFFRE_ADD_CUSTOM_TRANSLATION_LABEL","Label key");
define("_CONF_FORM_CANDI_OFFRE_ADD_CUSTOM_COMPULSORY_LABEL","Obligatoire");
define("_CONF_FORM_CANDI_OFFRE_ADD_CUSTOM_TRANSLATION_ERRORMSG","Erreur key");
define("_CONF_FORM_CANDI_OFFRE_ADD_CUSTOM_DBA_LABEL","Champs en dba");

define("_CONF_FORM_CANDI_OFFRE_ADD_SUBMIT_BTN","Sauvegarder"); // pas "
define("_CONF_FORM_CANDI_OFFRE_ADD_RETURN_MSG","Elément ajouté");
define("_CONF_FORM_CANDI_OFFRE_ADD_RETURN_MSG2","Elément sauvegardé");

define("_CONF_FORM_CANDI_SPN_PAGE_TITLE","Formulaire candidature spontanée");
define("_CONF_FORM_PROFIL_PAGE_TITLE","Formulaire profil candidat");

define("_CONF_FORM_MAIL_RELANCE_PAGE_TITLE","Mailing relance configuration");
define("_CONF_FORM_MAIL_RELANCE_ADD_LABEL","Ajouter");
define("_CONF_FORM_MAIL_RELANCE_ADD_ALT","Ajouter un e-mail dans une nouvelle langue"); //pas "
define("_CONF_FORM_MAIL_RELANCE_ADD_POPUP","Ajout d\'un e-mail dans une nouvelle langue"); // \'
define("_CONF_FORM_MAIL_RELANCE_COL_LANGUE","Langue");
define("_CONF_FORM_MAIL_RELANCE_COL_OBJET","Objet");
define("_CONF_FORM_MAIL_RELANCE_COL_OBJET_ALT","Editez l'e-mail"); //pas "
define("_CONF_FORM_MAIL_RELANCE_COL_OBJET_POPUP","Editer l\'e-mail"); // \'
define("_CONF_FORM_MAIL_RELANCE_COL_BODY","Corps du message");
define("_CONF_FORM_MAIL_RELANCE_COL_DATECREATION","Date création");
define("_CONF_FORM_MAIL_RELANCE_COL_DATEUPDATE","Date update");
define("_CONF_FORM_MAIL_RELANCE_COL_DEFAULT","Défaut");
define("_CONF_FORM_MAIL_RELANCE_COL_DEL_ALT","Effacer"); //pas "
define("_CONF_FORM_MAIL_RELANCE_COL_DEL_ASK","Voulez vous vraiment effacer la langue"); // \'
define("_CONF_FORM_MAIL_RELANCE_COL_DEL_RETURN","effacé"); // \'

define("_CONF_FORM_MAIL_RELANCE_ADD_LANGUE_LABEL","Langue");
define("_CONF_FORM_MAIL_RELANCE_ADD_LANGUE_JS_ERROR","Veuillez saisir le code de la langue (fr, en, de ...)"); // \"
define("_CONF_FORM_MAIL_RELANCE_ADD_OBJET_LABEL","Objet");
define("_CONF_FORM_MAIL_RELANCE_ADD_OBJET_JS_ERROR","Veuillez saisir l'objet"); // \"
define("_CONF_FORM_MAIL_RELANCE_ADD_BODY_LABEL","Corps du message");
define("_CONF_FORM_MAIL_RELANCE_ADD_BODY_JS_ERROR","Veuillez saisir le corps de l'e-mail"); // \"
define("_CONF_FORM_MAIL_RELANCE_ADD_DEFAULT_LABEL","Défaut");
define("_CONF_FORM_MAIL_RELANCE_ADD_DEFAULT_VAL0","Non");
define("_CONF_FORM_MAIL_RELANCE_ADD_DEFAULT_VAL1","Oui");

define("_CONF_FORM_MAIL_RELANCE_ADD_SUBMIT_BTN","Sauvegarder"); // pas "

define("_CONF_FORM_MAIL_RELANCE_ADD_RETURN_MSG","E-mail sauvegardé"); // \'
define("_CONF_FORM_MAIL_RELANCE_ADD_RETURN_MSG2","E-mail ajouté"); // \'

define("_CONF_FORM_MAIL_CANDIDATURE_PAGE_TITLE","Mailing candidature configuration");
define("_CONF_FORM_MAIL_CANDIDATURE_SPN_PAGE_TITLE","Mailing candidature spontanée configuration");

define("_CONF_FORM_MAIL_PROFIL_PAGE_TITLE","Mailing profil configuration");
?>
