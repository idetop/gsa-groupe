// élevel scope settins structure
var MENU_TPL = [
// root level configuration (level 0)
{
	// item sizes
	'height': 20,
	'width': 175,
	// absolute position of the menu on the page (in pixels)
	'block_top':102,
	'block_left':0,
	// offsets between items of the same level (in pixels)
	'top':  0,
	'left': 175,
	// time delay before menu is hidden after cursor left the menu (in milliseconds)
	'hide_delay': 200,
	// submenu expand delay after the rollover of the parent 
	'expd_delay': 200,
	// names of the CSS classes for the menu elements in different states
	// tag: [normal, hover, mousedown]
	'css' : {
		'outer' : ['m0l0oout', 'm0l0oover'],
		'inner' : ['m0l0iout', 'm0l0iover']
	}
},
// sub-menus configuration (level 1)
// any omitted parameters are inherited from parent level
{
	'height': 20,
	'width': 175,
	// position of the submenu relative to top left corner of the parent item
	'block_top': 20,
	'block_left': 0,
	'top': 20,
	'left': 0,	
	'css' : {
		'outer' : ['m0l1oout', 'm0l1oover'],
		'inner' : ['m0l1iout', 'm0l1iover']
	}
},
// sub-sub-menus configuration (level 2)
{
	'block_top': 0,
	'block_left': 151,
	'css' : {
		'outer' : ['m0l2oout', 'm0l2oover'],
		'inner' : ['m0l2iout', 'm0l2iover']
	}
},
// sub-sub-menus configuration (level 3)
{
	'block_top': 0,
	'block_left': 200,
	'width': 225,
	'css' : {
		'outer' : ['m0l3oout', 'm0l3oover'],
		'inner' : ['m0l3iout', 'm0l3iover']
	}
},

// sub-sub-menus configuration (level 4)
{
	'block_top': 0,
	'block_left': 225,
	'width': 220,
	'css' : {
		'outer' : ['m0l4oout', 'm0l4oover'],
		'inner' : ['m0l4iout', 'm0l4iover']
	}
}
// the depth of the menu is not limited
// make sure there is no comma after the last element
];
