<?php
class Treelist{

    var $name = null;   //VALEUR12
    var $childs = array();

    var $idhtml = "";
    var $idInputDisplay = null;
    var $idInputHidden = null;

    var $id  = null;
    var $selectedIDs = array();
    var $isMultiple = false;

    var $conn = null;

    public function  __construct($name) {
        $this->name = $name;
        Debug::d_echo("new treelist ".$name, 2,"Treelist-class.php");
    }

    static public function cast(Treelist $object) {
        return $object;
    }

    public function setConn($conn){
        $this->conn = $conn;
    }

    public function buildChilds(){
        Debug::d_echo("build ".$this->name, 2,"Treelist-class.php");
        
        $sql = "SELECT
                    VALEUR22 AS value,
                    T_ID_MOTCLE AS id
                FROM
                    treelist
                WHERE
                    VALEUR12 = '".$this->name."'
                    AND VALEUR22 = ''
                    
                

        ";
        $select = $this->conn->prepare($sql);
        $select->execute();
        $result = $select->fetchObject();
        if($result){
            $this->id = $result->id;
        }

        $sqlwherelangue = "";
        if(_ADMEN_USE_ADVERT_LINES){
            if(!empty($_SESSION['awa_language'])){
                $sqlwherelangue = " AND ( COUNTRY = '' OR COUNTRY IS NULL OR COUNTRY = '".$_SESSION['awa_language']."' ) ";
            }
        }
        
        $sql = "SELECT 
                    VALEUR22 AS value,
                    T_ID_MOTCLE AS id
                FROM
                    treelist
                WHERE
                    VALEUR12 = '".$this->name."'
                    AND ( VALEUR32 = '' OR VALEUR32 IS NULL )
                    AND ( VALEUR22 != '' AND VALEUR22 IS NOT NULL )
                    ".$sqlwherelangue."
                ORDER BY
                    VALEUR22 ASC

        ";
        $select = $this->conn->prepare($sql);
        $select->execute();
        while($row = $select->fetchObject()){
            $treelistElement = new TreelistElement($this->id,$row->id, $row->value,2);
            $treelistElement->setConn($this->conn);
            $treelistElement->buildChilds();

            $this->childs[] = $treelistElement;
        }
    }

    public function renderHTML(){
        Debug::d_echo("render ".$this->name, 2,"Treelist-class.php");
        $js = "";
        $js .= "<script type='text/javascript' >";

        $js .= "function toggleTreelistChild_".$this->idhtml."(id){";
        $js .= "   $('#div_".$this->idhtml."_child_'+id).toggle();";
        $js .= "";
        $js .= "}";

        if($this->isMultiple){
            $js .= "function displayCheckedBox_".$this->idhtml."(thisinputid){";
            $js .= "    var textInput = '';";
            $js .= "    var hiddenInput = '$$$$';";
            $js .= "    var countnbelement = 0;";
            $js .= "    $.each($(\"input[name='".$this->idhtml."[]']:checked\"), function() {";
            $js .= "        countnbelement += 1;";
            $js .= "        id = $(this).val();";
            $js .= "        hiddenInput += id+'|';";
            $js .= "        if(countnbelement <= 1 ){";
            $js .= "            textInput += $('#span_".$this->idhtml."_'+id).html()+'';";
            $js .= "        }else{";
            $js .= "            textInput += ' / '+$('#span_".$this->idhtml."_'+id).html();";
            $js .= "        }";
            $js .= "    });";
            $js .= "    $('#".$this->idInputDisplay."').val(textInput);";
            $js .= "    $('#".$this->idInputHidden."').val(hiddenInput);";
            $js .= "}";
        }else{
            $js .= "function displayCheckedBox_".$this->idhtml."(thisinputid){";
            $js .= "    var textInput = '';";
            $js .= "    var hiddenInput = '$$$$';";
            $js .= "    var selectedInputID = thisinputid;";
            $js .= "    $.each($(\"input[name='".$this->idhtml."[]']:checked\"), function() {";
            $js .= "        if($(this).val() != selectedInputID) {";
            $js .= "            $(this).attr('checked',false);";
            $js .= "        }else{";
            $js .= "            id = $(this).val();";
            $js .= "            hiddenInput += id+'|';";
            $js .= "            textInput += $('#span_".$this->idhtml."_'+id).html()+'';";
            $js .= "        }";
            $js .= "    });";
            $js .= "    $('#".$this->idInputDisplay."').val(textInput);";
            $js .= "    $('#".$this->idInputHidden."').val(hiddenInput);";
            $js .= "}";
        }
        $js .= "";
        $js .= "";
        $js .= "";
        $js .= "";
        $js .= "</script>";
        

        $css = "";
        $css .= "<style type=\"text/css\">";
        $css .= ".div_treelist{";
        $css .= "border:1px solid #58585A;";
        $css .= "}";
        $css .= ".div_treelist_parent{";
        $css .= "";
        $css .= "}";
        $css .= ".div_treelist_child{";
        $css .= "padding-left:20px;";
        $css .= "display:none;";
        $css .= "}";
        $css .= "";
        

        $html = "";
        $html .= "<div id=\"div_".$this->idhtml."\" class=\"div_treelist\">";
        foreach($this->childs as $child){
            //VALEUR22
            $child = TreelistElement::cast($child);

            $cursorStyle = "";
            $plusforexpend = "";
            if(count($child->childs)){
                $cursorStyle="cursor:pointer;";
                $plusforexpend = "+";
            }
            $checked = "";
            foreach($this->selectedIDs as $selectedID){
                if($selectedID == $child->id){
                    $checked = "checked=\"checked\"";
                }
            }
            $html .= "<div id=\"div_".$this->idhtml."_parent_".$child->id."\" class=\"div_treelist_parent\"><input type=\"checkbox\" ".$checked." name=\"".$this->idhtml."[]\" id=\"".$this->idhtml."_".$child->id."\" value=\"".$child->id."\" onclick=\"displayCheckedBox_".$this->idhtml."(".$child->id.");\"/><span onclick=\"toggleTreelistChild_".$this->idhtml."('".$child->id."');\" style=\"".$cursorStyle."\">".$plusforexpend."</span><span id=\"span_".$this->idhtml."_".$child->id."\" onclick=\"toggleTreelistChild_".$this->idhtml."('".$child->id."');\" style=\"".$cursorStyle."\">".$child->value."</span></div>";

            $html .= "<div id=\"div_".$this->idhtml."_child_".$child->id."\" class=\"div_treelist_child\">";
            $activechild32 = false;
            foreach($child->childs as $child2){
                //VALEUR32
                $child2 = TreelistElement::cast($child2);
                $cursorStyle = "";
                $plusforexpend = "";
                if(count($child2->childs)){
                    $cursorStyle="cursor:pointer;";
                    $plusforexpend = "+";
                }
                $checked = "";
                foreach($this->selectedIDs as $selectedID){
                    if($selectedID == $child2->id){
                        $checked = "checked=\"checked\"";
                        $activechild32 = true;
                    }
                }
                $html .= "<div id=\"div_".$this->idhtml."_parent_".$child2->id."\" class=\"div_treelist_parent\"><input type=\"checkbox\" ".$checked." name=\"".$this->idhtml."[]\" id=\"".$this->idhtml."_".$child2->id."\" value=\"".$child2->id."\" onclick=\"displayCheckedBox_".$this->idhtml."(".$child2->id.");\"/><span onclick=\"toggleTreelistChild_".$this->idhtml."('".$child2->id."');\" style=\"".$cursorStyle."\">".$plusforexpend."</span><span id=\"span_".$this->idhtml."_".$child2->id."\" onclick=\"toggleTreelistChild_".$this->idhtml."('".$child2->id."');\" style=\"".$cursorStyle."\">".$child2->value."</span></div>";
                $html .= "<div id=\"div_".$this->idhtml."_child_".$child2->id."\" class=\"div_treelist_child\">";
                $activechild42 = false;
                foreach($child2->childs as $child3){
                    //VALEUR42
                    $child3 = TreelistElement::cast($child3);
                    $cursorStyle = "";
                    $plusforexpend = "";
                    if(count($child3->childs)){
                        $cursorStyle="cursor:pointer;";
                        $plusforexpend = "+";
                    }
                    $checked = "";
                    foreach($this->selectedIDs as $selectedID){
                        if($selectedID == $child3->id){
                            $checked = "checked=\"checked\"";
                            $activechild42 = true;
                        }
                    }
                    $html .= "<div id=\"div_".$this->idhtml."_parent_".$child3->id."\" class=\"div_treelist_parent\"><input type=\"checkbox\" ".$checked." name=\"".$this->idhtml."[]\" id=\"".$this->idhtml."_".$child3->id."\" value=\"".$child3->id."\" onclick=\"displayCheckedBox_".$this->idhtml."(".$child3->id.");\"/><span onclick=\"toggleTreelistChild_".$this->idhtml."('".$child3->id."');\" style=\"".$cursorStyle."\">".$plusforexpend."</span><span id=\"span_".$this->idhtml."_".$child3->id."\" onclick=\"toggleTreelistChild_".$this->idhtml."('".$child3->id."');\" style=\"".$cursorStyle."\">".$child3->value."</span></div>";
                    $html .= "<div id=\"div_".$this->idhtml."_child_".$child3->id."\" class=\"div_treelist_child\">";
                    $activechild52 = false;
                    foreach($child3->childs as $child4){
                        //VALEUR52
                        $child4 = TreelistElement::cast($child4);
                        $checked = "";
                        foreach($this->selectedIDs as $selectedID){
                            if($selectedID == $child4->id){
                                $checked = "checked=\"checked\"";
                                $activechild52 = true;
                            }
                        }
                        $html .= "<div id=\"div_".$this->idhtml."_parent_".$child4->id."\" class=\"div_treelist_parent\"><input type=\"checkbox\" ".$checked." name=\"".$this->idhtml."[]\" id=\"".$this->idhtml."_".$child4->id."\" value=\"".$child4->id."\" onclick=\"displayCheckedBox_".$this->idhtml."(".$child4->id.");\"/><span id=\"span_".$this->idhtml."_".$child4->id."\" onclick=\"toggleTreelistChild_".$this->idhtml."('".$child4->id."');\" style=\"\">".$child4->value."</span></div>";
                    
                        
                    }
                    if($activechild52){
                        $css .= "#div_".$this->idhtml."_child_".$child3->id."{";
                        $css .= "display:block;";
                        $css .= "}";
                        $css .= "#div_".$this->idhtml."_child_".$child2->id."{";
                        $css .= "display:block;";
                        $css .= "}";                        
                        $css .= "#div_".$this->idhtml."_child_".$child->id."{";
                        $css .= "display:block;";
                        $css .= "}";
                    }
                    $html .= "</div>";
                }
                if($activechild42){
                        $css .= "#div_".$this->idhtml."_child_".$child2->id."{";
                        $css .= "display:block;";
                        $css .= "}";
                        $css .= "#div_".$this->idhtml."_child_".$child->id."{";
                        $css .= "display:block;";
                        $css .= "}";
                    }
                $html .= "</div>";

            }
            if($activechild32){
                $css .= "#div_".$this->idhtml."_child_".$child->id."{";
                $css .= "display:block;";
                $css .= "}";
            }
            $html .= "</div>";
        }
        
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "</div>";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";


        $css .= "</style>";

        return  $js.$css.$html;

    }




    public function renderJS(){

        $js = "";
        $js .= "<script type='text/javascript' >";

        $js .= "function toggleTreelistChild_".$this->idhtml."(id){";
        $js .= "   $('#div_".$this->idhtml."_child_'+id).toggle();";
        $js .= "";
        $js .= "}";

        if($this->isMultiple){
            $js .= "function displayCheckedBox_".$this->idhtml."(thisinputid){";
            $js .= "    var textInput = '';";
            $js .= "    var hiddenInput = '$$$$';";
            $js .= "    var countnbelement = 0;";
            $js .= "    $.each($(\"input[name='".$this->idhtml."[]']:checked\"), function() {";
            $js .= "        countnbelement += 1;";
            $js .= "        id = $(this).val();";
            $js .= "        hiddenInput += id+'|';";
            $js .= "        if(countnbelement <= 1 ){";
            $js .= "            textInput += $('#span_".$this->idhtml."_'+id).html()+'';";
            $js .= "        }else{";
            $js .= "            textInput += ' / '+$('#span_".$this->idhtml."_'+id).html();";
            $js .= "        }";
            $js .= "    });";
            $js .= "    $('#".$this->idInputDisplay."').val(textInput);";
            $js .= "    $('#".$this->idInputHidden."').val(hiddenInput);";
            $js .= "}";
        }else{
            $js .= "function displayCheckedBox_".$this->idhtml."(thisinputid){";
            $js .= "    var textInput = '';";
            $js .= "    var hiddenInput = '$$$$';";
            $js .= "    var selectedInputID = thisinputid;";
            $js .= "    $.each($(\"input[name='".$this->idhtml."[]']:checked\"), function() {";
            $js .= "        if($(this).val() != selectedInputID) {";
            $js .= "            $(this).attr('checked',false);";
            $js .= "        }else{";
            $js .= "            id = $(this).val();";
            $js .= "            hiddenInput += id+'|';";
            $js .= "            textInput += $('#span_".$this->idhtml."_'+id).html()+' ';";
            $js .= "        }";
            $js .= "    });";
            $js .= "    $('#".$this->idInputDisplay."').val(textInput);";
            $js .= "    $('#".$this->idInputHidden."').val(hiddenInput);";
            $js .= "}";
        }
        $js .= "";
        $js .= "";
        $js .= "";
        $js .= "";
        $js .= "</script>";


        $css = "";
        $css .= "<style type=\"text/css\">";
        $css .= ".div_treelist{";
        $css .= "border:1px solid #58585A;";
        $css .= "}";
        $css .= ".div_treelist_parent{";
        $css .= "";
        $css .= "}";
        $css .= ".div_treelist_child{";
        $css .= "padding-left:20px;";
        $css .= "display:none;";
        $css .= "}";
        $css .= "";


        $html = "";
        $html .= "<div id=\"div_".$this->idhtml."\" class=\"div_treelist\">";
        foreach($this->childs as $child){
            //VALEUR22
            $child = TreelistElement::cast($child);

            $cursorStyle = "";
            $plusforexpend = "";
            if(count($child->childs)){
                $cursorStyle="cursor:pointer;";
                $plusforexpend = "+";
            }
            $checked = "";
            foreach($this->selectedIDs as $selectedID){
                if($selectedID == $child->id){
                    $checked = "checked=\"checked\"";
                }
            }
            $html .= "<div id=\"div_".$this->idhtml."_parent_".$child->id."\" class=\"div_treelist_parent\"><input type=\"checkbox\" ".$checked." name=\"".$this->idhtml."[]\" id=\"".$this->idhtml."_".$child->id."\" value=\"".$child->id."\" onclick=\"displayCheckedBox_".$this->idhtml."(".$child->id.");\"/><span onclick=\"toggleTreelistChild_".$this->idhtml."('".$child->id."');\" style=\"".$cursorStyle."\">".$plusforexpend."</span><span id=\"span_".$this->idhtml."_".$child->id."\" onclick=\"toggleTreelistChild_".$this->idhtml."('".$child->id."');\" style=\"".$cursorStyle."\">".$child->value."</span></div>";

            $html .= "<div id=\"div_".$this->idhtml."_child_".$child->id."\" class=\"div_treelist_child\">";
            $activechild32 = false;
            foreach($child->childs as $child2){
                //VALEUR32
                $child2 = TreelistElement::cast($child2);
                $cursorStyle = "";
                $plusforexpend = "";
                if(count($child2->childs)){
                    $cursorStyle="cursor:pointer;";
                    $plusforexpend = "+";
                }
                $checked = "";
                foreach($this->selectedIDs as $selectedID){
                    if($selectedID == $child2->id){
                        $checked = "checked=\"checked\"";
                        $activechild32 = true;
                    }
                }
                $html .= "<div id=\"div_".$this->idhtml."_parent_".$child2->id."\" class=\"div_treelist_parent\"><input type=\"checkbox\" ".$checked." name=\"".$this->idhtml."[]\" id=\"".$this->idhtml."_".$child2->id."\" value=\"".$child2->id."\" onclick=\"displayCheckedBox_".$this->idhtml."(".$child2->id.");\"/><span onclick=\"toggleTreelistChild_".$this->idhtml."('".$child2->id."');\" style=\"".$cursorStyle."\">".$plusforexpend."</span><span id=\"span_".$this->idhtml."_".$child2->id."\" onclick=\"toggleTreelistChild_".$this->idhtml."('".$child2->id."');\" style=\"".$cursorStyle."\">".$child2->value."</span></div>";
                $html .= "<div id=\"div_".$this->idhtml."_child_".$child2->id."\" class=\"div_treelist_child\">";
                $activechild42 = false;
                foreach($child2->childs as $child3){
                    //VALEUR42
                    $child3 = TreelistElement::cast($child3);
                    $cursorStyle = "";
                    $plusforexpend = "";
                    if(count($child3->childs)){
                        $cursorStyle="cursor:pointer;";
                        $plusforexpend = "+";
                    }
                    $checked = "";
                    foreach($this->selectedIDs as $selectedID){
                        if($selectedID == $child3->id){
                            $checked = "checked=\"checked\"";
                            $activechild42 = true;
                        }
                    }
                    $html .= "<div id=\"div_".$this->idhtml."_parent_".$child3->id."\" class=\"div_treelist_parent\"><input type=\"checkbox\" ".$checked." name=\"".$this->idhtml."[]\" id=\"".$this->idhtml."_".$child3->id."\" value=\"".$child3->id."\" onclick=\"displayCheckedBox_".$this->idhtml."(".$child3->id.");\"/><span onclick=\"toggleTreelistChild_".$this->idhtml."('".$child3->id."');\" style=\"".$cursorStyle."\">".$plusforexpend."</span><span id=\"span_".$this->idhtml."_".$child3->id."\" onclick=\"toggleTreelistChild_".$this->idhtml."('".$child3->id."');\" style=\"".$cursorStyle."\">".$child3->value."</span></div>";
                    $html .= "<div id=\"div_".$this->idhtml."_child_".$child3->id."\" class=\"div_treelist_child\">";
                    $activechild52 = false;
                    foreach($child3->childs as $child4){
                        //VALEUR52
                        $child4 = TreelistElement::cast($child4);
                        $checked = "";
                        foreach($this->selectedIDs as $selectedID){
                            if($selectedID == $child4->id){
                                $checked = "checked=\"checked\"";
                                $activechild52 = true;
                            }
                        }
                        $html .= "<div id=\"div_".$this->idhtml."_parent_".$child4->id."\" class=\"div_treelist_parent\"><input type=\"checkbox\" ".$checked." name=\"".$this->idhtml."[]\" id=\"".$this->idhtml."_".$child4->id."\" value=\"".$child4->id."\" onclick=\"displayCheckedBox_".$this->idhtml."(".$child4->id.");\"/><span id=\"span_".$this->idhtml."_".$child4->id."\" onclick=\"toggleTreelistChild_".$this->idhtml."('".$child4->id."');\" style=\"\">".$child4->value."</span></div>";


                    }
                    if($activechild52){
                        $css .= "#div_".$this->idhtml."_child_".$child3->id."{";
                        $css .= "display:block;";
                        $css .= "}";
                        $css .= "#div_".$this->idhtml."_child_".$child2->id."{";
                        $css .= "display:block;";
                        $css .= "}";
                        $css .= "#div_".$this->idhtml."_child_".$child->id."{";
                        $css .= "display:block;";
                        $css .= "}";
                    }
                    $html .= "</div>";
                }
                if($activechild42){
                        $css .= "#div_".$this->idhtml."_child_".$child2->id."{";
                        $css .= "display:block;";
                        $css .= "}";
                        $css .= "#div_".$this->idhtml."_child_".$child->id."{";
                        $css .= "display:block;";
                        $css .= "}";
                    }
                $html .= "</div>";

            }
            if($activechild32){
                $css .= "#div_".$this->idhtml."_child_".$child->id."{";
                $css .= "display:block;";
                $css .= "}";
            }
            $html .= "</div>";
        }

        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "</div>";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";
        $html .= "";


        $css .= "</style>";

        return  $js.$css.$html;

    }
}



class TreelistElement{
    
    var $parentID = null;
    
    var $value = null;
    var $id = null;
    var $treelistLevel = 2;

    var $conn = null;

    var $childs = array();

    public function  __construct($parentID,$id,$value,$level) {
        $this->value = $value;
        $this->id = $id;
        $this->treelistLevel = $level;
        $this->parentID = $parentID;
    }

    static public function cast(TreelistElement $object) {
        return $object;
    }

    public function setConn($conn){
        $this->conn = $conn;
    }

    public function buildChilds(){
        $sql = "SELECT
                    *
                FROM
                    treelist
                WHERE
                    T_ID_MOTCLE = '".$this->id."'


        ";
        $select = $this->conn->prepare($sql);
        $select->execute();
        $parent = $select->fetchObject();

        $sqlwherelangue = "";
        if(_ADMEN_USE_ADVERT_LINES){
            if(!empty($_SESSION['awa_language'])){
                $sqlwherelangue = " AND ( COUNTRY = '' OR COUNTRY IS NULL OR COUNTRY = '".$_SESSION['awa_language']."' ) ";
            }
        }
        if($this->treelistLevel == 2){
            $sql = "SELECT
                    VALEUR32 AS value,
                    T_ID_MOTCLE AS id
                FROM
                    treelist
                WHERE
                    VALEUR12 = '".$parent->VALEUR12."'
                    AND VALEUR22 = '".$this->value."'
                    AND ( VALEUR32 != '' AND VALEUR32 IS NOT NULL )
                    AND ( VALEUR42 = '' OR VALEUR42 IS NULL )
                    ".$sqlwherelangue."
                    
                ORDER BY
                    VALEUR32 ASC

            ";
        }elseif($this->treelistLevel == 3){
            $sql = "SELECT
                    VALEUR42 AS value,
                    T_ID_MOTCLE AS id
                FROM
                    treelist
                WHERE
                    VALEUR12 = '".$parent->VALEUR12."'
                    AND VALEUR22 = '".$parent->VALEUR22."'
                    AND VALEUR32 = '".$this->value."'
                    AND ( VALEUR42 != '' AND VALEUR42 IS NOT NULL )
                    AND ( VALEUR52 = '' OR VALEUR52 IS NULL )
                    ".$sqlwherelangue."
                ORDER BY
                    VALEUR42 ASC

            ";
        }elseif($this->treelistLevel == 4){
            $sql = "SELECT
                    VALEUR52 AS value,
                    T_ID_MOTCLE AS id
                FROM
                    treelist
                WHERE
                    VALEUR12 = '".$parent->VALEUR12."'
                    AND VALEUR22 = '".$parent->VALEUR22."'
                    AND VALEUR32 = '".$parent->VALEUR32."'
                    AND VALEUR42 = '".$this->value."'
                    AND ( VALEUR52 != '' AND VALEUR52 IS NOT NULL )
                    ".$sqlwherelangue."
                ORDER BY
                    VALEUR42 ASC

            ";
        }

        
        $select = $this->conn->prepare($sql);
        $select->execute();
        while($row = $select->fetchObject()){
            $treelistElement = new TreelistElement($this->id,$row->id, $row->value,$this->treelistLevel+1);
            $treelistElement->setConn($this->conn);

            if($this->treelistLevel == 2 ||  $this->treelistLevel == 3){
                $treelistElement->buildChilds();
            }


            $this->childs[] = $treelistElement;
        }
    }
}

?>
