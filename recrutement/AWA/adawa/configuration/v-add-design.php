<?php
include_once "../include/display_errors_bo.php";
@session_start();

Debug::d_echo("acces ", 2,"v-add-design.php");
Debug::d_print_r($_GET, 1,"GET","v-add-design.php");
Debug::d_print_r($_POST, 1,"POST","v-add-design.php");
Debug::d_print_r($_SESSION, 1,"SESSION","v-add-design.php");

include_once('../../include/config.php');
@include_once '../include/pdo.php';
include_once "../include/framework.php";
include_once "../include/tableau.php";

$starttime = microtime();

/*
 * Variables communes
 */
$errorMsg = "";
$returnMsg = "";
$formsubmit = false;        //true après enregistrement du formulaire
$element_id = null;         //null si add et non null si edit
$edit = false;              //true si edit, false si add

/*
 * Control d'accès
 */
if(empty($_SESSION['boe_user_id'])){
    $_SESSION['boe_user_id'] = null;
}
if(empty ($_SESSION['boe_user_email'])){
    $_SESSION['boe_user_email'] = null;
}
test_session_boe($_SESSION['boe_user_id'], $_SESSION['boe_user_email'], "../", $conn);


/*
 * Traitement des données d'entrées ($_GET)
 */
if(!empty($_GET['element_id'])){
    $element_id = $_GET['element_id'];
}

echo "<iframe src=\"v-uploaddesign.php?element_id=".$element_id."\" frameborder=\"0\" style=\"height: 1580px; border: 0;width: 520px;display:block;border-style: none; padding:0; margin:0;background-color:#FAFAFA;\"></iframe>";
