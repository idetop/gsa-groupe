GSA Groupe
==========

## Outils projet

- Versioning : [Git](http://git-scm.com)
- [EditorConfig](http://editorconfig.org)

## Tools
- CMS Maison réalisé initialement par Pascal Koch <pasc.koch@free.fr>, voir notes ci-après.
- [Composer](https://getcomposer.org/doc/) a été ajouté a posteriori pour utiliser des libraires PHP externes, les anciennes librairies n’ont pas été portées vers Composer.
- [PHPdotenv](https://github.com/vlucas/phpdotenv) permet de définir des paramètres de configuration en fonction de l’environnement à l’aide d’un fichier `.env` situé à la racine du projet. Les informations confidentielles (password, mails…) doivent être renseignés dans ce fichier.


## Base de données
Une migration de PHP 5.2 à 5.6 a rendue nécessaire une réécriture minimale du système de connexion à la base de données. L’ensemble du code nécessaire est désormais réuni dans le fichier `/mysql/mysql.php`. 

