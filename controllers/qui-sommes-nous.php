<?php
// CONNEXION MYSQL
include('mysql/mysql.php');

##################################
# slides QUI SOMMES-NOUS
##################################
function slides_qui()
{
    $sql = mysqli_query($connexion, "SELECT * FROM gsa_about_slider ORDER BY ordre ASC");
    while($row = mysqli_fetch_array($sql)){
        echo '
		<div class="panel">
			<div class="panel-wrapper">
				<div class="legende-qui">' . stripslashes($row['texte']) . '</div>
			</div>
		</div>';
    }
}

##################################
# texte Ambition
##################################
function ambition()
{
    $sql = mysqli_query($connexion, "SELECT * FROM gsa_about_ambition WHERE id = '1'");
	$dat = mysqli_fetch_array($sql);
	echo stripslashes($dat['texte']);
}

##################################
# les hommes et les femmes clés
##################################
function equipe()
{
    $sql = mysqli_query($connexion, "SELECT * FROM gsa_about_equipe ORDER BY statut,ordre ASC");
    while ($row = mysqli_fetch_array($sql)) {
        echo '
		<div class="membre'.$row['statut'] . '">
            <img src="images/'.$row['photo'] . '" />
            <h4>' . stripslashes($row['nom']) . '</h4>
            <h5>' . stripslashes($row['poste']) . '<br />' . stripslashes($row['pole']) . '</h5>
        </div>';
    }
}

##################################
# slides HISTORIQUES
##################################
function slides_story($side)
{
    $sql      = mysqli_query($connexion, "SELECT * FROM gsa_about_historique ORDER BY annee ASC");
    $i        = 1;
    $selected = '';

    // colonne gauche
    if ($side == 'left') {
        while ($row = mysqli_fetch_array($sql)) {
            echo '<li id="#dates"><a href="#' . $row['annee'] . '" ' . $selected . '>' . $row['annee'] . '</a></li>';
            $i++;
        }
    }
    // slides
    else {
        while ($row = mysqli_fetch_array($sql)) {
            echo '
            <li id="' . $row['annee'] . '" ' . $selected . '>
                <h1>' . $row['annee'] . '</h1>
                <img src="js/timeline/images/arrow-p-issues.png" class="arrow-p" /><br style="clear:both" />
                <p>' . stripslashes($row['texte']) . '</p>
            </li>';
            $i++;
        }
    }
}
