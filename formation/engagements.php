s<?php include('controllers/function_textes.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    
<head>
    <title>Portail de GSA, une entreprise du groupe GTF</title>
    <meta name="google-site-verification" content="cfM15zJRYHBtIL-8teSrrVgsNsNz42UCDbYARVLyP8w" />
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <meta name="description" content="GSA, une entreprise du groupe GTF." />
    <meta name="keywords" content="" />
    <link type="text/css" rel="stylesheet" href="css/styles.css" />
    <link href="http://fonts.googleapis.com/css?family=Michroma" rel="stylesheet" type="text/css" />
</head> 

<body class="type">

<?php include('../header.php'); ?>
	
<!-- BANDEAU -->
<div id="wrapper">
	<div id="bandeau2">
		<a href="contact.php" class="contact">contacter GSA formation</a>
		<a href="index.php" class="retour-accueil">Retour Accueil</a>
		<a href="http://gsa-groupe.fr" class="gsa-groupe">Visiter GSA Groupe</a>
		<img src="images/titre-disponibilite.png" />
		<p><?php textes('intro',$pole,$_SERVER['REQUEST_URI']); ?></p>
	</div>
	<!-- CONTENU -->
	<div id="contenu">
		<!-- colonne de gauche : menu -->
		<?php include('menu_left.php'); ?>
        
		<!-- colonne centrale : contenu principal -->
		<div id="type-central">
			<?php textes('articles',$pole,$_SERVER['REQUEST_URI']);  ?>
		</div>
		
		<!-- colonne droite : contenu annexe -->
		<div id="type-coldroite">
		
			<br />
			<!--
			<h3>La charte de d�ontologie</h3>
			<p>
			GSA Formation est signataire de la charte de d�ontologie des membres de l'organisation professionnelle de la prestation de services aux industries de sant�.<br />
			<a href="../oppsis.html" target="_blank" style="text-decoration:none; color: #222222;"> > Consulter la charte</a>
			</p>
			-->
			<br />
			<img src="images/illus-coldroite.jpg" />
		</div>
	</div>
</div>

<?php include('../footer.php'); ?>

</body>
</html>