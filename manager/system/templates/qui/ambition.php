<?php include('system/libs/tinymce/tinymce.php');  ?>


    <!-- Bouton nouveau -->
    <div id="body_btn_nouveau">
        <a href="<?= $site; ?>/qui-sommes-nous.html" class="link_nouveau" target="_blank">
            <div class="btn_nouveau"><span class="span_nouveau">Accéder à la page <font color="#b9001f">&raquo;</font></span></div>
        </a>
    </div>
    <div class="clear"></div>
</div>


<div id="content">
    <!-- Colonne gauche -->
    <?php include($b_templates.'side_left.php'); ?>

    <!-- Colonne droite -->
    <div class="colonne_right">

		<div id="title_line"><span class="span_title_line">Notre ambition</span></div>

		<form name="modif_fiche" method="post" action="" enctype="multipart/form-data">
            <table class="table_right" width="760" cellpadding="6" cellspacing="0" style="border:1px solid #e9e7e7;border-bottom:0;">
                <tbody>
                    <tr style="background:#252525;">
                        <td colspan="2" style="padding:6px;color:#fff;">
                            &nbsp;&nbsp;<strong style="font-size:11px;">Texte</strong>
                        </td>
                    </tr>

                    <tr style="background:#ffffff">
                        <td valign="top" colspan="2" style="padding:10px;margin-right:20px;">
                            <textarea name="texte" style="position:relative;top:8px;width:500px;height:450px"><?= stripslashes($data_edit['texte']); ?></textarea>
                        </td>
                    </tr>

                    <tr><td colspan="2" height="10"></td></tr>

                </tbody>
            </table>
            <table class="table_right" width="760" cellpadding="0" cellspacing="0">
                <tbody>
                    <tr style="background:#e9e7e7;">
                        <td class="td_content" style="text-align:right;padding-top:0;padding-bottom:0;">
                            <input type="submit" name="update" class="table_submit" value="enregistrer mes modifications" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
		</form>

    </div>
    <div class="clear"></div>
</div>
