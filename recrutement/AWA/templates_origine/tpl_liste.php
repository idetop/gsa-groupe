<div id="container980">
    <div id="header"></div>
    <div id="subheader">
        <div id="pathway">
            <?php  echo $templateDatas['pathway'];  ?>
        </div>
        <div id="languesdisplay">
            <?php echo $templateDatas['flags']; ?>
        </div>
        <div class="spacer"></div>
    </div>
    <?php echo $templateDatas['candidatArea']; ?>
    <div id="middle">
        <div id="menu">
            <?php echo $templateDatas['menu']; ?>
        </div>
        <div id="content">
            <div id="location_popup_menu" >
                <?php  echo $templateDatas['filters']['countryPopupContent'];  ?>
            </div>
            <div class="contenttitle"><?php echo _OFFRE_LISTE_RECHERCHE_TITLE; ?></div>

            <div style="height: 20px;"></div>
            <div>

                <div style="float: left; width: 352px; padding-left: 20px;">
                    <div class="searchRow">
                        <div class="label labeldesign"><?php echo _OFFRE_LISTE_RECHERCHE_FONCTION_LABEL; ?></div>
                        <div class="input">
                            <select id="search_fonction" class="inputdesign">
                                <option value=""><?php echo _OFFRE_LISTE_RECHERCHE_FONCTION_DEFAULT; ?></option>
                                <?php echo $templateDatas['filters']['functionListContent']; ?>
                            </select>
                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="searchRow">
                        <div class="label labeldesign"><?php echo _OFFRE_LISTE_RECHERCHE_LOCATION_LABEL; ?></div>
                        <div class="input">
                            <input type="text" id="search_location" class="inputdesign" value="<?php echo $templateDatas['filters']['locationSelectedValue']; ?>" onclick="$('#location_popup_menu').show();" />
                        </div>
                        <div class="spacer"></div>
                    </div>
                </div>
                <div style="float: left;width: 352px; padding-left: 20px;">
                    <div class="searchRow">
                        <div class="label labeldesign"><?php echo _OFFRE_LISTE_RECHERCHE_SECTEUR_LABEL; ?></div>
                        <div class="input">
                            <select id="search_secteur" class="inputdesign">
                                <option value=""><?php echo _OFFRE_LISTE_RECHERCHE_SECTEUR_DEFAULT; ?></option>
                                <?php echo $templateDatas['filters']['sectorListContent']; ?>
                            </select>
                        </div>
                        <div class="spacer"></div>
                    </div>

                    <div class="searchRow">
                        <div class="label labeldesign"><?php echo _OFFRE_LISTE_RECHERCHE_KEYWORD_LABEL; ?></div>
                        <div class="input">
                            <input type="text" id="search_keyword" class="inputdesign" value="<?php echo $templateDatas['filters']['keywordSelectedValue']; ?>" />
                        </div>
                        <div class="spacer"></div>
                    </div>
                </div>
                <div class="spacer"></div>
            </div>

            <div style="height: 20px;"></div>
            <div id="buttonsarealiste">
                <div id="rechercherbtn" class="buttonclass" onclick="saveSearch();"><?php echo _OFFRE_LISTE_RECHERCHE_SUBMIT; ?></div>
            </div>


            <div style="height: 5px;"></div>
            <div class="contenttitle"><h1><?php echo _OFFRE_LISTE_TITLE; ?></h1></div>
            <?php
            /*
             * Boucle d'affichage des annonces
             */
            foreach($annonces as $annonce){
                ?>
                <div class="offre-intro">
                    <div class="row1">
                        <div class="intitule"><h2><a href="<?php echo rewriteAnnonceUrl($annonce->ID_ANNONCE,$annonce->LIBELLE); ?>" title="" ><?php echo $annonce->LIBELLE; ?> - <?php echo $annonce->REFERENCE; ?></a></h2></div>
                        <div class="datepublication"><?php if(!empty($annonce->DATE_DEBUT) && $annonce->DATE_DEBUT!="0000-00-00"){echo getDateDisplay($annonce->DATE_DEBUT,"-");} ?></div>
                        <div class="spacer"></div>
                    </div>
                    <div class="row2">
                        <div class="details">
                            <?php if(!empty($annonce->TYPECONTRAT)){ echo $annonce->TYPECONTRAT." - ";} ?>
                            <?php
                            if(!(empty($annonce->SALARY_FROM) && empty($annonce->SALARY_TO))){
                                 if(!empty($annonce->SALARY_FROM) && !empty($annonce->SALARY_TO)){
                                    echo $annonce->SALARY_FROM;
                                    echo " "._OFFRE_LISTE_SALARY_TO." ";
                                    echo $annonce->SALARY_TO;
                                }else{
                                    if(!empty($annonce->SALARY_FROM)){ echo $annonce->SALARY_FROM;}
                                    if(!empty($annonce->SALARY_TO)){ echo $annonce->SALARY_TO;}
                                }
                                echo " &euro;";
                            }
                            ?>
                        </div>
                        <div class="localisation">
                            <?php echo $annonce->PAYS; ?>
                            <?php echo $annonce->REGION; ?>
                            <?php echo $annonce->CITY; ?>
                        </div>
                        <div class="spacer"></div>
                    </div>
                    <div class="row3"><?php echo $annonce->TEXTE_ANNONCE; ?></div>
                </div>
                <?php
            }
            ?>

            <?php echo $templateDatas['noResultText']; ?>
            
            <div style="height: 20px;"></div>
            <div id="pagination">
                <?php if(_SEPARATE_SEARCH) { ?>
                <span class="pagi"><a href ="<?php echo $templateDatas['urlToSearchEngine']; ?>" alt=""><?php echo _OFFRE_LISTE_NEW_SEARCH; ?></a></span> 
                <?php } ?>
                <?php echo $templateDatas['pagination']; ?>
            </div>
            <div style="height: 20px;"></div>
        </div>
        <div class="spacer"></div>
    </div>
    <div id="footer">
         <?php echo $templateDatas['footerText']; ?>
    </div>
</div>