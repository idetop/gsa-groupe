<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
//include_once('include/pdo.php');
//include_once('include/framework.php');

Debug::d_echo("deconnexion candidat ".$_SESSION['awa_candidat_id'], 2,"logoff.php");
Debug::d_print_r($_GET, 1,"GET","logoff.php");
Debug::d_print_r($_POST, 1,"POST","logoff.php");
Debug::d_print_r($_SESSION, 1,"SESSION","logoff.php");

if(isset($_SESSION['awa_candidat_id'])){
     $_SESSION['awa_candidat_id'] = "";
}
if(isset($_SESSION['awa_candidat_login'])){
    $_SESSION['awa_candidat_login'] = "";
}
header("Location: http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER);
exit();
?>
