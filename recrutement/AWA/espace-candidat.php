<?php
session_start();
include_once('include/config.php');
include_once('include/display_errors_fo.php');
include_once('include/pdo.php');
include_once('include/framework.php');
include_once('include/Treelist-class.php');

Debug::d_echo("acces ", 2,"espace-candidat.php");
Debug::d_print_r($_GET, 1,"GET","espace-candidat.php");
Debug::d_print_r($_POST, 1,"POST","espace-candidat.php");
Debug::d_print_r($_SESSION, 1,"SESSION","espace-candidat.php");

$backToOfferId = "";
if(!empty($_GET['oid'])){
    $backToOfferId = $_GET['oid'];
}

$source = "";
if(!empty($_GET['source'])){
    $source = $_GET['source'];
}

$ID_PERSONNE_WEB = "";
if(!empty($_SESSION['awa_candidat_id']) && !empty($_SESSION['awa_candidat_login'])){
    $sql = "SELECT
                personnes.ID_PERSONNE_WEB
            FROM
                awa_candidats
            INNER JOIN personnes ON personnes.ID_PERSONNE_WEB = awa_candidats.ID_PERSONNE_WEB

            WHERE
                awa_candidats.ID =:ID
                AND awa_candidats.LOGIN =:LOGIN
    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID', $_SESSION['awa_candidat_id'], PDO::PARAM_INT);
    $select->bindParam(':LOGIN', $_SESSION['awa_candidat_login'], PDO::PARAM_STR);
    $select->execute();
    $candidat = null;
    $candidat = $select->fetchObject();
    if($candidat){
        $ID_PERSONNE_WEB = $candidat->ID_PERSONNE_WEB;
        Debug::d_echo("affichage profil candidat id personne web ".$ID_PERSONNE_WEB, 2,"espace-candidat.php");
    }else{
        $_SESSION['awa_candidat_id'] = "";
        $_SESSION['awa_candidat_login'] = "";
    }
}

$personne = null;
$experiences = array();
$formations =  array();
$langues = array();
$documents = array();

$formulaire_JS = "";
$formulaire_CHECKJS = "";
$formulaire_HTML = "";
    
if(!empty($ID_PERSONNE_WEB)){
    //personne
    $sql = "SELECT
                personnes.*
            FROM
                personnes

            WHERE
                personnes.ID_PERSONNE_WEB =:ID_PERSONNE_WEB
                
    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    
    $personne = $select->fetchObject();
    
    //experiences
    $sql = "SELECT
                experiences_professionnelles.*
            FROM
                experiences_professionnelles

            WHERE
                experiences_professionnelles.ID_PERSONNE_WEB =:ID_PERSONNE_WEB

    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    while($row = $select->fetchObject()){
        $experiences[] = $row;
    }

    //formations
    $sql = "SELECT
                histo_formations.*
            FROM
                histo_formations

            WHERE
                histo_formations.ID_PERSONNE_WEB =:ID_PERSONNE_WEB

    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    while($row = $select->fetchObject()){
        $formations[] = $row;
    }


    //langues
    $sql = "SELECT
                parler_langues.*
            FROM
                parler_langues

            WHERE
                parler_langues.ID_PERSONNE_WEB =:ID_PERSONNE_WEB

    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    while($row = $select->fetchObject()){
        $langues[] = $row;
    }

    //documents
    $sql = "SELECT
                documents.ID_PERSONNE_WEB,
                documents.TITRE_DOC,
                documents.PATHNAME
            FROM
                documents

            WHERE
                documents.ID_PERSONNE_WEB =:ID_PERSONNE_WEB

    ";
    $select = $conn->prepare($sql);
    $select->bindParam(':ID_PERSONNE_WEB', $ID_PERSONNE_WEB, PDO::PARAM_INT);
    $select->execute();
    while($row = $select->fetchObject()){
        $documents[] = $row;
    }


    $contenu = null;
    
    $sql = "SELECT
            `fo`.COMPULSORY,
            fe.LANGUAGE_KEY,
            fe.REF,
            fo.OPTIONS,
            fo.VALEUR12,
            fo.IS_MULTIPLE

        FROM
            `awa_formulairepo_composition` AS `fo`
        INNER JOIN awa_formulaire_elements AS fe ON fe.id = fo.ID_FORMULAIRE_ELEMENT


        GROUP BY `fo`.`ID`

        ORDER BY fo.POSITION ASC
        ";

    //print_t($sql);
    $select = $conn->prepare($sql);
    $select->execute();

    $formulaire_elements = array();
    while($row = $select->fetchObject()){
        $formulaire_elements[] = $row;
    }
    $formulaire_JS = "";
    $formulaire_CHECKJS = "";
    $formulaire_HTML = "";

    $formulaireInputs['personne'] = $personne;
    $formulaireInputs['experiences'] = $experiences;
    $formulaireInputs['formations'] = $formations;
    $formulaireInputs['langues'] = $langues;
    $formulaireInputs['documents'] = $documents;

    foreach($formulaire_elements as $formulaire_element){
        $treelist = null;
        $treelist_multi = false;
        if(!empty($formulaire_element->VALEUR12)){
            $treelist = new Treelist($formulaire_element->VALEUR12);
        }
        if(!empty($formulaire_element->IS_MULTIPLE)){
            $treelist_multi = true;
        }
        $elementReturn = array();
        $elementReturn = getFormulaireHTMLJSelement($formulaire_element->REF,$formulaire_element->COMPULSORY,$formulaire_element->OPTIONS,$formulaireInputs,$treelist,$treelist_multi,$conn,$contenu,"profil");
        $formulaire_JS .= $elementReturn['JS'];
        $formulaire_CHECKJS .= $elementReturn['CHECKJS'];
        $formulaire_HTML .= $elementReturn['HTML'];
    }


}else{
    Debug::d_echo("affichage login ", 2,"espace-candidat.php");
}
//print_t($personne);


$emailPreselect = "";
if(!empty($_GET['email'])){
    $emailPreselect = urldecode($_GET['email']);
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <title><?php echo _ESPACECANDIDAT_TITLE; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="description" content=""/>
        <meta name="keywords" content=""/>
        <?php  if(!_USE_CUSTOM_CSS){ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?>getcss_custom.php" rel="stylesheet" type="text/css" />
        <?php }else{ ?>
        <link href="<?php echo _CONFIG_ROOTFOLDER; ?><?php echo _TEMPLATE_FOLDER_NAME; ?>/custom.css" rel="stylesheet" type="text/css" />
        <?php } ?>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="<?php echo _CONFIG_ROOTFOLDER; ?>include/framework.js"></script>
        <script type="text/javascript" >
            $.ajaxSettings.cache = false;
            $(document).ready(function(){
                 <?php if(_IS_IFRAME_RESIZE){ ?>
                resizeiframe();
                <?php } ?>
            });

            function changeLanguage(language){
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-change_language.php", { 'language': language});
                $.post("<?php echo _CONFIG_ROOTFOLDER; ?>act-get_url.php",
                    { 'pageref':'espacecandidat', 'langue':language },
                    function(data){
                        window.location=data;
                    }
                );
            }
             <?php echo $formulaire_JS; ?>
             function checkForm(){
                <?php echo $formulaire_CHECKJS; ?>

                $('#candidature_form').submit();
            }

            function checkEmailDba(id){
                
            }

        </script>
    </head>
    <body>
        <?php
        $templateDatas = array();

        $templateDatas['pathway'] = "";
        $pathway = array();
        $pathway[0]['text'] = _ESPACECANDIDAT_TITLE;
        $pathway[0]['url'] = null;
        $templateDatas['pathway'] =  displayPathway($pathway);

        $templateDatas['header'] = "";
        $templateDatas['header'] = displayHeader();
        
        //$templateDatas['flags'] = "";
        //$templateDatas['flags'] = displayFlags();

        $templateDatas['candidatArea'] = "";
        $templateDatas['candidatArea'] = displayCandidatArea($conn);

        $templateDatas['menu'] = "";
        $templateDatas['menu'] = displayMenu();



        $templateDatas['emailPreselect'] = "";
        $templateDatas['emailPreselect'] = $emailPreselect;


        $templateDatas['forgetPasswordButtonLink'] = "";
        $templateDatas['forgetPasswordButtonLink'] = "http://"._CONFIG_DOMAIN_NAME._CONFIG_ROOTFOLDER.constant("_URL_CONFIG_MDP_OUBLIE_".$_SESSION['awa_language']);


        //Footer
        $templateDatas['footerText'] = "";
        $templateDatas['footerText'] = displayFooter();

        ?>
        <?php
        if(empty($ID_PERSONNE_WEB)){
            include(_TEMPLATE_FOLDER_NAME."/tpl_login.php");
        }else{
            include(_TEMPLATE_FOLDER_NAME."/tpl_candidat-profil.php");
        }
        ?>
    </body>
</html>
<?php

//print_t($_SESSION);
//echo "<pre>";
//print_r($fonctions);
//echo "</pre>";
//echo "<pre>";
//print_r($secteurs);
//echo "</pre>";
?>