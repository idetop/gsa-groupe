
<div id="location_popup_menu" style="">
    <?php  echo $templateDatas['filters']['countryPopupContent'];  ?>
</div>
<div class="contenttitle"><?php echo _OFFRE_LISTE_RECHERCHE_TITLE; ?></div>

<div style="height: 20px;"></div>
<div>

    <div style="float: left; width: 322px; padding-left: 10px;">
        <div class="searchRow">
            <div class="label labeldesign"><?php echo _OFFRE_LISTE_RECHERCHE_FONCTION_LABEL; ?></div>
            <div class="input">
                <select id="search_fonction" class="inputdesign">
                    <option value=""><?php echo _OFFRE_LISTE_RECHERCHE_FONCTION_DEFAULT; ?></option>
                    <?php echo $templateDatas['filters']['functionListContent']; ?>
                </select>
            </div>
            <div class="spacer"></div>
        </div>
        <div class="searchRow">
            <div class="label labeldesign"><?php echo _OFFRE_LISTE_RECHERCHE_LOCATION_LABEL; ?></div>
            <div class="input">
                <input type="text" id="search_location" class="inputdesign" value="<?php echo $templateDatas['filters']['locationSelectedValue']; ?>" onclick="$('#location_popup_menu').show();" />
            </div>
            <div class="spacer"></div>
        </div>
    </div>
    <div style="float: left;width: 322px; padding-left: 0px;">
        <div class="searchRow">
            <div class="label labeldesign"><?php echo _OFFRE_LISTE_RECHERCHE_SECTEUR_LABEL; ?></div>
            <div class="input">
                <select id="search_secteur" class="inputdesign">
                    <option value=""><?php echo _OFFRE_LISTE_RECHERCHE_SECTEUR_DEFAULT; ?></option>
                    <?php echo $templateDatas['filters']['sectorListContent']; ?>
                </select>
            </div>
            <div class="spacer"></div>
        </div>

        <div class="searchRow">
            <div class="label labeldesign"><?php echo _OFFRE_LISTE_RECHERCHE_KEYWORD_LABEL; ?></div>
            <div class="input">
                <input type="text" id="search_keyword" class="inputdesign" value="<?php echo $templateDatas['filters']['keywordSelectedValue']; ?>" />
            </div>
            <div class="spacer"></div>
        </div>
    </div>
    <div class="spacer"></div>
</div>

<div style="height: 20px;"></div>
<div id="buttonsarealiste">
    <div id="rechercherbtn" class="buttonclass" onclick="saveSearch();"><?php echo _OFFRE_LISTE_RECHERCHE_SUBMIT; ?></div>
</div>

