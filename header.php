<div id="wrapper">
	<div id="header">
		<a href="http://gsa-groupe.fr/" class="logo"><img src="images/logo.png" alt="" /></a>
		<div class="menutop">
			<a href="http://gsa-medical.fr" class="medical">
				<img src="images/spacer.gif" />
			</a>
			<a href="http://gsa-pharma.fr" class="pharma">
				<img src="images/spacer.gif" />
			</a>
			<a href="http://gsa-formation.fr" class="formation">
				<img src="images/spacer.gif" />
			</a>
			<a href="http://gsa-recrutement.fr" class="recrutement">
				<img src="images/spacer.gif" />
			</a>
		</div>

		<p class="coordonnees">
			<?php info_poles($pole, 'tel'); ?><br />
			<?php
			if (! empty($pole)) {
				info_poles($pole,'mobile');
				echo '<br />';
				info_poles($pole,'email');
				echo '<br />';
			}
			?>
			<?php info_poles($pole,'adresse'); ?><br />
			<?php info_poles($pole,'cp'); ?> <?php info_poles($pole,'ville'); ?>
		</p>
	</div>
	<br style="clear: both" />
</div>
